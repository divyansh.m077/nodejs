import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material';
import { TranslationModule } from 'angular-l10n';

import { NearMapRoutingModule } from './near-map-routing.module';
import { NearMapComponent } from './near-map.component';
import { StoreDetailComponent } from './store-detail/store-detail.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    NearMapRoutingModule,
    MatButtonModule,
    TranslationModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    NearMapComponent,
    StoreDetailComponent
  ]
})
export class NearMapModule { }
