import { Component, OnInit, Input } from '@angular/core';
import { Language } from 'angular-l10n';

@Component({
  selector: 'app-coupon-discount',
  templateUrl: './coupon-discount.component.html',
  styleUrls: ['./coupon-discount.component.scss']
})
export class CouponDiscountComponent implements OnInit {
  @Language() lang: string;
  discount: any;
  freeShipping: any;
  type: any;
  couponCreatedBy: any;
  @Input() set couponDiscount(val) {
    this.discount = val;
  }
  @Input() set shipping(val) {
    this.freeShipping = val;
  }
  @Input() set discountType(val) {
    this.type = val;
  }
  @Input() set couponCreated(val) {
    this.couponCreatedBy = val;
  }
  constructor() { }

  ngOnInit() {

  }

}
