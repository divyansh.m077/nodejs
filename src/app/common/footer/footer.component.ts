import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { Language, TranslationService } from 'angular-l10n';

import { ComonService } from '../comon.service';
import { API_STATUS_CODE } from '../../app.constants';
import { HomeService } from '../../home/home.service';

import { REGISTRATION_RULE, SNACKBAR_ALERT, SNACKBAR_SUCCESS } from './../../app-setting';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  cmsList: any;
  @Language() lang: string;
  socialMediaList: any = ['socialMediaId', 'socialMediaName', 'socialMediaUrl', 'socialMediaLogo', 'isActive'];
  signInSubscribe$: any;
  showSubscription: boolean;
  userEmail: any;
  email = REGISTRATION_RULE.EMAIL_PATTERN;
  constructor(private router: Router, private comonService: ComonService,
    private homeService: HomeService, private snackBar: MatSnackBar,
    private translation: TranslationService) {
    this.showSubscription = true;
    this.signInSubscribe$ = this.homeService.getSignInStatus().subscribe((response: any) => {
      this.showSubscription = !response;
    });
  }

  ngOnInit() {
    this.getCmsList();
    this.getSocialMediaList();
  }

  moveToCms(detail) {
    this.router.navigate(['page', detail.pageName , {pageTitle : detail.pageTitle }]);
  }

  getCmsList() {
    this.comonService.getCmsList().subscribe((response: any) => {
      if (response) {
        this.cmsList = response.data;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getSocialMediaList() {
    this.comonService.getSocailMediaList().subscribe((response: any) => {
      if (response) {
        this.socialMediaList = response.data;
      } else {
        this.socialMediaList = undefined;
      }
    },
      (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
  }

  addEmailSubscription(value) {
    this.comonService.addEmailSubscription(value).subscribe((response: any) => {
      if (response) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        this.userEmail = '';
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.conflict) {
        this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.error), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
      if (error.status === API_STATUS_CODE.internalServer) {
        this.snackBar.open(this.translation.translate(error.error.error), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }
}
