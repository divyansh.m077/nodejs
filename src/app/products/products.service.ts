import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {forEach, uniq, find} from 'lodash';

import { PRODUCT_URL } from './products.url-config';
import { EMPTY } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {
  }

  getProductList(productData) {
    return this.http.get(PRODUCT_URL.PRODUCTS_BASED_CATEGORY, { params: productData });
  }

  getProductListBasedBrand(productData) {
    return this.http.get(PRODUCT_URL.PRODUCTS_BASED_BRAND, { params: productData });
  }

  getProductListByCategory(productObj: any) {
    return this.http.get(PRODUCT_URL.PRODUCTS_BASED_ON_MAIN_CATEGORY, { params: productObj });
  }

  getWishList(productData) {
    return this.http.get(PRODUCT_URL.GET_WISH_LIST, { params: productData });
  }

  getFilterList(categoryId) {
    return this.http.get(PRODUCT_URL.FILTER + categoryId);
  }

  getHotProductList(productData) {
    return this.http.get(PRODUCT_URL.HOT_PRODUCT, { params: productData });
  }

  getBestSellingProductList(productData) {
    return this.http.get(PRODUCT_URL.BEST_SELLING_PRODUCT, { params: productData });
  }

  getBestSellingProductCount() {
    return this.http.get(PRODUCT_URL.BEST_SELLING_PRODUCT_COUNT);
  }

  getFeaturedProductList(productData) {
    return this.http.get(PRODUCT_URL.FEATURED_PRODUCT_LIST, { params: productData });
  }

  getSoldProductList(productData) {
    return this.http.get(PRODUCT_URL.SOLD_PRODUCT, { params: productData });
  }

  getProductDetails(productData) {
    return this.http.get(PRODUCT_URL.DETAIL, { params: productData });
  }

  getProductReviews(reviewData) {
    return this.http.get(PRODUCT_URL.PRODUCT_REVIEWS, { params: reviewData });
  }

  groupingProductOptionDetails(productOption, originalProductOption) {
    let productOptionList = [];
    forEach(productOption, (product) => {
      forEach(product, (option) => {
        productOptionList.push(option.attributeType);
      });
    });
    productOptionList = uniq(productOptionList, 'attributeType');
    const productGroupOption = [];
    forEach(productOptionList, (option) => {
      productGroupOption.push({'attributeType': option, 'attributeName': '', 'optionRequired': '', 'options': []});
      forEach(originalProductOption.productOption, (proOption) => {
        if (option === proOption.attributeType) {
          const obj = find(productGroupOption, {'attributeType': proOption.attributeType});
          if (obj) {
            obj.attributeName = proOption.attributeName;
            obj.optionRequired = proOption.productOptionRequired;
            obj.options.push(proOption);
          }
        }
      });
    });
    return productGroupOption;
  }

  addProductRating(ratingData) {
    return this.http.put(PRODUCT_URL.ADD_RATING, ratingData);
  }

  addProductReview(reviewDetails) {
    return this.http.put(PRODUCT_URL.ADD_REVIEW, reviewDetails);
  }

  getAllProductsList(productData) {
    return this.http.get(PRODUCT_URL.PRODUCTS, { params: productData });
  }

  addProductToWishList(productData) {
    return this.http.put(PRODUCT_URL.WISH_LIST, productData);
  }

  getParticularProductReviewDetails(productData) {
    return this.http.post(PRODUCT_URL.RATING_REVIEW_BY_USER, productData);
  }

  getFilterProductLists(productData) {
    return this.http.post(PRODUCT_URL.PRODUCT_BY_FILTER, productData);
  }

  getSearchProducts(searchData) {
    return this.http.get(PRODUCT_URL.SEARCH, { params: searchData });
  }

  getCompareProductDetails(productData) {
    if(productData.vendorId) {
       return this.http.get(PRODUCT_URL.COMPARE, {params: { productId: productData.productId.toLocaleString(), vendorId: productData.vendorId.toLocaleString() }});
    } else {
       return this.http.get(PRODUCT_URL.COMPARE, {params: { productId: productData.productId.toLocaleString()}});
    }
   
  }

  trackCompareProductDetails(productData) {
    return EMPTY;
  }

  getReplyMessage(reviewId) {
    return this.http.get(PRODUCT_URL.REPLY_MESSAGE + reviewId);
  }

  checkProductAvailability(productData)  {
    return this.http.post(PRODUCT_URL.PRODUCT_AVAILABLE_URL, productData);
  }

  getSimilarProducts(productData) {
    return this.http.get(PRODUCT_URL.GET_SIMILAR_PRODUCTS, { params: productData });
  }
}
