export const environment = {
  production: true,
  serverUrl: 'https://sv-dev.purchasecommerce.com:1443/',
  assetsUrl: `https://sv-dev.purchasecommerce.com/`,
  becomeaVendorURL: '',
  SSRPort: 8082,
  cookieConfig: {
    path: '/',
    domain: 'sv-dev.purchasecommerce.com',
    expires: '01.01.2021',
    secure: true,
    httpOnly: false
  }
};
