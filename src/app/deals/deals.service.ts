import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { DEALS_URL } from './deals.url-config';

@Injectable({
  providedIn: 'root'
})
export class DealsService {
  constructor(private http: HttpClient) { }

  getAllDealsList(data) {
    return this.http.get(DEALS_URL, { params: data });
  }
}
