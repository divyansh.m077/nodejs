export const environment = {
    production: true,
    serverUrl: 'https://uat.ichhapurti.com:2443/',
    assetsUrl: `https://uat.ichhapurti.com/`,
    becomeaVendorURL: 'https://uat.ichhapurti.com/become-vendor/signup',
    salesPortalURL: 'https://sales-uat.ichhapurti.com/',
    SSRPort: 8081,
    cookieConfig: {
      path: '/',
      domain: 'uat.ichhapurti.com',
      expires: '01.01.2025',
      secure: true,
      httpOnly: false
    }
};