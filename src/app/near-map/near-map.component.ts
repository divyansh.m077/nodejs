import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Language, TranslationService } from 'angular-l10n';
import { slice, concat } from 'lodash';
import { MatSnackBar } from '@angular/material';

import { NEAR_MAP_SETTINGS, SNACKBAR_ALERT } from '../app-setting';
import { NearMapService } from './near-map.service';
import { SharedService } from '../shared/shared.service';
import { StorageControllerService } from '../storage-controller.service';

@Component({
  selector: 'app-near-map',
  templateUrl: './near-map.component.html',
  styleUrls: ['./near-map.component.scss']
})
export class NearMapComponent implements OnInit {
  @Language() lang: string;
  locationDetails: any;
  range: number;
  nearByStores: any;
  originalNearByStores: any;
  nearByStoresCount: number;
  nearStoreCount = NEAR_MAP_SETTINGS;
  searchString: string;
  existingLocationDetails: any;
  sourceAddress: any;
  isLoading: boolean;
  siteSettingDetails: any;
  locationAllowed = true;
  constructor(private nearMapService: NearMapService,
              private router: Router,
              private storage: StorageControllerService,
              private translation: TranslationService,
              private snackBar: MatSnackBar,
              private sharedService: SharedService) { }

  ngOnInit() {
    this.getRange();
    this.storage.remove('storeDetails');
    this.searchString = '';
    this.isLoading = true;
    this.initializeGlobalVariable();
    this.getLocationDetails();
    // this.getLatitudeAndLongitude();
    this.getLocation();
  }

  getRange() {
    this.siteSettingDetails = this.sharedService.getSiteSettingDetails();
    if (this.siteSettingDetails) {
      this.range = this.siteSettingDetails.defaultSearchRange;
    } else {
      this.range = 4;
    }
  }

  getLocationDetails() {
    this.existingLocationDetails = this.nearMapService.getLocationDetails();
    if (this.existingLocationDetails) {
      this.range = this.existingLocationDetails.searchRange;
    } else {
      this.getRange();
    }
  }

  initializeGlobalVariable() {
    // this.locationAllowed = false;
    this.nearStoreCount.START_COUNT = 0;
    this.nearStoreCount.END_COUNT = 10;
    this.nearByStores = [];
    this.nearByStoresCount = 0;
  }

  // getLatitudeAndLongitude() {
  //   return (navigator as any).permissions.query({name: 'geolocation'}).then((result: any) => {
  //     if (result.state === 'granted') {
  //       this.locationAllowed = true;
  //     } else if (result.state === 'prompt') {
  //       this.locationAllowed = false;
  //     } else {
  //       this.locationAllowed = false;
  //     }
  //     this.getLocation();
  //   });
  // }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.locationAllowed = true;
        this.locationDetails = position.coords;
        if (this.existingLocationDetails) {
          if ((this.existingLocationDetails.origins[0] !== this.locationDetails.latitude) ||
              (this.existingLocationDetails.origins[1] !== this.locationDetails.longitude) ||
              (this.existingLocationDetails.searchRange !== this.range)) {
                this.setLocationDetailsInService();
                this.getStoreList();
          } else {
            this.getExistingStoreList();
          }
        } else {
          this.setLocationDetailsInService();
          this.getStoreList();
        }
      }, () => {
        this.locationAllowed = false;
        this.getStoreList();
      });
    }
  }

  setLocationDetailsInService() {
    const locationData = {
      origins: [this.locationDetails.latitude, this.locationDetails.longitude],
      searchRange: this.range
    };
    this.nearMapService.setLocationDetails(locationData);
  }

  updateRangeValue() {
    if (this.range >= this.nearStoreCount.RANGE_MIN_VALUE && this.range <= this.nearStoreCount.RANGE_MAX_VALUE) {
        this.setLocationDetailsInService();
        this.initializeGlobalVariable();
        this.getStoreList();
    } else {
      const msg = this.translation.translate('ERROR_RANGE_ERROR') + this.nearStoreCount.RANGE_MIN_VALUE +
                  this.translation.translate('LABEL_RANGE_ERROR') + this.nearStoreCount.RANGE_MAX_VALUE;
      this.snackBar.open(msg, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    }
  }

  getExistingStoreList() {
    this.isLoading = false;
    const existingData = this.nearMapService.getStoreDetails();
    if (existingData) {
      this.originalNearByStores = existingData.vendorStoreDetails;
      this.sourceAddress = existingData.originAddress;
      this.getNearByStores();
    } else {
      this.nearByStoresCount = 0;
    }
  }

  getStoreList() {
    this.isLoading = true;
    const latLng = this.locationDetails.latitude + ',' + this.locationDetails.longitude;
    const locationData = {
      origins: [latLng],
      searchRange: this.range
    };
    this.nearMapService.getStoreList(locationData).subscribe((response: any) => {
      this.isLoading = false;
      if (response) {
        this.originalNearByStores = response.data.vendorStoreDetails;
        this.sourceAddress = response.data.originAddress;
        this.nearMapService.initializeStoreList(response.data);
        this.getNearByStores();
      } else {
        this.nearByStoresCount = 0;
      }
    }, (error: any) => {
      this.isLoading = false;
    });
  }

  getNearByStores() {
    this.nearByStoresCount = this.originalNearByStores.length;
    const slicedStore = slice(this.originalNearByStores, this.nearStoreCount.START_COUNT, this.nearStoreCount.END_COUNT);
    this.nearByStores = concat(this.nearByStores, slicedStore);
  }

  showMoreProduct() {
    this.nearStoreCount.START_COUNT += this.nearStoreCount.SHOW_MORE_COUNT;
    this.nearStoreCount.END_COUNT += this.nearStoreCount.SHOW_MORE_COUNT;
    if (this.searchString === '') {
      this.getNearByStores();
    } else {
      this.searchStores(this.searchString);
    }
  }

  searchStores(searchString, from?) {
    if (from) {
      this.initializeGlobalVariable();
    }
    const filteredData = this.originalNearByStores.filter(obj => obj['companyName'].toLowerCase().includes(searchString.toLowerCase()));
    this.nearByStoresCount = filteredData.length;
    const slicedStore = slice(filteredData, this.nearStoreCount.START_COUNT, this.nearStoreCount.END_COUNT);
    this.nearByStores = concat(this.nearByStores, slicedStore);
  }

  moveToStoreDetail(storeData) {
    const storeDetail = JSON.stringify(storeData);
    this.storage.set('storeDetails', storeDetail);
    this.router.navigate(['/near-map/store-detail']);
  }

  clearSearchStore() {
    this.nearByStores = [];
    this.searchString = '';
    this.getNearByStores();
  }
}
