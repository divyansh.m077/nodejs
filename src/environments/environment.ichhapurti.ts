export const environment = {
    production: true,
    serverUrl: 'https://ichhapurti.purchasecommerce.com:2443/',
    assetsUrl: `https://ichhapurti.purchasecommerce.com/`,
    becomeaVendorURL: 'https://ichhapurti.purchasecommerce.com/become-vendor/signup',
    salesPortalURL: 'https://sales-ichhapurti.purchasecommerce.com',
    SSRPort: 8081,
    cookieConfig: {
      path: '/',
      domain: 'ichhapurti.purchasecommerce.com',
      expires: '01.01.2021',
      secure: true,
      httpOnly: false
    }
};