import { Injectable, Inject } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { StorageControllerService } from '../storage-controller.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private storage: StorageControllerService,
    private router: Router) { }

  canActivate() {
    const token = this.storage.get('token');
    if (!token) {
      this.router.navigate(['/signin']);
      return false;
    }
    return true;
  }
}
