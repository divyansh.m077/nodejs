import { Component, OnInit, Input } from '@angular/core';
import { Language } from 'angular-l10n';

@Component({
  selector: 'app-rating-popup',
  templateUrl: './rating-popup.component.html',
  styleUrls: ['./rating-popup.component.scss']
})
export class RatingPopupComponent implements OnInit {

  @Language() lang: string;
  @Input() ratingList;
  @Input() from;
  constructor() { }

  ngOnInit() {
  }

}
