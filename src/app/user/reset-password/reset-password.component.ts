import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { Language, TranslationService } from 'angular-l10n';

import { UserService } from './../user.service';
import { RESET_PASSWORD_RULE, settings, SNACKBAR_ALERT, SNACKBAR_SUCCESS } from './../../app-setting';
import { API_STATUS_CODE } from '../../app.constants';

import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  @Language() lang: string;
  resetPasswordForm: FormGroup;
  public passwordRule: boolean;
  public urlParam: any;
  public userId;
  siteKey;
  captchaSize;
  captchaTheme;
  captchaType;
  captchaLanguage;
  isInBrowser;
  public forgotPasswordToken;
  constructor(private fb: FormBuilder, public userService: UserService, private router: Router, private snackBar: MatSnackBar, private translation: TranslationService) {
    if (typeof window !== 'undefined') {
      this.isInBrowser = true;
    }
    this.passwordRule = false;
    const currentUrl = this.router.url;
    const url = currentUrl.split('=');
    this.urlParam = url[1];
    const userDetails = this.urlParam.split('&');
    this.userId = userDetails[0];
    this.forgotPasswordToken = url[2];
    this.siteKey = settings.captchaKey;
    this.captchaSize = 'Normal';
    this.captchaTheme = 'Light';
    this.captchaType = 'Image';
    this.captchaLanguage = 'en';
   }

  formInitialization() {
    this.resetPasswordForm = this.fb.group({
      'newPassword': ['', [Validators.required,
                           Validators.pattern(RESET_PASSWORD_RULE.PASSWORD_PATTERN)]],
      'confirmPassword': ['', Validators.required],
      'tokenFlag': [false],
      'isCaptchaVerified': ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.formInitialization();
  }

  showPasswordRules() {
    this.passwordRule = true;
  }

  hidePasswordRule() {
    this.passwordRule = false;
  }

  removeCurrentPassword() {
    if (this.resetPasswordForm.controls['confirmPassword'].value !== this.resetPasswordForm.controls['newPassword'].value) {
      this.resetPasswordForm.controls['confirmPassword'].setValue('');
    }
  }

  onSubmit() {
    const passwordDetails = {
      'password': this.resetPasswordForm.value.confirmPassword,
      'userId': this.userId,
      'forgotPasswordToken': this.forgotPasswordToken,
      'tokenFlag': this.resetPasswordForm.value.tokenFlag,
      'isCaptchaVerified': this.resetPasswordForm.value.isCaptchaVerified
    };
    this.userService.resetPassword(passwordDetails).subscribe((response: any) => {
      if (response) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        this.router.navigate(['/signin']);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        if (error.error.message === 'PASSWORD_RESET_ALREADY') {
          this.router.navigate(['/unauthorized', {from : 'reset-password'}]);
        } else {
          this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
        
      }
    });
  }
}
