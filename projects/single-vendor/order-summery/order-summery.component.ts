import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Language } from 'angular-l10n';



@Component({
  selector: 'app-vendor-order-summery',
  templateUrl: './order-summery.component.html',
  styleUrls: ['./order-summery.component.scss']
})
export class VendorOrderSummeryComponent implements OnInit {
  @Language() lang: string;
  @Input() orderDetails;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  continueShopping() {
    this.router.navigate(['/']);
  }
}
