import { Component, OnInit , Inject} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { Language, TranslationService } from 'angular-l10n';
import { forEach } from 'lodash';
import { MatSnackBar, MatDialog } from '@angular/material';

import { DescriptionPopupComponent } from '../../shared/description-popup/description-popup.component';

import { UserService } from './../user.service';
import { API_STATUS_CODE, COMPLAINT_TYPE } from '../../app.constants';
import { settings, SNACKBAR_ALERT, SNACKBAR_WARNING, SNACKBAR_SUCCESS , ADD_COMPLAINT_RULE} from '../../app-setting';
import { ComplaintImageComponent } from './complaint-image/complaint-image.component';
@Component({
  selector: 'app-order-complaints',
  templateUrl: './order-complaints.component.html',
  styleUrls: ['./order-complaints.component.scss']
})
export class OrderComplaintsComponent implements OnInit {
  @Language() lang: string;
  orderComplaintForm: FormGroup;
  showManageComplaintsDetails: any;
  orderTypeList: any;
  orderStatusList: any;
  orderList: any;
  productList: any;
  showOthers: boolean;
  url: string;
  fileData: any;
  orderComplaintList: any;
  defaultOrderStatus: string;
  defaultStatus: any;
  acceptImageTypes = settings.acceptImageTypes;
  imageTypes: string[];
  addComplaintRules = ADD_COMPLAINT_RULE;
  complaintType = COMPLAINT_TYPE;
  showImage: boolean;
  fileSize: number;
  complaintTypeReason: any;
    constructor(
      private fb: FormBuilder,
      private userService: UserService,
      private translation: TranslationService,
      private snackBar: MatSnackBar,
      private dialog: MatDialog
    ) {
    this.showImage = false;
    this.showManageComplaintsDetails = false;
    this.showOthers = false;
    this.url = '';
    this.defaultOrderStatus = 'new';
    this.formInitialization();
  }

  ngOnInit() {
    this.imageTypes = settings.acceptImageTypes.split(',');
    this.fileSize = settings.complaintImageSize / 1000000;
    this.getOrderComplaintStatusList();
    this.getOrderComplaintTypeList();
    this.getOrderList();
    this.getOrderComplaintList();
  }

  formInitialization() {
    this.orderComplaintForm = this.fb.group({
      'orderId': ['', Validators.required],
      'productId': ['', Validators.required],
      'complaintTypeId': ['', Validators.required],
      'complaintStatus': [this.defaultStatus ? this.defaultStatus.statusName : '', Validators.required],
      'complaintReason': ['', [Validators.required, Validators.maxLength(ADD_COMPLAINT_RULE.MAX_LENGTH)]],
      'complaintImage': ['', Validators.required],
      'otherReason': ['', Validators.maxLength(ADD_COMPLAINT_RULE.MAX_LENGTH)]
    });
  }

  getOrderList() {
    this.userService.getOrderList().subscribe((response: any) => {
      if (response) {
        this.orderList = response.data;
      } else {
        this.orderList = undefined;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getOrderComplaintTypeList() {
    this.userService.getOrderComplaintTypeList().subscribe((response: any) => {
      if (response) {
        this.orderTypeList = response.data;
     } else {
        this.orderTypeList = undefined;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getOrderComplaintList() {
    this.userService.getOrderComplaintList().subscribe((response: any) => {
      if (response) {
        this.orderComplaintList = response;
        forEach(this.orderComplaintList, (order, index) => {
          order.sno = index + 1;
        });
      } else {
        this.orderComplaintList = undefined;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  onChangeType(typeValue) {
    if (typeValue === 4) {
      this.showOthers = true;
      this.orderComplaintForm.controls['otherReason'].setValidators([Validators.required]);
      this.orderComplaintForm.controls['otherReason'].updateValueAndValidity();
    } else {
      this.showOthers = false;
      this.orderComplaintForm.get('otherReason').clearValidators();
      this.orderComplaintForm.get('otherReason').updateValueAndValidity();
    }
  }

  getOrderComplaintStatusList() {
    this.userService.getOrderComplaintStatusList().subscribe((response: any) => {
      if (response) {
        this.orderStatusList = response.data;
        forEach(this.orderStatusList, (order) => {
          if (order.statusName.toLowerCase() === this.defaultOrderStatus) {
            this.defaultStatus = order;
          }
        });
        this.orderComplaintForm.controls['complaintStatus'].setValue(this.defaultStatus.statusName);
      } else {
        this.orderStatusList = undefined;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getProductBasedOnOrder(orderId) {
    this.orderComplaintForm.controls['productId'].reset();
    this.orderComplaintForm.controls['complaintTypeId'].reset();
    this.complaintTypeReason = [];
    this.userService.getProductBasedOnOrder(orderId).subscribe((response: any) => {
      if (response) {
        this.productList = response.data;
        if (this.productList[0].orderStatus !== 4 && this.orderTypeList) { // OrderStatusId indicates delivered status
// To filter complaint reason based on order status
          forEach(this.orderTypeList, (complaint) => {
            if (complaint.complaintTypeId === this.complaintType.delayInDelivery ||
                complaint.complaintTypeId === this.complaintType.paymentIssues) {
                this.complaintTypeReason.push(complaint);
            }
           });
         } else {
         this.complaintTypeReason = this.orderTypeList;
       }
      } else {
        this.productList = undefined; 
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  onClickAddComplaints() {
    if (this.showManageComplaintsDetails) {
      this.snackBar.open(this.translation.translate('alertFieldFill'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
    }
    this.showManageComplaintsDetails = true;
    this.formInitialization();
  }

  saveOrderComplaint() {
    const formData = new FormData;
    const complaintVerified = '0';
    const verifiedDescription = '';
    formData.append('complaintImage', this.fileData, this.fileData.name);
    formData.append('orderId', this.orderComplaintForm.value.orderId);
    formData.append('complaintReason', this.orderComplaintForm.value.complaintReason);
    formData.append('complaintStatus', this.defaultStatus.statusId);
    formData.append('complaintTypeId', this.orderComplaintForm.value.complaintTypeId);
    formData.append('complaintVerified', complaintVerified);
    formData.append('verifiedDescription', verifiedDescription);
    formData.append('productId', this.orderComplaintForm.value.productId);
    formData.append('otherReason', this.orderComplaintForm.value.otherReason);
    this.userService.saveOrderComplaint(formData).subscribe((response: any) => {
      if (response) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        this.showManageComplaintsDetails = false;
        this.resetForm();
        this.getOrderComplaintList();
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.conflict) {
        this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  onSelectFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      if (this.imageTypes.indexOf(event.target.files[0].type) >= 0) {
        if (event.target.files[0].size < settings.complaintImageSize) {
          const reader = new FileReader();
          reader.readAsDataURL(event.target.files[0]);
          reader.onload = ((eve: any) => {
            this.url = eve.target.result;
          });
          this.orderComplaintForm.controls['complaintImage'].setErrors(null);
          this.fileData = event.target.files[0];
          this.showImage = true;
        } else {
          this.setRequiredForImage();
          this.url = '';
          this.showImage = false;
          this.snackBar.open(this.translation.translate('alertImageSize'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
        }
      } else {
        this.setRequiredForImage();
        this.url = '';
        this.showImage = false;
        this.snackBar.open(this.translation.translate('alertImageOnly'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
      }
    }
  }

  setRequiredForImage() {
    this.orderComplaintForm.controls['complaintImage'].setValidators([Validators.required]);
    this.orderComplaintForm.controls['complaintImage'].updateValueAndValidity();
  }

  resetForm() {
    this.orderComplaintForm.reset();
    this.url = '';
    this.fileData = undefined;
    this.formInitialization();
  }

  closeComplaint() {
    this.resetForm();
    this.showManageComplaintsDetails = false;
  }

  openImage(complaintData) {
    this.dialog.open(ComplaintImageComponent, {
      data: {
        imageUrl: complaintData.complaintImage
      }
    });
  }

  loadDescription(description) {
    this.dialog.open(DescriptionPopupComponent, {
      data: {
        title: this.translation.translate('labelOtherReason'),
        message: description,
        buttonName: this.translation.translate('buttonClose')
      },
      disableClose: true
    });
  }
}
