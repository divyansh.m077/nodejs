import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';

import { concat } from 'lodash';
import { Language,TranslationService } from 'angular-l10n';
import { NotificationsService } from 'angular2-notifications';

import { HomeService } from '../home/home.service';
import { BRAND_SETTINGS } from '../app-setting';
import { API_STATUS_CODE } from '../app.constants';

import { SharedService } from '../shared/shared.service';
import { SNACKBAR_ALERT } from '../app-setting';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.scss']
})
export class BrandsComponent implements OnInit {
  @Language() lang: string;
  public featuredBrandsList = [];
  public productFrom: string;
  public brandsCount: any = BRAND_SETTINGS;
  public brandLength: number;
  constructor(private homeService: HomeService, private notify: NotificationsService, private sharedService: SharedService, private snackBar: MatSnackBar, private translation: TranslationService) {
    this.productFrom = 'home';
    this.brandLength = 0;
  }

  ngOnInit() {
    this.brandsCount.START_COUNT = 0;
    this.getFeaturedBrandList();
  }

  getFeaturedBrandList() {
    this.homeService.getAllBrandsList({'startLimit': this.brandsCount.START_COUNT,
    'endLimit': this.brandsCount.END_COUNT}).subscribe((response: any) => {
      if (response) {
        this.featuredBrandsList = concat(this.featuredBrandsList, response.product);
        this.brandLength = response.length;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  showMoreBrands() {
    this.brandsCount.START_COUNT += this.brandsCount.SHOW_MORE_COUNT;
    this.getFeaturedBrandList();
  }
}
