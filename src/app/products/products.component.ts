import { Component, OnInit, OnDestroy , Inject} from '@angular/core';

import { MatSnackBar } from '@angular/material';
import { concat, uniqBy, find } from 'lodash';
import { Language, TranslationService } from 'angular-l10n';

import { ProductService } from './products.service';
import { PRODUCT_LIST_SETTINGS } from './../app-setting';
import { API_STATUS_CODE } from '../app.constants';
import { HomeService } from './../home/home.service';
import { ComonService } from './../common/comon.service';

import { SNACKBAR_ALERT } from '../app-setting';
import { SharedService } from '../shared/shared.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {
  @Language() lang: string;
  public productFrom = 'product';
  public productList: any = [];
  public productsCount: any = PRODUCT_LIST_SETTINGS;
  public productTotalLength: number;
  public productSubscribe$: any;
  public compareCount: any = 0;
  siteSettingDetails: any;
  constructor(private productsService: ProductService,
              private homeService: HomeService,
              private comonService: ComonService,
              private snackBar: MatSnackBar,
              private translation: TranslationService,
              private sharedService: SharedService
            ) {
    this.productSubscribe$ = this.homeService.getSignInStatus().subscribe((response: any) => {
      if (response === false) {
        this.productsCount.START_COUNT = 0;
        this.productList = [];
        this.getAllProductList();
      }
    });
  }

  ngOnInit() {
    this.getCompareCount();
    this.productsCount.START_COUNT = 0;
    this.productList = [];
    this.getAllProductList();
  }

  getCompareCount() {
    this.siteSettingDetails = this.sharedService.getSiteSettingDetails();
    if (this.siteSettingDetails) {
      this.compareCount = this.siteSettingDetails.compareLimit;
    } else {
      this.compareCount = 0;
    }
  }

  getAllProductList() {
    const productObj = {
      'startLimit': this.productsCount.START_COUNT,
      'endLimit': this.productsCount.END_COUNT,
      'targetCurrency': 'USD'
    };
    this.productsService.getAllProductsList(productObj)
    .subscribe((response: any) => {
      if (response) {
        this.productList = concat(this.productList, response.products.product);
        this.productList = uniqBy(this.productList, 'productId');
        this.productList = this.comonService.findCartCompareSelection(this.productList);
        this.productTotalLength = response.products.productlength;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  showMoreProducts() {
    this.productsCount.START_COUNT += this.productsCount.SHOW_MORE_COUNT;
    this.getAllProductList();
  }

  getProductActionDetails($event: any) {
    if ($event.action === 'wishList') {
      const productData = find(this.productList, {'productId': $event.productId});
      productData.favUser = $event.status === 1 ? 1 : 0;
    } else if ($event.action === 'cart') {
      const productData = find(this.productList, {'productId': $event.productId});
      productData.cartUser = 1;
    } else {
      const productData = find(this.productList, {'productId': $event.productId});
      productData.compareProduct = 1;
    }
  }

  ngOnDestroy() {
    if (this.productSubscribe$) {
      this.productSubscribe$.unsubscribe();
    }
  }

}
