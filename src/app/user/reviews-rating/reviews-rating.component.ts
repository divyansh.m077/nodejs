import { Component, OnInit, OnDestroy , Inject} from '@angular/core';

import { Language, TranslationService } from 'angular-l10n';
import { MatDialog, MatSnackBar } from '@angular/material';

import { UserService } from '../../user/user.service';
import { API_STATUS_CODE, ALERT_SYSTEM_TITLE } from '../../app.constants';
import { ConfirmationPopupComponent } from '../../shared/confirmation-popup/confirmation-popup.component';
import { SNACKBAR_ALERT, SNACKBAR_SUCCESS } from '../../app-setting';
@Component({
  selector: 'app-reviews-rating',
  templateUrl: './reviews-rating.component.html',
  styleUrls: ['./reviews-rating.component.scss']
})
export class ReviewsRatingComponent implements OnInit, OnDestroy {
  @Language() lang: string;
  ratingReviewList;
  reviewTotalLength;
  startLimit = 5;
  offSet = 0;
  currentPage = 1;
  totalPages;
  deleteDialogSubscibtion;
  isLoading: boolean;
  pluralMapping = {
    'replyCount': {
      '=1': 'LABEL_REPLY',
      'other': 'LABEL_REPLIES'
    }
  };
  constructor(
    private userService: UserService,
    private matDialog: MatDialog,
    private translation: TranslationService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.getRatingAndReview();
  }

  getRatingAndReview() {
    this.isLoading = true;
    this.userService.getReviewsAndRating(
      this.startLimit,
      this.offSet
    ).subscribe((response: any) => {
      if (response) {
        this.reviewTotalLength = response.totalCount;
        this.totalPages = Math.ceil(this.reviewTotalLength / this.startLimit);
        this.ratingReviewList = response.data;
        this.isLoading = false;
      } else {
        this.ratingReviewList = undefined;
        this.isLoading = false;
      }
    }, (error: any) => {
      this.isLoading = false;
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  showProductsRight() {
    this.currentPage++;
    this.offSet += this.startLimit;
    this.getRatingAndReview();
  }

  showProductsLeft() {
    this.currentPage--;
    this.offSet -= this.startLimit;
    this.getRatingAndReview();
  }

  deleteReview(ratingReview) {
    const dialogRef = this.matDialog.open(ConfirmationPopupComponent, {
      data: {
        title: this.translation.translate('labelDialogTitle'),
        message: this.translation.translate('alertMsgReviewRemove'),
        action: ALERT_SYSTEM_TITLE.warning,
        buttonOneName: this.translation.translate('buttonDelete'),
        buttonTwoName: this.translation.translate('buttonCancel'),
      }
    });
    this.deleteDialogSubscibtion = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
      if (data) {
        this.userService.deleteReviewUsingProductIdAndUserId(ratingReview.productId).subscribe((response: any) => {
          if (response) {
            this.getRatingAndReview();
            this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
            this.matDialog.closeAll();
          }
        }, (error: any) => {
          this.matDialog.closeAll();
          if (error.status === API_STATUS_CODE.badRequest) {
            this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
          }
        });
      }
    });
  }

  ngOnDestroy() {
    if (this.deleteDialogSubscibtion) {
      this.deleteDialogSubscibtion.unsubscribe();
    }
  }

}

