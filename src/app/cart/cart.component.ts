import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, ViewChild, OnDestroy , Inject, PLATFORM_ID, NgZone, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { CurrencyPipe } from '@angular/common';

import { Language, TranslationService } from 'angular-l10n';
import { forEach, find, remove, uniqBy, groupBy, uniq, sum, filter} from 'lodash';
import { MatDialog, MatExpansionPanel, MatSnackBar } from '@angular/material';

import { CartService } from './cart.service';
import { HomeService } from './../home/home.service';
import { CART_RULE } from './../app-setting';
import { API_STATUS_CODE, ALERT_SYSTEM_TITLE } from '../app.constants';
import { PaymentComponent } from './payment/payment.component';
import { CreateAddressComponent } from '../shared/create-address/create-address.component';
import { SharedService } from '../shared/shared.service';
import { ConfirmationPopupComponent } from '../shared/confirmation-popup/confirmation-popup.component';
import { AlertPopupComponent } from '../shared/alert-popup/alert-popup.component';
import { ComonService } from '../common/comon.service';
import { SNACKBAR_ALERT, SNACKBAR_SUCCESS, SNACKBAR_WARNING } from '../app-setting';
import { CurrencyFormatPipe } from './../shared/custom-currency.pipe';
import { CustomDatePipe } from './../shared/custom-date.pipe';
declare var Razorpay: any;
import { StorageControllerService } from '../storage-controller.service';
import { OtpComponent } from './../shared/otp/otp.component';
import { MembershipComponent } from './../shared/membership/membership.component';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})

export class CartComponent implements OnInit, OnDestroy {
  enableDecrementButton: boolean = false;
  currency: any;
  @ViewChild('panel1') addressPanel: MatExpansionPanel;
  @ViewChild('panel2') paymentPanel: MatExpansionPanel;
  @ViewChild('panelm1') addressMobPanel: MatExpansionPanel;
  @ViewChild('panelm2') paymentMobPanel: MatExpansionPanel;
  @ViewChild('form') form: ElementRef;
  @Language() lang: string;
  cartList: any;
  public subTotal: number;
  public estimatedTax: number;
  public shippingFee: any;
  public grandTotal: any;
  cartSubscribe$: any;
  actionSubscribe$: any;
  public currencySymbol: any;
  public cartStatus: boolean;
  public isLoading: boolean;
  public cartRules: any = CART_RULE;
  addressList: any;
  shippingAddress: any;
  billingAddress: any;
  disableShipping: boolean;
  enableAddressToggle: boolean;
  enablePaymentToggle: boolean;
  profileDetails: any;
  cartCountSubscribe$: any;
  cartDetailSubscribe$: any;
  cartItems: any;
  stripeResponse: any;
  paymentType: string;
  paymentDetail: any;
  stripeToken: any;
  paymentTotal: any;
  showDiscount: any;
  prodDetails: any;
  currencyFormat: any;
  currencySetting: any;
  defaultNumber: any;
  currencyCode: any;
  walletAmount = 0;
  walletTotal = 0;
  walletStatus = false;
  emptyWallet = true;
  fullPaymentOnWallet = false;
  originalWalletAmount: any;
  enableCheckBox = true;
  eventCheckbox = false;
  applyEvent = {checked: false, walletData: false};
  userQuantityUpdate = new Subject<string>();
  unsubScribeQuantityUpdate$: any;
  userId: number;
  stripeKey: string;
  isShppingSameAsBilling: boolean;
  strikeoutAmount: number;
  couponCode: any;
  couponDiscount: number;
  couponMaxDiscount: number;
  grossTotal: number;
  couponDiscountDetails: any;
  couponFreeShipping: boolean;
  couponShippingAmount: any;
  isBrowser;
  step: number;
  rzp1;
  razorPayIdDetail;
  shippingProductDetails: any;
  shippmentCharge: any;
  shippmentAmountApplied: boolean;
  encRequest: String;
  accessCode: String;
  accountUrl: String;
  paymentStatus;
  showSnackBar;
  paymentList: any;
  isPlacingOrder = false;
  paymentOption = true;
  cashBackObj: any;

  constructor(
              private storage: StorageControllerService,
              @Inject(WINDOW) private window: any,
              private cartService: CartService,
              public dialog: MatDialog,
              private homeService: HomeService,
              private translation: TranslationService,
              private router: Router,
              private sharedService: SharedService,
              private comonService: ComonService,
              private snackBar: MatSnackBar,
              private currencyPipe: CurrencyFormatPipe,
              private curencyPipe: CurrencyPipe,
              private customDatePipe: CustomDatePipe,
              private ngZone: NgZone,
              private route: ActivatedRoute
              ) {
    this.initializeGlobalVariables();
    this.getSiteSettingProdDetails();
    this.cartCountSubscribe$ = this.homeService.getCartCountChange().subscribe((response: any) => {
      this.paymentStatus = this.route.snapshot.queryParams.payment;
      if(this.paymentStatus === 'failure') {
        this.snackBar.open(this.translation.translate('PAYMENT_FAILED'), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
      if (response === true) {
          if (!!this.storage.get('cartStorage')) {
            this.cartItems = JSON.parse(this.storage.get('cartStorage'));
          }
      }
    });

    this.cartDetailSubscribe$ = this.homeService.getCartDetailStatus().subscribe((response: any) => {
      if (response) {
        const cartDetails = response.product;
        if (cartDetails.length !== 0 ) {
          forEach(cartDetails, (cart) => {
            if (cart.productOption.length !== 0) {
              this.strikeoutAmount = cart.salesPrice;
              forEach(cart.productOption, (option) => {
                if (cart.discount > 0) {
                  // cart.discountAmount += option.productOptionPrice;
                  cart.strikeoutAmount = this.strikeoutAmount += option.productOptionPrice;
                } else {
                  cart.salesPrice += option.productOptionPrice;
                }
              });
            } else {
              if (cart.discount > 0) {
                cart.strikeoutAmount = cart.salesPrice;
              }
            }
            this.updatePriceValueWithTaxAmount(cart);
          });
        }
        this.currencySymbol = response.currencySymbol;
        this.calculateInvoiceData(cartDetails);
        this.cartStatus = true;
        this.generateSno();
      }
    });

    // Debounce search.
    this.unsubScribeQuantityUpdate$ = this.userQuantityUpdate.pipe(
      debounceTime(300))
      .subscribe(value => {
        this.updateCartQuantity(value);
      });
  }

  displayNgZone(message) {
    this.ngZone.run(() => {
      this.snackBar.open(this.translation.translate(message),
      this.translation.translate('buttonClose'),
      SNACKBAR_ALERT);
  })
}

  getSiteSettingProdDetails() {
    this.prodDetails = this.sharedService.getSiteSettingDetails();
    if (this.prodDetails) {
      this.showDiscount = this.prodDetails.productSettings.displayDiscountedPrice;
       const currency = this.curencyPipe.transform(1, this.prodDetails.currencyCode, 'symbol-narrow');
       this.currency = currency.split('1');
     }
  }

  setStep(index: number) {
    this.step = index;
  } 

  // Phone mandatory alert
  checkPhonNumber(isBilling?: boolean){
    const subMessage = (isBilling ? 'labelBilling' : 'labelShipping');
    const message = `${this.translation.translate('CART_PHONE_MANDATORY_ALERT')} ${this.translation.translate(subMessage)}.`;
    const dialogRef =  this.dialog.open(AlertPopupComponent, {
      data: {
        title: this.translation.translate('ALERT'),
        message: [message],
        action: ALERT_SYSTEM_TITLE.info,
        buttonName: this.translation.translate('GOTO_MANAGE_ADDRESS')
      }
    });
    const ref = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
      if (data) {
        this.router.navigate(['/profile/address']);
        this.dialog.closeAll();
      }
    });

  }

  gotoNextTab() {
    if (!!this.storage.get('id')) {
      this.getUserAddressDetails();
      if (this.step === 0) {
        this.step++;
        this.enableAddressToggle = false;
      } else if (this.step === 1) {

        if(!this.billingAddress.phone){
          this.checkPhonNumber(true);
          return;
        }else if(!this.shippingAddress.phone){
          this.checkPhonNumber();
          return;
        }
        
        this.getMembershipDetails();
        this.getShippingAmount(true);
        this.getPaymentListDetails();
      }
    } else {
      this.snackBar.open(this.translation.translate('alertLoginCart'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
      this.router.navigate(['/signin']);
    }
  }

  enablePaymentToggleFromAddress() {
    this.enablePaymentToggle = true;
  }

  initializeGlobalVariables() {
    this.shippmentCharge = 0;
    this.step = 0;
    //this.paymentType = '1';
    this.subTotal = 0;
    this.estimatedTax = 0;
    this.couponDiscount = 0;
    this.couponMaxDiscount = 0;
    this.grossTotal = 0;
    this.shippingFee = 0;
    this.grandTotal = 0;
    this.couponShippingAmount = 0;
    // this.cartStatus = false;
    this.disableShipping = true;
    this.enableAddressToggle = true;
    this.enablePaymentToggle = true;
    this.couponFreeShipping = false;
    this.shippmentAmountApplied = false;
  }

  ngOnInit() {
    this.isBrowser = typeof window !== 'undefined' ? true : false;
    this.userId = +this.storage.get('id');
    this.prodDetails = this.sharedService.getSiteSettingDetails();
    this.couponCode = this.storage.get('couponCode');
    this.getCartDetails();
    if (!!this.storage.get('id')) {
      // this.getWalletAmount();
      // this.enableAddressToggle = false;
      this.getUserAddressDetails();
      this.getUserInfoDetails();
      
      // Get Cash back meter details
      this.getCashMeterDetails();
    }

    this.sharedService.getLocation();
  }


  getCartDetails(isFromApply?) {
    this.cartList = null;
    this.estimatedTax = 0;
    this.shippingFee = 0;
    this.subTotal = 0;
    this.grossTotal = 0;
    this.grandTotal = null;
    this.isLoading = true;
    if (!!this.storage.get('id')) {
      const cartObj = {
        'targetCurrency': 'USD'
      };
      this.cartService.getCartDetails(cartObj).subscribe((response: any) => {
        this.isLoading = false;
        if (response) {

          // Get Cash back meter details
          this.getCashMeterDetails();

         // this.cartList = response.product;          
          const cartDetails = response.product;
          if (cartDetails.length !== 0 ) {
            forEach(cartDetails, (cart) => {
              if (cart.productOption.length !== 0) {
                this.strikeoutAmount = cart.salesPrice;
                forEach(cart.productOption, (option) => {
                  if (cart.discount > 0) {
                    // cart.discountAmount += option.productOptionPrice;
                    cart.strikeoutAmount = this.strikeoutAmount += option.productOptionPrice;
                  } else {
                    cart.salesPrice += option.productOptionPrice;
                  }
                });
              } else {
                if (cart.discount > 0) {
                  cart.strikeoutAmount = cart.salesPrice;
                }
              }
              this.updatePriceValueWithTaxAmount(cart);
            });
          }
          // this.cartList = cartDetails;
          this.currencySymbol = response.currencySymbol;
          this.calculateInvoiceData(cartDetails, isFromApply);
          this.generateSno();
          this.cartStatus = response.product.length > 0 ? true : false;
        } else {
          this.cartList = undefined;
          this.cartStatus = false;
        }
      }, (error: any) => {
        this.isLoading = false;
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
    } else {
      this.isLoading = false;
      const cartItems: any[] = !!this.storage.get('cartStorage') ? JSON.parse(this.storage.get('cartStorage')) : [];
      if (cartItems.length !== 0 ) {
        forEach(cartItems, (cart) => {
          if (cart.productOption.length !== 0) {
            this.strikeoutAmount = cart.salesPrice;
            forEach(cart.productOption, (option) => {
              if (cart.discountAmount !== 0) {
                cart.strikeoutAmount = this.strikeoutAmount += option.productOptionPrice;
              }
            });
          } else {
            if (cart.discount > 0) {
              cart.strikeoutAmount = cart.salesPrice;
            }
          }
          this.updatePriceValueWithTaxAmount(cart);
        });
      }
      // const cartList = uniqBy(cartItems, 'productId', 'vendorId');
      this.currencySymbol = cartItems.length > 0 ? cartItems[0].currencySymbol : '';
      this.cartStatus = cartItems ? (cartItems.length > 0 ? true : false) : false;
      this.calculateInvoiceData(cartItems);
      this.generateSno();
    }
  }

  updatePriceValueWithTaxAmount(product) {
    // Display price value with tax amount based on site settings
    let salesPriceTaxAmount = 0;
    let discountPriceTaxAmount = 0;
    let strikeoutPriceTaxAmount = 0;
    let mrpsalesPriceTaxAmount = 0;
    if (this.prodDetails && this.prodDetails.productSettings.displayProductPriceWithTax === 1) {
      if (product.salesPrice) {
        salesPriceTaxAmount = product.taxAmount;
      }
      if (product.discountAmount) {
        discountPriceTaxAmount = product.taxAmount;
      } else {
        product.taxDiscountAmount = 0;
      }
      if (product.strikeoutAmount) {
        if (product.discountAmount) {
          strikeoutPriceTaxAmount = product.taxType === 1 ? product.taxValue : ((product.strikeoutAmount * product.taxValue) / 100);
        } else {
          strikeoutPriceTaxAmount = product.taxAmount;
        }
      }
      if (product.mrpSalesPrice) {
        mrpsalesPriceTaxAmount = product.taxAmount;
      }
    } else {
      salesPriceTaxAmount = 0;
      discountPriceTaxAmount = 0;
      strikeoutPriceTaxAmount = 0;
      mrpsalesPriceTaxAmount = 0;
    }
    product.taxSalesPrice = product.salesPrice + salesPriceTaxAmount;
    product.taxDiscountAmount = product.discountAmount + discountPriceTaxAmount;
    product.strikeoutAmount = product.strikeoutAmount + strikeoutPriceTaxAmount;
    product.mrpSalesPriceTaxAmount = Number(product.mrpSalesPrice) + mrpsalesPriceTaxAmount;
  }

  generateSno() {
    if (this.cartList.length) {
      let i = 0;
      forEach(this.cartList, (cart) => {
        // if (cart.productOption.length > 0) {
        //   forEach(cart.productOption, (attribute) => {
        //     if (cart.discountAmount === 0) {
        //       cart.salesPrice = attribute.productOptionPrice + cart.salesPrice;
        //     }
        //   });
        // }
        i += 1;
        cart.sno = i;
        cart.minimumQuantity = (cart.minimumQuantity && Number(cart.minimumQuantity !== 0)) ?
                              cart.minimumQuantity : 1;
        cart.actualMaximumQuantity = this.cartService.caluculateMaximumQuantity(cart);
        // cart.maximumQuantity = (cart.maximumQuantity && Number(cart.maximumQuantity !== 0)) ?
        //                         ((cart.maximumQuantity > (Number(cart.quantity) - cart.soldOut)) ?
        //                         (Number(cart.quantity) - cart.soldOut) : cart.maximumQuantity) :
        //                         (Number(cart.quantity) - cart.soldOut);
      });
    }
  }

  calculateInvoiceData(cartData: any, isFromApply?) {
    this.subTotal = 0;
    this.estimatedTax = 0;
    this.shippingFee = 0;
    this.grossTotal = 0;
    this.couponDiscount = 0;
    this.couponMaxDiscount = 0;
    this.couponShippingAmount = 0;
    let couponAmt = 0;
    let isMultiVendor;
    if (cartData && cartData[0] && cartData[0].hasOwnProperty('vendorId')) {
      isMultiVendor = true;
      if (cartData[0].couponCreatedBy && cartData[0].couponCreatedBy === 1) {
        const sumTotal = cartData.map((element) => element.couponDiscountAmount).reduce(function(sum, elem) {
          return sum + elem;
        });
        const maxamount = uniq(cartData.map((elem) => (elem.couponMaxAmount)));
        const amount = maxamount[0];
        if (amount) {
          if (sumTotal > amount) {
            couponAmt = amount;
          } else {
            couponAmt = sumTotal;
          }
        } else {
          couponAmt = sumTotal;
        }
        this.couponDiscount = couponAmt ? couponAmt : 0;
      } else {
        const couponData = groupBy(cartData, 'vendorId');
        const couponSumAmount = [];
        const couponDiscountDetails = [];
        forEach(couponData, (coupon) => {
          forEach(coupon, (data) => {
            const isCouponData = find(couponDiscountDetails, {vendorId: data.vendorId});
            if (!isCouponData) {
              couponDiscountDetails.push({
                vendorId: data.vendorId,
                couponDiscount: data.couponDiscountAmount,
                couponMaxAmount: data.couponMaxAmount,
              });
            }
          });
        });
        uniqBy(couponDiscountDetails, 'vendorId');
        forEach(couponDiscountDetails, (coupon) => {
          const couponDiscount = coupon.couponDiscount ? coupon.couponDiscount : 0;
          const couponAmount = coupon.couponMaxAmount ?
          (coupon.couponDiscount > coupon.couponMaxAmount ? coupon.couponMaxAmount : couponDiscount) :
          couponDiscount;
          couponSumAmount.push(couponAmount);
        });
        this.couponDiscount = this.couponDiscount + sum(couponSumAmount);
      }
    } else {
      isMultiVendor = false;
    }
    forEach(cartData, (cart) => {
      // cart.taxTotal = cart.taxTotal ? this.currencyPipe.transform(cart.taxTotal).replace(this.currency[0], '') : 0;
      // cart.shippingValue = cart.shippingValue ? this.currencyPipe.transform(cart.shippingValue).replace(this.currency[0], '') : 0;
      cart.taxTotal = cart.taxTotal ? this.formatPriceValue(cart.taxTotal) : 0;
      cart.shippingValue = cart.shippingValue ? this.formatPriceValue(cart.shippingValue) : 0;
      this.subTotal = Number(this.subTotal) + Number(cart.subTotal);
      // this.estimatedTax = this.estimatedTax + Number(this.formatAmount(cart.taxTotal));
      this.estimatedTax = this.estimatedTax + Number(cart.taxTotal);
      // this.estimatedTax = this.estimatedTax + cart.taxTotal;
      // const shipAmount = cart.shippingValue ? (cart.freeShipping === 0 ? 0 : (cart.couponFreeShipping === 1 ? 0 : cart.shippingValue)) : 0;
      // this.shippingFee = this.shippingFee + Number(shipAmount);
      // this.shippingFee = this.shippingFee + shipAmount;
      let shipAmount;
      if (this.shippingProductDetails) { // find shipping amount based on shiping gateway
        this.shippmentAmountApplied = true;
        if (isMultiVendor) {
          const shippingDetails = find(this.shippingProductDetails, {'vendorId': cart.vendorId, 'productId': cart.productId});
          if (shippingDetails) {
            shipAmount = shippingDetails.shippmentCharge;
          }
        } else {
          const shippingDetails = find(this.shippingProductDetails, {'productId': cart.productId});
          if (shippingDetails) {
            shipAmount = shippingDetails.shippmentCharge;
          }
        }
      } else {
        this.shippmentAmountApplied = false;
        shipAmount = 0;
      }
      const shipFees =  shipAmount ? (cart.freeShipping === 0 ? 0 :
          (cart.couponFreeShipping === 1 ? 0 : shipAmount)) : 0;
      this.shippingFee = this.shippingFee + shipFees;
      this.couponShippingAmount = this.shippingFee;
      // const mrp = this.currencyPipe.transform(cart.mrpSalesPrice).replace(this.currency[0], '');
      // cart.mrpSalesPrice = this.formatAmount(mrp);
      const mrp = this.formatPriceValue(cart.mrpSalesPrice);
      cart.mrpSalesPrice = mrp;
      let couponDiscount;
      couponDiscount = cart.couponDiscountAmount ? cart.couponDiscountAmount : 0;
      if (cart.vendorId) {
        this.couponFreeShipping = false;
      } else {
        this.couponDiscount = cart.couponMaxAmount ? (couponDiscount > cart.couponMaxAmount ? cart.couponMaxAmount : couponDiscount) :
          couponDiscount;
        this.couponFreeShipping = cart.couponFreeShipping ? (cart.couponFreeShipping === 0 ? false : true) : false;
        this.shippingFee = this.couponFreeShipping === true ? 0 : this.couponShippingAmount;
      }
    });
    // const sTotal = this.currencyPipe.transform(this.subTotal).replace(this.currency[0], '');
    // const subTotal = this.formatAmount(sTotal);
    // const eTax =  this.currencyPipe.transform(this.estimatedTax).replace(this.currency[0], '');
    // const estimatedTax = Number(this.formatAmount(eTax));

    const sTotal = this.formatPriceValue(this.subTotal);
    const subTotal = sTotal;
    const eTax =  this.formatPriceValue(this.estimatedTax);
    const estimatedTax = Number(eTax);

    // const estimatedTax = this.formatAmount(this.estimatedTax);
    // const sFee =  this.currencyPipe.transform(this.shippingFee).replace(this.currency[0], '');
    // const shippingFee = this.formatAmount(sFee);

    const sFee =  this.formatPriceValue(this.shippingFee);
    const shippingFee = sFee;
    // const shippingFee = this.formatAmount(this.shippingFee);
    this.grossTotal = Number(subTotal) - this.couponDiscount;
    this.grandTotal = this.grossTotal + Number(estimatedTax) + Number(shippingFee);
    this.cartList = cartData;
    if (this.applyEvent) {
      if (!!this.storage.get('id')) {
        this.getWalletAmount();
        this.applyWallet(this.applyEvent);
      }
    }
    if (isFromApply && this.couponDiscount !== 0) {
      const currency = this.curencyPipe.transform(1, this.prodDetails.currencyCode, 'symbol-narrow');
      const symbol = currency.split('1');
      const currencySymbol = symbol[0];
      const title = this.translation.translate('ERROR_COUPON_MAX_DISCOUNT_AMOUNT');
      const msg = this.translation.translate('ERROR_COUPON_CODE') + this.couponCode +
                this.translation.translate('LABEL_COUPON_MAX_DISCOUNT_AMOUNT') + currencySymbol + this.couponDiscount;
      this.showCouponAlert(title, [msg]);
    }
    if (this.shippmentCharge) {
      this.couponShippingAmount = this.shippmentCharge - this.shippingFee;
    }
  }

  updateCartQuantity(cartData: any, event?: any, showSnackBar?) {
     const cartId = [];
     forEach(this.cartList, (cart) => {
        cartId.push(cart.cartId);
     });
    //  const prodPrice = this.currencyPipe.transform(parseFloat((cartData.discountAmount === 0 ? cartData.salesPrice : cartData.discountAmount))).replace(this.currency[0], '');
    //  const price = this.formatAmount(prodPrice);
    //  const prodTax = this.currencyPipe.transform(cartData.taxAmount).replace(this.currency[0], '');
    //  const tax = this.formatAmount(cartData.taxAmount);

    const prodPrice = this.formatPriceValue((cartData.discountAmount === 0 ? cartData.salesPrice : cartData.discountAmount));
    const price = prodPrice;
    const prodTax = this.formatPriceValue(cartData.taxAmount);
    const tax = cartData.taxAmount;
     const sortPattern = CART_RULE.QUANTITY_PATTERN;
     const availableQuantity = Number(cartData.quantity) - cartData.soldOut;
     // String.fromCharCode(event.target.value);
        if (cartData.cartQuantity !== '' &&
           ((availableQuantity < Number(cartData.minimumQuantity)) ?
           (cartData.cartQuantity >= availableQuantity) : (cartData.cartQuantity >= Number(cartData.minimumQuantity))) &&
           (cartData.maximumQuantity ? cartData.cartQuantity <= Number(cartData.maximumQuantity) : cartData.cartQuantity)) {
          if (!!this.storage.get('id')) {
            let subTotalCal = 0;
            subTotalCal = Number(price) * parseFloat(cartData.cartQuantity);
              // if (cartData.productOption.length > 0) {
              //   subTotalCal = (cartData.productOption[0].productOptionPrice * cartData.cartQuantity) + subTotalCal;
              // }
              const subTotalTax = Number(tax) * cartData.cartQuantity;
            this.cartService.updateCartQuantity({
              'userId': this.storage.get('id'), 'productId': cartData.productId,
              'quantity': cartData.cartQuantity, 'subTotal': subTotalCal,
              'taxTotal': subTotalTax, 'mrpSalesPrice': cartData.mrpSalesPrice,
              'vendorId': cartData.vendorId, 'vendorProductId': cartData.vendorProductId,
              'couponCode': cartData.couponCode, 'isDeleteCoupon': event ? true : false,
              'cartId': cartId
            })
              .subscribe((response: any) => {
                if (response) {
                  if (showSnackBar !== 'selfCall') {
                    if (!this.showSnackBar) {
                      this.snackBar.open(this.translation.translate(response.data),
                      this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
                    }
                    this.showSnackBar = false;
                  }
                  this.getCartDetails();
                }
              }, (error: any) => {
                if (error.status === API_STATUS_CODE.badRequest) {
                   let data: any;
                   data = error.error.message;
                   cartData.cartQuantity = +(data.availableQuantity);
                   if (data.availableQuantity) {
                    this.snackBar.open(`${this.translation.translate('ONLY')}
                      ${data.availableQuantity} ${this.translation.translate(data.message)} ${data.productName}`,
                      this.translation.translate('buttonClose'), SNACKBAR_ALERT);
                      let cartUpdateObj;
                      if (cartData.vendorId) {
                      cartUpdateObj = filter(this.cartList, { 'vendorId': cartData.vendorId, 'productId': cartData.productId });
                    } else {
                      cartUpdateObj = filter(this.cartList, { 'productId': cartData.productId });
                    }
                    this.showSnackBar = true;
                    this.updateCartQuantity(cartUpdateObj[0], this.showSnackBar);
                      this.calculateInvoiceData(this.cartList);
                   } else {
                      this.snackBar.open(`${data.productName} ${this.translation.translate(data.message)}`,
                      this.translation.translate('buttonClose'), SNACKBAR_ALERT);
                   }
                 } else if (error.status === API_STATUS_CODE.preconditionFailed) {
                  const productDetails = error.error.data[0];
                  const cartDetails = find(this.cartList, {'productId': productDetails.productId});
                  this.comonService.showAlertForProductQuantity(productDetails, cartDetails);
                } else if (error.status === API_STATUS_CODE.conflict) {
                  if (error.error.message === 'MIN_ORDER_AMT_EXCEEDED' || error.error.message === 'MAX_DISCOUNT_AMT_EXCEEDED') {
                    let message;
                    const currency = this.curencyPipe.transform(1, this.prodDetails.currencyCode, 'symbol-narrow');
                    const symbol = currency.split('1');
                    const currencySymbol = symbol[0];
                    if (error.error.message === 'MIN_ORDER_AMT_EXCEEDED') {
                      message = this.translation.translate('ERROR_COUPON_CHANGE') + cartData.couponCode +
                        this.translation.translate('ERROR_COUPON_MINIMUM_ORDER_VALUE') + currencySymbol + this.couponDiscount +
                        this.translation.translate('ERROR_REMOVED');
                    } else {
                      message = this.translation.translate('ERROR_COUPON_MAX_CHANGE');
                    }
                    const dialogRef = this.dialog.open(ConfirmationPopupComponent, {
                      data: {
                        title: this.translation.translate('ERROR_COUPON_CHANGE_TITLE'),
                        message: message,
                        action: ALERT_SYSTEM_TITLE.warning,
                        buttonOneName: this.translation.translate('labelYes'),
                        buttonTwoName: this.translation.translate('labelNo'),
                      }
                    });
                    this.cartSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
                      this.dialog.closeAll();
                      if (data) {
                        this.couponCode = '';
                        this.updateCartQuantity(cartData, true);
                        this.getCartDetails();
                      } else {
                        this.getCartDetails();
                      }
                    });
                  }
                }
              });
          } else {
            const availableQuantity = (+(cartData.quantity) - cartData.soldOut);
            if (cartData.cartQuantity <= availableQuantity) {
             const subTotalCal = Number(price) * parseFloat(cartData.cartQuantity);
              // subTotalCal = cartData.productOption.length > 0 ? cartData.productOption[0].productOptionPrice + subTotalCal : subTotalCal;
            const subTotalTax = Number(tax) * parseFloat(cartData.cartQuantity);
            const cartDetail = find(this.cartList, ['productId', cartData.productId]);
            cartDetail.subTotal = subTotalCal;
            cartDetail.taxTotal = subTotalTax;
            this.storage.set('cartStorage', JSON.stringify(this.cartList));
            this.getCartDetails();
          } else {
              cartData.cartQuantity = availableQuantity;
              this.snackBar.open(`${this.translation.translate('ONLY')}
              ${availableQuantity}  ${this.translation.translate('PIECES_IS_AVAILABLE_IN')}
              ${cartData.productName}` , this.translation.translate('buttonClose'), SNACKBAR_ALERT);
            }
          }
        } else {
          if (cartData.maximumQuantity && cartData.cartQuantity > +(cartData.maximumQuantity)) {
            cartData.cartQuantity = +(cartData.maximumQuantity);
            // this.getCartDetails();
            const subTotalTax = Number(tax) * cartData.cartQuantity;
            let subTotalCal = 0;
            subTotalCal = Number(price) * parseFloat(cartData.cartQuantity);
              this.snackBar.open(`${this.translation.translate('alertQuantityMaximum')} ${cartData.maximumQuantity}
              ${this.translation.translate('alertQuantityMaximumError')}`, this.translation.translate('buttonClose'), SNACKBAR_WARNING);
          } else if (cartData.cartQuantity < cartData.minimumQuantity) {
            cartData.cartQuantity = cartData.minimumQuantity;
            const subTotalTax = Number(tax) * cartData.cartQuantity;
            let subTotalCal = 0;
            subTotalCal = Number(price) * parseFloat(cartData.cartQuantity);
              this.snackBar.open(`${this.translation.translate('alertQuantityMinimum')} ${cartData.minimumQuantity}`,
              this.translation.translate('buttonClose'), SNACKBAR_WARNING);
          }
           this.updateCartQuantity(cartData, false, 'selfCall');
        }
  }


  openOTP() {
    this.dialog.open(OtpComponent);
  }

  // cartQuantityUpdate(cartData, subTotalTax, subTotalCal, minMaxMessage) {
  //   this.cartService.updateCartQuantity({
  //     'userId': this.storage.get('id'), 'productId': cartData.productId,
  //     'quantity': cartData.cartQuantity, 'subTotal': subTotalCal,
  //     'taxTotal': subTotalTax, 'mrpSalesPrice': cartData.mrpSalesPrice,
  //     'vendorId': cartData.vendorId, 'vendorProductId': cartData.vendorProductId
  //   })
  //     .subscribe((response: any) => {
  //       if (response) {
  //         this.snackBar.open(this.translation.translate(response.data), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
  //         this.getCartDetails();
  //         if (minMaxMessage === 0) {
  //           this.snackBar.open(this.translation.translate('alertQuantityMinimum') + cartData.minimumQuantity,
  //           this.translation.translate('buttonClose'), SNACKBAR_WARNING);
  //         } else if (minMaxMessage === 1) {
  //               this.snackBar.open(this.translation.translate('alertQuantityMaximum') + cartData.maximumQuantity +
  //               this.translation.translate('alertQuantityMaximumError'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
  //         } else if (minMaxMessage === 2) {
  //               this.snackBar.open(this.translation.translate('ONLY') + ' ' +
  //               cartData.cartQuantity + ' ' + this.translation.translate('labelAvailable'),
  //               this.translation.translate('buttonClose'), SNACKBAR_ALERT);
  //         }
  //       }
  //     }, (error: any) => {
  //       if (error.status === API_STATUS_CODE.badRequest) {
  //         this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
  //         if (minMaxMessage === 0) {
  //           this.snackBar.open(this.translation.translate('alertQuantityMinimum') + cartData.minimumQuantity,
  //           this.translation.translate('buttonClose'), SNACKBAR_WARNING);
  //         } else {
  //               this.snackBar.open(this.translation.translate('alertQuantityMaximum') + cartData.maximumQuantity +
  //               this.translation.translate('alertQuantityMaximumError'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
  //         }
  //       } else if (error.status === API_STATUS_CODE.preconditionFailed) {
  //         const productDetails = error.error.data[0];
  //         const cartDetails = find(this.cartList, {'productId': productDetails.productId});
  //         this.comonService.showAlertForProductQuantity(productDetails, cartDetails);
  //       }
  //     });
  // }

  removeCartDetails(productId: any, cartId: any, vendorId: any, couponCode: any, deleteCoupon: boolean) {
    const dialogRef = this.dialog.open(ConfirmationPopupComponent, {
      data: {
        title: this.translation.translate('labelDialogTitle'),
        message: this.translation.translate('alertMsgProductRemove'),
        action: ALERT_SYSTEM_TITLE.warning,
        buttonOneName: this.translation.translate('buttonDelete'),
        buttonTwoName: this.translation.translate('buttonCancel'),
      }
    });
    this.cartSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
      if (data) {
        this.removeProductFromCart(productId, cartId, vendorId, couponCode, deleteCoupon, true);
      }
    });
  }

  removeProductFromCart(productId: any, cartId: any, vendorId: any, couponCode: any, deleteCoupon: boolean, fromCart?) {
    const cartIds = [];
    forEach(this.cartList, (cart) => {
      cartIds.push(cart.cartId);
    });
    if (!!this.storage.get('id')) {
      this.cartService.removeCartDetails({ 'productId': productId, 'cartId': cartId,
        'isDeleteCoupon': deleteCoupon, 'couponCode': couponCode, 'cartList': cartIds })
      .subscribe((response: any) => {
        this.getCartDetails();
        if (fromCart) {
          this.snackBar.open(this.translation.translate(response.data), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        }
        this.dialog.closeAll();
        this.homeService.setCartCountChange(true);
      }, (error: any) => {
        this.dialog.closeAll();
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        } else if (error.status === API_STATUS_CODE.conflict) {
          if (error.error.data === 'MIN_ORDER_AMT_EXCEEDED' || error.error.data === 'MAX_DISCOUNT_AMT_EXCEEDED') {
            let message;
            const currency = this.curencyPipe.transform(1, this.prodDetails.currencyCode, 'symbol-narrow');
            const symbol = currency.split('1');
            const currencySymbol = symbol[0];
            if (error.error.data === 'MIN_ORDER_AMT_EXCEEDED') {
              message = this.translation.translate('ERROR_COUPON_CHANGE') + couponCode +
                this.translation.translate('ERROR_COUPON_MINIMUM_ORDER_VALUE') + currencySymbol + this.couponDiscount +
                this.translation.translate('ERROR_REMOVED');
            } else {
              message = this.translation.translate('ERROR_COUPON_MAX_CHANGE');
            }
            const dialogRef = this.dialog.open(ConfirmationPopupComponent, {
              data: {
                title: this.translation.translate('ERROR_COUPON_CHANGE_TITLE'),
                message: message,
                action: ALERT_SYSTEM_TITLE.warning,
                buttonOneName: this.translation.translate('labelYes'),
                buttonTwoName: this.translation.translate('labelNo'),
              }
            });
            this.cartSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
             if (data) {
                this.dialog.closeAll();
                this.removeProductFromCart(productId, cartId, vendorId, couponCode, true);
             }
            });
          }
        }
      });
    } else {
      this.cartList = this.storage.get('cartStorage') ? JSON.parse(this.storage.get('cartStorage')) : [];
      if (vendorId) {
        remove(this.cartList, {productId: productId, vendorId: vendorId});
      } else {
        remove(this.cartList, {productId: productId});
      }
      this.cartList.length !== 0 ? this.storage.set('cartStorage', JSON.stringify(this.cartList)) :
      this.storage.remove('cartStorage');
      this.cartStatus = this.cartList.length > 0 ? true : false;
      this.snackBar.open(this.translation.translate('alertProductRemove'), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      this.dialog.closeAll();
      this.homeService.setCartCountChange(true);
      this.calculateInvoiceData(this.cartList);
    }
  }

  goToAddress() {
    if (!!this.storage.get('id')) {
      this.addressPanel.toggle();
      this.addressMobPanel.toggle();
      this.getUserAddressDetails();
      this.enableAddressToggle = false;
    } else {
      this.enableAddressToggle = true;
      this.snackBar.open(this.translation.translate('alertLoginCart'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
      this.router.navigate(['/signin']);
    }
  }

  goToPayment() {
    if (!!this.storage.get('id')) {
      this.paymentPanel.toggle();
      this.paymentMobPanel.toggle();
      this.getUserAddressDetails();
      this.enableAddressToggle = false;
      this.enablePaymentToggle = false;
    } else {
      this.enableAddressToggle = true;
      this.enablePaymentToggle = true;
      this.snackBar.open(this.translation.translate('alertLoginCart'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
      this.router.navigate(['/signin']);
    }
  }

  getUserAddressDetails() {

    this.cartService.getAddressList().subscribe((response: any) => {
      if (response) {
        this.addressList = response.data;
      }

    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });

  }
  getPaymentListDetails(){
    this.cartService.getPaymentList().subscribe((response: any) => {
      if(response) {
        this.paymentList = response.data;
        this.paymentType = this.paymentList[0].paymentModeId;
        // if(this.shippingProductDetails.length > 1) {
        //   const paymentStatus = true;
        //   this.paymentList =  this.paymentList.map(x => { return {...x, paymentStatus} });
        //   forEach(this.paymentList, (paymentObj) => {
        //     if(this.shippingProductDetails[1].postal_code.pre_paid === 'Y') {
        //       if(paymentObj.pgName !== 'COD') {
        //         paymentObj.paymentStatus = false;
        //       }
        //     }
        //     if(this.shippingProductDetails[1].postal_code.cod === 'Y') {
        //       if(paymentObj.pgName === 'COD') {
        //         paymentObj.paymentStatus = false;
        //       }
        //     }
        //   });
        // }
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });

  }

  getUserInfoDetails() {
    this.cartService.getUserProfileDetails().subscribe((response: any) => {
      if (response) {
        this.profileDetails = response.data[0];
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  changeShippingAddress(event: any) {
    if (event.checked) {      
      this.disableShipping = true;
      this.shippingAddress = this.billingAddress;       
    } else {
      this.disableShipping = false;
      this.shippingAddress = '';
      this.enablePaymentToggle = true;
    }
  }

  changeBillingAddress() {
    if (this.billingAddress) {
      if (!this.enableCheckBox && this.isShppingSameAsBilling) {
        this.shippingAddress = this.billingAddress;
      } else {
        this.disableShipping = false;
        this.enableCheckBox = false;
      }
    }
  }

  stripeMethod() {
    const dialogRef = this.dialog.open(PaymentComponent);
    dialogRef.afterClosed().subscribe((paymentResponse: any) => {
      this.stripeResponse = paymentResponse;
      if (this.stripeResponse.error) {
        this.snackBar.open(this.stripeResponse.error.code, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      } else {
        this.saveCheckoutDetails(this.stripeResponse);
      }
    });
  }

  // saveOrderDetails() {
  //   if (Number(this.paymentType) === 2) {
  //    this.openCheckout(this.grandTotal);
  //   } else {
  //     this.saveCheckoutDetails();
  //   }
  // }

  navigateToProductDetailPage(cartDetail) {
    if (cartDetail.vendorId) {
      this.router.navigate(['/product-detail', cartDetail.productId], {queryParams : {'id': cartDetail.vendorId}});
    } else {
      this.router.navigate(['/product-detail', cartDetail.productId]);
    }
  }

  openCheckout(stripeDetails) {
    this.stripeToken = undefined;
    this.cartService.getStripePublicKey().subscribe((response: any) => {
      if (response) {
        this.stripeKey = JSON.parse(response.data[0].publicKey);
        const handler = this.window.StripeCheckout.configure({
          key: this.stripeKey,
          locale: 'auto',
          token:  (token: any) => {
            this.stripeToken = token;
            if  (this.stripeToken) {
              this.stripeCheckOutProductInfo(this.stripeToken, stripeDetails);
            }
          },
          closed: () => {
            if (!this.stripeToken) {
              this.releaseStripeOrder(stripeDetails.orderId);
            }
          }
        });
      const siteSetting: any = this.sharedService.getSiteSettingDetails();
      const stripePaymentTotal = (this.walletStatus ? this.walletTotal : this.grandTotal)  * 100;
      handler.open({
        name: siteSetting.siteTitle,
        description: '',
        amount: Math.round(stripePaymentTotal),
        currency: siteSetting.currencyCode
      });
      }
  });
  }

  checkoutCartDetails() {
    const productDetails = [];
    const couponDetails = [];
    forEach(this.cartList, (cart) => {
      const options = [];
      if (cart.productOption.length > 0) {
        forEach(cart.productOption, (option) => {
          // const optionPrice = this.currencyPipe.transform(option.productOptionPrice).replace(this.currency[0], '');
          const optionPrice = this.formatPriceValue(option.productOptionPrice);
          options.push({
            productOptionDetailId: option.productOptionDetailId,
            optionName: option.optionName,
            optionValue: option.optionValue,
            orderOption: [{
              optionValueName: option.optionValueName,
              optionValuePrice: Number(optionPrice)
              }]
          });
        });
      }
      // const prodSalesPrice = this.currencyPipe.transform(cart.salesPrice).replace(this.currency[0], '');
      // const prodTax = this.currencyPipe.transform(cart.taxAmount).replace(this.currency[0], '');
      // const prodTaxTotal = this.currencyPipe.transform(cart.taxAmount).replace(this.currency[0], '');
      // const prodTotal = this.currencyPipe.transform(cart.subTotal).replace(this.currency[0], '');
      // const prodShipAmount = this.currencyPipe.transform(cart.shippingValue).replace(this.currency[0], '');
      // const mrpSales = this.currencyPipe.transform(cart.mrpSalesPrice).replace(this.currency[0], '');

      const prodSalesPrice = this.formatPriceValue(cart.salesPrice);
      const prodTax = this.formatPriceValue(cart.taxAmount);
      const prodTaxTotal = this.formatPriceValue(cart.taxAmount);
      const prodTotal = this.formatPriceValue(cart.subTotal);
      const prodShipAmount = this.formatPriceValue(cart.shippingValue);
      const mrpSales = this.formatPriceValue(cart.mrpSalesPrice);
      productDetails.push({
        productId: cart.productId,
        productName: cart.productName,
        quantity: cart.cartQuantity,
        price: Number(prodSalesPrice),
        mrpSalesPrice: Number(mrpSales),
        discount: cart.discount ? Number(cart.discount) : 0,
        tax: Number(cart.taxAmount),
        orderTaxTotal: Number(cart.taxTotal),
        total: Number(prodTotal),
        optionDetail: options,
        vendorId: cart.vendorId ? cart.vendorId : null,
        couponDiscountAmount: cart.couponCode ? cart.couponDiscountAmount : null,
        couponCreatedBy: cart.couponCreatedBy ? cart.couponCreatedBy : null,
        shippingAmount: cart.couponFreeShipping === 1 ? 0 : (cart.freeShipping === 1 ?  Number(prodShipAmount) : 0),
        companyName: cart.vendorId ? cart.companyName : null,
        vendorProductId: cart.vendorProductId ? cart.vendorProductId : null,
        weight: cart.productWeight,
        freeShipping: cart.freeShipping,
        couponFreeShipping: cart.couponFreeShipping,
        cartId: cart.cartId
      });
      if (cart.couponCode) {
        // const couponDis  = this.currencyPipe.transform(cart.couponDiscountAmount).replace(this.currency[0], '');
        const couponDis  = this.formatPriceValue(cart.couponDiscountAmount);
        couponDetails.push({
          productId: cart.productId,
          couponDiscountAmount: couponDis,
          couponFreeShipping: cart.couponFreeShipping,
          couponCode: cart.couponCode,
          vendorId: cart.vendorId ? cart.vendorId : undefined
        });
      }
    });
    uniqBy(couponDetails, 'couponCode');
    // const orderTax = this.currencyPipe.transform(this.estimatedTax).replace(this.currency[0], '');
    // const walAmount = this.currencyPipe.transform(this.walletAmount).replace(this.currency[0], '');
    // const walTotal = this.currencyPipe.transform(this.walletTotal).replace(this.currency[0], '');
    // const shipAmount = this.currencyPipe.transform(this.shippingFee).replace(this.currency[0], '');
    const orderTax = this.formatPriceValue(this.estimatedTax);
    const walAmount = this.formatPriceValue(this.walletAmount);
    const walTotal = this.formatPriceValue(this.walletTotal);
    const shipAmount = this.formatPriceValue(this.shippingFee);
    const cartDetails = {
      shippingAddress: [{
        addressId: this.shippingAddress.addressId,
        firstName: this.shippingAddress.firstName,
        lastName: this.shippingAddress.lastName,
        addressTypeId: this.shippingAddress.addressTypeID,
        addressOne: this.shippingAddress.addressOne,
        addressTwo: this.shippingAddress.addressTwo,
        zipCode: this.shippingAddress.zipCode,
        countryId: this.shippingAddress.countryId,
        countryName: this.shippingAddress.countryName,
        stateId: this.shippingAddress.stateId,
        stateName: this.shippingAddress.stateName,
        cityId: this.shippingAddress.townVillageId,
        cityName: this.shippingAddress.townVillageName,
        userMobile: this.shippingAddress.phone,
        landLine: this.shippingAddress.alternateMobile
      }],
      billingAddress: [{
        addressId: this.billingAddress.addressId,
        firstName: this.billingAddress.firstName,
        lastName: this.billingAddress.lastName,
        addressTypeId: this.billingAddress.addressTypeID,
        addressOne: this.billingAddress.addressOne,
        addressTwo: this.billingAddress.addressTwo,
        zipCode: this.billingAddress.zipCode,
        countryId: this.billingAddress.countryId,
        stateId: this.billingAddress.stateId,
        cityId: this.billingAddress.townVillageId,
        userMobile: this.billingAddress.phone,
        landLine: this.billingAddress.alternateMobile
      }],
      userId: this.storage.get('id'),
      vendorId: 1, // Need to change after integration multi vendor
      paymentId: Number(this.paymentType),
      thirdPartyId: 1, // Need to change after integration third party settings
      ipAddress: null,
      orderTotal: this.grandTotal,
      couponDetails: couponDetails,
      orderTax: Number(orderTax),
      shippingSettingId: 1, // Need to change after integration shipping gateway
      orderStatus: 1,
      productDetails: productDetails,
      walletStatus: this.walletStatus,
      walletAmount: Number(walAmount),
      walletTotal: Number(walTotal),
      shippingAmount: Number(shipAmount),
      fullPaymentOnWallet: this.fullPaymentOnWallet,
      paymentToken: [this.stripeToken ? this.stripeToken : null]
    };
    return cartDetails;
  }

  releaseStripeOrder(orderId) {
    this.cartService.stripeReleaseOrder(orderId).subscribe(
      (response: any) => {
        this.snackBar.open(this.translation.translate(response.data.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }, 
      (error: any) =>{
        this.snackBar.open(this.translation.translate(error.data.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      });
      this.isPlacingOrder = false;
  }

  checkShipMentCharge(shippingDetails: any) {
    if(shippingDetails.shippmentCharge && shippingDetails.freeShipping === 1 && !(shippingDetails.couponFreeShipping === 1)) {
      return shippingDetails.shippmentCharge;
    } else {
      return 0;
    }
  };

  getMembershipDetails() {
    this.sharedService.getSubscriptionDetails().subscribe((response: any) => {
      if(response) {
        const subscriptionPlan = response.subscriptionList[0].subscriptionPlan;
        this.storage.set('subscriptionPlan', subscriptionPlan);
      }
    }, (error) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.message, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      } 
    });
  }

  subscribeMemberShipPlan(paymentToken?) {
    const subscriptionPlan = this.storage.get('subscriptionPlan');
    if (!subscriptionPlan || subscriptionPlan == "null") {
      const dialogRef = this.dialog.open(MembershipComponent, {
        data: {
          orderTotal: this.grandTotal
        }
      });
      dialogRef.afterClosed().subscribe((response: any) => {
        if (response ) {
          this.dialog.closeAll();
          this.saveCheckoutDetails(paymentToken);
        }
      });
    } else {
      this.saveCheckoutDetails(paymentToken);
    }  
  }

  // Get dynamic error message
  getErrorKey(error){
    let returnKey = error ? error : 'SOMETHING_WENT_WRONG_UNABLE_TO_PLACE_ORDER';
    return this.translation.translate(returnKey);
  }

  saveCheckoutDetails(paymentToken?) {
    // Check whether total amount is Zero, after applied Wallet & Coupon amount
    this.isPlacingOrder = true;
    if(this.walletTotal <= 0 && this.walletStatus){
      this.paymentType = '1';
    }
    
    const cartDetails: any = this.checkoutCartDetails();
    const isMultiVendor = cartDetails.productDetails[0].hasOwnProperty('vendorId') ? true : false;
    forEach(cartDetails.productDetails, (product) => {
      delete product.weight;
      if (isMultiVendor) {
        const shippingDetails = find(this.shippingProductDetails, {'vendorId': product.vendorId, 'productId': product.productId});
        if (shippingDetails) {
          product.shippingServiceProvider = shippingDetails.providerId;
          product.shippingServiceProvider = shippingDetails.shippingServiceProvider;
          product.shippingGatewayId = shippingDetails.shippingGatewayId;
          product.shippingAmount = this.checkShipMentCharge(shippingDetails);
          delete product.freeShipping;
          delete product.couponFreeShipping;
        }
      } else {
        const shippingDetails = find(this.shippingProductDetails, {'productId': product.productId});
        if (shippingDetails) {
          product.shippingAmount = this.checkShipMentCharge(shippingDetails);
          delete product.freeShipping;
          delete product.couponFreeShipping;
        }
      }
      delete product.freeShipping;
      delete product.couponFreeShipping;
    });
    if (!isMultiVendor) {
      cartDetails.shippingServiceProvider = this.shippingProductDetails[0].providerId;
      cartDetails.shippingGatewayId = this.shippingProductDetails[0].shippingGatewayId;
    }

    let locationObj = this.sharedService.getTrackLocationData();
    cartDetails.longitude = locationObj.longitude;
    cartDetails.latitude = locationObj.latitude;
    cartDetails.createdBy =  'self';

    this.cartService.saveOrderCheckout(cartDetails, Number(this.paymentType)).subscribe((response: any) => {
      if (response) {
        if (Number(this.paymentType) === 1) {
          this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
          this.homeService.setCartCountChange(true);
          this.router.navigate(['/order-summary', response.data.orderId]);
        }
        if (Number(this.paymentType) === 2) {
          this.openCheckout(response.data);
        }
        if (Number(this.paymentType) === 7) {
          this.razorPayOpenPopup(response.data.razorPayId, response.data.orderId);
        }
        if (Number(this.paymentType) === 8) {
          this.encRequest = response.data.hdfcOrderId;
          this.accessCode = response.data.accessCode;
          this.accountUrl = response.data.accountUrl;
          if (this.accessCode && this.encRequest) {
            setTimeout(_ => this.form.nativeElement.submit());
          }
        }
        if(Number(this.paymentType) === 9) {
          this.cashFreePopup(response.data, cartDetails);
        }
      }
    }, (error: any) => {
      this.isPlacingOrder = false;
      if (error.status === API_STATUS_CODE.badRequest) {
        if ((error.error.message === 'ORDER_TOTAL_NOT_MATCHED') ||
            (error.error.message === 'PAYMENT_ID_NOT_FOUND') ||
            (error.error.message === 'WALLET_AMOUNT_NOT_MATCHED')) {
                this.comonService.logoutUserSession().subscribe(() =>
                 (error: any) => {
                     if (error.status === API_STATUS_CODE.badRequest) {
                         this.snackBar.open(this.getErrorKey(error.error.message),
                           this.translation.translate('buttonClose'), SNACKBAR_ALERT);
                              }
                     });
        this.snackBar.open(this.getErrorKey(error.error.message),
        this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        this.homeService.setSignInStatus(false);
        this.homeService.setCartCountChange(false);
        this.sharedService.clearUserInfoFromStorage();
        this.router.navigate(['/signin']);
        } else {
          this.snackBar.open(error.error.productName ?
            (error.error.productName + ' ' + this.translation.translate(error.error.message))
             : this.translation.translate(error.error.message),
               this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      }
      if (error.status === API_STATUS_CODE.internalServer) {
        this.snackBar.open(this.getErrorKey(error.error.error), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
      if (error.status === API_STATUS_CODE.preconditionFailed) {
        let data: any;
        data = error.error;
        if (!data) {
          return;
        }
        if (error.error.message === 'order_by_stripe') {
          this.openCheckout(error.error.data[0]);
        } else if (error.error.data && error.error.data.razorPayId) {
            this.razorPayOpenPopup(error.error.data.razorPayId, error.error.data.orderId);
        } else {
          if (error.error[0].message === 'PIECES_ARE_AVAILABLE_IN') {
            this.snackBar.open(this.translation.translate('ONLY') + ' ' +
            error.error[0].quantity + ' ' + this.getErrorKey(error.error[0].message) + ' ' + error.error[0].productName,
            this.translation.translate('buttonClose'), SNACKBAR_ALERT);
            this.enableDecrementButton = true;
          } else {
            this.snackBar.open(error.error.map(err => err.productName + ' ' ) + ' ' + this.getErrorKey(error.error[0].message),
            this.translation.translate('buttonClose'), SNACKBAR_ALERT);
          }
        }
      }
    });
  }



  addAddress() {
    const addDialogRef = this.dialog.open(CreateAddressComponent, {
      width: '40%',
      height: 'auto'
    });
    addDialogRef.afterClosed().subscribe((response: any) => {
      if (response && response !== true) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        this.getUserAddressDetails();
        if (!this.isShppingSameAsBilling) {
          this.enableCheckBox = true;
        }
      }
    });
  }

  continueShopping() {
    this.router.navigate(['/']);
  }

  formatAmount(amount: any) {
    if (amount && amount.toString().indexOf(',') !== -1) {
      return amount.toString().replace(/,/g, '');
    } else {
      return amount;
    }
  }

  getWalletAmount() {
    this.cartService.getWalletAmount().subscribe((response: any) => {
      if (response) {
        this.originalWalletAmount = response.data[0].amount;
        this.walletAmount = response.data[0].amount;
        this.applyEvent.walletData = true;
        if (this.applyEvent) {
          this.applyWallet(this.applyEvent);
        }
        if (response.data[0].amount < 1) {
          this.emptyWallet = false;
        }
      } else {
        this.emptyWallet = false;
      }
    });
  }

  applyWallet(event) {
      this.applyEvent = event;
      if (event.checked || this.applyEvent.checked) {
          this.walletStatus = true;
          if (this.grandTotal >= this.walletAmount) {
            this.walletTotal = this.grandTotal - this.walletAmount;
            this.walletAmount = 0;
          } else if (this.walletAmount > this.grandTotal) {
            this.walletAmount =  this.walletAmount - this.grandTotal;
            this.originalWalletAmount = this.originalWalletAmount - this.walletAmount;
            this.walletTotal = 0;
          }
      } else {
        if (this.applyEvent.walletData !== true) {
          this.getWalletAmount();
        }
        this.walletStatus = false;
      }
      if (this.walletStatus && this.walletTotal === 0) {
        this.fullPaymentOnWallet = true;
      } else {
        this.fullPaymentOnWallet = false;
      }
  }

  moveToWishList(cartData) {
    if (!!this.storage.get('id')) {
      const productId = cartData.productId;
    this.sharedService.addProductToWishList({
      'productId': cartData.productId,
      'status': 1,
      'vendorId': cartData.vendorId ? cartData.vendorId : null,
      'vendorProductId' : cartData.vendorProductId ? cartData.vendorProductId : null}).subscribe(
      (res: any) => {
        if (res) {
          this.removeProductFromCart(cartData.productId, cartData.cartId, cartData.vendorId, cartData.couponCode, false);
          this.comonService.setWhislistCount(true);
          this.homeService.setCartCountChange(true);
          this.snackBar.open(this.translation.translate(res.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        }
      },
      (err: any) => {
        if (err.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(this.translation.translate(err.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
        if (err.status === API_STATUS_CODE.internalServer) {
          this.snackBar.open(this.translation.translate(err.error.error), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
        if (err.status === API_STATUS_CODE.conflict) {
          this.snackBar.open(this.translation.translate(err.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      }
    );
    } else {
      this.snackBar.open(this.translation.translate('alertLoginCart'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
      this.router.navigate(['/signin']);
    }
  }

  applyCoupon() {
    if (!!this.storage.get('id')) {
      const productIds = [];
      forEach(this.cartList, (cart) => {
        // const prodTotal = this.currencyPipe.transform(cart.subTotal).replace(this.currency[0], '');
        const prodTotal = this.formatPriceValue(cart.subTotal);
        productIds.push({
          productId: cart.productId,
          vendorId: cart.vendorId ? cart.vendorId : undefined,
          total: Number(prodTotal)
        });
      });
      const couponData = {
        couponCode: this.couponCode,
        orderTotal: this.grandTotal,
        productId: productIds,
        subTotal: this.subTotal,
        cartId: this.cartList[0].cartId,
        createdBy: this.cartList[0].couponCreatedBy
      };
      this.cartService.validateCouponDetails(couponData).subscribe((response: any) => {
        this.storage.remove('couponCode');
        if (response) {
          this.step = 0;
          this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
          let maxCouponDiscount = false;
          maxCouponDiscount = response.data.maxDiscountAmount ? true : false;
          this.getCartDetails(maxCouponDiscount);
        }
      }, (error: any) => {
        let msg;
        let title; // to show the alerts based on the condition

        switch (error.error.message) {
          case 'INVALID_COUPON':
              title = this.translation.translate('INVALID_COUPON_CODE');
              msg = this.translation.translate('ERROR_COUPON_CODE') + this.couponCode + this.translation.translate('ERROR_INVALID_COUPON');
              this.showCouponAlert(title, [msg]);
              break;
          case 'COUPON_ALREADY_USED':
              title = this.translation.translate('ERROR_COUPON_ALREADY_USED');
              msg = this.translation.translate('ERROR_COUPON_CODE') + this.couponCode +
                    this.translation.translate('ERROR_COUPON_ALREADY_USED_ORDER') + error.error.data.orderNumber;
              this.showCouponAlert(title, [msg]);
              break;
          case 'NOT_A_ACTIVE_COUPON':
              title = this.translation.translate('ERROR_COUPON_NOT_ACTIVE');
              msg = this.translation.translate('ERROR_COUPON_CODE') + this.couponCode +
                    this.translation.translate('LABEL_COUPON_NOT_ACTIVE');
              this.showCouponAlert(title, [msg]);
              break;
          case 'COUPON_NOT_APPLICABLE':
              const currency = this.curencyPipe.transform(1, this.prodDetails.currencyCode, 'symbol-narrow');
              const symbol = currency.split('1');
              const currencySymbol = symbol[0];
              title = this.translation.translate('ERROR_COUPON_NOT_APPLICABLE');
              msg = this.translation.translate('ERROR_COUPON_CODE') + this.couponCode +
                    this.translation.translate('LABEL_COUPON_NOT_APPLICABLE') + currencySymbol + error.error.data.minOrderAmount;
              this.showCouponAlert(title, [msg]);
              break;
          case 'COUPON_EXPIRED':
            title = this.translation.translate('ERROR_COUPON_EXPIRED');
            msg = this.translation.translate('ERROR_COUPON_CODE') + this.couponCode +
                  this.translation.translate('LABEL_COUPON_EXPIRED') +
                  this.customDatePipe.transform(error.error.data.couponDate);
            this.showCouponAlert(title, [msg]);
          break;  
          case 'COUPON_ALREADY_APPLIED':
              title = this.translation.translate('ERROR_COUPON_ALREADY_APPLIED');
              msg = this.translation.translate('ERROR_COUPON_CODE') + error.error.data.couponCode +
                    this.translation.translate('LABEL_COUPON_ALREADY_APPLIED');
              this.showCouponAlert(title, [msg]);
              break;
          case 'COUPON_NOT_ACTIVATED':
              title = this.translation.translate('ERROR_COUPON_NOT_ACTIVATED');
              msg = this.translation.translate('ERROR_COUPON_CODE') + this.couponCode +
                    this.translation.translate('LABEL_COUPON_NOT_ACTIVATED')
                    + this.customDatePipe.transform(error.error.data.couponDate);
              this.showCouponAlert(title, [msg]);
              break;
          case 'VENDOR_COUPON_ALREADY_APPLIED':
              title = this.translation.translate('ERROR_COUPON_ALREADY_APPLIED');
              const msg1 = this.translation.translate('ERROR_COUPON_CODE') + error.error.data.couponCode +
                          this.translation.translate('LABEL_COUPON_ALREADY_APPLIED');
              const msg2 = this.translation.translate('ERROR_OTHER_COUPON_APPLIED') + this.couponCode +
                            this.translation.translate('LABEL_CANNOT_BE_APPLIED');
              msg = [msg1, msg2];
              this.showCouponAlert(title, msg);
              break;
          case 'ADMIN_COUPON_ALREADY_APPLIED':
              title = this.translation.translate('ERROR_COUPON_ALREADY_APPLIED');
              const msg3 = this.translation.translate('ERROR_COUPON_CODE') + error.error.data.couponCode +
              this.translation.translate('LABEL_COUPON_ALREADY_APPLIED');
              const msg4 = this.translation.translate('ERROR_OTHER_COUPON_APPLIED') + this.couponCode +
                        this.translation.translate('LABEL_CANNOT_BE_APPLIED');
              msg = [msg3, msg4];
              this.showCouponAlert(title, msg);
              break;
          case 'COUPON_USAGE_LIMIT_EXCEEDED':
              title = this.translation.translate('ERROR_COUPON_USAGE_LIMIT_EXCEEDED');
              msg = this.translation.translate('ERROR_COUPON_CODE') + this.couponCode +
                    this.translation.translate('LABEL_COUPON_USAGE_LIMIT_EXCEEDED') + error.error.data.couponOrderLimit;
              this.showCouponAlert(title, [msg]);
              break;
          case 'CUSTOMER_USAGE_LIMIT_EXCEEDED':
            title = this.translation.translate('ERROR_CUSTOMER_USAGE_LIMIT_EXCEEDED');
            const msg5 = this.translation.translate('ERROR_COUPON_CODE') + this.couponCode +
                         this.translation.translate('LABEL_COUPON_USAGE_LIMIT_EXCEEDED') + error.error.data.couponOrderLimit;
            const msg6 = this.translation.translate('LABEL_CUSTOMER_USED_ORDERS') + error.error.data.orderNumber.toString();
            msg = [msg5, msg6];
            this.showCouponAlert(title, msg);
        }
      });
    } else {
      this.storage.set('couponCode', this.couponCode);
      this.snackBar.open(this.translation.translate('alertLoginCart'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
      this.router.navigate(['/signin']);
    }
  }

  showCouponAlert(title, msg) {
    const dialogRef =  this.dialog.open(AlertPopupComponent, {
      data: {
        title: title,
        message: msg,
        action: ALERT_SYSTEM_TITLE.warning,
        buttonName: this.translation.translate('buttonOk')
      }
    });
    this.actionSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
      if (data) {
        this.dialog.closeAll();
      }
    });
  }

  reduceOneCountOnClickingTheMinusSignButton(cart) {
       cart.cartQuantity--;
       this.userQuantityUpdate.next(cart);
  }

  increaseOneCountOnClickingThePlusSignButton(cart) {
    cart.cartQuantity++;
    this.userQuantityUpdate.next(cart);
  }
  
  ngOnDestroy() {
    // if (this.cartCountSubscribe$) {
    //   this.cartCountSubscribe$.unSubscribe();
    // }
    if (this.unsubScribeQuantityUpdate$) {
      this.unsubScribeQuantityUpdate$.unsubscribe();
    }
    if (this.cartDetailSubscribe$) {
      this.cartDetailSubscribe$.unsubscribe();
    }
  }

  getShippingAmount(fromNext?) {
    if (this.prodDetails.isShippingGatewayAvailable !== 0) {
      const cartDetails = this.checkoutCartDetails();
      forEach(cartDetails.productDetails, (product) => {
        product.freeShipping = product.couponFreeShipping === 1 ? 0 : product.freeShipping;
      });
      if (this.fullPaymentOnWallet) {
        cartDetails.paymentId = 4;
      }
      this.shippmentCharge = 0;
      this.cartService.getShippementAmount(cartDetails).subscribe((response: any) => {
        if (response) {
          this.shippingProductDetails = response;
          if(!this.paymentPanel.expanded){
            this.paymentPanel.toggle();
            this.paymentMobPanel.toggle();
          }
          this.enablePaymentToggle = false;
          if(this.shippingProductDetails) {
            this.getPaymentListDetails();
          }
          // if (fromNext) {
          //   this.step++;
          // }
          
          forEach(response, (product) => {
            this.shippmentCharge = this.shippmentCharge + product.shippmentCharge;
          });
          
          this.snackBar.open(this.translation.translate('SUCCESS_SHIPPING_FEE'),
          this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
          this.calculateInvoiceData(this.cartList);
        }
      }, (error: any) => {
        this.enablePaymentToggle = true;
        this.shippingProductDetails = undefined;
        if (error.status === API_STATUS_CODE.preconditionFailed) {
          let productName = '';
          productName = error.error.toString();
          const title = this.translation.translate('LABEL_SERVICE_NOT_AVAILABLE');
          const msg = this.translation.translate('LABEL_THE_PRODUCT');
          const msg1 = this.translation.translate('ERROR_SHIP_NOT_AVAILABLE');
          const msg2 = this.translation.translate('ERROR_EITHER_REMOVE');
          const msg3 = this.translation.translate('ERROR_CHANGE_ADDRESS');
          const msg4 = error.error.length === 1 ? this.translation.translate('LABEL_IT') : this.translation.translate('LABEL_THEM');
          const message1 = msg + '"' + productName + '"' + msg1;
          const message2 = msg2 + msg4 + msg3;
          this.showCouponAlert(title, [message1, message2]);
        }
      });
    } else {
      if (fromNext) {
        this.step = 2;
      }
    }
  }

  onChangePayment(data) {
    if (data.value) {
      this.paymentType = data.value;
      // this.getShippingAmount();
    }
  }

  stripeCheckOutProductInfo(token, stripeDetails) {
    const reqObj = {
     paymentToken: [{id: token.id}],
     stripeOrderId: stripeDetails.orderId
    };
    this.cartService.stripeCheckOut(reqObj).subscribe((response: any) => {
      if (response) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        this.homeService.setCartCountChange(true);
        this.router.navigate(['/order-summary', response.data.orderId]);
      }
    }, (errorResponse) => {
      this.isPlacingOrder = false;
      if (errorResponse.status === API_STATUS_CODE.badRequest || API_STATUS_CODE.conflict) {
        this.snackBar.open(this.translation.translate(errorResponse.error.error), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  razorPayOpenPopup(razorPayOrderId, orderId) {
    this.cartService.getRazorPayPublicKey().subscribe((res: any) => {
      const options = {
        'key': JSON.parse(res.data[0].publicKey),
        'order_id': razorPayOrderId,
        'payment_capture': '2',
        'handler': (response) => {
          if (response) {
            const razorPayIdDetail = {
              orderId: orderId,
              razorPayId: response.razorpay_order_id,
              razorpay_payment_id: response.razorpay_payment_id,
              razorpay_signature: response.razorpay_signature
            };
            this.razorPayCheckOutDetail(razorPayIdDetail);
          }
        },
        'modal': {
          'ondismiss': () => {
            this.cartService.releaseOrder(orderId).subscribe(
              (response: any) => {
            this.displayNgZone(response.data.message);
            },
              (error: any) => {
           this.displayNgZone(error.data.message);
              });
          }
      }
      };
      this.rzp1 = this.window.Razorpay(options);
      this.rzp1.open();
      event.preventDefault();
      this.isPlacingOrder = false;
    });
  }

  razorPayCheckOutDetail(razorPayDetail) {
    this.cartService.razorPayCheckOut(razorPayDetail).subscribe((response: any) => {
      if (response) {
        this.homeService.setCartCountChange(true);
        this.ngZone.run(() => this.router.navigate(['/order-summary', response.data.orderId]));
      }
    }, (errorResponse) => {
      this.isPlacingOrder = false;
      if (errorResponse.status === API_STATUS_CODE.badRequest || API_STATUS_CODE.conflict) {
      this.snackBar.open(this.translation.translate(errorResponse.error.error), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    }});
  }

  formatPriceValue(price) {
    if (this.prodDetails.formatCurrency === 4) {
      return Math.round(price);
    } else {
      return price;
    }
  }

  cashFreeCheckOutDetail(orderId) {
    this.cartService.cashFreeCheckOut({orderId: orderId}).subscribe((response: any) => {
      if (response) {
        this.homeService.setCartCountChange(true);
        this.ngZone.run(() => this.router.navigate(['/order-summary', response.data.orderId]));
      }
    }, (errorResponse) => {
      if (errorResponse.status === API_STATUS_CODE.badRequest || API_STATUS_CODE.conflict) {
      this.snackBar.open(this.translation.translate(errorResponse.error.error), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    }});
  }


  cashFreePopup(cashFreeDetails, checkoutDetails) {
    const config = {
    layout: {view: "popup", width: "650"},
    mode: cashFreeDetails.mode,
  }
    const response = this.window.CashFree.init(config);
    if (response.status == "OK") {
      this.window.cfInitialized = true;
      if (this.window.cfInitialized) {
        var data: any = {};
        const paymentTotal = checkoutDetails.walletStatus ? checkoutDetails.walletTotal : checkoutDetails.orderTotal;
        data.orderId = cashFreeDetails.orderNumber;
        data.orderAmount = cashFreeDetails.orderTotal;
        data.customerName = checkoutDetails.shippingAddress[0].firstName + checkoutDetails.shippingAddress[0].firstName;
        data.customerPhone = checkoutDetails.shippingAddress[0].userMobile;
        data.customerEmail = "";
        data.returnUrl = "";
        data.notifyUrl = "";
        data.appId = cashFreeDetails.appId;
        data.paymentToken = cashFreeDetails.cashFreeSignature;
        
        const callback = function (event) {
          const eventName = event.name;
          switch(eventName) {
            case "PAYMENT_REQUEST":
              // this.snackBar.open(event.message, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
            break;
            case "PAYMENT_RESPONSE":
              if(event.response.txStatus === 'FAILED' || event.response.txStatus === 'CANCELLED') {
                this.cashFreeReleaseOrder(cashFreeDetails.orderId);
              } else {
                this.cashFreeCheckOutDetail(cashFreeDetails.orderId);
              }
              break;
            default:
              //this.snackBar.open(event.message, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
          };
        }.bind(this)
        this.window.CashFree.makePayment(data, callback);
      }
    } else {
      this.snackBar.open(response.message, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    }
  }

  cashFreeReleaseOrder(orderId) {
    this.cartService.cashFreeOrderReturn(orderId).subscribe(
      (response: any) => {
        this.snackBar.open(this.translation.translate(response.data.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }, 
      (error: any) =>{
        this.snackBar.open(this.translation.translate(error.data.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      });
      this.isPlacingOrder = false;
  }

  // Get Cash back meter details
  getCashMeterDetails() {    
    this.cashBackObj = undefined;    
    this.cartService.getCashBackMeter().subscribe((response: any) => {
      if (response) {        
        this.cashBackObj = response.data;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }


}
