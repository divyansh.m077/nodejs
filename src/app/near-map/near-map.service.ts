import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { NEAR_MAP_URL } from './near-map.url-config';
@Injectable({
  providedIn: 'root'
})
export class NearMapService {
  storeList: any;
  locationDetails: any;
  constructor(private http: HttpClient) { }

  getStoreList(storeData) {
    return this.http.post(NEAR_MAP_URL.STORE_LIST, storeData);
  }

  setLocationDetails(locationData) {
    this.locationDetails = locationData;
  }

  getLocationDetails() {
    return this.locationDetails;
  }

  initializeStoreList(storeData) {
    this.storeList = storeData;
  }

  getStoreDetails() {
    return this.storeList;
  }

  getAllProductsList(productData) {
    return this.http.get(NEAR_MAP_URL.PRODUCT_LIST, { params: productData });
  }
}
