import { Pipe, PipeTransform } from '@angular/core';
import { SharedService } from './shared.service';
import { CURRENCY_FORMAT } from '../app-setting';
import { CurrencyPipe } from '@angular/common';
@Pipe({
  name: 'customCurrencyFormat'
})
export class CurrencyFormatPipe implements PipeTransform {
 currencySetting: any;
 numberFormat: any;
 currencyCode: any;
 constructor(private sharedService: SharedService, private currencyPipe: CurrencyPipe) {
    this.currencySetting = this.sharedService.getSiteSettingDetails();
 }

  transform(val: number) {

      // here we just remove the commas from value
        if (this.currencySetting.formatCurrency === 1) {
            /* Comma Separator without decimal (Ex:1,000) */
            // this.numberFormat = CURRENCY_FORMAT.COMMA_SEPERATOR;
            this.numberFormat = this.currencyPipe.transform(val, this.currencySetting.currencyCode,
                                'symbol-narrow', CURRENCY_FORMAT.COMMA_SEPERATOR);
          } else if (this.currencySetting.formatCurrency === 2) {
            /* Comma Separator with Round off (Ex: 1,000 = 999.90) */
            this.numberFormat = this.currencyPipe.transform(val, this.currencySetting.currencyCode,
                                'symbol-narrow', CURRENCY_FORMAT.ROUND_OFF);
          } else if (this.currencySetting.formatCurrency === 3) {
            /* Comma Separator with 2 Decimal Points (Ex:1,000.00) */
            this.numberFormat = this.currencyPipe.transform(val, this.currencySetting.currencyCode,
                                'symbol-narrow', CURRENCY_FORMAT.TWO_DECIMAL);
          } else if (this.currencySetting.formatCurrency === 4) {
            /* Round off (or) no decimal(Ex: 1000) */
            this.numberFormat = this.currencyPipe.transform(val, this.currencySetting.currencyCode,
                                'symbol-narrow', CURRENCY_FORMAT.ROUND_OFF);
            this.numberFormat = this.numberFormat.toString().replace(',', '');
          } else if (this.currencySetting.formatCurrency === 5) {
            /* Comma Separator with 3 Decimal Points (Ex:1,000.000) */
            this.numberFormat = this.currencyPipe.transform(val, this.currencySetting.currencyCode,
                                'symbol-narrow', CURRENCY_FORMAT.THREE_DECIMAL);
          } else if (this.currencySetting.formatCurrency === 6) {
            /* Comma Separator with 4 Decimal Points (Ex:1,000.0000) */
            this.numberFormat = this.currencyPipe.transform(val, this.currencySetting.currencyCode,
                                'symbol-narrow', CURRENCY_FORMAT.FOUR_DECIMAL);
          }
          return this.numberFormat;
  }
}
