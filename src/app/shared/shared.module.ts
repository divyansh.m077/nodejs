import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MatSelectModule, MatButtonModule, MatButtonToggleModule, MatRippleModule, MatIconModule,
        MatDialogModule, MatProgressBarModule, MatRadioModule, MatFormFieldModule,
         MatInputModule, MatCheckboxModule, MatExpansionModule, MatTableModule, MatListModule} from '@angular/material';
import { BarRatingModule } from 'ngx-bar-rating';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { TranslationModule } from 'angular-l10n';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

import { ProductTileComponent } from './product-tile/product-tile.component';
import { DealsTileComponent } from './deals-tile/deals-tile.component';
import { BrandTileComponent } from './brand-tile/brand-tile.component';
import { AlertPopupComponent } from './alert-popup/alert-popup.component';
import { RatingPopupComponent } from './rating-popup/rating-popup.component';
import { PasswordRuleComponent } from './password-rule/password-rule.component';
import { CommonPaginationComponent } from './common-pagination/common-pagination.component';
// import { CustomDatePipe } from './custom-date.pipe';
// import { CurrencyFormatPipe } from './custom-currency.pipe';
import { SharedPipesModule } from './shared-pipes.module';
import { ConfirmationPopupComponent } from './confirmation-popup/confirmation-popup.component';
import { NotAllowedComponent } from './not-allowed/not-allowed.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SharedVendorModule } from './shared-sv.module';
import { UploadImageComponent } from './user-profile/upload-image/upload-image.component';
import { ImageCropComponent } from './image-crop/image-crop.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { BeforeInstallPromptComponent } from '../before-install-prompt/before-install-prompt.component';
import { CurrencyFormatPipe } from './custom-currency.pipe';
//import { PaymentComponent } from '../cart/payment/payment.component';
import { MatSelectSearchComponent } from './mat-select-search/mat-select-search.component';
import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';
import { DescriptionPopupComponent } from './description-popup/description-popup.component';

import { MaxNumDirective, MinNumDirective, DynamicWidthDirective } from './number-validator.directive';
import { OtpComponent } from './otp/otp.component';
import { CreateAddressComponent } from './create-address/create-address.component';
import { MembershipComponent } from './membership/membership.component';
import { MembershipPolicyComponent } from './membership-policy/membership-policy.component';
import { CashBackMeterComponent } from './cash-back-meter/cash-back-meter.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatRippleModule,
    AngularFontAwesomeModule,
    BarRatingModule,
    MatDialogModule,
    TranslationModule,
    ScrollToModule.forRoot(),
    ImageCropperModule,
    MatProgressBarModule,
    AngularSvgIconModule,
    SharedPipesModule,
    NgxMatSelectSearchModule,
    ReactiveFormsModule,
    FormsModule,
    LazyLoadImageModule.forRoot({
      preset: intersectionObserverPreset
    }),
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatTableModule,
    MatListModule,
    RouterModule,
  ],
  declarations: [
    BeforeInstallPromptComponent,
    ProductTileComponent,
    DealsTileComponent,
    BrandTileComponent,
    AlertPopupComponent,
    RatingPopupComponent,
    PasswordRuleComponent,
    CommonPaginationComponent,
    // CustomDatePipe,
    // CurrencyFormatPipe,
    ConfirmationPopupComponent,
    NotAllowedComponent,
    UserProfileComponent,
    UploadImageComponent,
    ImageCropComponent,
    MatSelectSearchComponent,
    DescriptionPopupComponent,
    MaxNumDirective,
    MinNumDirective,
    DynamicWidthDirective,
    OtpComponent,
    CreateAddressComponent,
    MembershipComponent,
    MembershipPolicyComponent,
    CashBackMeterComponent,
    PageNotFoundComponent
  ],
  exports: [
    BeforeInstallPromptComponent,
    ProductTileComponent,
    DealsTileComponent,
    BrandTileComponent,
    RatingPopupComponent,
    PasswordRuleComponent,
    CommonPaginationComponent,
    SharedPipesModule,
    UserProfileComponent,
    SharedVendorModule,
    AngularSvgIconModule,
    MatSelectSearchComponent,
    MaxNumDirective,
    MinNumDirective,
    DynamicWidthDirective,
    CashBackMeterComponent
  ],
  providers: [CurrencyPipe, CurrencyFormatPipe],
  entryComponents: [
    AlertPopupComponent,
    ConfirmationPopupComponent,
    UploadImageComponent,
    BeforeInstallPromptComponent,
    DescriptionPopupComponent,
    OtpComponent,
    CreateAddressComponent,
    MembershipComponent,
    MembershipPolicyComponent
    //PaymentComponent
  ]
})
export class SharedModule { }
