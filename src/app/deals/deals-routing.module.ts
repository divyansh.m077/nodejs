import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { DealsComponent } from './deals.component';

const dealsRoutes: Routes = [
  {
    path: '',
    component: DealsComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      dealsRoutes
    )
  ],
  declarations: [],
  exports: [RouterModule],
})
export class DealsRoutingModule { }
