import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatePipe } from '@angular/common';

import { MatSelectModule, MatButtonModule, MatButtonToggleModule, MatIconModule } from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslationModule } from 'angular-l10n';

import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home.component';
import { ProductGroupsComponent } from './product-groups/product-groups.component';
import { BannerComponent } from './banner/banner.component';
import { HomeService } from './home.service';
import { AdsComponent } from './ads/ads.component';
import { ComonModule } from '../common/common.module';

@NgModule({
  imports: [
    CommonModule,
    MatSelectModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatIconModule,
    HomeRoutingModule,
    SharedModule,
    NgbModule,
    TranslationModule,
    ComonModule
  ],
  declarations: [
    HomeComponent,
    ProductGroupsComponent,
    BannerComponent,
    AdsComponent
  ],
  providers: [DatePipe]
})
export class HomeModule { }
