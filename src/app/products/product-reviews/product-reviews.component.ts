import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';

import { Language, TranslationService } from 'angular-l10n';
import { concat } from 'lodash';

import { ProductService } from '../products.service';
import { REVIEW_SETTINGS } from '../../app-setting';
import { API_STATUS_CODE } from '../../app.constants';
import { ReplyPopupComponent } from '../product-detail/reply-popup/reply-popup.component';

import { SNACKBAR_ALERT } from '../../app-setting';

@Component({
  selector: 'app-product-reviews',
  templateUrl: './product-reviews.component.html',
  styleUrls: ['./product-reviews.component.scss']
})
export class ProductReviewsComponent implements OnInit {
  vendorId: any;
  @Language() lang: string;
  public from: string;
  public productId: any;
  public reviewList: any = [];
  public ratingList: any;
  public reviewCount: any = REVIEW_SETTINGS;
  public reviewLength: number;
  pluralMapping = {
    'replyCount': {
      '=1': 'LABEL_REPLY',
      'other': 'LABEL_REPLIES'
    }
  };
  constructor(private productService: ProductService,
              private router: Router, private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private translation: TranslationService,
              private matDialog: MatDialog) {
    this.reviewLength = 0;
    this.from = 'detail';
    this.route.params.subscribe(params => {
      this.productId = params.id;
    });
    this.route.queryParams.subscribe(params => {
      this.vendorId = params.vid;
    });

  }

  ngOnInit() {
    this.getProductReviewLists();
  }

  getProductReviewLists() {
    this.productService.getProductReviews({'productId': this.productId,
        'startLimit': this.reviewCount.START_COUNT, 'endLimit': this.reviewCount.END_COUNT}).subscribe((response: any) => {
          if (response) {
            this.reviewList = concat(this.reviewList, response.message.reviews);
            this.reviewLength = response.message.productlength;
            this.ratingList = response.message.reveiwandRating[0];
            this.ratingCalculationForPopup();
          }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  ratingCalculationForPopup() {
    this.ratingList.fiveStarStatus = '0%';
    this.ratingList.fourStarStatus = '0%';
    this.ratingList.threeStarStatus = '0%';
    this.ratingList.twoStarStatus = '0%';
    this.ratingList.oneStarStatus = '0%';
    this.ratingList.fiveStarStatus = (this.ratingList.fivestarRatingCount
        / this.ratingList.ratingProductCount) * 100 === Infinity ? '0%' :
        ((this.ratingList.fivestarRatingCount
        / this.ratingList.ratingProductCount) * 100).toString() + '%';
    this.ratingList.fourStarStatus = (this.ratingList.fourStarRatingCount
      / this.ratingList.ratingProductCount) * 100 === Infinity ? '0%' :
      ((this.ratingList.fourStarRatingCount
      / this.ratingList.ratingProductCount) * 100).toString() + '%';
    this.ratingList.threeStarStatus = (this.ratingList.threeStarRatingCount
      / this.ratingList.ratingProductCount) * 100 === Infinity ? '0%' :
      ((this.ratingList.threeStarRatingCount
      / this.ratingList.ratingProductCount) * 100).toString() + '%';
    this.ratingList.twoStarStatus = (this.ratingList.twoStarRatingCount
      / this.ratingList.ratingProductCount) * 100 === Infinity ? '0%' :
      ((this.ratingList.twoStarRatingCount
      / this.ratingList.ratingProductCount) * 100).toString() + '%';
    this.ratingList.oneStarStatus = (this.ratingList.oneStarRatingCount
      / this.ratingList.ratingProductCount) * 100 === Infinity ? '0%' :
      ((this.ratingList.oneStarRatingCount
      / this.ratingList.ratingProductCount) * 100).toString() + '%';
  }

  showMoreReviews() {
    this.reviewCount.START_COUNT += this.reviewCount.SHOW_MORE_COUNT;
    this.getProductReviewLists();
  }

  goToProductDetails() {
    if (this.vendorId) {
      this.router.navigate(['/product-detail', this.productId], {queryParams : {'id' : this.vendorId}});
    } else {
      this.router.navigate(['/product-detail', this.productId]);
    }
  }

  openreplyPopup(reviewId) {
    const dialogRef = this.matDialog.open(ReplyPopupComponent, {
      panelClass: 'view-business-wrap',
      data: reviewId,
      disableClose: true
    });
  }

}
