import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, Input, Inject } from '@angular/core';

import { SharedService } from '../../shared/shared.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

  @Input() bannersList;
  constructor(@Inject(WINDOW) private window: any, private sharedService: SharedService,
    private router: Router) { }

  ngOnInit() {
  }

  redirectToUrl(data) {
    const toUrl = new URL(data.linkedToURL).hostname;
    const routerUrl = new URL(window.location.href).hostname;
    const add_url = toUrl.split('.');
    const liveUrl = routerUrl.split('.');
    if (add_url[add_url.length - 2] === liveUrl[liveUrl.length - 2]) {
      const pathname = new URL(data.linkedToURL).pathname;
      const search = new URL(data.linkedToURL).search;
      this.router.navigateByUrl(pathname + search);
    } else {
      this.window.open(data.linkedToURL, '_blank');
    }
  }
}
