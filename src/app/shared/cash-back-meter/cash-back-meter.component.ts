import { Component, OnInit, Input } from '@angular/core';
import { Language } from 'angular-l10n';
import { CASHBACK_PLAN } from '../../app.constants';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-cash-back-meter',
  templateUrl: './cash-back-meter.component.html',
  styleUrls: ['./cash-back-meter.component.scss']
})
export class CashBackMeterComponent implements OnInit {
  @Input() cashback;
  @Language() lang: string;
  cashbackPlan = CASHBACK_PLAN;
  get settings() { return this.sharedService.getSiteSettingDetails(); }

  constructor(private sharedService: SharedService) { }

  ngOnInit() {
  }

  // Generate Initial cashback
  getInitialCashBack(targetValue, maxValue){
    if (!targetValue || !maxValue) {
      return 0;
    }

    const totalMaxValue: any = maxValue;
    const detectedValue: any = maxValue - targetValue;
    const calculatedPercentage: any = parseFloat((((totalMaxValue - detectedValue) / totalMaxValue) * 100).toFixed(3)).toString();   
    return calculatedPercentage;
  }

  // Generate value for Cashback meter
  getCashBack(cashback) {
    const targetValue = cashback.totalOrderAmount;
    const maxValue = cashback.maxCBOrderAmount
    if(cashback.minOrderAmount > cashback.totalOrderAmount){
      return 0;
    }    
    if (!targetValue || !maxValue) {
      return 0;
    }
    const totalMaxValue: any = maxValue - cashback.minOrderAmount;
    const detectedValue: any = maxValue - targetValue;
    const calculatedPercentage: any = parseFloat((((totalMaxValue - detectedValue) / totalMaxValue) * 100).toFixed(3)).toString();   
    return calculatedPercentage;
  }

  // Calculate cashback 
  calculateCashback(cashback){
    if(cashback.minOrderAmount > cashback.totalOrderAmount){
      return 0;
    }
    const totalOrderAmount = (cashback.totalOrderAmount >= cashback.maxCBOrderAmount) ? cashback.maxCBOrderAmount : cashback.totalOrderAmount;
    const planOffer = cashback.subscriptionPlanOffer;
    return ((totalOrderAmount * (planOffer / 100)).toFixed(2));
  }

}
