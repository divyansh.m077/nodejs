import { Component, OnInit } from '@angular/core';

import { Language } from 'angular-l10n';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  @Language() lang: string;
  constructor() { }

  ngOnInit() {
  }

}
