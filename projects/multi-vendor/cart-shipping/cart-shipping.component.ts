import { Component, OnInit, Input } from '@angular/core';
import { Language } from 'angular-l10n';

@Component({
  selector: 'app-cart-shipping',
  templateUrl: './cart-shipping.component.html',
  styleUrls: ['./cart-shipping.component.scss']
})
export class CartShippingComponent implements OnInit {
  @Language() lang: string;
  shippingValue: any;
  isFreeShipping: boolean;
  shippingAmount: any;
  shippmentAmountApplied: boolean;
  @Input() set shipping(val) {
    this.shippingValue = val;
  }
  @Input() set freeShipping(val) {
    this.isFreeShipping = val;
  }
  @Input() set shippmentApplied(val) {
    this.shippmentAmountApplied = val;
  }
  @Input() set couponShipping(val) {
    this.shippingAmount = val;
  }
  constructor() { }

  ngOnInit() {

  }

}
