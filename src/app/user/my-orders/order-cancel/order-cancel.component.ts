import { Component, OnInit, Inject } from '@angular/core';
import { Language, TranslationService } from 'angular-l10n';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { UserService } from '../../../user/user.service';
import { Router } from '@angular/router';
import { API_STATUS_CODE } from '../../../app.constants';
import { SNACKBAR_ALERT, ORDER_CANCEL_RULE } from '../../../app-setting';

@Component({
  selector: 'app-order-cancel',
  templateUrl: './order-cancel.component.html',
  styleUrls: ['./order-cancel.component.scss']
})

export class OrderCancelComponent implements OnInit {

  @Language() lang: string;
  orderCancelForm: FormGroup;
  reasonList: any;
  isRequired: boolean;
  descMaxLength = ORDER_CANCEL_RULE.MAX_LENGTH;

  constructor( @Inject(MAT_DIALOG_DATA) public data: any,
  private fb: FormBuilder,
  private matDialogRef: MatDialogRef<OrderCancelComponent>,
  private userService: UserService,
  private snackBar: MatSnackBar,
  private translation: TranslationService,
  private router: Router ) { }

  ngOnInit() {
    this.isRequired = false;
    this.formInitialization();
    this.getCancelReasonList();
  }

  formInitialization() {
    this.orderCancelForm = this.fb.group({
      'reason': ['', [Validators.required]],
      'description': ['', Validators.maxLength(ORDER_CANCEL_RULE.MAX_LENGTH)]
    });
  }

  catchValue(Id: number) {
    if (Id === 15) {
       this.isRequired = true;
    } else {
      this.isRequired = false;
    }
 }

  getCancelReasonList() {
    this.userService.getCancelReasonList().subscribe((response: any) => {
      if (response) {
        this.reasonList = response.data;
                    }
      }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });

  }

  onSubmit() {
    const orderCancelData = {
      orderCancelTypeId: this.orderCancelForm.value.reason,
      otherReason : this.orderCancelForm.value.description,
      statusId: 5,
      orderNo: this.data.orderNo
    };
    this.userService.confirmOrderCancel(orderCancelData).subscribe((response: any) => {
      if (response) {
        this.matDialogRef.close(response);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
      if (error.status === API_STATUS_CODE.conflict) {
        this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        this.matDialogRef.close();
      }
    });
  }

  }

