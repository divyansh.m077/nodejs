/**
 * 
 * @param {number} time 
 * 
 * This is used in deals and product page to calculate the H, M ans S from the time in millisecond.
 */
function getHourMinuteSecFromTime(event) {
    const distance = event.data;
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);
    const timer = hours.toString().padStart(2, '0') + ':' + minutes.toString().padStart(2, '0') + ':' + seconds.toString().padStart(2, '0');
    postMessage(timer);
}

self.onmessage = getHourMinuteSecFromTime;