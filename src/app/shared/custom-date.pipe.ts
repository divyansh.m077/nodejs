import { PipeTransform, Pipe } from '@angular/core';

import * as moment from 'moment';
import { trim } from 'lodash';

import { SharedService } from './shared.service';
@Pipe({ name: 'customDate' })
export class CustomDatePipe implements PipeTransform {
    patternDetails: any;
    constructor(private sharedService: SharedService) {
        this.patternDetails = this.sharedService.getSiteSettingDetails();
    }

    /** Custom date filter */
    transform(date, time?) {
        let format = trim(this.patternDetails.dateFormat);
        if (date === '0000-00-00 00:00:00' || date === null) {
           return '';
        } else {
            if (time === 'fullTime') {
                format = format + ' hh:mm:ss';
                return moment(date).format(format);
            } else {
                return moment(date).format(format);
            }
        }
    }
}
