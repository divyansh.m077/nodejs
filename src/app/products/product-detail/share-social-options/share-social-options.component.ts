import { Component, Input, HostListener, ElementRef, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'share-social-buttons',
    templateUrl: './share-social-options.component.html'
})
export class ShareSocialOptionsComponent {
    @Input() theme;
    @Input() include;
    @Input() show;
    @Input() size;
    @Input() title;
    @Input() description;
    @Input() image;
    @Input() trigger;
    @Output() onClickOut = new EventEmitter<any>();
    @Output('opened') onOpened = new EventEmitter<any>();
    @Output('closed') onClosed = new EventEmitter<any>();

    @HostListener('document:click', ['$event'])
    clickout(event) {
        if (!this.trigger.contains(event.target) && !this.eRef.nativeElement.contains(event.target)) {
            this.onClickOut.emit();
        }
    }

    constructor(private eRef: ElementRef) {
        
    }

    onBtnOpened(event: string) {
        this.onOpened.emit(event);
    }

    onBtnClosed(event: string) {
        this.onClosed.emit(event);
    }
}