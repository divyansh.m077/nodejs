
import { Directive, Input, ElementRef, Renderer2, OnChanges, SimpleChanges } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

@Directive({
    selector: '[maxNum][formControlName],[maxNum][formControl],[maxNum][ngModel]',
    providers: [{ provide: NG_VALIDATORS, useExisting: MaxNumDirective, multi: true }]
})
export class MaxNumDirective implements Validator {
    @Input()
    maxNum: number;

    validate(c: FormControl): { [key: string]: any } {
        const value = c.value;
        if (value === '' || value === null || value === undefined) {
            return;
        }
        if (value > this.maxNum) {
            c.patchValue(this.maxNum);
            return { 'maxNum': true };
        }
        return null;
    }
}

@Directive({
    selector: '[minNum][formControlName],[minNum][formControl],[minNum][ngModel]',
    providers: [{ provide: NG_VALIDATORS, useExisting: MinNumDirective, multi: true }]
})
export class MinNumDirective implements Validator {
    @Input()
    minNum: number;

    validate(c: FormControl): { [key: string]: any } {
        const value = c.value;
        if (value === '' || value === null || value === undefined) {
            return;
        }
        if (value < this.minNum) {
            c.patchValue(this.minNum);
            return { 'minNum': true };
        }
        return null;
    }
}
@Directive({
    selector: '[dynamicWidth][formControlName],[dynamicWidth][formControl],[dynamicWidth][ngModel]',
})
export class DynamicWidthDirective implements OnChanges {
    rangeValue: any = '4';
    @Input()
    set dynamicWidth(data: any) {
        if (data) {
            this.rangeValue = data.toString();
        }
    }
    constructor(private elem: ElementRef, private renderer: Renderer2) {
        this.setWidth();
    }

    setWidth() {
        const range = (this.rangeValue.length + 1) * 15;
        this.renderer.setStyle(this.elem.nativeElement, 'minWidth', range + 'px');
    }

    ngOnChanges(changes: SimpleChanges) {

        if (changes) {
            this.setWidth();
        }
    }
}
