import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatInputModule, MatButtonModule, MatCheckboxModule,
  MatExpansionModule, MatTableModule, MatFormFieldModule,
   MatRippleModule, MatSelectModule, MatListModule, MatDialogModule, MatRadioModule } from '@angular/material';
import { TranslationModule } from 'angular-l10n';

import { ListVendorsComponent } from './list-vendors/list-vendors.component';
import { VendorProductNameComponent } from './product-details/vendor-name.components';
import { VendorOrderSummeryComponent } from './order-summery/order-summery.component';

import { SharedPipesModule } from '../../src/app/shared/shared-pipes.module';
import { VendorNameComponent } from './vendor-name/vendor-name.component';
import { CouponDiscountComponent } from './coupon-discount/coupon-discount.component';
import { CartShippingComponent } from './cart-shipping/cart-shipping.component';

@NgModule({
  declarations: [ListVendorsComponent, VendorProductNameComponent,
    VendorOrderSummeryComponent,
    VendorNameComponent,
    CouponDiscountComponent,
    CartShippingComponent],
  imports: [
    CommonModule,
    MatExpansionModule,
    MatButtonModule,
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatTableModule,
    MatFormFieldModule,
    MatRippleModule,
    MatSelectModule,
    MatListModule,
    MatDialogModule,
    MatRadioModule,
    TranslationModule,
    SharedPipesModule
  ],
  exports: [ListVendorsComponent, VendorProductNameComponent,
    VendorOrderSummeryComponent,
    VendorNameComponent,
    CouponDiscountComponent,
    CartShippingComponent]
})
export class VendorModule { }
