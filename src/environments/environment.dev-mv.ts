export const environment = {
  production: true,
  serverUrl: 'https://dev.purchasecommerce.com/pcapi/',
  assetsUrl: `https://dev.purchasecommerce.com/`,
  becomeaVendorURL: '',
  SSRPort: 8080,
  cookieConfig: {
    path: '/',
    domain: 'dev.purchasecommerce.com',
    expires: '01.01.2021',
    secure: true,
    httpOnly: false
  }
};
