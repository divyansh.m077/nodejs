import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { AppComponent } from './app.component';
import { NotAllowedComponent } from './shared/not-allowed/not-allowed.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';

const appRoutes: Routes = [
  { path: '',   redirectTo: '/', pathMatch: 'full' },
  { path: '', loadChildren: './products/products.module#ProductsModule' },
  { path: '', loadChildren: './cart/cart.module#CartModule' },
  { path: '', loadChildren: './user/user.module#UserModule' },
  { path: '', loadChildren: './contact-us/contact-us.module#ContactUsModule' },
  { path: 'deals',
    children: [{
      path: '',
      loadChildren: './deals/deals.module#DealsModule'
    }]
  },
  { path: 'near-map',
    children: [{
      path: '',
      loadChildren: './near-map/near-map.module#NearMapModule'
    },
    {
      path: 'store-detail',
      loadChildren: './near-map/near-map.module#NearMapModule'
    }]
  },
  { path: 'blog',
    children: [{
      path: '',
      loadChildren: './blog/blog.module#BlogModule'
    }]
  },
  { path: 'sold-out',
    children: [{
      path: '',
      loadChildren: './sold-out/sold-out.module#SoldOutModule'
    }]
  },
  { path: 'brands',
    children: [{
      path: '',
      loadChildren: './brands/brands.module#BrandsModule'
    }]
  },
  { path: 'unauthorized', component: NotAllowedComponent},
  {
    path: '404', component: PageNotFoundComponent,
    data: {
      title: 'PurchaseCommerce | Page Not Found'
    }
  },
  {
    path: '**', redirectTo: '404'
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes, {
        scrollPositionRestoration: 'enabled',
        initialNavigation: 'enabled',
        preloadingStrategy: PreloadAllModules
      }
    )
  ],
  declarations: [],
  exports: [RouterModule],
})
export class AppRoutingModule { }
