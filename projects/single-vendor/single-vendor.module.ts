import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslationModule } from 'angular-l10n';

import { MatInputModule, MatButtonModule, MatCheckboxModule,
  MatExpansionModule, MatTableModule, MatFormFieldModule,
   MatRippleModule, MatSelectModule, MatListModule, MatDialogModule, MatRadioModule } from '@angular/material';

import { ListVendorsComponent } from './list-vendors/list-vendors.component';
import { VendorProductNameComponent } from './product-details/vendor-name.components';
import { VendorOrderSummeryComponent } from './order-summery/order-summery.component';
// import { CustomDatePipe } from '../../src/app/shared/custom-date.pipe';
import { SharedPipesModule } from '../../src/app/shared/shared-pipes.module';
import { VendorNameComponent } from './vendor-name/vendor-name.component';
import { CouponDiscountComponent } from './coupon-discount/coupon-discount.component';
import { CartShippingComponent } from './cart-shipping/cart-shipping.component';
@NgModule({
  declarations: [ListVendorsComponent,
                 VendorProductNameComponent,
                 VendorOrderSummeryComponent,
                //  CustomDatePipe,
                //  CurrencyFormatPipe
                VendorNameComponent,
                CouponDiscountComponent,
                CartShippingComponent
                ],
  imports: [
    CommonModule,
    TranslationModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatTableModule,
    MatFormFieldModule,
    MatRippleModule,
    MatSelectModule,
    MatListModule,
    MatDialogModule,
    MatRadioModule,
    SharedPipesModule
  ],
  // providers: [CustomDatePipe, CurrencyFormatPipe],
  exports: [ListVendorsComponent,
            VendorProductNameComponent,
            VendorOrderSummeryComponent,
            VendorNameComponent,
            CouponDiscountComponent,
            CartShippingComponent
            // CustomDatePipe,
            // CurrencyFormatPipe
          ]
})
export class VendorModule { }
