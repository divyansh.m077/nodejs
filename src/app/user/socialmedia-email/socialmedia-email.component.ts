import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Language, TranslationService } from 'angular-l10n';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { REGISTRATION_RULE, SNACKBAR_ALERT} from './../../app-setting';
import { UserService } from '../user.service';
import { HomeService } from './../../home/home.service';
import { API_STATUS_CODE, ALERT_SYSTEM_TITLE } from '../../app.constants';
import { MatSnackBar, MatDialog } from '@angular/material';
import { AlertPopupComponent } from '../../shared/alert-popup/alert-popup.component';
import { StorageControllerService } from '../../storage-controller.service';

@Component({
  selector: 'app-socialmedia-email',
  templateUrl: './socialmedia-email.component.html',
  styleUrls: ['./socialmedia-email.component.scss']
})
export class SocialmediaEmailComponent implements OnInit {

  @Language() lang: string;
  emailForm: FormGroup;
  userDetails;
  actionSubscribe$: any;

  constructor( private storage: StorageControllerService,
               private router: Router,
               private fb: FormBuilder,
               private homeService: HomeService,
               private userService: UserService,
               private snackBar: MatSnackBar,
               private translation: TranslationService,
               public dialog: MatDialog,
              ) {
                 this.userDetails = this.userService.getUserDetails();
                }

  ngOnInit() {
    if (this.userDetails) {
      this.formInitialization();
    } else {
      this.router.navigate(['/signin']);
    }
  }

  formInitialization() {
    this.emailForm = this.fb.group({
      'userEmail': ['', [Validators.required, Validators.pattern(REGISTRATION_RULE.EMAIL_PATTERN)]]
    });
  }

  onSubmit() {
    this.userDetails.userEmail = this.emailForm.value.userEmail;
    this.userService.saveSocialUserRegisterationDetails(this.userDetails)
          .subscribe((response: any) => {
             if (response.data) {
              const userDetails = response.data;
              const name = userDetails.firstName + ' ' + userDetails.lastName;
              this.storage.set('token', userDetails.token);
              this.storage.set('userName', name);
              this.storage.set('userDetails', JSON.stringify(userDetails));
              this.storage.set('id', userDetails.userId);
              this.storage.set('token', userDetails.token);
              this.storage.set('id', userDetails.userId);
              let previousUrl: any;
              let preUrl = this.storage.get('previousUrl');
              if (preUrl.includes('verify-email')) { preUrl = null; }
              if (preUrl) {
                const splitUrl = preUrl.split('?');
                if (preUrl === '/register' || preUrl === '/forgot-password' || (splitUrl && splitUrl[0] === '/reset-password') ||
                (preUrl === '/socialmedia-email')) {
                  previousUrl = '/';
                } else {
                  previousUrl = preUrl;
                }
              }
              this.homeService.setSignInStatus(true);
              this.homeService.setCartCountChange(true);
              this.router.navigate([previousUrl ? previousUrl : '/']);
            } else {
              const dialogRef = this.dialog.open(AlertPopupComponent, {
                data: {
                  title: this.translation.translate('labelSuccess'),
                  message: [this.translation.translate('labelRegisterSuccess'), this.translation.translate('verifyMsg')],
                  action: ALERT_SYSTEM_TITLE.success,
                  buttonName: this.translation.translate('buttonOk')
                },
                disableClose: true
              });
              this.actionSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
                if (data) {
                  this.dialog.closeAll();
                  this.router.navigate(['/signin']);
                }
              });
            }
          }, (error: any) => {
            if (error.status === API_STATUS_CODE.badRequest) {
              this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
            } else if (error.status === API_STATUS_CODE.conflict) {
              this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
            } else if (error.status === API_STATUS_CODE.preconditionFailed) {
                this.router.navigate(['/verify-email'], { queryParams: { userEmail: this.userDetails.userEmail } });
            }
          });
  }

}
