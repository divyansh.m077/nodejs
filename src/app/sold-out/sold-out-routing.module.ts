import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SoldOutComponent } from './sold-out.component';

const soldOutRoutes: Routes = [
  {
    path: '',
    component: SoldOutComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      soldOutRoutes
    )
  ],
  declarations: [],
  exports: [RouterModule],
})
export class SoldOutRoutingModule { }
