export const environment = {
  production: true,
  serverUrl: 'http://dev.purchasecommerce.com:6443/',
  assetsUrl: `http://dev.purchasecommerce.com/`,
  becomeaVendorURL: '',
  SSRPort: 8080,
  cookieConfig: {
    path: '/',
    domain: 'dev.purchasecommerce.com',
    expires: '01.01.2021',
    secure: true,
    httpOnly: false
  }
};
