import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TitleCasePipe } from '@angular/common';

import { Language, TranslationService } from 'angular-l10n';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { find } from 'lodash';
import { MANAGE_ADDRESS_RULE, MANAGE_ADD_RULE } from '../../app-setting';
import { API_STATUS_CODE, COUNTRY_CODE } from '../../app.constants';
import { SharedService } from '../shared.service';
import { UserService } from '../../user/user.service';
import { SNACKBAR_ALERT } from '../../app-setting';
import { StorageControllerService } from '../../storage-controller.service';

@Component({
  selector: 'app-create-address',
  templateUrl: './create-address.component.html',
  styleUrls: ['./create-address.component.scss']
})
export class CreateAddressComponent implements OnInit {

  @Language() lang: string;
  countryList: any[] = [];
  stateList: any[] = [];
  cityList: any[] = [];
  addressTypeList: any[] = [];
  regionList: any[] = [];
  districtList: any[] = [];
  addressForm: FormGroup;
  manageRule: any = MANAGE_ADD_RULE();
  patternDetails: any;
  countryCode = COUNTRY_CODE;
  constructor(
    private userService: UserService,
    private storage: StorageControllerService,
    private formBuilder: FormBuilder,
    private titleCase: TitleCasePipe,
    private translation: TranslationService,
    private sharedService: SharedService,
    private snackBar: MatSnackBar,
    private matDialogRef: MatDialogRef<CreateAddressComponent>,
  ) {
    this.formInitialization();
  }

  formInitialization(data?) {
    this.addressForm = this.formBuilder.group({
      firstName: [data ? data.firstName : '', [Validators.required, Validators.pattern(this.manageRule.FIRST_NAME),
      Validators.minLength(this.manageRule.FIRST_NAME_MIN_LENGTH),
      Validators.maxLength(this.manageRule.FIRST_NAME_MAX_LENGTH)]],
      lastName: [data ? data.lastName : '', [Validators.required, Validators.pattern(this.manageRule.LAST_NAME),
      Validators.minLength(this.manageRule.LAST_NAME_MIN_LENGTH),
      Validators.maxLength(this.manageRule.LAST_NAME_MAX_LENGTH)]],
      Mobile: [data ? data.userMobile : '', [Validators.required]],
      alternateMobile: [''],
      Address1: ['', [Validators.required, Validators.minLength(this.manageRule.ADDRESS_MIN_LENGTH),
      Validators.maxLength(this.manageRule.ADDRESS_MAX_LENGTH)]],
      Address2: ['', [Validators.minLength(this.manageRule.ADDRESS_MIN_LENGTH),
      Validators.maxLength(this.manageRule.ADDRESS_MAX_LENGTH)]],
      Country: ['', [Validators.required]],
      State: ['', [Validators.required]],
      divisionId: [null, [Validators.required]],
      districtId: [null, [Validators.required]],
      City: ['', [Validators.required]],
      Pincode: ['', [Validators.required, Validators.pattern(this.manageRule.PINCODE_PATTERN),
      Validators.minLength(this.manageRule.PINCODE_MIN_LENGTH), Validators.maxLength(this.manageRule.PINCODE_MAX_LENGTH)]],
      AddressType: ['', [Validators.required]]
    });
    this.getSiteSettingDetailForPatterns();
  }

  getSiteSettingDetailForPatterns() {
    this.patternDetails = this.sharedService.getSiteSettingDetails();
    if (this.patternDetails) {
      if (this.patternDetails.phoneNumberFormat) {
        const phoneRegx = new RegExp(this.patternDetails.phoneNumberFormat);
        if (this.addressForm.controls['Mobile'].value) {
          this.addressForm.controls['Mobile'].markAsTouched();
        }
        if (this.addressForm.controls['alternateMobile'].value) {
          this.addressForm.controls['alternateMobile'].markAsTouched();
        }
        this.addressForm.controls['Mobile'].setValidators([Validators.pattern(phoneRegx)]);
        this.addressForm.controls['Mobile'].updateValueAndValidity();
        this.addressForm.controls['alternateMobile'].setValidators([Validators.pattern(phoneRegx)]);
        this.addressForm.controls['alternateMobile'].updateValueAndValidity();
      }
    }
  }

  ngOnInit() {
    this.getCountryList();
    this.getAddressTypeList();
    if (!!this.storage.get('id')) {
      this.getProfileInfo();
    }
  }

  getProfileInfo() {
    const userInfo = JSON.parse(this.storage.get('userDetails'));
    this.formInitialization(userInfo);
  }

  getAddressTypeList() {
    this.userService.getAddressType().subscribe((response: any) => {
      if (response) {
        this.addressTypeList = response.data;
      }
    }, (errorResponse: any) => {
      this.addressTypeList = [];
      if (errorResponse.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(errorResponse.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  pinCodePatternCheck(event) {
    const pincodePattern = MANAGE_ADDRESS_RULE.PINCODE_KEYPRESS_PATTERN;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pincodePattern.test(inputChar)) {
      // event.preventDefault();
    }
  }

  phonePatternCheck(event) {
    if (this.patternDetails) {
      const pincodePattern = new RegExp(this.patternDetails.phoneNumberFormat);
      const inputChar = String.fromCharCode(event.charCode);
      if (!pincodePattern.test(inputChar)) {
        // event.preventDefault();
      }
    }
  }

  saveAddressDetails() {
    this.userService.addManageAddress(this.formToJson()).subscribe((response: any) => {
      if (response) {
        this.matDialogRef.close(response);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  formToJson() {
    const formValues = this.addressForm.value;
    const addressFormValue = {
      'firstName': this.titleCase.transform(formValues.firstName),
      'lastName': formValues.lastName,
      'phone': formValues.Mobile,
      'alternateMobile': formValues.alternateMobile,
      // 'companyName': this.addressForm.value.Company,
      'zipCode': formValues.Pincode,
      'address1': formValues.Address1,
      'address2': formValues.Address2,
      'addressType': formValues.AddressType,
      'country': formValues.Country,
      'state': formValues.State,
      'regionId': formValues.divisionId,
      'districtId': formValues.districtId,
      'townVillageId': formValues.City,
      'userId': this.storage.get('id'),
    };
    return addressFormValue;
  }

  getCountryList() {
    this.userService.getCountryList().subscribe((response: any) => {
      if (response) {
        this.countryList = response.data;
        this.setZipCodePattern();
        let defalutCountry = find(this.countryList, { countryName: 'India' });
        this.addressForm.patchValue({
          Country: defalutCountry ? defalutCountry.countryId : undefined
        });
        this.onChangeCountry(false);
      }
    }, (errorResponse: any) => {
      this.countryList = [];
    });
  }

  // On Change Country to get States
  onChangeCountry(isReset?: boolean) {
    if (this.addressForm.value.Country) {
      this.setZipCodePattern();
      this.getStateList(this.addressForm.value.Country);
      if (isReset)
        this.resetLocationList('Country');
    }
  }

  // Get State list
  getStateList(countryId) {
    this.userService.getStateList(countryId).subscribe((response: any) => {
      if (response) {
        this.stateList = response.data;
      }
    }, (errorResponse: any) => {
      this.stateList = [];
      this.resetLocationList('State');
    });
  }

  // On Change State 
  onChangeState(isReset?: boolean) {
    if (this.addressForm.value.State) {
      this.getRegionList(this.addressForm.value.State);
      if (isReset)
        this.resetLocationList('State');
    }
  }

  // Set Zipcode pattern based on Country
  setZipCodePattern() {
    let countryObj: any = find(this.countryList, { countryId: this.addressForm.value.Country });
    if (countryObj && countryObj.zipCodeRegex) {
      this.addressForm.controls['Pincode'].setValidators([Validators.required, Validators.pattern(countryObj.zipCodeRegex),
      Validators.minLength(this.manageRule.PINCODE_MIN_LENGTH), Validators.maxLength(this.manageRule.PINCODE_MAX_LENGTH)]);
      this.addressForm.controls['Pincode'].updateValueAndValidity();
    }
  }

  // Get Region list
  getRegionList(stateId) {
    this.userService.getRegionList(stateId).subscribe((response: any) => {
      if (response) {
        this.regionList = response.data;
      }
    }, (errorResponse: any) => {
      this.regionList = [];
      this.resetLocationList('Region');
    });
  }

  // Get District list
  getDistrictList(divisionId) {
    this.userService.getDistrictList(divisionId).subscribe((response: any) => {
      if (response) {
        this.districtList = response.data;
      }
    }, (errorResponse: any) => {
      this.districtList = [];
      this.resetLocationList('District');
    });
  }

  // Get City list
  getCityList(districtId) {
    const reqObj = {
      "districtId": districtId,
      "selectedId": null,
      "searchString": ""
    }
    this.sharedService.getCityTown(reqObj).subscribe((response: any) => {
      if (response) {
        this.cityList = response.data;
      }
    }, (errorResponse: any) => {
      this.cityList = [];
      this.addressForm.patchValue({
        City: undefined
      })
    });
  }

  // On Change Region 
  onChangeRegion(isReset?: boolean) {
    if (this.addressForm.value.divisionId) {
      this.getDistrictList(this.addressForm.value.divisionId);
      if (isReset)
        this.resetLocationList('Region');
    }
  }
  // On Change District 
  onChangeDistrict(isReset?: boolean) {
    if (this.addressForm.value.districtId) {
      this.getCityList(this.addressForm.value.districtId);
      if (isReset)
        this.resetLocationList('District');
    }
  }

  // Reset list
  resetLocationList(targetLocation, isReset?: boolean) {
    if (targetLocation == 'Country') {
      this.stateList = [];
      this.regionList = [];
      this.districtList = [];
      this.cityList = [];
      this.addressForm.patchValue({
        State: undefined,
        divisionId: undefined,
        districtId: undefined,
        City: undefined
      })
    } else if (targetLocation == 'State') {
      this.regionList = [];
      this.districtList = [];
      this.cityList = [];
      this.addressForm.patchValue({
        divisionId: undefined,
        districtId: undefined,
        City: undefined
      })
    } else if (targetLocation == 'Region') {
      this.districtList = [];
      this.cityList = [];
      this.addressForm.patchValue({
        districtId: undefined,
        City: undefined
      })
    } else if (targetLocation == 'District') {
      this.cityList = [];
      this.addressForm.patchValue({
        City: undefined
      })
    }
  }
}
