import { Injectable } from '@angular/core';

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as moment from 'moment';
import { trim } from 'lodash';

import { SharedService } from './shared/shared.service';

@Injectable()
export class CustomDateAdapter extends MomentDateAdapter {
  constructor(private sharedService: SharedService) {
    super('en-US');
  }

  public format(date: moment.Moment, displayFormat: string): string {
    const patternDetails = this.sharedService.getSiteSettingDetails();
    const format = trim(patternDetails.dateFormat);
    return date.format(format);
  }
}
