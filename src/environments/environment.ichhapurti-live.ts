export const environment = {
    production: true,
    serverUrl: 'https://www.ichhapurti.com:2443/',
    assetsUrl: `https://www.ichhapurti.com/`,
    becomeaVendorURL: 'https://www.ichhapurti.com/become-vendor/signup',
    salesPortalURL: 'https://sales.ichhapurti.com',
    SSRPort: 8081,
    cookieConfig: {
      path: '/',
      domain: 'ichhapurti.com',
      expires: '01.01.2025',
      secure: true,
      httpOnly: false
    }
  };