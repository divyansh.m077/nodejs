import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CONTACT_US_URL } from './contact-us.url-config';
@Injectable({
  providedIn: 'root'
})
export class ContactUsService {

  constructor(private http: HttpClient) { }

  addEnquiryDetails(enquiryData) {
    return this.http.put(CONTACT_US_URL.ENQUIRY, enquiryData);
  }

  getStoreContactDetails() {
    return this.http.get(CONTACT_US_URL.CONTACT_DETAILS);
  }
}
