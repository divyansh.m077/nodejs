import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit , Inject, HostListener} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { Language, TranslationService } from 'angular-l10n';
import { MatDialog, MatSnackBar } from '@angular/material';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';

import { UserService } from './../user.service';
import { AlertPopupComponent } from '../../shared/alert-popup/alert-popup.component';
import { HomeService } from './../../home/home.service';
import { API_STATUS_CODE, ALERT_SYSTEM_TITLE, COUNTRY_CODE } from '../../app.constants';
import { SharedService } from '../../shared/shared.service';
import { REGISTRATION_RULE, SNACKBAR_ALERT } from './../../app-setting';
import { StorageControllerService } from '../../storage-controller.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  @Language() lang: string;
  signInForm: FormGroup;
  resendButton: boolean;
  isPhone: boolean;
  actionSubscribe$: any;
  userEmail: any;
  countryCode = COUNTRY_CODE;
  @HostListener('document:keydown.enter') onKeydownHandler(event) {
    if( this.signInForm.valid ) {
      this.onSubmit();
    }
  }
  get settings() { return this.sharedService.getSiteSettingDetails(); }

  constructor(
    private storage: StorageControllerService,
    private router: Router,
    private fb: FormBuilder,
    public userService: UserService,
    public dialog: MatDialog,
    private homeService: HomeService,
    private socialAuthService: AuthService,
    private translation: TranslationService,
    private sharedService: SharedService,
    private snackBar: MatSnackBar,
    @Inject(WINDOW) private window: any
  ) {
    this.formInitialization();
  }

  formInitialization() {
    this.signInForm = this.fb.group({
      'userName': ['', [Validators.required, Validators.pattern(REGISTRATION_RULE.EMAIL_PATTERN)]],
      'password': ['', Validators.required]
    });
  }

  ngOnInit() {
    this.resendButton = false;
    this.sharedService.getLocation();
  }

  moveToSignup() {
    this.router.navigate(['/register']);
  }

  onSubmit() {
    this.userService.signInUserDetails(this.getSignInDetails()).subscribe((response: any) => {
      if (response) {
        const userDetails = response.data[0];
        let mergedName;
        if (userDetails.firstName || userDetails.lastName) {
          mergedName = userDetails.firstName + ' ' + userDetails.lastName;
        }
        let name = mergedName ? mergedName : userDetails.userMobile;
        this.storage.set('token', response.token);
        this.storage.set('userName', name);
        this.storage.set('userDetails', JSON.stringify(userDetails));
        this.storage.set('userName', name);
        this.storage.set('token', response.token);
        if (userDetails.userImage) {
          this.storage.set('userImage', userDetails.userImage);
        }
        this.storage.set('id', userDetails.userId);
        this.storage.set('id', userDetails.userId);
        let previousUrl: any;
        let _previousUrl = this.storage.get('previousUrl');
        let preUrl = (_previousUrl != null && _previousUrl != 'null') ? decodeURIComponent(this.storage.get('previousUrl')) : '/';
        if (preUrl && preUrl.includes('verify-email')) { preUrl = null; }   // temporary fix
        if (preUrl) {
          const splitUrl = preUrl.split('?');
          if (preUrl === '/register' || preUrl === '/forgot-password' || preUrl === '/signin' ||
            (splitUrl && splitUrl[0] === '/reset-password' || splitUrl && splitUrl[0] === '/set-password')) {
            previousUrl = '/';
          } else {
            previousUrl = preUrl;
          }
        }
        this.homeService.setSignInStatus(true);
        this.homeService.setCartCountChange(true);
        previousUrl = (previousUrl != 'null' && previousUrl != null) ? previousUrl : '/';
        previousUrl = mergedName ? previousUrl : 'profile';
        this.router.navigateByUrl(previousUrl);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.showErrorAlert(this.translation.translate(error.error.message));
      } 
      else if (error.status === API_STATUS_CODE.preconditionFailed) {
        this.router.navigate(['/verify-email'], { queryParams: { userEmail: this.signInForm.value.userName } });
      } 
      else if (error.status === API_STATUS_CODE.conflict) {
        this.showErrorAlert(this.translation.translate('REG_LOGGEDIN_AS_FBUSER'));
      }
    });
  }

  showErrorAlert(message) {
    const dialogRef = this.dialog.open(AlertPopupComponent, {
      data: {
        title: this.translation.translate('labelInfo'),
        message: [message],
        action: ALERT_SYSTEM_TITLE.info,
        buttonName: this.translation.translate('buttonOk')
      }
    });
    this.actionSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
      if (data) {
        this.dialog.closeAll();
      }
    });
  }

  getSignInDetails() {
    let locationObj = this.sharedService.getTrackLocationData();
    const signInData = {
      userName: this.signInForm.value.userName,
      password: this.signInForm.value.password,
      latitude: locationObj.latitude,
      longitude: locationObj.longitude
    };
    return signInData;
  }

public socialSignIn(socialPlatform: string) {
    let signObj: any = this.getSignInDetails();
    const signInDetails: any = {
      userEmail: signObj.userName,
      password: signObj.password,
      latitude: signObj.latitude,
      longitude: signObj.longitude
    };
    let socialPlatformProvider;
    if (socialPlatform === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(userData);
        signInDetails.providers = socialPlatform === 'facebook' ? 1 : 2;
        signInDetails.token = userData.authToken;
        signInDetails.userEmail = userData.email ? userData.email : '';
        signInDetails.userImage = userData.facebook.picture.data.url;
        signInDetails.firstName = userData.facebook.first_name;
        signInDetails.lastName = userData.facebook.last_name;
        signInDetails.socialMediaId = userData.facebook.id;
        signInDetails.isFBMobileAndMailExist = signInDetails.userEmail ? 1 : 0;
          this.userService.saveSocialUserRegisterationDetails(signInDetails)
          .subscribe((response: any) => {
            if (response.data) {
              const userDetails = response.data;
              this.userEmail = userDetails.userEmail;
              if (this.userEmail) {
                const name = userDetails.firstName + ' ' + userDetails.lastName;
                this.storage.set('token', userDetails.token);
                this.storage.set('userName', name);
                this.storage.set('userDetails', JSON.stringify(userDetails));
                this.storage.set('id', userDetails.userId);
                this.storage.set('userName', name);
                this.storage.set('token', userDetails.token);
                this.storage.set('id', userDetails.userId);
                let previousUrl: any;
                let preUrl = this.storage.get('previousUrl');
                if (preUrl && preUrl.includes('verify-email')) { preUrl = null; } 
                if (preUrl) {
                  const splitUrl = preUrl.split('?');
                  if (preUrl === '/register' || preUrl === '/forgot-password' ||
                     (splitUrl && splitUrl[0] === '/reset-password' || splitUrl && splitUrl[0] === '/set-password')) {
                    previousUrl = '/';
                  } else {
                    previousUrl = preUrl;
                  }
                }
                this.homeService.setSignInStatus(true);
                this.homeService.setCartCountChange(true);
                this.router.navigate([previousUrl ? previousUrl : '/']);
              } else {
                this.userService.initializeUserDetails(signInDetails);
                this.router.navigate(['/socialmedia-email']);
              }
            } else {
              const dialogRef = this.dialog.open(AlertPopupComponent, {
                data: {
                  title: this.translation.translate('labelSuccess'),
                  message: [this.translation.translate('labelRegisterSuccess'), this.translation.translate('verifyMsg')],
                  action: ALERT_SYSTEM_TITLE.success,
                  buttonName: this.translation.translate('buttonOk')
                },
                disableClose: true
              });
              this.actionSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
                if (data) {
                  this.dialog.closeAll();
                  this.router.navigate(['/signin']);
                }
              });
            }
          }, (error: any) => {
            if (error.status === API_STATUS_CODE.badRequest) {
              this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
            } else if (error.status === API_STATUS_CODE.conflict) {
              this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
            } else if (error.status === API_STATUS_CODE.preconditionFailed) {
                this.router.navigate(['/verify-email'], { queryParams: { userEmail: signInDetails.userEmail } });
            }
          });
      }
    );
  }

  // Check Email or Phone
  checkEmailOrPhone(event) {
    event = event.trim(' ');
    if (event === '' || event === null || event === undefined)
      return;

    if (event.length >= 5 && event.match(/^[0-9]+$/) != null) {
      this.isPhone = true;
      this.signInForm.get('userName').setValidators([Validators.required, Validators.pattern(this.settings.phoneNumberFormat)]);      
      this.signInForm.controls['userName'].updateValueAndValidity();
    }
    else {
      this.isPhone = false;
      this.signInForm.get('userName').setValidators([Validators.required, Validators.pattern(REGISTRATION_RULE.EMAIL_PATTERN)]);
      this.signInForm.controls['userName'].updateValueAndValidity();
    }
  }


}
