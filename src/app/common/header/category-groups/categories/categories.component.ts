import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { Language, TranslationService } from 'angular-l10n';

import { ComonService } from '../../../comon.service';
import { API_STATUS_CODE } from '../../../../app.constants';

import { SNACKBAR_ALERT } from '../../../../app-setting';
import { StorageControllerService } from '../../../../storage-controller.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  @Language() lang: string;
  public mainCategoryList;
  constructor(
    private comonService: ComonService,
    private router: Router,
    private snackBar: MatSnackBar,
    private translation: TranslationService,
    private storage: StorageControllerService
  ) {}

  ngOnInit() {
    this.getMainCategoryList();
  }

  getMainCategoryList() {
    this.comonService.getAllCategories().subscribe((response: any) => {
      this.mainCategoryList = response.data;
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  // showProductList(subCategoryId) {
  //   return this.router.navigate(['/product-list', subCategoryId]);
  // }

  showProductList(category: any, isCategory: boolean) {
    if (isCategory) {
      this.storage.set('isFromCategory', 'true');
      return this.router.navigate(['/product-list', category.categoryId]);
    } else {
      this.storage.remove('isFromCategory');
      return this.router.navigate(['/product-list', category.subCategoryId]);
    }
  }
}
