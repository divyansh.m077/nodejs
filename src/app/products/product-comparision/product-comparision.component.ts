import { Component, OnInit , Inject} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { forEach, remove} from 'lodash';
import { Language, TranslationService } from 'angular-l10n';

import { ProductService } from '../products.service';
import { HomeService } from './../../home/home.service';
import { API_STATUS_CODE } from '../../app.constants';
import { CartService } from '../../cart/cart.service';
import { SharedService } from '../../shared/shared.service';
import { ComonService } from '../../common/comon.service';

import { SNACKBAR_ALERT, SNACKBAR_WARNING, SNACKBAR_SUCCESS } from '../../app-setting';
import { StorageControllerService } from '../../storage-controller.service';
@Component({
  selector: 'app-product-comparision',
  templateUrl: './product-comparision.component.html',
  styleUrls: ['./product-comparision.component.scss']
})
export class ProductComparisionComponent implements OnInit {
  vendorIds: any[];
  @Language() lang: string;
  public productIds: any = [];
  productCompareList: any = [];
  showDiscount: any;
  prodDetails: any;
  isLoading: boolean;
  isBrowser;
  constructor(
    private storage: StorageControllerService,
    private productService: ProductService,
    private router: Router, private route: ActivatedRoute,
    private homeService: HomeService,
    private translation: TranslationService,
    private cartService: CartService,
    private sharedService: SharedService,
    private comonService: ComonService,
    private snackBar: MatSnackBar,
  ) {
    this.isLoading = true;
      this.getSiteSettingProdDetails();
  }

  getSiteSettingProdDetails() {
    this.prodDetails = this.sharedService.getSiteSettingDetails();
    if (this.prodDetails) {
      this.showDiscount = this.prodDetails.productSettings.displayDiscountedPrice;
     }
  }

  ngOnInit() {
    this.isBrowser = typeof window !== 'undefined' ? true : false;
    this.getProductIds();
  }

  getProductIds() {
    this.productIds = [];
    this.vendorIds = [];
    if (!!this.storage.get('compareStorage')) {
      const storedItems = this.storage.get('compareStorage') ? JSON.parse(this.storage.get('compareStorage')) : [];
      forEach(storedItems, (items) => {
        this.productIds.push(items.productId);
        items.vendorId ? this.vendorIds.push(items.vendorId) : this.vendorIds = null;
      });
      this.getCompareProductDetails();
      if (!!this.storage.get('id')) {
        this.trackProductComparisonDetails();
      }
    }
  }

  getCompareProductDetails() {
    this.isLoading = true;
    this.productCompareList = [];
      const productObj = {
      'productId': this.productIds,
      'vendorId' : this.vendorIds
    };
    this.productService.getCompareProductDetails(productObj).subscribe((response: any) => {
      this.isLoading = false;
      if (response) {
        forEach(response.data.productCompareList, (product) => {
          const productPrice = this.comonService.calculatePriceAmountWithTax(product.salesPrice, product.discountAmount, product.taxAmount,
            product.taxType, product.taxValue);
          product.taxDiscountPrice = productPrice.taxDiscountPrice;
          product.taxSalesPrice = productPrice.taxSalesPrice;
          this.productCompareList.push(product);
        });
        if (!this.storage.get('id')) {
         this.findProductInCart();
        }
      } else {
        this.productCompareList = [];
      }
    }, (error: any) => {
      this.isLoading = false;
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  findProductInCart() {
    if (!!this.storage.get('cartStorage')) {
      const cartItems = !!this.storage.get('cartStorage') ? JSON.parse(this.storage.get('cartStorage')) : [];
      forEach(cartItems, (cart) => {
        forEach(this.productCompareList, (product) => {
          if (cart.productId === product.productId && cart.vendorId === product.vendorId) {
            product.cartUser = 1;
          }
        });
      });
    }
  }

  removeProduct(Id: any) {
    if (Id) {
      const Item = this.storage.get('compareStorage') ? JSON.parse(this.storage.get('compareStorage')) : [];
      remove(Item, {productId: Id});
      this.storage.set('compareStorage', null);
      this.storage.set('compareStorage', JSON.stringify(Item));
      remove(this.productCompareList, {productCatalogId: Id});
      this.homeService.setCompareCountChange(true);
      if (Item.length === 0) {
        this.storage.remove('compareStorage');
      }
    }
  }

  trackProductComparisonDetails() {
    this.productService.trackCompareProductDetails({'productId': this.productIds}).subscribe((response: any) => {
      if (response) {
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  moveToProducts() {
    this.router.navigate(['/products']);
  }

  addProductToCart(productDetail) {
    if ((productDetail.quantity - productDetail.soldOut) === 0) {
      this.snackBar.open(this.translation.translate('alertProductOutOfStock'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
    } else {
      if (productDetail.isProductInAttribute === 1 || (Number(productDetail.minimumQuantity) > 1)) {
        if (productDetail.vendorId) {
         this.router.navigate(['/product-detail', productDetail.productId], {queryParams : {'id' : productDetail.vendorId}});
        } else {
          this.router.navigate(['/product-detail', productDetail.productId]);
        }
      } else {
        if (productDetail.cartUser === 0) {
          this.addProductDetailsToCart(productDetail);
        } else {
          this.router.navigate(['/cart-checkout']);
        }
      }
    }
  }

  addProductDetailsToCart(productDetail) {
    const productPrice = productDetail.discountAmount === 0 ? productDetail.salesPrice :
                         productDetail.discountAmount;
    const taxAmount = productDetail.taxAmount;
    const quantity = (productDetail.minimumQuantity && Number(productDetail.minimumQuantity !== 0)) ?
                     productDetail.minimumQuantity : 1;
    const mrpSalesPrice = productDetail.discountAmount === 0 ? productDetail.salesPrice : productDetail.discountAmount;
    if (productDetail.isProductInAttribute === 0) {
      productDetail.cartUser = 1;
      if (!!this.storage.get('id')) {
        const cartObj = {
          'productCart': [{
            'productId': productDetail.productId,
            'price': productPrice,
            'cartQuantity': quantity,
            'subTotal': productPrice * quantity,
            'taxTotal': taxAmount * quantity,
            'productOption': [],
            'vendorId' : productDetail.vendorId ? productDetail.vendorId : null,
            'vendorProductId' : productDetail.vendorProductId ? productDetail.vendorProductId : null,
            'mrpSalesPrice' : mrpSalesPrice
          }]
        };
        this.cartService.addCartDetails(cartObj).subscribe((response: any) => {
          if (response) {
            this.homeService.setCartCountChange(true);
            this.snackBar.open(this.translation.translate(response.data), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
          }
        }, (error: any) => {
          if (error.status === API_STATUS_CODE.badRequest) {
            this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
          } else if (error.status === API_STATUS_CODE.preconditionFailed) {
            const productDetails = error.error.data[0];
            this.comonService.showAlertForProductQuantity(productDetails, productDetail);
          }
        });
      } else {
        const cartStorage = [];
        const productObj = {
          'productId': productDetail.productId,
          'taxValue': productDetail.taxValue,
          'taxType': productDetail.taxType,
          'discountAmount': productDetail.discountAmount,
          'taxAmount': productDetail.taxAmount,
          'salesPrice': productDetail.salesPrice,
          'productName': productDetail.productName,
          'productImage': productDetail.productImage,
          'productCode': productDetail.productCode,
          'shippingValue': productDetail.shippingValue,
          'freeShipping': productDetail.freeShipping,
          'quantity': productDetail.quantity,
          'minimumQuantity': productDetail.minimumQuantity,
          'maximumQuantity': productDetail.maximumQuantity,
          'mrpSalesPrice' : mrpSalesPrice,
          'lowStockName': productDetail.lowStockName,
          'currencySymbol': productDetail.currencySymbol,
          'cartQuantity': quantity,
          'subTotal': productPrice * quantity,
          'taxTotal': taxAmount * quantity,
          'productOption': [],
          'soldOut': productDetail.soldOut,
          'cartUser': productDetail.cartUser,
          'vendorId' : productDetail.vendorId ? productDetail.vendorId : null,
          'vendorProductId' : productDetail.vendorProductId ? productDetail.vendorProductId : null,
        };
        cartStorage.push(productObj);
        if (!!this.storage.get('cartStorage')) {
          const storedItems = JSON.parse(this.storage.get('cartStorage'));
          forEach(storedItems, (items) => {
            cartStorage.push(items);
          });
        }
        this.storage.set('cartStorage', JSON.stringify(cartStorage));
        this.homeService.setCartCountChange(true);
        this.snackBar.open(this.translation.translate('alertProductCart'), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      }
    } else {
      if (productDetail.vendorId) {
        this.router.navigate(['/product-detail', productDetail.productId], {queryParams : { 'id' : productDetail.vendorId}});
      } else {
        this.router.navigate(['/product-detail', productDetail.productId]);
      }
    }
  }

  moveToCart() {
    this.router.navigate(['/cart-checkout']);
  }
}
