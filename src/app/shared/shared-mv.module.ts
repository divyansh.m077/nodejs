import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorModule } from '../../../projects/multi-vendor/multi-vendor.module';

@NgModule({
  imports: [
    CommonModule,
    VendorModule
  ],
  declarations: [
  ],
  exports: [
    VendorModule
  ],
  providers: []
})
export class SharedVendorModule { }
