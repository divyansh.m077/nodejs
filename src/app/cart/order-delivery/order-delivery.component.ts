import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { Language, TranslationService } from 'angular-l10n';

import { CartService } from '../cart.service';
import { API_STATUS_CODE } from '../../app.constants';

import { SharedService } from '../../shared/shared.service';
import { SNACKBAR_ALERT } from '../../app-setting';
@Component({
  selector: 'app-order-delivery',
  templateUrl: './order-delivery.component.html',
  styleUrls: ['./order-delivery.component.scss']
})
export class OrderDeliveryComponent implements OnInit {
  @Language() lang: string;
  orderId: any;
  orderDetails: any;
  constructor(private router: Router,
              private route: ActivatedRoute,
              private cartService: CartService,
              private snackBar: MatSnackBar,
              private translation: TranslationService,
              private sharedService: SharedService) {
    this.route.params.subscribe(params => {
      this.orderId = params.id;
    });
  }

  ngOnInit() {
    this.getOrderDetails();
  }

  getOrderDetails() {
    this.cartService.getOrderDetailById(this.orderId).subscribe((response: any) => {
      if (response) {
        this.orderDetails = response.data;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  continueShopping() {
    this.router.navigate(['/']);
  }

}
