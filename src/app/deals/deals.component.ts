import { Component, OnInit , Inject, PLATFORM_ID, NgZone, OnDestroy} from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { formatDate, isPlatformBrowser, DatePipe } from '@angular/common';

import { concat, uniqBy, find } from 'lodash';
import { Language, TranslationService } from 'angular-l10n';

import { DealsService } from './deals.service';
import { HomeService } from './../home/home.service';
import { DEALS_SETTINGS, HOME_SETTINGS } from '../app-setting';
import { ComonService } from './../common/comon.service';
import { API_STATUS_CODE } from '../app.constants';
import { SNACKBAR_ALERT } from '../app-setting';
import { SharedService } from '../shared/shared.service';
import { WebWorkerService } from '../web-worker.service';
@Component({
  selector: 'app-deals',
  templateUrl: './deals.component.html',
  styleUrls: ['./deals.component.scss']
})
export class DealsComponent implements OnInit, OnDestroy {

  @Language() lang: string;
  public dealsList: any = [];
  public from: string;
  public todayDate = new Date();
  public currentDate: any;
  public dealsCount: any = DEALS_SETTINGS;
  public dealTotalLength: number;
  public productSubscribe$: any;
  public compareCount: any;
  public isLoading: boolean;
  timer: string;
  siteSettingDetails: any;
  homeSettings = HOME_SETTINGS;
  now;
  timerInterval;
  timerSetInterval;

  constructor(@Inject(PLATFORM_ID) private platform: Object,
              private dealsService: DealsService,
              private homeService: HomeService,
              private comonService: ComonService,
              private snackBar: MatSnackBar,
              private translation: TranslationService,
              private zone: NgZone,
              private datepipe: DatePipe,
              private webWorkerService: WebWorkerService,
              private sharedService: SharedService
            ) {
    this.dealTotalLength = 0;
     this.compareCount = 0;
    this.isLoading = true;
    this.productSubscribe$ = this.homeService.getSignInStatus().subscribe((response: any) => {
      if (response === false) {
        this.dealsCount.START_COUNT = 0;
        this.dealsList = [];
        this.showAllDealsList();
      }
    });
  }

  initializeGlobalVariables() {
    this.currentDate = formatDate(this.todayDate, 'yyyy-MM-dd', 'en-US');
    this.from = 'deals';
    this.dealTotalLength = 0;
  }

  ngOnInit() {
    this.dealsCount.START_COUNT = 0;
    this.getCompareCount();

    const dealObj = {
      'startLimit': this.homeSettings.START_COUNT,
      'endLimit': this.homeSettings.END_COUNT,
      'currentDate': this.datepipe.transform(new Date(), 'yyyy-MM-dd'),
      'desc': true
    };

    this.homeService.getTodayProductList(dealObj).subscribe((x: any) => {
      this.isLoading = false;
      if (x) {
        this.todayDate = ('startTime' in x && x !== null) ? x.startTime : new Date();
        this.now = new Date(this.todayDate).getTime();
        this.zone.runOutsideAngular(() =>
          this.timerInterval = setInterval(_ => {
            this.now = this.now + 1000;
          }, 1000)
        );
        this.initializeGlobalVariables();
        this.getCurrentDate();
        this.showAllDealsList();
      }
    });
  }

  getCompareCount() {
    this.siteSettingDetails = this.sharedService.getSiteSettingDetails();
    if (this.siteSettingDetails) {
      this.compareCount = this.siteSettingDetails.compareLimit;
    } else {
      this.compareCount = 0;
    }
  }

  getCurrentDate() {
    const curDate = new Date();
    curDate.setHours(23, 59, 59, 9999);
    this.calculateCountdownTime(curDate.getTime());
  }

  calculateCountdownTime(countDownDate) {
    if (isPlatformBrowser(this.platform)) {
        this.timerSetInterval = setInterval(() => {
          const distance = countDownDate - this.now;
          this.webWorkerService.getHourMinuteSecFromTime(distance);
          this.timer = this.webWorkerService.calculatedTime;
          if (distance < 0) {
            this.timer = undefined;
            clearInterval(this.timerSetInterval);
            this.todayDate.setDate(this.todayDate.getDate() + 1);
            this.currentDate = formatDate(this.todayDate, 'yyyy-MM-dd', 'en-US');
            this.getCurrentDate();
            this.showAllDealsList();
          }
        }, 1000);
    }
  }

  showAllDealsList() {
    const productObj = {
      'startLimit': this.dealsCount.START_COUNT,
      'endLimit': this.dealsCount.END_COUNT,
      'currentDate': this.currentDate,
      'targetCurrency': 'USD'
    };
    this.isLoading = true;
    this.dealTotalLength = 0;
    this.dealsService.getAllDealsList(productObj).subscribe((response: any) => {
      this.isLoading = false;
      if (response) {
        this.dealsList = concat(this.dealsList, response.product);
        this.dealsList = uniqBy(this.dealsList, 'productId');
        this.dealsList = this.comonService.findCartCompareSelection(this.dealsList);
        this.dealTotalLength = response.productlength;
      }
    }, (error: any) => {
      this.isLoading = false;
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  showMoreProducts() {
    this.dealsCount.START_COUNT += this.dealsCount.SHOW_MORE_COUNT;
    this.showAllDealsList();
  }

  getProductActionDetails($event: any) {
    if ($event.action === 'wishList') {
      const productData = find(this.dealsList, {'productId': $event.productId});
      productData.favUser = $event.status === 1 ? 1 : 0;
    } else if ($event.action === 'cart') {
      const productData = find(this.dealsList, {'productId': $event.productId});
      productData.cartUser = 1;
    } else {
      const productData = find(this.dealsList, {'productId': $event.productId});
      productData.compareProduct = 1;
    }
  }

  ngOnDestroy() {
    clearInterval(this.timerInterval);
    clearInterval(this.timerSetInterval);
    this.webWorkerService.terminateGetHourMinuteSecFromTime();
  }
}
