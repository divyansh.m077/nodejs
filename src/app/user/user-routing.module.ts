import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { SigninComponent } from './signin/signin.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ProfileComponent } from './profile/profile.component';
import { ManageAddressComponent } from './manage-address/manage-address.component';
import { ProfileInfoComponent } from './profile-info/profile-info.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { WishListComponent } from './wish-list/wish-list.component';
import { ReviewsRatingComponent } from './reviews-rating/reviews-rating.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotificationComponent } from './notification/notification.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ProfileDetailsComponent } from './profile-info/profile-details/profile-details.component';
import { ChangePasswordComponent } from './profile-info/change-password/change-password.component';
import { OrderComplaintsComponent } from './order-complaints/order-complaints.component';
import { WalletComponent } from './wallet/wallet.component';
import { AuthGuard } from './auth-guard.service';
import { NonAuth } from './non-auth-redirect.service';
import { OrderSummaryComponent } from './my-orders/order-summary/order-summary.component';
import { UserActivationComponent } from './user-activation/user-activation.component';
import { SocialmediaEmailComponent } from './socialmedia-email/socialmedia-email.component';
import { SetPasswordComponent } from './set-password/set-password.component';

const userRoutes: Routes = [
    {
      path: 'register',
      component: RegisterComponent, canActivate: [NonAuth]
    },
    {
      path: 'verify-email',
      component: UserActivationComponent, canActivate: [NonAuth]
    },
    {
      path: 'signin',
      component: SigninComponent, canActivate: [NonAuth]
    },
    {
      path: 'forgot-password',
      component: ForgotPasswordComponent, canActivate: [NonAuth]
    },
    {
      path: 'reset-password',
      component: ResetPasswordComponent, canActivate: [NonAuth]
    },
    {
      path: 'set-password',
      component: SetPasswordComponent, canActivate: [NonAuth]
    },
    {
      path: 'socialmedia-email',
      component: SocialmediaEmailComponent, canActivate: [NonAuth]
    },
    {
      path: 'profile',
      component: ProfileComponent,
      children: [
        {
          path: 'info',
          component: ProfileDetailsComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'change-password',
          component: ChangePasswordComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'address',
          component: ManageAddressComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'orders',
          component: MyOrdersComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'order-detail/:id/:isVendor',
          component: OrderSummaryComponent
        },
        {
          path: 'wishlist',
          component: WishListComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'rating',
          component: ReviewsRatingComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'dashboard',
          component: DashboardComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'notification',
          component: NotificationComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'order-complaint',
          component: OrderComplaintsComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'my-wallet',
          component: WalletComponent,
          canActivate: [AuthGuard]
        },
        {
          path: '',
          redirectTo: 'info',
          pathMatch: 'full'
        }
      ]
    },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      userRoutes
    )
  ],
  declarations: [],
  exports: [RouterModule],
})
export class UserRoutingModule { }
