import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Title } from '@angular/platform-browser';

import { COMMON_URL } from './common.url-config';
import { SharedService } from '../shared/shared.service';

@Injectable()
export class AppInitializerService {

  private config: any;
  private loaded: false;

  constructor(private http: HttpClient, private sharedService: SharedService, private titleService: Title) { }

  load() {
    const configPromise = new Promise((resolve, reject) => {
      this.http.get(COMMON_URL.SITE_SETTING_URL).subscribe((response: any) => {
        this.config = response;
        this.setTitle(response.data[0].systemTitle);
        this.sharedService.initializeSiteSettingDetails(response.data[0]);
        resolve();
      });
    });
    return configPromise;
  }

  setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }
}
