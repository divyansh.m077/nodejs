import { NgModule } from '@angular/core';
import { CustomDatePipe } from './custom-date.pipe';
import { CurrencyFormatPipe } from './custom-currency.pipe';
import { SafePipe } from './safe.pipe';
@NgModule({
  imports: [
  ],
  declarations: [CustomDatePipe, CurrencyFormatPipe, SafePipe],
  exports: [
    CustomDatePipe,
    CurrencyFormatPipe,
    SafePipe
  ],
  providers: [CustomDatePipe]
})
export class SharedPipesModule { }
