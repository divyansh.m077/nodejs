import { Component, OnInit } from '@angular/core';
import { Language } from 'angular-l10n';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-not-allowed',
  templateUrl: './not-allowed.component.html',
  styleUrls: ['./not-allowed.component.scss']
})
export class NotAllowedComponent implements OnInit {
  fromResetPassword: boolean;

  @Language() lang;

  constructor(private route: ActivatedRoute) { 
     this.route.params.subscribe(params => {
      this.fromResetPassword = params.from ? true : false;
    });
  }

  ngOnInit() {
  }

}
