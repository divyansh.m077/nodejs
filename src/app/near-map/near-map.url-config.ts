import { BASE_URL } from '../common/common.url-config';

export const NEAR_MAP_URL =  {
  STORE_LIST: BASE_URL + 'near-by-store',
  PRODUCT_LIST: BASE_URL + 'get-product-all'
};
