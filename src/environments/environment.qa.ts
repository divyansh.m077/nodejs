export const environment = {
    production: true,
    serverUrl: 'https://sv-qa.purchasecommerce.com:3443/',
    assetsUrl: `https://sv-qa.purchasecommerce.com/`,
    becomeaVendorURL: '',
    SSRPort: 8083,
    cookieConfig: {
        path: '/',
        domain: 'sv-qa.purchasecommerce.com',
        expires: '01.01.2021',
        secure: true,
        httpOnly: false
    }
};
