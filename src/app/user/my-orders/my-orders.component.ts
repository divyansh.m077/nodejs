import { Component, OnInit , Inject, PLATFORM_ID} from '@angular/core';

import { Language, TranslationService } from 'angular-l10n';
import { UserService } from '../user.service';
import { API_STATUS_CODE, ORDER_STATUS } from '../../app.constants';

import { Router, ActivatedRoute } from '@angular/router';
import { SNACKBAR_ALERT } from '../../app-setting';
import { MatSnackBar } from '@angular/material';
import { StorageControllerService } from '../../storage-controller.service';
import { SharedService } from '../../shared/shared.service';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.scss']
})
export class MyOrdersComponent implements OnInit {
  @Language() lang: string;
  orderList;
  orderTotalLength;
  startLimit = 5;
  offSet = 0;
  currentPage = 1;
  totalPages;
  isLoading: boolean;
  orderStatusList = ORDER_STATUS;
  constructor(@Inject(PLATFORM_ID) private platform: Object,
              private userService: UserService,
              private router: Router,
              private snackBar: MatSnackBar,
              private translation: TranslationService,
              private route: ActivatedRoute,
              private storage: StorageControllerService,
              private sharedService: SharedService
             ) {}


  ngOnInit() {
    this.idCheckToShowSMSOrder();
    this.isLoading = true;
    this.getMyOrders();
  }

  getMyOrders() {
    this.isLoading = true;
    this.userService.getMyOrders(
      this.startLimit,
      this.offSet).subscribe((response: any) => {
      if (response) {
        this.orderTotalLength = response.totalCount;
        this.totalPages = Math.ceil(this.orderTotalLength / this.startLimit);
        this.orderList = response.data;
        this.isLoading = false;
      } else {
        this.isLoading = false;
        this.orderList = undefined;
      }
    }, (error: any) => {
      this.isLoading = false;
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  onViewOrder(order) {
    let isVendor;
    if (order.vendorId && order.companyName) {
      isVendor = 1;
    } else {
      isVendor = 0;
    }
    this.router.navigate(['profile/order-detail/', order.orderId, isVendor]);
  }

  showProductsRight() {
    this.currentPage++;
    this.offSet += this.startLimit;
    this.getMyOrders();
  }

  showProductsLeft() {
    this.currentPage--;
    this.offSet -= this.startLimit;
    this.getMyOrders();
  }

  idCheckToShowSMSOrder() {
    if (isPlatformBrowser(this.platform)) {
    if (typeof window !== 'undefined') {
      const id = this.route.snapshot.queryParams.id;
      console.log(this.storage.get('id'));
      console.log(id);
      if (id && this.storage.get('id') !== id) {
            this.sharedService.clearUserInfoFromStorage();
            this.router.navigate(['/signin']);
      }
      }
  }
}

}
