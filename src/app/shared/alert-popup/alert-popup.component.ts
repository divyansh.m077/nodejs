import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';

import { Language } from 'angular-l10n';

import {MatDialog, MAT_DIALOG_DATA} from '@angular/material';
import { ALERT_SYSTEM_TITLE } from '../../app.constants';
@Component({
  selector: 'app-alert-popup',
  templateUrl: './alert-popup.component.html',
  styleUrls: ['./alert-popup.component.scss']
})
export class AlertPopupComponent implements OnInit {
  alterTypes = ALERT_SYSTEM_TITLE;
  @Language() lang: string;
  @Output() actionEvent = new EventEmitter<boolean>();
  constructor(public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

   confirmAction() {
    this.actionEvent.emit(true);
  }
}
