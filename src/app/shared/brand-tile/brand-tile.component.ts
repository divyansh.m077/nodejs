import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { NO_IMAGE_URL } from '../../../app/app.constants';
import { StorageControllerService } from '../../storage-controller.service';
@Component({
  selector: 'app-brand-tile',
  templateUrl: './brand-tile.component.html',
  styleUrls: ['./brand-tile.component.scss']
})
export class BrandTileComponent implements OnInit {
  @Input() brandDetail;
  @Input() from;
  toggleCardAnimation = false;
  isBrowser = false;
  noImgUrl = NO_IMAGE_URL;

  constructor(
    private router: Router,
    private storage: StorageControllerService
  ) { }

  ngOnInit() {


  }

  toggleCardAnimationEffect(value: boolean) {
    this.toggleCardAnimation = value;
  }

  goToProductList(brandDetail) {
    this.storage.remove('isFromCategory');
    return this.router.navigate(['/product-list', brandDetail.brandId], { queryParams: { isFromBrand: true } });
  }
}
