import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import { PRODUCT_URL } from '../../src/app/products/products.url-config';

@Injectable({
  providedIn: 'root'
})
export class ProductDetailService {

  constructor(private http: HttpClient) {
  }

  getProductList(productData) {
    return this.http.post(PRODUCT_URL.DETAIL, productData);
  }
}
