import {Component, Inject, PLATFORM_ID, ViewChild, OnInit, Injector} from '@angular/core';

import {Spinkit} from 'ng-http-loader';
import {Router, RoutesRecognized} from '@angular/router';
import {filter, pairwise} from 'rxjs/operators';

import {UserService} from './user/user.service';
import {API_STATUS_CODE} from './app.constants';
import {HomeService} from './home/home.service';
import {ComonService} from './common/comon.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatDialog, MatSnackBar } from '@angular/material';
import { MobileOrNotService } from './mobile-or-not.service';
import { SwUpdate } from '@angular/service-worker';
import { TranslationService } from 'angular-l10n';
import { StorageControllerService } from './storage-controller.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('zoomIn', [
      state('false', style({
        transform: 'scale(0.2)'
      })),
      state('true', style({
        transform: 'scale(1)'
      })),
      transition('1 => 0', animate('0.2s ease-in')),
      transition('0 => 1', animate('0.2s ease-out'))
    ])
  ]
})
export class AppComponent implements OnInit {
  public spinkit = Spinkit;
  public showJet = false;
  pwaDeferredPrompt;
  public notificationOptions = {
    timeOut: 4000,
    showProgressBar: false,
    pauseOnHover: false,
    clickToClose: true,
    preventDuplicates: true,
    // position: ['top', 'right']
  };
  scrollValue: any;
  swUpdate: SwUpdate;

  constructor(
    private storage: StorageControllerService,
    private router: Router, private userService: UserService,
    public dialog: MatDialog,
    private homeService: HomeService,
    private comonSer: ComonService,
    private mobileOrNot: MobileOrNotService,
    private injector: Injector,
    private snackBar: MatSnackBar,
    private translation: TranslationService
  ) {
    if (typeof window !== 'undefined') {
      this.captureRouteEvents();
      this.getUserDetails();
    }
  }

  ngOnInit() {
    if (typeof window !== 'undefined' && this.mobileOrNot.isMobile()) {
      window.addEventListener('beforeinstallprompt', (e) => {
        e.preventDefault();
        this.pwaDeferredPrompt = e;
        this.pwaDeferredPrompt.prompt();
        this.pwaDeferredPrompt.userChoice
        .then((choiceResult) => {
          if (choiceResult.outcome === 'accepted') {
            console.log('User accepted the A2HS prompt');
          } else {
            console.log('User dismissed the A2HS prompt');
          }
          this.pwaDeferredPrompt = null;
          },
          _ => { console.log('pwaDeferredPrompt.userChoice promise rejected');
        });
      });
    }
    if (typeof window !== 'undefined') { this.reloadBrowserOnAvailabilityOfNewUpdate(); }

  }

  reloadBrowserOnAvailabilityOfNewUpdate() {
    this.swUpdate = <SwUpdate>this.injector.get(SwUpdate);
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        const snackBarRef = this.snackBar.open(this.translation.translate('NEW_UPDATE_AVAILABLE'), this.translation.translate('RELOAD'));
        snackBarRef.onAction().subscribe(_ => {
          window.location.reload();
        });
      });
    }
  }


  captureRouteEvents() {
    this.router.events
      .pipe(filter((e: any) => e instanceof RoutesRecognized),
        pairwise()
      ).subscribe((e: any) => {
      this.storage.set('previousUrl', e[0].urlAfterRedirects);
      this.storage.set('currentUrl', e[1].urlAfterRedirects);
    });
  }

  getUserDetails() {
    if (!!this.storage.get('id') && !!this.storage.get('token')) {
      this.userService.getUserProfileDetails()
        .subscribe((response: any) => {
          if (response) {
            const name = response.data[0].firstName + ' ' + response.data[0].lastName;
            this.storage.set('userName', name);
            if (response.data[0].userImage) {
              this.storage.set('userImage', response.data[0].userImage);
            }
            this.homeService.setSignInStatus(true);
            this.homeService.setCartCountChange(true);
          }
        }, (error: any) => {
          if (error.status === API_STATUS_CODE.badRequest) {

          }
        });
    }
  }

  onScroll(event) {
    if (event.target.documentElement.scrollTop) {
      if (event.target.documentElement.scrollTop <= 150) {
        this.scrollValue = false;
      } else {
        this.scrollValue = true;
      }
      this.comonSer.setScrollEmitValues(this.scrollValue);
    }
    if (this.scrollValue === true) {
      this.showJet = true;
    } else {
      this.showJet = false;
    }
  }
//   onSwipe(evt) {
//     const x = Math.abs(evt.deltaX) > 40 ? (evt.deltaX > 0 ? 'right' : 'left'):'';
//     const y = Math.abs(evt.deltaY) > 40 ? (evt.deltaY > 0 ? 'down' : 'up') : '';
//   }
 }
