import { Component, OnInit } from '@angular/core';

import { Language, TranslationService } from 'angular-l10n';
import { MatDialog, MatSnackBar } from '@angular/material';

import { settings, PROFILE_INFO_RULE, SNACKBAR_SUCCESS, SNACKBAR_ALERT } from '../../app-setting';
import { API_STATUS_CODE, ALERT_SYSTEM_TITLE } from '../../app.constants';
import { ConfirmationPopupComponent } from '../../shared/confirmation-popup/confirmation-popup.component';
import { UploadImageComponent } from './upload-image/upload-image.component';
import { UserService } from '../../user/user.service';
import { StorageControllerService } from '../../storage-controller.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  fileData: any;
  @Language() lang: string;
  public userName: any;
  userNameSubscribe$: any;
  profileDetails = PROFILE_INFO_RULE;
  url = '';
  profileSubscribe$: any;
  pagePositionSubscribe$: any;
  acceptImageTypes = settings.acceptImageTypes;
  imageTypes: string[];
  constructor(
    private storage: StorageControllerService,
    private userService: UserService,
    private translation: TranslationService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.imageTypes = settings.acceptImageTypes.split(',');
    this.setUserImage();
  }

  onSelectFile() {
    const dialogRef = this.dialog.open(UploadImageComponent, {
      data: {
        width:  '80%', height: '80%'
      }
    });
    dialogRef.afterClosed().subscribe((response: any) => {
      if (response && response !== true) {
        this.url = response;
        this.updateUserImage(this.url);
      }
    });
  }

  updateUserImage(imageData) {
    this.userService.updateProfileImage({avatar: imageData}).subscribe((response: any) => {
      if (response) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        this.storage.remove('userImage');
        this.storage.set('userImage', response.data.imageUrl);
        this.userService.setUserName(true);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
      if (error.status === API_STATUS_CODE.internalServer) {
        this.snackBar.open(error.error.error, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
      this.setUserImage();
    });
  }

  setUserImage() {
    const userImage = this.storage.get('userImage');
    if (userImage) {
      this.url = userImage;
    } else {
      this.url = 'assets/images/user.png';
    }
  }

  deleteImage() {
    const dialogRef = this.dialog.open(ConfirmationPopupComponent, {
      data: {
        title: this.translation.translate('labelDialogTitle'),
        message: this.translation.translate('alertMsgProfileRemove'),
        action: ALERT_SYSTEM_TITLE.warning,
        buttonOneName: this.translation.translate('buttonDelete'),
        buttonTwoName: this.translation.translate('buttonCancel'),
      }
    });
    this.profileSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
      if (data) {
        this.userService.deleteProfileImage().subscribe((response: any) => {
          if (response) {
            this.dialog.closeAll();
            this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
            this.storage.remove('userImage');
            this.userService.setUserName(true);
            this.setUserImage();
          }
        }, (error: any) => {
          this.dialog.closeAll();
          if (error.status === API_STATUS_CODE.badRequest) {
            this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
          }
          if (error.status === API_STATUS_CODE.internalServer) {
            this.snackBar.open(error.error.error, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
          }
        });
      }
    });
  }
}
