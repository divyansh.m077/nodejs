import { Component, OnInit } from '@angular/core';

import { Language } from 'angular-l10n';

@Component({
  selector: 'app-password-rule',
  templateUrl: './password-rule.component.html',
  styleUrls: ['./password-rule.component.scss']
})
export class PasswordRuleComponent implements OnInit {
  @Language() lang: string;
  constructor() { }

  ngOnInit() {
  }

}
