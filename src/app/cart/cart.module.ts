import { NgModule } from '@angular/core';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TranslationModule } from 'angular-l10n';
import { MatInputModule, MatButtonModule, MatCheckboxModule,
  MatExpansionModule, MatTableModule, MatFormFieldModule, MatProgressBarModule,
  MatRippleModule, MatSelectModule, MatListModule, MatDialogModule, MatRadioModule, MAT_SELECT_SCROLL_STRATEGY
} from '@angular/material';
import { NgxStripeModule } from 'ngx-stripe';

import { CartRoutingModule } from './cart-routing.module';
import { CartComponent } from './cart.component';
import { OrderDeliveryComponent } from './order-delivery/order-delivery.component';
import { PaymentComponent } from './payment/payment.component';
import { SharedModule } from '../shared/shared.module';
import { Overlay } from '@angular/cdk/overlay';

@NgModule({
  imports: [
    CommonModule,
    TranslationModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatTableModule,
    CartRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatRippleModule,
    MatSelectModule,
    MatListModule,
    NgxStripeModule.forRoot('pk_test_skONXvMQxbYdF6QJxBYyZIcL'),
    ReactiveFormsModule,
    MatDialogModule,
    MatRadioModule,
    MatProgressBarModule,
    SharedModule
  ],
  declarations: [
    CartComponent,
    OrderDeliveryComponent,
    PaymentComponent,
  ],
  entryComponents: [
    PaymentComponent
  ],
  providers: [
    {
      provide: MAT_SELECT_SCROLL_STRATEGY,
      deps: [ Overlay ],
      useFactory: function(overlay) { return () => overlay.scrollStrategies.block(); }
    },
  ]
})
export class CartModule { }
