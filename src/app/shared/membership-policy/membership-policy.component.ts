import { Component, OnInit, Inject } from '@angular/core';
import { Language} from 'angular-l10n';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';


@Component({
  selector: 'app-membership-policy',
  templateUrl: './membership-policy.component.html',
  styleUrls: ['./membership-policy.component.scss']
})
export class MembershipPolicyComponent implements OnInit {
  @Language() lang;
  subscriptionPlanList: any = [];

  constructor(public dialogRef: MatDialogRef<MembershipPolicyComponent>,
              public dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.subscriptionPlanList = this.data.subscriptionPlanDetails;
   }

   closePopup() {
    this.dialogRef.close();
   }

}
