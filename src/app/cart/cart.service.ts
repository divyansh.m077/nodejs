import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CART_URL } from './cart-url.config';
import { USER_URL } from '../user/user.url-config';
@Injectable({
  providedIn: 'root'
})
export class CartService {
  constructor(private http: HttpClient) {
  }

  addCartDetails(cartDetails) {
    return this.http.post(CART_URL.ADD_MULTI_CART, cartDetails);
  }

  getCartDetails(userData) {
    return this.http.get(CART_URL.CART_COUNT, { params: userData });
  }

  updateCartQuantity(cartData) {
    return this.http.put(CART_URL.UPDATE_CART, cartData);
  }

  removeCartDetails(cartDetails) {
    return this.http.post(CART_URL.REMOVE_CART, cartDetails);
  }

  getAddressList() {
    return this.http.get(CART_URL.ADDRESS_LIST );
  }
  getPaymentList() {
    return this.http.get(CART_URL.PAYMENT_LIST );
  }

  // saveOrderCheckout(orderData) {
  //   return this.http.post(CART_URL.SAVE_ORDER, orderData);
  // }

  saveOrderCheckout(orderData, paymentType) {
    let targetURL;
    if (paymentType === 1) {
      targetURL = CART_URL.COD_URL;
    } else if (paymentType === 2) {
      targetURL = CART_URL.STRIPE_URL;
    } else if (paymentType === 7) {
      targetURL = CART_URL.RAZORPAY_URL;
    } else if (paymentType === 8) {
      targetURL = CART_URL.HDFC_URL;
    } else if(paymentType === 9) {
      targetURL = CART_URL.CASH_FREE_URL;
    }
    return this.http.post(targetURL, orderData);
  }


  getUserProfileDetails() {
    return this.http.get(CART_URL.PROFILE_DETAIL );
  }

  getOrderDetailById(orderId) {
    return this.http.get(CART_URL.ORDER_DETAIL + orderId);
  }

  getWalletAmount() {
    return this.http.get(USER_URL.WALLET_AMOUNT );
  }

  getOrderStatus() {
    return this.http.get(USER_URL.ORDER_STATUS);
  }

  getStripePublicKey() {
    return this.http.get(CART_URL.STRIPE_KEY);
  }

  validateCouponDetails(couponData) {
    return this.http.post(CART_URL.COUPON_VALIDATE, couponData);
  }

  stripeCheckOut(stripeDetails) {
    return this.http.post(CART_URL.STRIPE_CHECK_OUT, stripeDetails );
  }

  getShippementAmount(cartData) {
    return this.http.post(CART_URL.SHIPMENT_CHARGE_URL, cartData);
  }
  
  razorPayCheckOut(razorPayDetails) {
    return this.http.post(CART_URL.RAZORPAY_PAYMENT_URL, razorPayDetails);
  }

  cashFreeCheckOut(orderDetails) {
    return this.http.post(CART_URL.CASH_FREE_PAYMENT_URL, orderDetails);
  }

  getRazorPayPublicKey() {
    return this.http.get(CART_URL.GET_RAZOR_PAY_KEY);
  }

  releaseOrder(orderId) {
    return this.http.get(CART_URL.RELEASE_ORDER + orderId);
  }

  cashFreeOrderReturn(orderId) {
    return this.http.get(CART_URL.CASH_FREE_RELEASE_ORDER + orderId);
  }

  stripeReleaseOrder(orderId) {
    return this.http.get(CART_URL.ORDER_STRIPE + orderId);
  }

  caluculateMaximumQuantity(item: any) {
    if(item.maximumQuantity) {
      return item.maximumQuantity;
    } else {
      let max: any = item.quantity - item.soldOut;
      return max;
    }
  }

  getCashBackMeter() {
    return this.http.get(CART_URL.CASH_BACK_METER_URL );
  }
}
