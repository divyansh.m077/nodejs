export const environment = {
  production: true,
  serverUrl: 'https://sv-dev.purchasecommerce.com:1443/',
  assetsUrl: `https://pwa.purchasecommerce.com/`,
  becomeaVendorURL: '',
  SSRPort: 8080,
  cookieConfig: {
    path: '/',
    domain: 'pwa.purchasecommerce.com',
    expires: '01.01.2021',
    secure: true,
    httpOnly: false
  }
};
