import { Component, OnInit, OnDestroy , Inject} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { concat, uniqBy, find } from 'lodash';
import { Language, TranslationService } from 'angular-l10n';
import { MatSnackBar } from '@angular/material';

import { ProductService } from '../products.service';
import { HomeService } from './../../home/home.service';
import { TRENDING_PRODUCT_SETTINGS } from '../../app-setting';
import { API_STATUS_CODE } from '../../app.constants';
import { ComonService } from './../../common/comon.service';
import { SNACKBAR_ALERT } from '../../app-setting';
import { SharedService } from '../../shared/shared.service';
@Component({
  selector: 'app-trending-products',
  templateUrl: './trending-products.component.html',
  styleUrls: ['./trending-products.component.scss']
})
export class TrendingProductsComponent implements OnInit, OnDestroy {

  @Language() lang: string;
  public from: string;
  public productsCount: any = TRENDING_PRODUCT_SETTINGS;
  public productList: any = [];
  public rootFrom: any;
  public productListTotalLength: number;
  public productSubscribe$: any;
  public compareCount: any;
  siteSettingDetails: any;
  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private comonService: ComonService,
    private homeService: HomeService,
    private snackBar: MatSnackBar,
    private translation: TranslationService,
    private sharedService: SharedService
  ) {
    this.route.params.subscribe(params => {
      this.rootFrom = params.from;
    });
    this.from = 'products';
    this.compareCount = 0;
    this.productSubscribe$ = this.homeService.getSignInStatus().subscribe((response: any) => {
      if (response === false) {
        this.productsCount.START_COUNT = 0;
        this.productList = [];
        this.getRespectiveProducts();
      }
    });
  }

  ngOnInit() {
    this.getCompareCount();
    this.productsCount.START_COUNT = 0;
    this.productList = [];
    this.getRespectiveProducts();
  }

  getCompareCount() {
    this.siteSettingDetails = this.sharedService.getSiteSettingDetails();
    if (this.siteSettingDetails) {
      this.compareCount = this.siteSettingDetails.compareLimit;
    } else {
      this.compareCount = 0;
    }
  }

  getRespectiveProducts() {
    if (this.rootFrom === 'HOT_PRODUCTS') {
      this.getHotProductList();
    } else if (this.rootFrom === 'BEST_SELLING_PRODUCTS') {
      this.getBestSellingProductList();
      this.getBestSellingProductCount();
    } else if (this.rootFrom === 'FEATURED_PRODUCTS') {
      this.getFeaturedProductList();
    }
  }

  getHotProductList() {
    const productObj = {
      'startLimit': this.productsCount.START_COUNT,
      'endLimit': this.productsCount.END_COUNT,
      'targetCurrency': 'USD',
    };
    this.productService.getHotProductList(productObj).subscribe((response: any) => {
      this.productList = concat(this.productList, response.product);
      this.productList = uniqBy(this.productList, 'productId');
      this.productList = this.comonService.findCartCompareSelection(this.productList);
      this.productListTotalLength = response.productLength;
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getBestSellingProductList() {
    const productObj = {
      'startLimit': this.productsCount.START_COUNT,
      'endLimit': this.productsCount.END_COUNT,
    };
    this.productService.getBestSellingProductList(productObj)
    .subscribe((response: any) => {
      this.productList = concat(this.productList, response.data);
      this.productList = uniqBy(this.productList, 'productId');
      this.productList = this.comonService.findCartCompareSelection(this.productList);
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getBestSellingProductCount() {
    this.productService.getBestSellingProductCount().subscribe((response: any) => {
      if (response) {
        this.productListTotalLength = response.productLength;
      } else {
        this.productListTotalLength = 0;
      }
    }, (error: any) => {
      this.productListTotalLength = 0;
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getFeaturedProductList() {
    const productObj = {
      'startLimit': this.productsCount.START_COUNT,
      'endLimit': this.productsCount.END_COUNT,
    };
    this.productService.getFeaturedProductList(productObj)
    .subscribe((response: any) => {
      this.productList = concat(this.productList, response.product);
      this.productList = uniqBy(this.productList, 'productId');
      this.productList = this.comonService.findCartCompareSelection(this.productList);
      this.productListTotalLength = response.productLength;
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  showMoreProducts() {
    this.productsCount.START_COUNT += this.productsCount.SHOW_MORE_COUNT;
    if (this.rootFrom === 'HOT_PRODUCTS') {
      this.getHotProductList();
    } else if (this.rootFrom === 'BEST_SELLING_PRODUCTS') {
      this.getBestSellingProductList();
    } else if (this.rootFrom === 'FEATURED_PRODUCTS') {
      this.getFeaturedProductList();
    }
  }

  getProductActionDetails($event: any) {
    if ($event.action === 'wishList') {
      const productData = find(this.productList, {'productId': $event.productId});
      productData.favUser = $event.status === 1 ? 1 : 0;
    } else if ($event.action === 'cart') {
      const productData = find(this.productList, {'productId': $event.productId});
      productData.cartUser = 1;
    } else {
      const productData = find(this.productList, {'productId': $event.productId});
      productData.compareProduct = 1;
    }
  }

  ngOnDestroy() {
    if (this.productSubscribe$) {
      this.productSubscribe$.unsubscribe();
    }
  }
}
