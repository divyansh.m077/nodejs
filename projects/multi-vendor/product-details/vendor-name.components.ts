import { Component, OnInit, Input } from '@angular/core';


import { Language } from 'angular-l10n';


@Component({
  selector: 'app-vendor-name',
  templateUrl: './vendor-name.component.html',
  styleUrls: ['./vendor-name.component.scss']
})
export class VendorProductNameComponent implements OnInit {
  @Input() productDetail;
  @Language() lang: string;
  constructor() {}
  ngOnInit() {

  }

}
