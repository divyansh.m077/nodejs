import { Component, OnInit, Input, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { TranslationService, Language } from 'angular-l10n';
import { groupBy, forEach, find } from 'lodash';
import { ProductService } from '../../../src/app/products/products.service';
import { HomeService } from '../../../src/app/home/home.service';
import { CartService } from '../../../src/app/cart/cart.service';
import { ComonService } from '../../../src/app/common/comon.service';
import { API_STATUS_CODE, COUNTRY_CODE } from '../../../src/app/app.constants';
import { SNACKBAR_ALERT, SNACKBAR_SUCCESS, SNACKBAR_WARNING } from '../../../src/app/app-setting';
import { StorageControllerService } from '../../../src/app/storage-controller.service';
@Component({
  selector: 'app-list-vendors',
  templateUrl: './list-vendors.component.html',
  styleUrls: ['./list-vendors.component.scss']
})
export class ListVendorsComponent implements OnInit {
  @Input() productDetail;
  @Input() showDiscount;
  @Language() lang: string;
  products: any = {};
  public productGroupOptionList: any;
  cartProductDetail;
  countryCode = COUNTRY_CODE;

  showVendorList = 5;
  constructor(
    private storage: StorageControllerService,
    private router: Router,
    private translation: TranslationService,
    private productService: ProductService,
    private homeService: HomeService,
    private cartService: CartService,
    private comonService: ComonService,
    private snackBar: MatSnackBar,
  ) {
      // this.getCartDetail();
    }

  gotoCart () {
    this.router.navigate(['/cart-checkout']);
  }

  buyProduct(vendorProductDetail, vendorProduct) {
    this.addCart(vendorProductDetail, vendorProduct, true);
  }

  addCart(vendorProductDetail, vendorProduct, fromBuyNow?) {
    if (vendorProductDetail) {
      const productOption = groupBy(vendorProductDetail.productOption, 'attributeType');
      this.productGroupOptionList = this.productService.groupingProductOptionDetails(productOption, vendorProductDetail);
    }
    this.products.vendorProductDetail = vendorProductDetail;
    this.products.vendorProduct = vendorProduct;
    // this.vendorProductAction.emit(this.products);
    this.getVendorProductActionDetails(this.products, fromBuyNow);
  }

  getVendorProductActionDetails(product, fromBuyNow?) {
    // product.vendorProductDetail.productId = product.vendorProduct.productId;
    // product.vendorProductDetail.vendorId = product.vendorProduct.vendorId;
    // product.vendorProductDetail.companyName = product.vendorProduct.companyName;
    // product.vendorProductDetail.minimumQuantity = product.vendorProduct.minimumQuantity;
    // product.vendorProductDetail.maximumQuantity = product.vendorProduct.maximumQuantity;
    // product.vendorProductDetail.quantity = product.vendorProduct.quantity;
    // product.vendorProductDetail.soldOut = product.vendorProduct.soldOut;
    // product.vendorProductDetail.discountAmount = product.vendorProduct.discountAmount;
    // product.vendorProductDetail.salesPrice = product.vendorProduct.salesPrice;
    // product.vendorProductDetail.vendorProductId = product.vendorProduct.vendorProductId;
    // product.vendorProductDetail.taxAmount = product.vendorProduct.taxAmount;
       product.vendorProduct.mainImage = product.vendorProductDetail.mainImage;
       product.vendorProduct.productName = product.vendorProductDetail.productName;
       product.vendorProduct.productCode = product.vendorProductDetail.productCode;
       product.vendorProduct.productOption = product.vendorProductDetail.productOption;
       this.addProductToCart(product.vendorProduct, fromBuyNow);
  }

  addProductToCart($event: any, fromBuyNow?) {
    if ($event.productOption.length === 0) {
      this.addProductToCartWithOption($event, false, fromBuyNow);
    } else {
      const optionRequired = find(this.productGroupOptionList, { 'optionRequired': 0 });
      if (optionRequired) {
        const isOptionSelected = find(optionRequired.options, { 'selected': true });
        if (isOptionSelected) {
          this.addProductToCartWithOption($event, true, fromBuyNow);
        } else {
          this.snackBar.open(this.translation.translate('alertProductOption'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
        }
      } else {
        const optionNotRequired = find(this.productGroupOptionList, { 'optionRequired': 1 });
        if (optionNotRequired) {
        const isOptionSelected = find(optionNotRequired.options, { 'selected': true });
        if (isOptionSelected) {
          this.addProductToCartWithOption($event, true, fromBuyNow);
        } else {
          this.addProductToCartWithOption($event, false, fromBuyNow);
        }
      }
    }
  }
  }

  addProductToCartWithOption($event, isOption, fromBuyNow?) {
    const quantity = ($event.minimumQuantity && Number($event.minimumQuantity) !== 0) ? $event.minimumQuantity : 1;
    const productPrice = $event.discountAmount === 0 ? $event.salesPrice : $event.discountAmount;
    let taxAmount;
    if ($event.taxType === 1) {
      taxAmount = $event.taxAmount;
    } else {
      taxAmount = (productPrice * $event.taxValue) / 100;
    }
    const productOptionDetails = [];
    if (isOption) {
      forEach(this.productGroupOptionList, (option) => {
        const optionSelected = find(option.options, { 'selected': true });
        if (optionSelected) {
          productOptionDetails.push({
            'productOptionId': optionSelected.productOptionDetailsId,
            'optionValueName': optionSelected.productOptionValue,
            'optionName': optionSelected.attributeType,
            'productOptionPrice': optionSelected.productOptionPrice
          });
        }
      });
    }
    if (!!this.storage.get('id')) {
      const cartObj = {
        'productCart': [{
          'productId': $event.productId,
          'price': productPrice,
          'cartQuantity': quantity,
          'subTotal': productPrice * quantity,
          'taxTotal': taxAmount * quantity,
          'productOption': isOption ? productOptionDetails : [],
          'mrpSalesPrice': productPrice,
          'vendorId': $event.vendorId,
          'companyName': $event.companyName,
          'vendorProductId': $event.vendorProductId
        }],
      };
      this.cartService.addCartDetails(cartObj).subscribe((response: any) => {
        this.homeService.setCartCountChange(true);
        this.snackBar.open(this.translation.translate(response.data), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        } else if (error.status === API_STATUS_CODE.preconditionFailed) {
          const productDetails = error.error.data[0];
          this.comonService.showAlertForProductQuantity(productDetails, $event);
        }
      });
    } else {
      const cartStorage = [];
      const productObj = {
        'productId': $event.productId,
        'taxValue': $event.taxValue,
        'taxAmount': taxAmount,
        'taxType': $event.taxType,
        'discountAmount': $event.discountAmount,
        'salesPrice': $event.salesPrice,
        'productName': $event.productName,
        'productImage': $event.mainImage,
        'productCode': $event.productCode,
        'shippingValue': $event.shippingValue,
        'freeShipping': $event.freeShipping,
        'quantity': $event.quantity,
        'minimumQuantity': $event.minimumQuantity,
        'maximumQuantity': $event.maximumQuantity,
        'lowStockName': $event.lowStockName,
        'currencySymbol': $event.currencySymbol,
        'cartQuantity': quantity,
        'subTotal': productPrice * quantity,
        'taxTotal': taxAmount * quantity,
        'productOption': isOption ? productOptionDetails : [],
        'soldOut': $event.soldOut,
        'mrpSalesPrice': productPrice,
        'cartUser': 1,
        'vendorId': $event.vendorId,
        'companyName': $event.companyName,
        'vendorProductId': $event.vendorProductId
      };
      cartStorage.push(productObj);
      if (this.storage.get('cartStorage')) {
        const storedItems = JSON.parse(this.storage.get('cartStorage'));
        forEach(storedItems, (items) => {
          cartStorage.push(items);
        });
      }
      this.storage.set('cartStorage', JSON.stringify(cartStorage));
      this.homeService.setCartCountChange(true);
      this.snackBar.open(this.translation.translate('alertProductCart'), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      $event.cartUser = 1;
    }
    forEach(this.productDetail.vendorProduct, (products) => {
      if ($event.productId === products.productId && $event.vendorId === products.vendorId) {
        products.cartUser = 1;
      }
    });
    if (this.storage.get('cartStorage')) {
      const localCartData = JSON.parse(this.storage.get('cartStorage'));
      forEach(this.productDetail.vendorProduct, (product) => {
        forEach(localCartData, (cart) => {
          if (product.productId === cart.productId && product.vendorId === cart.vendorId) {
            product.cartUser = 1;
          }
        });
      });
    }
    if (fromBuyNow) {
      this.router.navigate(['/cart-checkout']);
    }
  }

  getCartCount() {
    if (this.storage.get('id')) {
      const cartObj = {
        'targetCurrency': 'USD'
      };
      this.comonService.getCartCount(cartObj).subscribe((cartProduct: any) => {
        if (cartProduct) {
          this.cartProductDetail = cartProduct.product;
        }
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
    }
  }

  ngOnInit() {
    if (!!this.storage.get('id') && this.storage.get('previousUrl') === '/signin' && this.productDetail
    && (!!this.storage.get('cartStorage') || this.comonService.localCartData)) {
    this. setCartUser('afterLogin');
    }
    this.getCartDetail();
  }

  getCartDetail() {
    if (this.storage.get('id')) {
      this.getCartCount();
    }
    if (this.productDetail) {
      // goto cart button changed on local storage
      if (!this.storage.get('id')) {
        this.setCartUser();
      } else {
        // goto cart button changed on api call
        if (this.cartProductDetail) {
          if (this.productDetail.vendorProduct.length > 0) {
            forEach(this.productDetail.vendorProduct, (product) => {
              forEach(this.cartProductDetail, (cart) => {
                if (product.productId === cart.productId && product.vendorId === cart.vendorId) {
                  // cart user is 1 button changed to go to cart
                  product.cartUser = 1;
                }
                if (this.productDetail.productCatalogId === cart.productId && this.productDetail.vendorId === cart.vendorId) {
                  // cart user is 1 button changed to go to cart
                  this.productDetail.cartUser = 1;
                }
              });
            });
          }
          forEach(this.cartProductDetail, (cart) => {
            if (this.productDetail.productCatalogId === cart.productId && this.productDetail.vendorId === cart.vendorId) {
              // cart user is 1 button changed to go to cart
              this.productDetail.cartUser = 1;
            }
          });
        }
      }
    }
  }

  setCartUser(afterLogin?) {
    const cartExist = this.storage.get('cartStorage');
    const localCartData = cartExist ? JSON.parse(this.storage.get('cartStorage')) : 
                          (this.comonService.localCartData ? this.comonService.localCartData : null);
        // Clear vendor cart setting
        if (!localCartData) {
          forEach(this.productDetail.vendorProduct, (product) => {
              product.cartUser = 0;
              this.productDetail.cartUser = 0;
          });
        }
        // Vendor add to cart setting
        if (this.productDetail && this.productDetail.vendorProduct && this.productDetail.vendorProduct.length > 0) {
        this.productDetail.cartUser = 0;
        forEach(this.productDetail.vendorProduct, (product) => {
          product.cartUser = 0;
          forEach(localCartData, (cart) => {
              if (product.vendorProductId === cart.vendorProductId && product.vendorId === cart.vendorId) {
                // cart user is 1 button changed to go to cart
                product.cartUser = 1;
              }
          });
        });
        }
        // Main vendor cart setting
        forEach(localCartData, (cart) => {
          if (this.productDetail.productCatalogId === cart.productId && this.productDetail.vendorId === cart.vendorId) {
            // cart user is 1 button changed to go to cart
            this.productDetail.cartUser = 1;
          }
        });
        if (afterLogin && (!!this.storage.get('cartStorage') || this.comonService.localCartData)) {
          this.storage.get('cartStorage') ? this.storage.remove('cartStorage') : this.comonService.localCartData = null;
        }
  }
  increaseVendorList() {
    this.showVendorList += 5;
  }
}
