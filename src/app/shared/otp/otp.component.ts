import { Component, OnInit, Inject } from '@angular/core';
import { Router, Event } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Language, TranslationService } from 'angular-l10n';
import { MatDialog, MAT_DIALOG_DATA, MatSnackBar, MatDialogRef } from '@angular/material';
import { OTP_RULE, SNACKBAR_ALERT, SNACKBAR_SUCCESS, SNACKBAR_WARNING } from '../../app-setting';
import { CreateAddressComponent } from '../create-address/create-address.component';
import { UserService } from '../../user/user.service';
import { HomeService } from '../../home/home.service';
import { StorageControllerService } from '../../storage-controller.service';
@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss']
})
export class OtpComponent implements OnInit {
  @Language() lang: string;
  otpForm: FormGroup;
  otpRule = OTP_RULE;
  otpTimer;
  disableResendOtp: boolean = false;
  resendOtpTrigger: number = 1;

  constructor(
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private translation: TranslationService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private matDialogRef: MatDialogRef<OtpComponent>,
    public userService: UserService,
    public homeService: HomeService,
    private storage: StorageControllerService,
    private router: Router,
  ) {
    this.otpForm = this.formBuilder.group({
      otp: [null, [Validators.required, Validators.minLength(this.otpRule.LENGTH), Validators.pattern(this.otpRule.PATTERN)]]
    })
    this.setOtpTimeout();
  }

  ngOnInit() {
  }

  // Send OTP 
  sendOtp() {
    if (this.otpForm.invalid) {
      return;
    }
    if(this.data.gender) {
      this.profileUpdate();
    } else {
      this.userRegistration();
    }
    
  }

  userRegistration() {
    this.userService.saveUserRegisterationDetails({
      'firstName': this.data.registerObj.firstName,
      'lastName': this.data.registerObj.lastName,
      'mobileNumber': this.data.registerObj.userMobile,
      'email': this.data.registerObj.userEmail,
      'password': this.data.registerObj.password,
      'isCaptchaVerified': this.data.registerObj.isCaptchaVerified,
      'otp': this.otpForm.controls.otp.value
    }).subscribe((response: any) => {

      const userDetails = response.data[0];
      const name = userDetails.firstName + ' ' + userDetails.lastName;
      this.storage.set('token', userDetails.token);
      this.storage.set('userName', name);
      this.storage.set('userDetails', JSON.stringify(userDetails));
      if (userDetails.userImage) {
        this.storage.set('userImage', userDetails.userImage);
      }
      this.storage.set('id', userDetails.userId);      
      this.homeService.setSignInStatus(true);
      this.homeService.setCartCountChange(true);
      this.router.navigate(['/profile']);
      this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      this.createAddress();
    }, (errorResponse: any) => {
      this.matDialogRef.close();
      const errorKey = (errorResponse.error.message == "SOMETHING_WEND_WRONG_UNABLE_TO_UPDATE_PROFILE" ? "INVALID_OTP" : errorResponse.error.message);
      this.snackBar.open(this.translation.translate(errorKey), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    });
  }

  profileUpdate() {
    this.userService.updateUserProfile({
      "firstName": this.data.firstName,
      "lastName": this.data.lastName,
      "userEmail": this.data.userEmail,
      "userMobile": this.data.userMobile,
      "gender": this.data.gender,
      "isEmailSubscription": this.data.isEmailSubscription,
      "dateOfBirth": this.data.dateOfBirth,
      "otp": this.otpForm.controls.otp.value
    }).subscribe((response: any) => { 
      if(response) {
        this.matDialogRef.close();
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);  
      }
    },(errorResponse: any) => {
      this.matDialogRef.close();
      const errorKey = (errorResponse.error.message == "SOMETHING_WEND_WRONG_UNABLE_TO_UPDATE_PROFILE" ? "INVALID_OTP" : errorResponse.error.message);
      this.snackBar.open(this.translation.translate(errorKey), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    });
  }

  // Create new Address
  createAddress() {
    this.matDialogRef.close();
    const addDialogRef = this.dialog.open(CreateAddressComponent, {
      width: '40%',
      height: 'auto',
      disableClose: true
    });
  }

  // Set timeout
  setOtpTimeout() {
    this.disableResendOtp = true; 
    this.otpTimer = 60;
    let interval = setInterval(() => {
      if (this.otpTimer > 1) {
        this.otpTimer -= 1;
      } else {
        this.disableResendOtp = false;
        clearInterval(interval);
      }
    }, 1000)
  }

  // Resend OTP
  resendOTP() {
    // Close OTP popup when ReSend button trigger exceed 3 times
    if (this.resendOtpTrigger > 3) {
      this.matDialogRef.close();
      return;
    }
    this.resendOtpTrigger += 1;
    if(this.data.gender) {
      this.resendProfileOTP();
    } else {
      this.resendRegisterOTP();
    }
  }

  resendRegisterOTP() {
    let reqObj: any = {
      "mobileNumber": this.data.mobileNumber
    };
    this.userService.resendRegisterOtp(reqObj).subscribe((response: any) => {
      this.setOtpTimeout();
      this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
    }, (errorResponse: any) => {
      this.setOtpTimeout();
      this.snackBar.open(this.translation.translate(errorResponse.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    });
  }

  resendProfileOTP() {
    let reqObj: any = {
      "firstName": this.data.firstName,
      "lastName": this.data.lastName,
      "userEmail": this.data.userEmail,
      "userMobile": this.data.userMobile,
      "gender": this.data.gender,
      "isEmailSubscription": this.data.isEmailSubscription,
      "dateOfBirth": this.data.dateOfBirth
    };
    this.userService.saveUserProfileDetails(reqObj).subscribe((response: any) => { 
      if(response) {
        this.setOtpTimeout();
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      }
    }, (error: any) => {
        this.setOtpTimeout();
        this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    })
  }
}
