import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';

import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';

import { ALERT_SYSTEM_TITLE } from '../../app.constants';

@Component({
  selector: 'app-confirmation-popup',
  templateUrl: './confirmation-popup.component.html',
  styleUrls: ['./confirmation-popup.component.scss']
})
export class ConfirmationPopupComponent implements OnInit {
  alterTypes = ALERT_SYSTEM_TITLE;
  @Output() actionEvent = new EventEmitter<boolean>();
  constructor(public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

  confirmAction(isClose?) {
    if (isClose) {
      this.dialog.closeAll();
      this.actionEvent.emit(false);
    } else {
      this.actionEvent.emit(true);
    }
  }
}
