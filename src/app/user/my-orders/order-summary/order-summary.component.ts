import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import { CurrencyPipe } from '@angular/common';

import { Language, TranslationService } from 'angular-l10n';
import { MatDialog, MatSnackBar } from '@angular/material';
import { forEach, find, remove, filter, cloneDeep, uniqBy } from 'lodash';

import { UserService } from '../../../user/user.service';
import { API_STATUS_CODE, TRACK_ORDER_STATUS } from '../../../app.constants';
import { OrderCancelComponent } from '../order-cancel/order-cancel.component';
import { CartService } from '../../../cart/cart.service';
import { SharedService } from '../../../shared/shared.service';
import { TrackingDetailsComponent } from './tracking-details/tracking-details.component';
import { ORDER_SUMMARY_IMAGES, SNACKBAR_ALERT, SNACKBAR_SUCCESS } from './../../../app-setting';
import { TrackingComponent } from './tracking/tracking.component';
export interface PeriodicElement {
  trackingId:number;
  status: string;
  weight: number;
  length: number;
  height: number;
  width: number;
  carriervalue: number;
  cod:number;
  quantity: number;
}

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class OrderSummaryComponent implements OnInit {
  OrderStatusRef: any = TRACK_ORDER_STATUS;
  dimensionList: any;
  packageDetails: any;
  displayedColumns: string[] = ['trackingId', 'status', 'weight', 'length', 'height', 'width', 'carriervalue', 'codValue', 'quantity'];
  currencyFormat: any;
  currency: any;
  @Language() lang: string;
  orderDetails: any;
  orderId: any;
  discount: number[];
  subTotal: number;
  estimatedTax: any;
  shippingFee: any;
  grandTotal: any;
  cancelOrder: boolean;
  orderStatus;
  orderTracking: any;
  paymentList: any[] = [];
  showDiscount: any;
  prodDetails: any;
  walletAmount;
  expandEnabled = true;
  trackOptionStaus;
  trackCompleteStatus;
  strikeoutAmount: number;
  isVendor: number;
  grossTotal: number;
  stripeAmount: number;
  codAmount: number;
  razorpayAmount: number;
  orderStatusId;
  hdfcAmount: any;
  cashfreeAmount;
  advanceAmount: any;
  constructor(private cartService: CartService,
              private sharedService: SharedService,
              private matDialog: MatDialog,
              private userService: UserService,
              private route: Router,
              private activatedRoute: ActivatedRoute,
              private snackBar: MatSnackBar,
              private translation: TranslationService,
              private currencyPipe: CurrencyPipe) {
    this.activatedRoute.params.subscribe(params => {
      this.orderId = params.id;
      this.isVendor = Number(params.isVendor);
    });
    this.trackOptionStaus = [];
    this.subTotal = 0;
    this.grossTotal = 0;
    this.getSiteSettingProdDetails();
    this.trackCompleteStatus = [];
  }

  getSiteSettingProdDetails() {
    this.prodDetails = this.sharedService.getSiteSettingDetails();
    if (this.prodDetails) {
      this.showDiscount = this.prodDetails.productSettings.displayDiscountedPrice;
      const currency = this.currencyPipe.transform(1, this.prodDetails.currencyCode, 'symbol-narrow');
      this.currency = currency.split('1');
      this.currencyFormat = this.prodDetails.formatCurrency === 4 ? false : true;
     }
  }

  getOrderStatus() {
    this.cartService.getOrderStatus().subscribe((response: any) => {
      if (response) {
        this.orderStatus = response.data;
        this.getOrderSummary();
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  buildTrackOptions() {
    forEach(this.orderStatus, (status) => {
      status.isCompleted = false;
      status.show = false;
    });
    this.trackOptionStaus = cloneDeep(this.orderStatus);
  }

  ngOnInit() {
    this.getOrderStatus();
    this.cancelOrder = false;
  }

  onCancelOrder() {
    const cancelDialogRef = this.matDialog.open(OrderCancelComponent, {
      data: {
        orderNo: this.orderDetails.orderDetails[0].orderNumber
      }
    });
    cancelDialogRef.afterClosed().subscribe((response: any) => {
      if (response && response.message) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        this.cancelOrder = false;
        this.getOrderSummary();
      }
    });
  }

  onBack() {
    this.route.navigate(['/profile/orders']);
  }

  getOrderSummary() {
    this.subTotal = 0;
    let orderDetail;
    orderDetail = this.isVendor === 1 ? this.orderId + '?vendor=true' : this.orderId;
    this.trackOptionStaus = [];
    this.userService.getOrderSummary(orderDetail).subscribe((response: any) => {
      if (response) {
        this.getPackageDetails();
        this.orderDetails = response.data;
        this.orderTracking = response.data.orderTracking;
        this.trackOptionStaus = response.data.orderTracking;
        if (this.orderDetails.orderTracking && this.orderDetails.orderTracking.length) {
          this.buildTrackOptions();
          this.selectionOfStatus();
        }
        this.estimatedTax = this.orderDetails.orderDetails[0].orderTax;
        this.orderStatusId = this.orderDetails.orderDetails[0].orderStatus;

        if (this.orderDetails.orderDetails[0].orderStatus === 1 ||
           this.orderDetails.orderDetails[0].orderStatus === 2 ||
            this.orderDetails.orderDetails[0].orderStatus === 6) {
          this.cancelOrder = true;
        }
        const walletDetails = find(this.orderDetails.orderTransaction, {'paymentModeId': 4});
        if (walletDetails) {
          this.walletAmount = walletDetails.amount;
          this.advanceAmount = walletDetails.advanceAmount;
        }
        // stripe payment mode id is 2
        const stripeDetails = find(this.orderDetails.orderTransaction, {'paymentModeId': 2});
        if (stripeDetails) {
          this.stripeAmount = stripeDetails.amount;
        }
        //cash on delivery(cod) payment mode id is 1
        const codDetails = find(this.orderDetails.orderTransaction, {'paymentModeId': 1});
        if (codDetails) {
          this.codAmount = codDetails.amount;
          this.advanceAmount = codDetails.advanceAmount;
        }
        const razorDetails = find(this.orderDetails.orderTransaction, {'paymentModeId': 7});
        if (razorDetails) {
          this.razorpayAmount = razorDetails.amount;
        }
        const hdfcDetails = find(this.orderDetails.orderTransaction, {'paymentModeId': 8});
        if (hdfcDetails) {
          this.hdfcAmount = hdfcDetails.amount;
        }
        const cashfreeDetails = find(this.orderDetails.orderTransaction, {'paymentModeId': 9});
        if (cashfreeDetails) {
          this.cashfreeAmount = cashfreeDetails.amount;
        }
        this.orderDetails.orderTransaction = uniqBy(this.orderDetails.orderTransaction, 'paymentModeId');
        this.paymentList = [];
        forEach(this.orderDetails.orderTransaction, (payment) => {
          let imageUrl;
          if (payment.paymentModeId === 1) {
            imageUrl = ORDER_SUMMARY_IMAGES.COD_IMAGE;
          } else if (payment.paymentModeId === 2) {
            imageUrl = ORDER_SUMMARY_IMAGES.STRIPE_IMAGE;
          } else if (payment.paymentModeId === 4) {
            imageUrl = ORDER_SUMMARY_IMAGES.WALLET_IMAGE;
          } else if (payment.paymentModeId === 7) {
            imageUrl = ORDER_SUMMARY_IMAGES.RAZOR_PAY;
          } else if (payment.paymentModeId === 8) {
            imageUrl = ORDER_SUMMARY_IMAGES.HDFC_IMAGE;
          } else if (payment.paymentModeId === 9) {
            imageUrl = ORDER_SUMMARY_IMAGES.CASH_FREE_IMAGE;
          }
          this.paymentList.push({'paymenId': payment.paymentModeId, 'imageUrl': imageUrl, 'paymentMethod' : payment.paymentMode });
        });
        forEach(this.orderDetails.productDetails, (product) => {
          let attributePrice = 0;
          forEach(product.productAttributes, (attr) => {
           attributePrice += attr.optionValuePrice;
          });
           product.strikeoutAmount = product.price + attributePrice;
           this.subTotal = this.subTotal + product.total;
        });
        this.grossTotal = this.subTotal - this.orderDetails.orderDetails[0].couponOrderDiscountAmount;
        this.orderDetails.orderDetails[0].shippingAmount = this.orderDetails.orderDetails[0].couponFreeShipping === 1 ? 0 :
                                                           this.orderDetails.orderDetails[0].shippingAmount;
      } else {
        this.orderDetails = undefined;
      }

    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  selectionOfStatus() {
    const orderCancelled = find(this.orderDetails.orderTracking, {'orderStatus': 5});
    if ((this.orderDetails.orderTracking.length === 1 || this.orderDetails.orderTracking.length > 1) && !orderCancelled) {
        this.trackOptionStaus[0].show = true;
        this.trackOptionStaus[1].show = true;
        this.trackOptionStaus[2].show = true;
        this.trackOptionStaus[3].show = true;
        const orderReturned = find(this.orderDetails.orderTracking, {'orderStatus': 7});
        const orderDeliver = find(this.orderDetails.orderTracking, {'orderStatus': 4});
        if (orderReturned && !orderDeliver) {
          this.trackOptionStaus[3].show = false;
        }
        this.makeSelectionOfStatus();
    } else {
        this.makeSelectionOfStatus();
    }
    const holdStatus = find(this.trackOptionStaus, {'statusId': 6});
    if (holdStatus) {
      this.trackOptionStaus[5].show = false;
    }
    remove(this.trackOptionStaus, {'show': false});
    this.trackCompleteStatus = filter(this.trackOptionStaus, {'isCompleted': true});
  }

  makeSelectionOfStatus() {
    forEach(this.orderDetails.orderTracking, (order) => {
      const status = find(this.trackOptionStaus, {'statusId': order.orderStatus});
      if (status && status.statusId !== 6) {
        status.isCompleted = true;
        status.show = true;
      }
    });
  }

  getPackageDetails() {
    this.userService.getPackageDetails(this.orderId).subscribe((response) => {
      if (response) {
         this.packageDetails = response;
         this.dimensionList = this.packageDetails.map(x =>x.dimensionList).flat();

      }
    }, (error) => {
         if (error.status === API_STATUS_CODE.badRequest || API_STATUS_CODE.conflict) {
        this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  openTrackingDetails() {
    this.matDialog.open(TrackingDetailsComponent, {
      data: {
        trackingDetails: this.orderDetails.orderTracking,
        statusList: this.orderStatus,
        trackingId: this.orderDetails.orderDetails ? this.orderDetails.orderDetails[0].orderNumber : null
      }
    });
  }

  openInitialTracking() {
    this.matDialog.open(TrackingComponent, {
      data: {
        trackingDetails: this.orderDetails.orderTracking,
        statusList: this.orderStatus,
        trackingId: this.orderDetails.orderDetails ? this.orderDetails.orderDetails[0].orderNumber : null
      }
    });
  }


  openTrackingStatus(trackNumber) {
     this.matDialog.open(TrackingDetailsComponent, {
      data: {
        trackingId: trackNumber
      }
    });
  }
}
