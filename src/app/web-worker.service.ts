import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WebWorkerService {

  timerWorkerInstance;
  calculatedTime;

  constructor() {}

  getHourMinuteSecFromTime(time) {
    if (typeof(window) !== 'undefined') {
      if(typeof(this.timerWorkerInstance) === 'undefined') {
        this.timerWorkerInstance = new Worker('assets/web-workers/timer-web-worker.js');
      }
      this.timerWorkerInstance.postMessage(time);
      this.timerWorkerInstance.onmessage = (event) => {
        this.calculatedTime = event.data;
      };
    }
  }

  terminateGetHourMinuteSecFromTime() {
    if ((typeof(window) !== 'undefined' && typeof(this.timerWorkerInstance) !== 'undefined')) {
      this.timerWorkerInstance.terminate();
      this.timerWorkerInstance = undefined;
    }
  }

}
