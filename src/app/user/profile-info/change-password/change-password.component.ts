import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { Language, TranslationService } from 'angular-l10n';

import { RESET_PASSWORD_RULE, SNACKBAR_SUCCESS, SNACKBAR_ALERT } from '../../../app-setting';
import { API_STATUS_CODE } from '../../../app.constants';
import { UserService } from './../../user.service';

import { SharedService } from '../../../shared/shared.service';
import { ComonService } from '../../../common/comon.service';
import { HomeService } from '../../../home/home.service';
import { MatSnackBar } from '@angular/material';
import { StorageControllerService } from '../../../storage-controller.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  @Language() lang: string;
  changePasswordForm: FormGroup;
  newPasswordRule = false;
  confirmPassword = false;
  oldPassword = false;
  isConfirmPasswordError: boolean;
  isNewPasswordError: boolean;
  constructor(
    private storage: StorageControllerService,
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private sharedService: SharedService,
    private homeService: HomeService,
    private comonService: ComonService,
    private snackBar: MatSnackBar,
    private translation: TranslationService
  ) {
    this.formInitialization();
  }

  formInitialization() {
    this.changePasswordForm = this.fb.group({
      'oldPassword': ['', [Validators.required,
      Validators.pattern(RESET_PASSWORD_RULE.PASSWORD_PATTERN)]],
      'newPassword': ['', [Validators.required,
      Validators.pattern(RESET_PASSWORD_RULE.PASSWORD_PATTERN)]],
      'confirmPassword': ['', Validators.required],
      'tokenFlag': [false],
    });
  }
  ngOnInit() {
    this.sharedService.getLocation();
    const userDetails = JSON.parse(this.storage.get('userDetails'));
    if (userDetails && (userDetails.providers === 1 || userDetails.providers === 2)) {
      this.router.navigate(['/']);
    }
  }

  showNewPasswordRules() {
    this.newPasswordRule = true;
  }

  hideNewPasswordRule() {
    this.newPasswordRule = false;
  }

  showConfirmPasswordRules() {
    this.confirmPassword = true;
  }

  hideConfirmPasswordRule() {
    this.confirmPassword = false;
  }

  showOldPasswordRules() {
    this.oldPassword = true;
  }

  hideOldPasswordRule() {
    this.oldPassword = false;
  }

  validateCurrentPassword($event) {    
    this.isNewPasswordError = ($event.target.value === this.changePasswordForm.value.oldPassword);
    if (this.changePasswordForm.controls['oldPassword'].value === this.changePasswordForm.controls['newPassword'].value) {
      this.changePasswordForm.controls['newPassword'].setValue('');
    }
  }

  validateNewPassword($event) {
      this.isNewPasswordError = ($event.target.value === this.changePasswordForm.value.oldPassword);
      if (this.changePasswordForm.controls['confirmPassword'].value !== this.changePasswordForm.controls['newPassword'].value) {
        this.changePasswordForm.controls['confirmPassword'].setValue('');
      }
  }

  validateConfirmPassword($event) {
    if ($event.target.value === this.changePasswordForm.value.oldPassword) {
      this.changePasswordForm.controls['confirmPassword'].setErrors({ 'incorrect': true });
      this.isConfirmPasswordError = true;
    } else {
      this.isConfirmPasswordError = false;
    }
  }

  onSubmit() {
    this.userService.changePassword(this.getPasswordData())
      .subscribe((response: any) => {
        if (response) {
          this.changePasswordForm.reset();
          this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
          this.homeService.setSignInStatus(false);
          this.homeService.setCartCountChange(false);
          this.logoutSession();
        }
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
  }

  logoutSession() {
    this.comonService.logoutUserSession().subscribe((response: any) => {
      if (response) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
    this.sharedService.clearUserInfoFromStorage();
    this.router.navigate(['/signin']);
  }

  getPasswordData() {
    let locationObj = this.sharedService.getTrackLocationData();
    return {
      'currentPassword': this.changePasswordForm.value.oldPassword,
      'newPassword': this.changePasswordForm.value.newPassword,
      'tokenFlag': this.changePasswordForm.value.tokenFlag,
      'longitude': locationObj.longitude,
      'latitude': locationObj.latitude,
      'createdBy': 'self'
    };
  }
}
