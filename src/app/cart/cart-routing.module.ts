import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { CartComponent } from './cart.component';
import { OrderDeliveryComponent } from './order-delivery/order-delivery.component';

const cartRoutes: Routes = [
  {
    path: 'cart-checkout',
    component: CartComponent
  },
  {
    path: 'order-summary/:id',
    component: OrderDeliveryComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      cartRoutes
    )
  ],
  declarations: [],
  exports: [RouterModule],
})
export class CartRoutingModule { }
