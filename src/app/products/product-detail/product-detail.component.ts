import { Component, OnInit, ViewChild, OnDestroy, Inject, PLATFORM_ID, Output, EventEmitter, ElementRef, Renderer2, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Title } from '@angular/platform-browser';

import { Language, TranslationService } from 'angular-l10n';
import { groupBy, forEach, take, find, findIndex } from 'lodash';
import { DragScrollDirective } from 'ngx-drag-scroll';

import { ProductService } from '../products.service';
import { HomeService } from '../../home/home.service';
import { CartService } from './../../cart/cart.service';
import { REVIEW_RULE, REV_RULE, PRODUCT_AVAILABLE_RULE } from '../../app-setting';
import { API_STATUS_CODE, LOADING_SPINNER_URL } from '../../app.constants';
import { SharedService } from '../../shared/shared.service';
import { ComonService } from '../../common/comon.service';
import { isPlatformBrowser, DOCUMENT } from '@angular/common';

import { SNACKBAR_ALERT, SNACKBAR_SUCCESS, SNACKBAR_WARNING } from '../../app-setting';
import { SeoService } from '../../shared/seo.service';
import { ReplyPopupComponent } from './reply-popup/reply-popup.component';
import { StorageControllerService } from '../../storage-controller.service';
import { NgxImgZoomComponent } from 'ngx-img-zoom';


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit, OnDestroy {
  homeVendorId;
  emitStatus: boolean;
  compareCount: any;
  compareStorage: any;
  @Language() lang: string;
  // @HostListener('window:click', ['$event']) closeShare(e) {
  //   if (e.target.parentElement.id !== 'shareButtonId') {
  //     this.shareBtns = false;
  //   }
  // }
  productReviewForm: FormGroup;
  public productFrom = 'product';
  public productId;
  public productDetail: any;
  public productOptions: any;
  public productGroupOptionList: any;
  public reviewsList: any;
  public isLoading: boolean;
  rating: number = 0;
  mainImage: any;
  zoomImage;
  from: string = 'detail';
  ratingPopup: boolean = false;
  ratingTitle: any;
  reviewRules: any = REV_RULE();
  // reviewDescMaxLength: number = REVIEW_RULE.MAX_LENGTH;
  toolTipPosition: string = 'before';
  firstimage = '';
  // 416*1.6
  zoomimage = '';
  // 416*0.6
  lensimage = '';
  signInSubscribe$: any;
  shareBtns: boolean = false;
  shareBtnsMobile: boolean = false;
  @Output() productAction = new EventEmitter<any>();
  @ViewChild('tileArrowNav', { read: DragScrollDirective }) arrowNav: DragScrollDirective;
  @ViewChild('mobileTileArrowNav', { read: DragScrollDirective }) mobTileArrowNav: DragScrollDirective;
  @ViewChild('slideshow') slideshow: any;
  @Output() reachesLeftBound = new EventEmitter<boolean>();
  @Output() reachesRightBound = new EventEmitter<boolean>();
  productOriginalAmount;
  prodDetails: any;
  showDiscount: any;
  showCount: boolean;
  showCountMessage = false;
  prodFromWhislist = false;
  prodQuantity: any;
  currencySetting: any;
  numberFormat: any;
  currencyFormat: any;
  currencyCode: any;
  maximumOrderQuantity: number;
  minimumOrderQuantity: number;
  defaultAttributePrice: number;
  selectedAttribute = [];
  attributePrice: number;
  selectedOptionValue: any;
  radioSelected: String = 'empty';
  strikeOutAmount: number;
  vendorProduct: any;
  cartProductDetail: any;
  ratingImage: string;
  slidingIndex;
  pluralMapping = {
    'replyCount': {
      '=1': 'LABEL_REPLY',
      'other': 'LABEL_REPLIES'
    }
  };
  isPriceWithTax: boolean;
  availableZipcode: any;
  productAvailableQuantity = PRODUCT_AVAILABLE_RULE;
  productAvailableDetails: any;
  imgZoom: HTMLImageElement;
  url;
  descriptionWithoutTemplate = '';
  isMenuOpen = false;
  disableArrowRight: boolean;
  disableArrowLeft: boolean;
  disableArrowRightMobile: any;
  disableArrowLeftMobile: any;
  vendorDetail: boolean;
  constructor(
    private storage: StorageControllerService,
    @Inject(PLATFORM_ID) private platform: any,
    @Inject(DOCUMENT) private _document: HTMLDocument,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductService,
    private homeService: HomeService,
    private cartService: CartService,
    private translation: TranslationService,
    private sharedService: SharedService,
    private comonService: ComonService,
    private snackBar: MatSnackBar,
    private matDialog: MatDialog,
    private titleService: Title,
    private seoService: SeoService
  ) {
    this.isPriceWithTax = false;
    this.showDiscount = 0;
    this.compareStorage = [];
    this.route.params.subscribe(params => {
      this.productId = params.productId;
      this.prodFromWhislist = params.fromWhislist ? true : false;
    });
    this.route.queryParams.subscribe(params => {
      this.homeVendorId = params.id;
    });
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    this.getCartCount();
    this.getProductDetails();
    this.getSiteSettingProdDetails();
    this.formInitialize();
    this.signInSubscribe$ = this.homeService.getSignInStatus().subscribe((response: any) => {
      if (response === true) {
        if (!!this.storage.get('id')) {
          this.getProductReviewRatingDetails();
        }
      } else {
        this.rating = 0;
        this.formInitialize();
        this.getProductDetails();
      }
    });
  }

  ngOnInit() {
    this.url = this.router.url;
    this.slidingIndex = 0;
    this.prodDetails = this.sharedService.getSiteSettingDetails();
    this.currencyCode = this.prodDetails.currencyCode;
    // this.getProductDetails();
    if (!!this.storage.get('id')) {
      this.getProductReviewRatingDetails();
    }
    // this.renderer.setStyle();
  }

  formInitialize(data?: any) {
    const reviewRule = REV_RULE();
    this.productReviewForm = this.fb.group({
      'productRating': ['', [Validators.required]],
      'reviewTitle': ['', [Validators.required, Validators.pattern(reviewRule.REVIEW_TITLE_PATTERN),
      Validators.minLength(reviewRule.REVIEW_MIN_LENGTH)]],
      'reviewDescription': ['', [Validators.required, Validators.minLength(reviewRule.MIN_LENGTH),
      Validators.maxLength(reviewRule.MAX_LENGTH)]]
    });
  }

  patternChecking(event: any) {
    const reviewPattern = REVIEW_RULE.KEYPRESS_REVIEW_TITLE_PATTERN;
    const inputChar = String.fromCharCode(event.charCode);
    if (!reviewPattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  getProductDetails() {
    this.defaultAttributePrice = 0;
    const productObj = {
      'productId': this.productId,
      'targetCurrency': 'USD',
      'vendorId' : Number(this.homeVendorId)
    };
    if (!this.homeVendorId) {
      delete productObj.vendorId;
    }
    this.isLoading = true;
    this.productService.getProductDetails(productObj).subscribe((response: any) => {
      this.isLoading = false;
      // this.smallImgs = response.data[0].productImage.map(x => x.thumbnailImage);
      if (response && response.data.length > 0) {
        this.getSimilarProductList(productObj);
        this.seoService.updateDescription(response.data[0].metaDescription);
        this.seoService.updateTitle(response.data[0].metaTitle);
        this.seoService.updateMetaKeywords(response.data[0].metaKeyword);
        this.seoService.updateOgImage(response.data[0].mainImage);
        // this.updateOgURL();
        this.productDetail = response.data[0];
        // this.descriptionWithoutTemplate = this.productDetail.productDescription.replace(/<\/?[^>]+(>|$)/g, '');
        if (!!this.storage.get('id') && this.storage.get('previousUrl') === '/signin'
            && (!!this.storage.get('cartStorage') || this.comonService.localCartData)) {
          this.setCartUserValue('afterLogin');
              } else {
                if (!this.storage.get('id')) {
                  this.setCartUserValue();
                }
              }
        this.descriptionWithoutTemplate = this.shareLinkContent(this.productDetail.productName, 
                                          this.getRawDescription(this.productDetail.productDescription));
        this.roundOff(this.productDetail.vendorProduct);
        this.minimumOrderQuantity = Number(response.data[0].minimumQuantity);
        this.maximumOrderQuantity = Number(response.data[0].maximumQuantity);
        this.productDetail.discountAmount = this.formatAmount(this.productDetail.discountAmount);
        this.prodQuantity = Number(this.productDetail.quantity) - this.productDetail.soldOut;
        this.getSiteSettingProdDetails();
        if (this.productDetail.productImage.length > 0) {
          this.changeImageForZoom(this.productDetail.productImage[0], 0);
          this.ratingImage = this.productDetail.productImage[0].smallImage;
        } else {
          this.mainImage = 'assets/images/no-image-found.jpg';
          this.ratingImage = 'assets/images/no-image-found.jpg';
        }
        if (this.productDetail.productReview.length > 2) {
          this.reviewsList = take(this.productDetail.productReview, 2);
        } else {
          this.reviewsList = this.productDetail.productReview;
        }
        if (this.productDetail.discountType === 1 && this.isPriceWithTax) {
          this.productDetail.taxDiscount = this.productDetail.taxType === 1 ?
            this.productDetail.discount :
            (((this.productDetail.discount * this.productDetail.taxValue) / 100) + this.productDetail.discount);
        } else {
          this.productDetail.taxDiscount = this.productDetail.discount;
        }
        this.ratingCalculationForPopup();
        if (this.productDetail.productOption.length > 0) {
          const productOption = groupBy(this.productDetail.productOption, 'attributeType');
          this.productGroupOptionList = this.productService.groupingProductOptionDetails(productOption, this.productDetail);
          forEach(this.productGroupOptionList, (option) => {
            forEach(option.options, (detail, i) => {
              if (i === 0 && option.optionRequired === 0) {
                detail.selected = true;
                this.defaultAttributePrice += detail.productOptionPrice;
                if (detail.attributeType === 'Select') {
                  this.selectedOptionValue = detail;
                }
                if (detail.attributeType === 'Radio') {
                  this.radioSelected = detail.productOptionValue;
                }
                this.selectedAttribute.push(detail);
              } else {
                if (i === 0 && option.optionRequired === 1 && option.attributeType === 'Select') {
                  option.options.push({
                    attributeType: 'Select', productOptionValue: 'Unselect', productOptionPrice: 0,
                    productOptionDetailsId: null, selected: 'false'
                  });
                }
                detail.selected = false;
              }
            });
          });

          if (this.productDetail.vendorProduct) {
            forEach(this.productDetail.vendorProduct, (product) => {
              forEach(this.cartProductDetail, (cart) => {
                if (product.productId === cart.productId && product.vendorId === cart.vendorId) {
                  product.cartUser = 1;
                }
                if (this.productDetail.productCatalogId === cart.productId && this.productDetail.vendorId === cart.vendorId) {
                  this.productDetail.cartUser = 1;
                }
              });
            });
            this.vendorProduct = this.productDetail;
            forEach(this.vendorProduct.vendorProduct, (vendor) => {
              // this.defaultAttributePrice += detail.productOptionPrice;
              vendor.originalAmount = vendor.discountAmount === 0 ? vendor.salesPrice : vendor.discountAmount;
              if (vendor.discountAmount > 0) {
                // vendor.discountAmount = vendor.originalAmount + this.defaultAttributePrice;
                vendor.discountAmount = this.calculateDiscountAmount(this.defaultAttributePrice, vendor);
              } else {
                vendor.salesPrice = vendor.originalAmount + this.defaultAttributePrice;
              }
              vendor.strikeOutAmount = vendor.salesPrice + this.defaultAttributePrice;
              this.updatePriceValueWithTaxAmount(vendor);
            });
            this.vendorProduct = this.productDetail;
          }
          /* multi vendor */
        } else {
          if (this.productDetail.vendorProduct) {
            forEach(this.productDetail.vendorProduct, (vendor) => {
              vendor.strikeOutAmount = vendor.salesPrice;
              this.updatePriceValueWithTaxAmount(vendor);
            });
            this.vendorProduct = this.productDetail;
          }
        }
        /* multi vendor */
        forEach(this.productDetail.vendorProduct, (product) => {
          if (product.discountType === 1 && this.isPriceWithTax) {
            product.taxDiscount = product.taxType === 1 ?
              product.discount :
              (((product.discount * product.taxValue) / 100) + product.discount);
          } else {
            product.taxDiscount = product.discount;
          }
          forEach(this.cartProductDetail, (cart) => {
            if (product.productId === cart.productId && product.vendorId === cart.vendorId) {
              product.cartUser = 1;
            }
            if (this.productDetail.productId === cart.productId && this.productDetail.vendorId === cart.vendorId) {
              this.productDetail.cartUser = 1;
            }
          });
        });
        if ( this.productDetail.vendorProduct && this.productDetail.vendorProduct.length === 0 ) {
          forEach(this.productDetail, (product) => {
            forEach(this.cartProductDetail, (cart) => {
              if (this.productDetail.vendorProductId === cart.vendorProductId && this.productDetail.vendorId === cart.vendorId) {
                this.productDetail.cartUser = 1;
              }
            });
          });
        }
        // this.productDetail.cartUser = this.checkProductInCart(this.productDetail.productId, this.productDetail.vendorId) ? 1 : 0;

        // forEach(this.productDetail.vendorProduct, (product) => {
        //   product.cartUser = this.checkProductInCart(product.productId, product.vendorId) ? 1 : 0;
        // });
        this.vendorProduct = this.productDetail;
        this.productOriginalAmount = this.productDetail.discountAmount === 0 ?
          this.productDetail.salesPrice : this.productDetail.discountAmount;
        this.calculatePrice();
        // if (!this.storage.get('id')) {
        //   this.setCartUserValue();
        // }
        // this.productDetail.compareProduct = 0;
        // if (this.storage.get('compareStorage')) {
        //   const compareData = JSON.parse(this.storage.get('compareStorage'));
        //   if (this.productDetail.vendorId) {
        //     forEach(compareData, (compare) => {
        //       if (compare.productId === this.productDetail.productCatalogId && compare.vendorId === this.productDetail.vendorId) {
        //         this.productDetail.compareProduct = 1;
        //       }
        //       if (this.productDetail.similarProduct && this.productDetail.similarProduct.length !== 0) {
        //         forEach(this.productDetail.similarProduct, (product) => {
        //           if (compare.productId === product.productId && compare.vendorId === product.vendorId) {
        //             product.compareProduct = 1;
        //           }
        //         });
        //       }
        //     });
        //   } else {
        //     forEach(compareData, (compare) => {
        //       if (compare.productId === this.productDetail.productCatalogId) {
        //         this.productDetail.compareProduct = 1;
        //       }
        //       if (this.productDetail.similarProduct && this.productDetail.similarProduct.length !== 0) {
        //         forEach(this.productDetail.similarProduct, (product) => {
        //           if (compare.productId === product.productId) {
        //             product.compareProduct = 1;
        //           }
        //         });
        //       }
        //     });
        //   }
        // }
      } else {
        this.productDetail = undefined;
      }
    }, (error: any) => {
      this.isLoading = false;
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getSimilarProductList(data: any) {
    this.productService.getSimilarProducts(data).subscribe(
      (response: any) => {
        this.productDetail.similarProduct = response.data;
        this.prepareSimilarProductList();
      },
      (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      }
    );
  }

  prepareSimilarProductList() {
    this.productDetail.compareProduct = 0;
      if (this.storage.get('compareStorage')) {
        const compareData = JSON.parse(this.storage.get('compareStorage'));
        if (this.productDetail.vendorId) {
          forEach(compareData, (compare) => {
            if (compare.productId === this.productDetail.productCatalogId && compare.vendorId === this.productDetail.vendorId) {
              this.productDetail.compareProduct = 1;
            }
            if (this.productDetail.similarProduct && this.productDetail.similarProduct.length !== 0) {
              forEach(this.productDetail.similarProduct, (product) => {
                if (compare.productId === product.productId && compare.vendorId === product.vendorId) {
                  product.compareProduct = 1;
                }
              });
            }
          });
        } else {
          forEach(compareData, (compare) => {
            if (compare.productId === this.productDetail.productCatalogId) {
              this.productDetail.compareProduct = 1;
            }
            if (this.productDetail.similarProduct && this.productDetail.similarProduct.length !== 0) {
              forEach(this.productDetail.similarProduct, (product) => {
                if (compare.productId === product.productId) {
                  product.compareProduct = 1;
                }
              });
            }
          });
        }
      }
      if (!this.storage.get('id')) {
        this.setCartUserValue();
      }
  }

  setCartUserValue(afterLogin?) {
    forEach(this.productDetail.similarProduct, (product) => {
      product.cartUser = 0;
    });
    if (!!this.storage.get('cartStorage') || this.comonService.localCartData) {
      const cartData = !!this.storage.get('cartStorage') ? JSON.parse(this.storage.get('cartStorage')) : 
                        (this.comonService.localCartData ? this.comonService.localCartData : []);
      forEach(cartData, (cart) => {
        forEach(this.productDetail.similarProduct, (product) => {
          if (cart.productId === product.productId) {
            product.cartUser = 1;
          }
        });
        if (this.productDetail.productCatalogId === cart.productId) {
          this.productDetail.cartUser = 1;
        }
      });
    }
    if (afterLogin && (!!this.storage.get('cartStorage') || this.comonService.localCartData)
    && (!this.productDetail.vendorProduct || this.productDetail.vendorProduct.length === 0)) {
      this.storage.get('cartStorage') ? this.storage.remove('cartStorage') : this.comonService.localCartData = null;
    }

  }

  checkProductInCart(productId, vendorId) {
    return findIndex(this.cartProductDetail, {
      productId: productId,
      vendorId: vendorId
    }) >= 0;
  }

  getSiteSettingProdDetails() {
    this.prodDetails = this.sharedService.getSiteSettingDetails();
    if (this.prodDetails) {
      this.isPriceWithTax = this.prodDetails.productSettings.displayProductPriceWithTax === 1 ? true : false;
      this.showDiscount = this.prodDetails.productSettings.displayDiscountedPrice;
      this.compareCount = this.prodDetails.compareLimit;
    }
    if (this.prodDetails.productSettings.displayAvailableQuantity === 1) {
      if (this.prodQuantity <= this.prodDetails.productSettings.displayRemainingQuantiy) {
        this.showCountMessage = true;
      }
    }
    if (this.prodDetails.systemTitle) {
      this.titleService.setTitle(this.prodDetails.systemTitle);
    }
  }

  ratingCalculationForPopup() {
    this.productDetail.reviewandRating[0].fiveStarStatus = '0%';
    this.productDetail.reviewandRating[0].fourStarStatus = '0%';
    this.productDetail.reviewandRating[0].threeStarStatus = '0%';
    this.productDetail.reviewandRating[0].twoStarStatus = '0%';
    this.productDetail.reviewandRating[0].oneStarStatus = '0%';
    this.productDetail.reviewandRating[0].fiveStarStatus = (this.productDetail.reviewandRating[0].fivestarRatingCount
      / this.productDetail.reviewandRating[0].ratingProductCount) * 100 === Infinity ? '0%' :
      ((this.productDetail.reviewandRating[0].fivestarRatingCount
        / this.productDetail.reviewandRating[0].ratingProductCount) * 100).toString() + '%';
    this.productDetail.reviewandRating[0].fourStarStatus = (this.productDetail.reviewandRating[0].fourStarRatingCount
      / this.productDetail.reviewandRating[0].ratingProductCount) * 100 === Infinity ? '0%' :
      ((this.productDetail.reviewandRating[0].fourStarRatingCount
        / this.productDetail.reviewandRating[0].ratingProductCount) * 100).toString() + '%';
    this.productDetail.reviewandRating[0].threeStarStatus = (this.productDetail.reviewandRating[0].threeStarRatingCount
      / this.productDetail.reviewandRating[0].ratingProductCount) * 100 === Infinity ? '0%' :
      ((this.productDetail.reviewandRating[0].threeStarRatingCount
        / this.productDetail.reviewandRating[0].ratingProductCount) * 100).toString() + '%';
    this.productDetail.reviewandRating[0].twoStarStatus = (this.productDetail.reviewandRating[0].twoStarRatingCount
      / this.productDetail.reviewandRating[0].ratingProductCount) * 100 === Infinity ? '0%' :
      ((this.productDetail.reviewandRating[0].twoStarRatingCount
        / this.productDetail.reviewandRating[0].ratingProductCount) * 100).toString() + '%';
    this.productDetail.reviewandRating[0].oneStarStatus = (this.productDetail.reviewandRating[0].oneStarRatingCount
      / this.productDetail.reviewandRating[0].ratingProductCount) * 100 === Infinity ? '0%' :
      ((this.productDetail.reviewandRating[0].oneStarRatingCount
        / this.productDetail.reviewandRating[0].ratingProductCount) * 100).toString() + '%';
  }

  changeImageForZoom(imageData: any, index?) {
    // this.renderer.setStyle(this.imageZoomComponent.nativeElement, )
    this.slidingIndex = index;
    this.mainImage = LOADING_SPINNER_URL;
    // this.zoomImage = LOADING_SPINNER_URL;
    this.zoomImage = '';
    if (this.imgZoom) {
      this.imgZoom.src = '';
    }

    const img = new Image();
    img.src = imageData.thumbnailImage;
    img.onload = () => {
      this.mainImage = imageData.thumbnailImage;
      this.zoomImage = imageData.thumbnailImage;
    };

    this.imgZoom = new Image();
    this.imgZoom.src = imageData.largeImage;
    this.imgZoom.onload = () => {
      this.zoomImage = imageData.largeImage;
    };
    // const img1 = new Image();
    // img1.src = imageData.largeImage;
    // img1.onload = () => {
    //     this.zoomImage = imageData.largeImage;
    // };
  }

  changeImageForSlider() {
    if (this.slidingIndex <= (this.productDetail.productImage.length - 1) && this.slidingIndex >= 0) {
      this.mainImage = LOADING_SPINNER_URL;

      const img = new Image();
      img.src = this.productDetail.productImage[this.slidingIndex].thumbnailImage;

      img.onload = () => {
        this.mainImage = this.productDetail.productImage[this.slidingIndex].thumbnailImage;
      };
    } else if (this.slidingIndex >= this.productDetail.productImage.length) {
      this.slidingIndex--;
    } else if (this.slidingIndex < 0) {
      this.slidingIndex++;
    }
  }

  showMoreReviews(vendorId) {
    if ( vendorId) {
      this.router.navigate(['/product-reviews', this.productId], {queryParams : {'vid' : vendorId}});
    } else {
      this.router.navigate(['/product-reviews', this.productId]);
    }
  }

  updateTitleOnHover(rating) {
    this.ratingTitle = rating === 0 ? this.translation.translate('BAD') : rating === 1 ? this.translation.translate('MEDIOCRE') : rating === 2 ? this.translation.translate('GOOD') : rating === 3 ? this.translation.translate('BEST') : this.translation.translate('AWESOME');
  }

  addProductRating(data: any) {
    if (this.storage.get('token')) {
      this.productService.addProductRating({ 'productId': this.productId, 'userRating': data })
        .subscribe((response: any) => {
          if (response) {
            this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
            this.getProductDetails();
          }
        }, (error: any) => {
          if (error.status === API_STATUS_CODE.badRequest) {
            this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
          }
        });
    } else {
      this.rating = 0;
      this.snackBar.open(this.translation.translate('alertProductRate'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
      this.router.navigate(['/signin']);
    }
  }

  addProductReview() {
    this.productService.addProductReview(this.formToJson()).subscribe((response: any) => {
      if (response) {
        this.productReviewForm.reset();
        this.getProductDetails();
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        this.productReviewForm.reset();
        Object.keys(this.productReviewForm.controls).map(key => {
          this.productReviewForm.controls[key].setErrors(null);
        });

      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  onSubmitRatingReview() {
    if (!!this.storage.get('token') && this.storage.get('id')) {
      this.addProductRating(this.productReviewForm.value.productRating);
      this.addProductReview();
    } else {
      this.snackBar.open(this.translation.translate('alertProductReview'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
      this.router.navigate(['/signin']);
    }
  }

  formToJson() {
    const reviewFormValue = {
      'productId': this.productId,
      'reviewTitle': this.productReviewForm.get('reviewTitle').value,
      'reviewMessage': this.productReviewForm.get('reviewDescription').value
    };
    return reviewFormValue;
  }

  showRatingPopup() {
    this.ratingPopup = true;
  }

  hideRatingPopup() {
    this.ratingPopup = false;
  }

  reachedLeft(e: any) {
    this.disableArrowLeft = e;
  }

  reachedRight(e: any) {
    this.disableArrowRight = e;
  }
  reachedRightMobile(e: any) {
    this.disableArrowRightMobile = e;
  }
  reachedLeftMobile(e: any) {
    this.disableArrowLeftMobile = e;
  }

  moveLeftMobile(scroll) {
    scroll.moveLeft();
  }

  moveRightMobile(scroll) {
    scroll.moveRight();
  }


  moveLeft(scroll) {
    scroll.moveLeft();
  }

  moveRight(scroll) {
    scroll.moveRight();
  }

  scroll(el) {
    el.scrollIntoView();
  }

  getProductReviewRatingDetails() {
    this.productService.getParticularProductReviewDetails({ 'productId': this.productId })
      .subscribe((response: any) => {
        if (response) {
          if (response.message.productReview.length > 0) {
            // this.formInitialize(response.message.productReview[0]);
          }
          this.rating = response.message.productRating.length > 0 ? response.message.productRating[0].userRating : 0;
        }
      });
  }

  addToWishList(productDetatil) {
    if (this.storage.get('id')) {
      const status = this.productDetail.favUser !== 1 ? 1 : 0;
      this.productService.addProductToWishList({
        'productId': productDetatil.productCatalogId,
        'status': status,
        'vendorId': productDetatil.vendorId ? productDetatil.vendorId : null,
        'vendorProductId' : productDetatil.vendorProductId ? productDetatil.vendorProductId : null})
        .subscribe((response: any) => {
          if (response) {
            this.productDetail.favUser = status;
            this.homeService.setSignInStatus(true);
            if (this.prodFromWhislist !== true) {
              this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
            }
          }
        }, (error: any) => {
          if (error.status === API_STATUS_CODE.badRequest) {
            this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
          }
        });
    } else {
      this.router.navigate(['/signin']);
      this.snackBar.open(this.translation.translate('alertProductWishlist'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
    }
  }

  getProductAction(productData: any) {
    const productObj = {
      'productId': productData.productCatalogId, 'productData': productData ? productData : ''
    };
    if (productData.compareProduct === 1) {
      const storedItems = this.storage.get('compareStorage') ? JSON.parse(this.storage.get('compareStorage')) : [];
      storedItems.length > 1 ? this.router.navigate(['/product-comparision']) :
        this.snackBar.open(this.translation.translate('alertAddProductMore'),
          this.translation.translate('buttonClose'), SNACKBAR_WARNING);
    } else {
      const compareObj = {
        'productId': productData.productCatalogId,
        'categoryId': productData.categoryId,
        'vendorId': productData.vendorId ? productData.vendorId : null
      };
      if (!!this.storage.get('compareStorage')) {
        const storedItems = this.storage.get('compareStorage') ? JSON.parse(this.storage.get('compareStorage')) : [];
        forEach(storedItems, (items) => {
          this.compareStorage.push(items);
        });
        if (this.compareStorage.length < this.compareCount) {
          if (this.compareStorage.length !== 0) {
            if (this.compareStorage[0].categoryId === compareObj.categoryId) {
              this.compareStorage.push(compareObj);
              this.emitStatus = true;
              this.snackBar.open(this.translation.translate('alertProductToCompare'),
                this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
            } else {
              this.emitStatus = false;
              this.snackBar.open(this.translation.translate('alertProductSameCategory'),
                this.translation.translate('buttonClose'), SNACKBAR_WARNING);
            }
          }
        } else {
          this.emitStatus = false;
          this.snackBar.open(this.translation.translate('alertProductComparisonLimit') + this.compareCount,
            this.translation.translate('buttonClose'), SNACKBAR_WARNING);
        }
      } else {
        this.emitStatus = true;
        this.compareStorage.push(compareObj);
        this.snackBar.open(this.translation.translate('alertProductToCompare'),
          this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      }
      if (this.emitStatus) {
        this.storage.set('compareStorage', JSON.stringify(this.compareStorage));
        productData.compareProduct = 1;
        this.homeService.setCompareCountChange(true);
        this.productAction.emit(productObj);
        if (!!this.storage.get('id')) {
          this.trackProductComparisonDetails(productData.productCatalogId);
        }
      }
    }
  }

  trackProductComparisonDetails(productId) {
    this.sharedService.trackCompareProductDetails({
      'productId': [productId]
    }).subscribe((response: any) => {
      if (response) {

      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getVendorProductActionDetails(product) {
    product.vendorProductDetail.vendorId = product.vendorProduct.vendorId;
    product.vendorProductDetail.companyName = product.vendorProduct.companyName;
    product.vendorProductDetail.minimumQuantity = product.vendorProduct.minimumQuantity;
    product.vendorProductDetail.maximumQuantity = product.vendorProduct.maximumQuantity;
    product.vendorProductDetail.quantity = product.vendorProduct.quantity;
    product.vendorProductDetail.soldOut = product.vendorProduct.soldOut;
    product.vendorProductDetail.discountAmount = product.vendorProduct.discountAmount;
    product.vendorProductDetail.salesPrice = product.vendorProduct.salesPrice;
    this.addProductToCart(product.vendorProductDetail);
  }

  addProductToCart($event: any, fromBuyNow?) {
    if ($event.productOption.length === 0) {
      this.addProductToCartWithOption($event, false, fromBuyNow);
    } else {
      const optionRequired = find(this.productGroupOptionList, { 'optionRequired': 0 });
      if (optionRequired) {
        const isOptionSelected = find(optionRequired.options, { 'selected': true });
        if (isOptionSelected) {
          this.addProductToCartWithOption($event, true, fromBuyNow);
        } else {
          this.snackBar.open(this.translation.translate('alertProductOption'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
        }
      } else {
        const optionNotRequired = find(this.productGroupOptionList, { 'optionRequired': 1 });
        if (optionNotRequired) {
          const isOptionSelected = find(optionNotRequired.options, { 'selected': true });
          if (isOptionSelected) {
            this.addProductToCartWithOption($event, true, fromBuyNow);
          } else {
            this.addProductToCartWithOption($event, false, fromBuyNow);
          }
        }
      }
    }
  }

  addProductToCartWithOption($event, isOption, fromBuyNow?) {
    const quantity = ($event.minimumQuantity && Number($event.minimumQuantity) !== 0) ? $event.minimumQuantity : 1;
    const productPrice = $event.discountAmount === 0 ? $event.salesPrice : $event.discountAmount;
    let taxAmount;
    if ($event.taxType === 1) {
      taxAmount = $event.taxAmount;
    } else {
      taxAmount = (productPrice * $event.taxValue) / 100;
    }
    const productOptionDetails = [];
    if (isOption) {
      forEach(this.productGroupOptionList, (option) => {
        const optionSelected = find(option.options, { 'selected': true });
        if (optionSelected) {
          productOptionDetails.push({
            'productOptionId': optionSelected.productOptionDetailsId,
            'optionValueName': optionSelected.productOptionValue,
            'optionName': optionSelected.attributeType,
            'productOptionPrice': optionSelected.productOptionPrice
          });
        }
      });
    }
    if (!!this.storage.get('id')) {
      const cartObj = {
        'productCart': [{
          'productId': $event.productCatalogId,
          'price': productPrice,
          'cartQuantity': quantity,
          'subTotal': productPrice * quantity,
          'taxTotal': taxAmount * quantity,
          'productOption': isOption ? productOptionDetails : [],
          'mrpSalesPrice': productPrice,
          'vendorId': $event.vendorId ? $event.vendorId : null,
          'companyName': $event.companyName ? $event.companyName : null,
          'vendorProductId': $event.vendorProductId ? $event.vendorProductId : null
        }],
      };
      this.cartService.addCartDetails(cartObj).subscribe((response: any) => {
        this.homeService.setCartCountChange(true);
        if (this.prodFromWhislist) {this.addToWishList($event);}
        this.snackBar.open(this.translation.translate(response.data), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        } else if (error.status === API_STATUS_CODE.preconditionFailed) {
          const productDetails = error.error.data[0];
          this.comonService.showAlertForProductQuantity(productDetails, $event);
        }
      });
    } else {
      const cartStorage = [];
      const productObj = {
        'productId': $event.productCatalogId,
        'taxValue': $event.taxValue,
        'taxType': $event.taxType,
        'taxAmount': taxAmount,
        'discountAmount': $event.discountAmount,
        'salesPrice': $event.salesPrice,
        'productName': $event.productName,
        'productImage': $event.mainImage,
        'productCode': $event.productCode,
        'shippingValue': $event.shippingValue,
        'freeShipping': $event.freeShipping,
        'quantity': $event.quantity,
        'minimumQuantity': $event.minimumQuantity,
        'maximumQuantity': $event.maximumQuantity,
        'lowStockName': $event.lowStockName,
        'currencySymbol': $event.currencySymbol,
        'cartQuantity': quantity,
        'subTotal': productPrice * quantity,
        'taxTotal': taxAmount * quantity,
        'productOption': isOption ? productOptionDetails : [],
        'soldOut': $event.soldOut,
        'mrpSalesPrice': productPrice,
        'cartUser': 1,
        'vendorId': $event.vendorId ? $event.vendorId : null,
        'companyName': $event.companyName ? $event.companyName : null,
        'vendorProductId': $event.vendorProductId ? $event.vendorProductId : null
      };
      cartStorage.push(productObj);
      if (!!this.storage.get('cartStorage')) {
        const storedItems = !!this.storage.get('cartStorage') ? JSON.parse(this.storage.get('cartStorage')) : [];
        forEach(storedItems, (items) => {
          cartStorage.push(items);
        });
      }
      this.storage.set('cartStorage', JSON.stringify(cartStorage));
      this.homeService.setCartCountChange(true);
      this.snackBar.open(this.translation.translate('alertProductCart'), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      $event.cartUser = 1;
    }
    // forEach(this.productDetail, (bestpriceproduct) => {
    if (this.productDetail.productId === $event.productId && this.productDetail.vendorId === $event.vendorId) {
      this.productDetail.cartUser = 1;
    }
    // });
    // this.getProductDetails();
    if (fromBuyNow) {
      this.router.navigate(['/cart-checkout']);
    }
  }
  
  buyProduct($event: any) {
    this.addProductToCart($event, true);
  }

  updatePriceValueWithTaxAmount(product) {
    // Display price value with tax amount based on site settings
    let salesPriceTaxAmount = 0;
    let discountPriceTaxAmount = 0;
    let strikeoutPriceTaxAmount = 0;
    if (this.prodDetails && this.prodDetails.productSettings.displayProductPriceWithTax === 1) {
      if (product.salesPrice) {
        salesPriceTaxAmount = product.taxType === 1 ? product.taxAmount :
          ((product.salesPrice * product.taxValue) / 100);
      }
      if (product.discountAmount) {
        discountPriceTaxAmount = product.taxType === 1 ? product.taxAmount :
          ((product.discountAmount * product.taxValue) / 100);
      } else {
        product.taxDiscountAmount = 0;
      }
      if (product.strikeOutAmount) {
        strikeoutPriceTaxAmount = product.taxType === 1 ? product.taxAmount :
          ((product.strikeOutAmount * product.taxValue) / 100);
      }
    } else {
      salesPriceTaxAmount = 0;
      discountPriceTaxAmount = 0;
      strikeoutPriceTaxAmount = 0;
    }
    product.taxSalesPrice = product.salesPrice + salesPriceTaxAmount;
    product.taxDiscountAmount = product.discountAmount + discountPriceTaxAmount;
    product.strikeOutAmount = product.strikeOutAmount + strikeoutPriceTaxAmount;
  }

  getProductActionDetails($event: any) {
    if ($event.action === 'wishList') {
      const productData = find(this.productDetail.similarProduct, { 'productId': $event.productId });
      productData.favUser = $event.status === 1 ? 1 : 0;
    } else if ($event.action === 'cart') {
      const productData = find(this.productDetail.similarProduct, { 'productId': $event.productId });
      productData.cartUser = 1;
    } else {
      const productData = find(this.productDetail.similarProduct, { 'productId': $event.productId });
      productData.compareProduct = 1;
    }
  }

  onSelect(newValue, currentValue) {
    if (newValue.productOptionRequired === 1) {
      if (isPlatformBrowser(this.platform)) {
        setTimeout(() => {
          if (newValue.productOptionValue === currentValue) {
            this.radioSelected = null;
          } else {
            this.radioSelected = newValue.productOptionValue;
          }
        });
      }
    }
  }

  updateSelection(option, optionDetail, radioSelected?) {
    if (radioSelected) {
      this.onSelect(optionDetail, radioSelected);
    }
    this.defaultAttributePrice = 0;
    if (option.optionRequired === 0) {
      forEach(option.options, (opt) => {
        opt.selected = opt.productOptionDetailsId === optionDetail.productOptionDetailsId ? optionDetail.selected : false;
        if (opt.productOptionDetailsId === optionDetail.productOptionDetailsId) {
          opt.selected = true;
        }
      });
    } else if (option.optionRequired === 1) {
      optionDetail.selected = !optionDetail.selected;
      forEach(option.options, (opt) => {
        if (opt.productOptionDetailsId !== optionDetail.productOptionDetailsId) {
          opt.selected = false;
        }
      });
    }
    if (optionDetail.selected) {
      this.selectedAttribute = this.selectedAttribute.filter(x => x.attributeType !== optionDetail.attributeType);
      this.selectedAttribute.push(optionDetail);
    } else {
      this.selectedAttribute = this.selectedAttribute.filter(x => x.attributeType !== optionDetail.attributeType);
    }
    forEach(this.selectedAttribute, (attr) => {
      this.defaultAttributePrice += attr.productOptionPrice;
    });
    this.vendorProduct = this.productDetail;
    forEach(this.productGroupOptionList, (attributeOption) => {
      forEach(attributeOption.options, (detail, i) => {
        if (i === 0 && attributeOption.optionRequired === 0) {
          forEach(this.vendorProduct.vendorProduct, (vendor) => {
            // this.defaultAttributePrice += detail.productOptionPrice;
            // vendor.originalAmount = vendor.discountAmount === 0 ? vendor.salesPrice : vendor.discountAmount;
            if (vendor.discountAmount > 0) {
              // vendor.discountAmount = vendor.originalAmount + this.defaultAttributePrice;
              vendor.discountAmount = this.calculateDiscountAmount(this.defaultAttributePrice, vendor);
            } else {
              vendor.salesPrice = 0;
              vendor.salesPrice = vendor.originalAmount + this.defaultAttributePrice;
            }
            vendor.strikeOutAmount = vendor.salesPrice + this.defaultAttributePrice;
            this.updatePriceValueWithTaxAmount(vendor);
          });
        }
      });
      this.vendorProduct = this.productDetail;
    });
    this.calculatePrice();
  }

  vendorProductUpdate() {

  }

  calculatePrice() {
    if (this.defaultAttributePrice) {
      if (this.productDetail.discountAmount === 0) {
        this.productDetail.salesPrice = Number(this.productOriginalAmount) + this.defaultAttributePrice;
      } else {
        // this.productDetail.discountAmount = Number(this.formatAmount(this.productOriginalAmount)) + this.defaultAttributePrice;
        this.productDetail.discountAmount = this.calculateDiscountAmount(this.defaultAttributePrice, this.productDetail);
      }
    } else {
      if (this.productDetail.discountAmount === 0) {
        this.productDetail.salesPrice = this.productOriginalAmount;
      } else {
        // this.productDetail.discountAmount = this.productOriginalAmount;
        this.productDetail.discountAmount = this.calculateDiscountAmount(0, this.productDetail);
      }
    }
    this.strikeOutAmount = this.productDetail.salesPrice + (this.defaultAttributePrice ? this.defaultAttributePrice : 0);
    // adding tax amount with product price based on site setting value
    let salesTaxAmount = 0;
    let discountTaxAmount = 0;
    let strikeOutTaxAmount = 0;
    if (this.prodDetails && this.prodDetails.productSettings.displayProductPriceWithTax === 1) {
      if (this.productDetail.salesPrice) {
        salesTaxAmount = this.productDetail.taxType === 1 ? this.productDetail.taxAmount :
          ((this.productDetail.salesPrice * this.productDetail.taxValue) / 100);
      }
      if (this.productDetail.discountAmount) {
        discountTaxAmount = this.productDetail.taxType === 1 ? this.productDetail.taxAmount :
          ((this.productDetail.discountAmount * this.productDetail.taxValue) / 100);
      } else {
        this.productDetail.taxDiscountAmount = 0;
      }
      if (this.strikeOutAmount) {
        strikeOutTaxAmount = this.productDetail.taxType === 1 ? this.productDetail.taxAmount :
          ((this.strikeOutAmount * this.productDetail.taxValue) / 100);
      }
    } else {
      salesTaxAmount = 0;
      discountTaxAmount = 0;
      strikeOutTaxAmount = 0;
    }
    this.productDetail.taxSalesPrice = this.productDetail.salesPrice + salesTaxAmount;
    this.productDetail.taxDiscountAmount = this.productDetail.discountAmount + discountTaxAmount;
    this.strikeOutAmount = this.strikeOutAmount + strikeOutTaxAmount;
  }

  calculateDiscountAmount(attributePrice, productData) {
    let discountAmount = 0;
    const productPrice = Number(this.formatAmount(productData.salesPrice)) + attributePrice;
    if (productData.discountType === 2) {
      discountAmount = (productPrice * productData.discount) / 100;
    } else {
      discountAmount = productData.discount;
    }
    return (productPrice - discountAmount);
  }

  getCartCount() {
    if (this.storage.get('id')) {
      const cartObj = {
        'targetCurrency': 'USD'
      };
      this.comonService.getCartCount(cartObj).subscribe((cartProduct: any) => {
        if (cartProduct) {
          this.cartProductDetail = cartProduct.product;
        }
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.signInSubscribe$) {
      this.signInSubscribe$.unsubscribe();
    }
  }

  formatAmount(amount: any) {
    if (amount && amount.toString().indexOf(',') !== -1) {
      return amount.toString().replace(/,/g, '');
    } else {
      return amount;
    }
  }
  moveToRespectivePages(page: string, id?: number) {
    switch (page) {
      case 'product-list':
        this.storage.set('isFromCategory', 'true');
        return this.router.navigate(['/product-list', this.productDetail.categoryId]);
      case 'product-list-a':
        this.storage.remove('isFromCategory');
        return this.router.navigate(['/product-list', this.productDetail.subcategoryId]);
      case 'home':
        return this.router.navigate(['/']);
      case 'main':
      // return this.comonService.setCategoryMenuOpenId(id);
    }
  }
  openreplyPopup(reviewId) {
    const dialogRef = this.matDialog.open(ReplyPopupComponent, {
      panelClass: 'view-business-wrap',
      data: reviewId,
      disableClose: true
    });
  }
  roundOff(value) {
    forEach(value, (productRating) => {
      productRating.avgRating = +(productRating.avgRating.toFixed(1));
    });
  }

  checkProductAvailability() {
    const productData = {
      zipcode: this.availableZipcode,
      weight: this.productDetail.productWeight,
      quantity: this.productDetail.minimumQuantity,
      vendorId: this.productDetail.vendorId ? this.productDetail.vendorId : undefined
    };
    this.productService.checkProductAvailability(productData).subscribe((response: any) => {
      if (response) {
        this.productAvailableDetails = response;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  onChangeZipCode() {
    if (this.availableZipcode === '') {
      this.productAvailableDetails = '';
    }
  }
  shareList() {
    this.shareBtns = !this.shareBtns;
  }

  shareListMobile() {
    this.shareBtnsMobile = !this.shareBtnsMobile;
  }

  getRawDescription(description) {
    if (isPlatformBrowser(this.platform)) {
      let elem = this._document.createElement("DIV");
      elem.innerHTML = description;
      const txt = elem.textContent || elem.innerText || "";
      elem.remove();
      return txt;
    }
  }

  shareLinkContent(productName, description) {
    return `${productName}\n${description}`;
  }

  onShareBtnClosed(event: string) {
    if(event === 'copy') {
      this.snackBar.open(this.translation.translate('SHARE_BTN_LINK_COPIED'), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
    }
  }

  updateOgURL() {
    if (isPlatformBrowser(this.platform)) {
      this.seoService.updateUrl(`${this.prodDetails.siteUrl}${this._document.URL}`);
    }
  }
  toggleVendorPopover() {
    this.vendorDetail = !this.vendorDetail;
  }
}
