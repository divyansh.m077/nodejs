import { BASE_URL } from '../common/common.url-config';

export const PRODUCT_URL = {
    SOLD_PRODUCT: BASE_URL + 'get-soldout',
    HOT_PRODUCT: BASE_URL + 'get-hot-product-all',
    BEST_SELLING_PRODUCT: BASE_URL + 'get-best-selling-product-all',
    BEST_SELLING_PRODUCT_COUNT: BASE_URL + 'get-best-selling-product-count',
    WISH_LIST: BASE_URL + 'favourite-product',
    GET_WISH_LIST: BASE_URL + 'favourite-product/wishlist',
    SEARCH: BASE_URL + 'search',
    PRODUCTS: BASE_URL + 'get-product-all',
    PRODUCTS_BASED_CATEGORY: BASE_URL + 'get-sort-product',
    PRODUCTS_BASED_ON_MAIN_CATEGORY: BASE_URL + 'sort-product-by-category',
    DETAIL: BASE_URL + 'get-product-detail',
    PRODUCT_BY_FILTER: BASE_URL + 'get-products-by-filter',
    COMPARE: BASE_URL + 'product-compare',
    COMPARE_TRACK: BASE_URL + 'product-compare/track',
    FILTER: BASE_URL + 'get-filters/',
    PRODUCT_REVIEWS: BASE_URL + 'product-review/review',
    ADD_RATING: BASE_URL + 'product-rating',
    ADD_REVIEW: BASE_URL + 'product-review',
    RATING_REVIEW_BY_USER: BASE_URL + 'product-review/user/review',
    PRODUCTS_BASED_BRAND: BASE_URL + 'sort-product-by-brand',
    FEATURED_PRODUCT_LIST: BASE_URL + 'get-featured-product-all',
    REPLY_MESSAGE: BASE_URL + 'review-reply/',
    PRODUCT_AVAILABLE_URL: BASE_URL + 'shipping-gateway/service-availability',
    GET_SIMILAR_PRODUCTS: BASE_URL + 'get-product-detail/similar-products'
};
