import { NgtUniversalModule } from '@ng-toolkit/universal';
import { NgModule, APP_INITIALIZER, Inject } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';


import { NgHttpLoaderModule } from 'ng-http-loader';
import { L10nConfig, L10nLoader, TranslationModule, StorageStrategy, ProviderType, LocaleService } from 'angular-l10n';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NouisliderModule } from 'ng2-nouislider';


import { ComonModule } from './common/common.module';
import { HomeModule } from './home/home.module';
import { AppRoutingModule } from './app-routing.module';


import { AppComponent } from './app.component';
import { APIInterceptor } from './apiInterceptor.service';
import { ComonService } from './common/comon.service';
import { AppInitializerService } from './common/app-initializer.service';

import { environment } from '../environments/environment';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { menuItems } from './common/menu-items';
import { StorageControllerService } from './storage-controller.service';

import { ShareButtonsModule } from '@ngx-share/buttons';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { MyHammerConfig } from './hammer.config';

const l10nConfig: L10nConfig = {
    locale: {
        languages: [
            { code: 'en', dir: 'ltr' },
            { code: 'es', dir: 'ltr' }
        ],
        language: 'en',
        storage: StorageStrategy.Cookie
    },
    translation: {
        providers: [
            { type: ProviderType.Static, prefix: `${environment.assetsUrl}assets/locale-` }
        ],
        caching: true,
        missingValue: 'No key'
    }
};

export function app_init(appInitializerService: AppInitializerService) {
  return () => appInitializerService.load();
}
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CommonModule,
    ShareButtonsModule,
    NgtUniversalModule,
    BrowserAnimationsModule,
    ComonModule,
    HomeModule,
    HttpClientModule,
    TransferHttpCacheModule,
    NgHttpLoaderModule.forRoot(),
    TranslationModule.forRoot(l10nConfig),
    SimpleNotificationsModule.forRoot(),
    AppRoutingModule,
    NouisliderModule,
    MatSnackBarModule,
    ScrollToModule.forRoot()
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: APIInterceptor,
    multi: true,
  },
    TitleCasePipe,
    AppInitializerService,
  {
    'provide': APP_INITIALIZER,
    'useFactory': app_init,
    'deps': [AppInitializerService],
    'multi': true
  },
  { provide: 'MENU_ITEMS', useValue: menuItems },
  { provide: HAMMER_GESTURE_CONFIG, useClass: MyHammerConfig }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    private storage: StorageControllerService,
    public l10nLoader: L10nLoader,
    private comonService: ComonService,
    private locale: LocaleService
  ) {
    this.l10nLoader.load();
    const lang = this.storage.get('defaultLocale');
    /** Retain the language from cookies */
    if(lang) {
      this.locale.setCurrentLanguage(lang);
    }
  }

  getLanguage() {
    this.comonService.getLangauges().subscribe((response: any) => {
    });
  }
}

