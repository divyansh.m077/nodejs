import { Component, OnInit } from '@angular/core';
import { Router, Event } from '@angular/router';
import { Language } from 'angular-l10n';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

  @Language() lang: string;
  constructor(private router: Router,) { }

  ngOnInit() {
  }
  redirectToHome(){
      this.router.navigate(['/']);
  }
}
