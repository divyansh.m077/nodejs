import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TitleCasePipe } from '@angular/common';
import { MANAGE_ADDRESS_RULE, MANAGE_ADD_RULE } from '../../app-setting';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Language, TranslationService } from 'angular-l10n';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';

import { HomeService } from './../../home/home.service';
import { UserService } from './../user.service';
import { REG_RULE, settings, SNACKBAR_ALERT, SNACKBAR_SUCCESS, OTP_RULE } from './../../app-setting';
import { API_STATUS_CODE, ALERT_SYSTEM_TITLE, COUNTRY_CODE } from '../../app.constants';
import { AlertPopupComponent } from '../../shared/alert-popup/alert-popup.component';
import { SharedService } from '../../shared/shared.service';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { StorageControllerService } from '../../storage-controller.service';
import { CreateAddressComponent } from '../../shared/create-address/create-address.component';
import { find } from 'lodash';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  userEmail: any;
  @Language() lang: string;
  registerForm: FormGroup;
  siteKey;
  captchaSize;
  captchaTheme;
  captchaType;
  captchaLanguage;
  public registrationRules: any = REG_RULE();
  otpRule = OTP_RULE;
  public passwordRule = false;
  patternDetails: any;
  actionSubscribe$: any;
  isInBrowser;
  otpTimer;
  disableResendOtp: boolean = true;
  hideSendOtp: boolean;
  otpTriggerCount: number = 1;
  countryCode = COUNTRY_CODE;
  manageRule: any = MANAGE_ADD_RULE();
  countryList: any[] = [];
  stateList: any[] = [];
  cityList: any[] = [];
  constructor(
    private storage: StorageControllerService,
    private router: Router,
    private fb: FormBuilder,
    public userService: UserService,
    public dialog: MatDialog,
    private socialAuthService: AuthService,
    private homeService: HomeService,
    private translation: TranslationService,
    private sharedService: SharedService,
    private snackBar: MatSnackBar,
    private titleCase: TitleCasePipe
  ) {
    if (typeof window !== 'undefined') {
      this.isInBrowser = true;
    }
    this.formInitialization();
    this.siteKey = settings.captchaKey;
    this.captchaSize = 'Normal';
    this.captchaTheme = 'Light';
    this.captchaType = 'Image';
    this.captchaLanguage = 'en';
  }

  formInitialization() {
    const registrationRules = REG_RULE();
    this.registerForm = this.fb.group({
      'firstName': ['', [Validators.required, Validators.minLength(registrationRules.FIRST_NAME_MIN_LENGTH),
      Validators.maxLength(registrationRules.NAME_MAX_LENGTH),
      Validators.pattern(registrationRules.NAME_PATTERN)]],
      'lastName': ['', [Validators.required, Validators.minLength(registrationRules.LAST_NAME_MIN_LENGTH),
      Validators.maxLength(registrationRules.NAME_MAX_LENGTH),
      Validators.pattern(registrationRules.NAME_PATTERN)]],
      'email': ['', [Validators.required, Validators.pattern(registrationRules.EMAIL_PATTERN)]],
      'phone': ['', [Validators.required]],
      'newPassword': ['', [Validators.required,
      Validators.pattern(registrationRules.PASSWORD_PATTERN)]],
      'confirmPassword': ['', Validators.required],
      'agree': [false, Validators.requiredTrue],
      'isCaptchaVerified': ['', [Validators.required]],
      'otp': ['', [Validators.required, Validators.minLength(this.otpRule.LENGTH), Validators.pattern(this.otpRule.PATTERN)]],
      'Address1': ['', [Validators.required, Validators.minLength(this.manageRule.ADDRESS_MIN_LENGTH),
                        Validators.maxLength(this.manageRule.ADDRESS_MAX_LENGTH)]],
      'Address2': ['', [Validators.minLength(this.manageRule.ADDRESS_MIN_LENGTH),
      Validators.maxLength(this.manageRule.ADDRESS_MAX_LENGTH)]],
      'Country': ['', [Validators.required]],
      'State': ['', [Validators.required]],

      'City': ['', [Validators.required]],
      'Pincode': ['', [Validators.required, Validators.pattern(this.manageRule.PINCODE_PATTERN),
        Validators.minLength(this.manageRule.PINCODE_MIN_LENGTH), Validators.maxLength(this.manageRule.PINCODE_MAX_LENGTH)]],
    });
    this.getSiteSettingDetailForPatterns();
  }

  ngOnInit() {
    this.sharedService.getLocation();
    this.getCountryList();
  }

  getSiteSettingDetailForPatterns() {
    this.patternDetails = this.sharedService.getSiteSettingDetails();
    if (this.patternDetails) {
      if (this.patternDetails.phoneNumberFormat) {
        const phoneRegx = new RegExp(this.patternDetails.phoneNumberFormat);
        this.registerForm.controls['phone'].setValidators([Validators.required, Validators.pattern(phoneRegx)]);
        this.registerForm.controls['phone'].updateValueAndValidity();
      }
    }
  }

  moveToSignin() {
    this.router.navigate(['/signin']);
  }

  showPasswordRules() {
    this.passwordRule = true;
  }

  hidePasswordRule() {
    this.passwordRule = false;
  }

  removeCurrentPassword() {
    if (this.registerForm.controls['confirmPassword'].value !== this.registerForm.controls['newPassword'].value) {
      this.registerForm.controls['confirmPassword'].setValue('');
    }
  }

  saveAddressDetails() {
    this.userService.addManageAddress(this.formToJson()).subscribe((response: any) => {
      
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  formToJson() {
    const formValues = this.registerForm.value;
    const addressFormValue = {
      'firstName': this.titleCase.transform(formValues.firstName),
      'lastName': formValues.lastName,
      'phone': formValues.phone,
      'alternateMobile': formValues.alternateMobile,
      // 'companyName': this.addressForm.value.Company,
      'zipCode': formValues.Pincode,
      'address1': formValues.Address1,
      'address2': formValues.Address2,
      'addressType': 1,
      'country': formValues.Country,
      'state': formValues.State,
      'regionId': null,
      'city': formValues.City,
      'districtId': null,
      'townVillageId': null,
      'userId': this.storage.get('id'),
    };
    return addressFormValue;
  }

  onSubmit() {
    let locationObj = this.sharedService.getTrackLocationData();
    this.userService.saveUserRegisterationDetails({
      'firstName': this.registerForm.value.firstName,
      'lastName': this.registerForm.value.lastName,
      'mobileNumber': this.registerForm.value.phone,
      'email': this.registerForm.value.email,
      'password': this.registerForm.value.newPassword,
      'isCaptchaVerified': this.registerForm.value.isCaptchaVerified,
      'otp': this.registerForm.controls.otp.value,
      'longitude': locationObj.longitude,
      'latitude': locationObj.latitude,
      'createdBy': 'self'
    }).subscribe((response: any) => {
      const userDetails = response.data[0];
      const name = userDetails.firstName + ' ' + userDetails.lastName;
      this.storage.set('token', userDetails.token);
      this.storage.set('userName', name);
      this.storage.set('userDetails', JSON.stringify(userDetails));
      if (userDetails.userImage) {
        this.storage.set('userImage', userDetails.userImage);
      }
      this.storage.set('id', userDetails.userId);
      this.homeService.setSignInStatus(true);
      this.homeService.setCartCountChange(true);
      this.router.navigate(['/']);
      this.formToJson();
      this.saveAddressDetails();
      this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      // this.createAddress();
      this.router.navigate(['/']);
    }, (errorResponse: any) => {
      this.snackBar.open(this.translation.translate(errorResponse.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    });
  }

  // public socialSignIn(socialPlatform: string) {
  //   let socialPlatformProvider;
  //   if (socialPlatform === 'facebook') {
  //     socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
  //   } else if (socialPlatform === 'google') {
  //     socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
  //   }
  //   this.socialAuthService.signIn(socialPlatformProvider).then(
  //     (userData) => {
  //       const userObj = {
  //         'userEmail': userData.email,
  //         'userName': userData.name,
  //         'providers': socialPlatform === 'facebook' ? 1 : 2,
  //         'token': socialPlatform === 'facebook' ? userData.authToken : userData.idToken
  //       };
  //       this.userService.saveSocialUserRegisterationDetails(userObj).subscribe((response: any) => {
  //         if (response) {
  //           const userDetails = response.data;
  //           const name = userDetails.firstName + ' ' + userDetails.lastName;
  //           this.storage.set('token', userDetails.token);
  //           this.storage.set('userName', name);
  //           this.storage.set('userDetails', JSON.stringify(userDetails));
  //           this.storage.set('id', userDetails.userId);
  //           this.storage.set('id', userDetails.userId);
  //           this.storage.set('token', userDetails.token);
  //           const previousUrl = this.storage.get('previousUrl') === '/register' ? '/' : this.storage.get('previousUrl');
  //           this.homeService.setSignInStatus(true);
  //           this.homeService.setCartCountChange(true);
  //           this.router.navigate([previousUrl ? previousUrl : '/']);
  //         }
  //       }, (error: any) => {
  //         if (error.status === API_STATUS_CODE.conflict) {
  //           this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
  //         }
  //       });
  //     }
  //   );
  // }

  public socialSignIn(socialPlatform: string) {

    let socialPlatformProvider;
    if (socialPlatform === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        const userObj = {
          'providers': socialPlatform === 'facebook' ? 1 : 2,
          'token': userData.authToken,
          'userEmail': userData.email ? userData.email : '',
          'userImage': userData.facebook.picture.data.url,
          'firstName': userData.facebook.first_name,
          'lastName': userData.facebook.last_name,
          'socialMediaId': userData.facebook.id,
          'isFBMobileAndMailExist': userData.email ? 1 : 0
        };
        console.log(userData);
        this.userService.saveSocialUserRegisterationDetails(userObj)
          .subscribe((response: any) => {
            if (response.data) {
              const userDetails = response.data;
              this.userEmail = userDetails.userEmail;
              if (this.userEmail) {
                const name = userDetails.firstName + ' ' + userDetails.lastName;
                this.storage.set('token', userDetails.token);
                this.storage.set('userName', name);
                this.storage.set('userDetails', JSON.stringify(userDetails));
                this.storage.set('id', userDetails.userId);
                this.storage.set('userName', name);
                this.storage.set('token', userDetails.token);
                this.storage.set('id', userDetails.userId);
                let previousUrl: any;
                let preUrl = this.storage.get('previousUrl');
                if (preUrl && preUrl.includes('verify-email')) { preUrl = null; }
                if (preUrl) {
                  const splitUrl = preUrl.split('?');
                  if (preUrl === '/register' || preUrl === '/forgot-password' ||
                    (splitUrl && splitUrl[0] === '/reset-password' || splitUrl && splitUrl[0] === '/set-password')) {
                    previousUrl = '/';
                  } else {
                    previousUrl = preUrl;
                  }
                }
                this.homeService.setSignInStatus(true);
                this.homeService.setCartCountChange(true);
                this.router.navigate([previousUrl ? previousUrl : '/']);
              } else {
                this.userService.initializeUserDetails(userObj);
                this.router.navigate(['/socialmedia-email']);
              }
            } else {
              const dialogRef = this.dialog.open(AlertPopupComponent, {
                data: {
                  title: this.translation.translate('labelSuccess'),
                  message: [this.translation.translate('labelRegisterSuccess'), this.translation.translate('verifyMsg')],
                  action: ALERT_SYSTEM_TITLE.success,
                  buttonName: this.translation.translate('buttonOk')
                },
                disableClose: true
              });
              this.actionSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
                if (data) {
                  this.dialog.closeAll();
                  this.router.navigate(['/signin']);
                }
              });
            }
          }, (error: any) => {
            if (error.status === API_STATUS_CODE.badRequest) {
              this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
            } else if (error.status === API_STATUS_CODE.conflict) {
              this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
            } else if (error.status === API_STATUS_CODE.preconditionFailed) {
              this.router.navigate(['/verify-email'], { queryParams: { userEmail: userObj.userEmail } });
            }
          });
      }
    );
  }

  navigateToTermsAndCondition() {
    const dialogRef = this.dialog.open(TermsConditionComponent);
    // this.router.navigate(['/page/terms-and-condition']);
  }

  // Get OTP
  getOTP() {
    let reqObj: any = {
      "mobileNumber": this.registerForm.value.phone
    };
    this.userService.getRegisterOtp(reqObj).subscribe((response: any) => {
      this.hideSendOtp = true;
      this.setOtpTimeout();
      this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
    }, (errorResponse: any) => {
      this.hideSendOtp = false;
      this.setOtpTimeout();
      this.snackBar.open(this.translation.translate(errorResponse.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    });
  }

  // Set timeout
  setOtpTimeout() {
    this.disableResendOtp = true;
    this.otpTimer = 60;
    let interval = setInterval(() => {
      if (this.otpTimer > 1) {
        this.otpTimer -= 1;
      } else {
        this.disableResendOtp = false;
        clearInterval(interval);
      }
    }, 1000)
  }

  // Resend OTP
  resendOTP() {
    if (this.otpTriggerCount > 3) {
      const dialogRef = this.dialog.open(AlertPopupComponent, {
        data: {
          title: this.translation.translate('ALERT'),
          message: [this.translation.translate('OTP_RESTRICT_ALERT')],
          action: ALERT_SYSTEM_TITLE.warning,
          buttonName: this.translation.translate('buttonOk')
        }
      });
      let ref = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
        if (data) {
          window.location.reload();
          this.dialog.closeAll();
        }
      });

      return;
    }    
    let reqObj: any = {
      "mobileNumber": this.registerForm.value.phone
    };
    this.userService.resendRegisterOtp(reqObj).subscribe((response: any) => {
      this.setOtpTimeout();
      this.otpTriggerCount += 1;
      this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
    }, (errorResponse: any) => {
      this.setOtpTimeout();
      this.snackBar.open(this.translation.translate(errorResponse.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    });
  }

  // Create new Address
  createAddress() {
    const addDialogRef = this.dialog.open(CreateAddressComponent, {
      width: '40%',
      height: 'auto',
      disableClose: true
    });
  }

  getCountryList() {
    this.userService.getCountryList().subscribe((response: any) => {
      if (response) {
        this.countryList = response.data;
        this.setZipCodePattern();
        let defalutCountry = find(this.countryList, { countryName: 'India' });
        this.registerForm.patchValue({
          Country: defalutCountry ? defalutCountry.countryId : undefined
        });
        this.onChangeCountry(false);
      }
    }, (errorResponse: any) => {
      this.countryList = [];
    });
  }

  // Set Zipcode pattern based on Country
  setZipCodePattern() {
    let countryObj: any = find(this.countryList, { countryId: this.registerForm.value.Country });
    if (countryObj && countryObj.zipCodeRegex) {
      this.registerForm.controls['Pincode'].setValidators([Validators.required, Validators.pattern(countryObj.zipCodeRegex),
      Validators.minLength(this.manageRule.PINCODE_MIN_LENGTH), Validators.maxLength(this.manageRule.PINCODE_MAX_LENGTH)]);
      this.registerForm.controls['Pincode'].updateValueAndValidity();
    }
  }

  // On Change Country to get States
  onChangeCountry(isReset?: boolean) {
    if (this.registerForm.value.Country) {
      this.setZipCodePattern();
      this.getStateList(this.registerForm.value.Country);
      if (isReset)
        this.resetLocationList('Country');
    }
  }

  // Get State list
  getStateList(countryId) {
    this.userService.getStateList(countryId).subscribe((response: any) => {
      if (response) {
        this.stateList = response.data;
      }
    }, (errorResponse: any) => {
      this.stateList = [];
      this.resetLocationList('State');
    });
  }

  // Reset list
  resetLocationList(targetLocation, isReset?: boolean) {
    if (targetLocation == 'Country') {
      this.stateList = [];
      this.cityList = [];
      this.registerForm.patchValue({
        State: undefined,
        City: undefined
      })
    } else if (targetLocation == 'State') {
      this.cityList = [];
      this.registerForm.patchValue({
        City: undefined
      })
    } 
  }

  // On Change State 
  onChangeState(isReset?: boolean) {
    if (this.registerForm.value.State) {
      this.getCityList(this.registerForm.value.State);
      if (isReset)
        this.resetLocationList('State');
    }
  }

  // Get City list
  getCityList(stateId) {
    this.userService.getCityList(stateId).subscribe((response: any) => {
      if (response) {
        this.cityList = response.data;
      }
    }, (errorResponse: any) => {
      this.cityList = [];
    });
  }

  pinCodePatternCheck(event) {
    const pincodePattern = MANAGE_ADDRESS_RULE.PINCODE_KEYPRESS_PATTERN;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pincodePattern.test(inputChar)) {
      // event.preventDefault();
    }
  }
}
