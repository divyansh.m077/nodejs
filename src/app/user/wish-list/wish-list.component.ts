import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit , Inject} from '@angular/core';
import { Router } from '@angular/router';

import { forEach, find } from 'lodash';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Language, TranslationService } from 'angular-l10n';
import { MatSort, MatTableDataSource } from '@angular/material';

import { ProductService } from './../../products/products.service';
import { UserService } from './../user.service';
import { HomeService } from './../../home/home.service';
import { CartService } from './../../cart/cart.service';
import { settings, SNACKBAR_ALERT, SNACKBAR_SUCCESS, SNACKBAR_WARNING } from './../../app-setting';
import { API_STATUS_CODE, ALERT_SYSTEM_TITLE } from '../../app.constants';
import { SharedService } from '../../shared/shared.service';
import { ComonService } from '../../common/comon.service';
import { ConfirmationPopupComponent } from '../../shared/confirmation-popup/confirmation-popup.component';
import { StorageControllerService } from '../../storage-controller.service';

@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.scss']
})
export class WishListComponent implements OnInit, OnDestroy, AfterViewInit {
  displayedColumns: string[] = ['sno', 'productImage', 'productName', 'lowStock', 'price', 'action'];
  @Language() lang: string;
  @ViewChild(MatSort) sort: MatSort;
  myWishList: any;
  wishlistSubscribe$: any;
  wishlistDataSubscribe$: any;
  totalPages: number;
  currentPage: number;
  showDiscount: any;
  prodDetails: any;
  data = [];
  totalRecord: number;
  limit: number;
  offSet: number;
  currencyFormat: any;
  currencyCode: any;
  public isLoading: boolean;
  constructor(
              private storage: StorageControllerService,
              private productService: ProductService,
              private userService: UserService,
              public dialog: MatDialog,
              private homeService: HomeService,
              private cartService: CartService,
              private router: Router,
              private translation: TranslationService,
              private sharedService: SharedService,
              private comonService: ComonService,
              private snackBar: MatSnackBar,
    ) {
    this.isLoading = true;
    this.getSiteSettingProdDetails();
    this.comonService.setWhislistCount(true);
  }

  ngAfterViewInit() {
    this.myWishList.sort = this.sort;
    this.getWishlistDetails();
  }

  getSiteSettingProdDetails() {
    this.prodDetails = this.sharedService.getSiteSettingDetails();
    if (this.prodDetails) {
      this.showDiscount = this.prodDetails.productSettings.displayDiscountedPrice;
     }
  }

  getWishlistDetails() {
    const wishlistDetails = {
      endLimit: this.offSet,
      startLimit: this.limit,
      targetCurrency: 'USD'
    };
    this.getMyWishlists(wishlistDetails);
  }

  ngOnInit() {
    this.prodDetails = this.sharedService.getSiteSettingDetails();
    this.currencyCode = this.prodDetails.currencyCode;
    this.totalPages = 0;
    this.limit = settings.LIMIT;
    this.offSet = settings.OFFSET;
    this.currentPage = 1;
    this.myWishList = new MatTableDataSource(this.data);
  }

  getMyWishlists(wishlistData) {
    this.data = [];
    this.isLoading = true;
    this.productService.getWishList(wishlistData).subscribe((response: any) => {
      this.isLoading = false;
      if (response) {
        this.totalRecord = response.data.productLength;
        this.totalPages = Math.ceil(this.totalRecord / settings.LIMIT);        
        forEach(response.data.product, (data) => {
          if (data.vendorId && data.totalAvailablity === 0 ||
          (!data.vendorId && !data.totalAvailablity && (data.quantity - data.soldOut === 0))) {
            data.lowStock = true;
          } else {
            data.lowStock = false;
          }
          const productPrice = this.comonService.calculatePriceAmountWithTax(data.salesPrice, data.discountAmount, data.taxAmount,
            data.taxType, data.taxValue);
          data.taxDiscountPrice = productPrice.taxDiscountPrice;
          data.taxSalesPrice = productPrice.taxSalesPrice;
          this.data.push(data);
        });
        this.generateSerialNo();
      } else {
        this.myWishList.data = [];
      }
    }, (error: any) => {
      this.isLoading = false;
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  generateSerialNo() {
    let indexPos = this.offSet;
    forEach(this.data, (data) => {
      indexPos += 1;
      data.sno = indexPos;
    });
    this.myWishList.data = this.data;
    forEach(this.myWishList.data, (myWhislist) => {
      myWhislist.price = (myWhislist.discountAmount === 0) ? myWhislist.salesPrice : myWhislist.discountAmount;
    });
  }

  onPageChange(offset) {
    this.offSet = offset;
    this.getWishlistDetails();
  }

  getProductAction(product: any, action: any) {
    if (action === 'removeWishlist') {
      const dialogRef = this.dialog.open(ConfirmationPopupComponent, {
        data: {
          title: this.translation.translate('labelDialogTitle'),
          message: this.translation.translate('alertMsgProductRemove'),
          action: ALERT_SYSTEM_TITLE.warning,
          buttonOneName: this.translation.translate('buttonDelete'),
          buttonTwoName: this.translation.translate('buttonCancel'),
        }
      });
      this.wishlistSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
        if (data) {
          this.userService.removeMyWishListProduct(product).subscribe((response: any) => {
            if (response) {
              this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
              this.dialog.closeAll();
              if (this.myWishList.data.length === 1 && this.offSet !== 0) {
                this.offSet = this.offSet - this.limit;
              }
              this.getWishlistDetails();
              this.homeService.setSignInStatus(true);
            }
          }, (error: any) => {
            this.dialog.closeAll();
            if (error.status === API_STATUS_CODE.badRequest) {
              this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
            }
          });
        }
      });
    } else {
      if (!!this.storage.get('id')) {
        const productPrice = product.discountAmount === 0 ? product.salesPrice :
                              product.discountAmount;
        const taxAmount = product.taxAmount;
        const quantity = (product.minimumQuantity && Number(product.minimumQuantity) !== 0) ?
                          Number(product.minimumQuantity) : 1;
        if (product.cartUser === 0) {
            if ((product.vendorId && product.totalAvailablity === 0) ||
            (!product.vendorId && !product.totalAvailablity && (product.quantity - product.soldOut === 0))) {
               this.snackBar.open(this.translation.translate('PROD_OUT_OF_STOCK'),
               this.translation.translate('buttonClose'), SNACKBAR_WARNING);
              } else if (product.vendorId && (product.quantity - product.soldOut === 0) && product.totalAvailablity > 0) {
               this.snackBar.open(this.translation.translate('OUT_STOCK_VENDOR'),
               this.translation.translate('buttonClose'), SNACKBAR_WARNING);
               this.userService.removeMyWishListProduct(product.favouriteId).subscribe((res: any) => {
                    if (res) {
                      this.homeService.setSignInStatus(true);
                      this.goToProductDetailPage(product.productId);
                    }
                  });
              } else {
                if (product.isProductInAttribute === 1 || (Number(product.minimumQuantity) > 1)) {
                     this.goToProductDetailPage(product.productId);
               } else {
              const mrpSalesPrice = product.discountAmount === 0 ? product.salesPrice : product.discountAmount;
              const cartObj = {
                'productCart': [{
                'productId': product.productId,
                'price': productPrice,
                'cartQuantity': quantity,
                'subTotal': productPrice * quantity,
                'taxTotal': taxAmount * quantity,
                'productOption' : [],
                'vendorId' : product.vendorId ? product.vendorId : null,
                'vendorProductId' : product.vendorProductId ? product.vendorProductId : null,
                'mrpSalesPrice' : mrpSalesPrice
                }]
              };
              this.cartService.addCartDetails(cartObj).subscribe((response: any) => {
                if (response) {
                  this.homeService.setCartCountChange(true);
                  const productData = find(this.myWishList.data, {'productId': product.productId});
                  productData.cartUser = 1;
                  this.snackBar.open(this.translation.translate(response.data),
                  this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
                  this.userService.removeMyWishListProduct(product.favouriteId).subscribe((res: any) => {
                    if (res) {
                      this.homeService.setSignInStatus(true);
                      this.getWishlistDetails();
                    }
                  });
                }
              }, (error: any) => {
                if (error.status === API_STATUS_CODE.badRequest) {
                  this.snackBar.open(this.translation.translate(error.error.data),
                  this.translation.translate('buttonClose'), SNACKBAR_ALERT);
                } else if (error.status === API_STATUS_CODE.preconditionFailed) {
                  const productDetails = error.error.data[0];
                  this.comonService.showAlertForProductQuantity(productDetails, product);
                }
              });
            }
          }
        } else {
          this.router.navigate(['/cart-checkout']);
        }
      }
     }
    }

  goToProductDetailPage(productId) {
     this.router.navigate(['/product-detail', productId, {'fromWhislist' : true}]);
  }

  ngOnDestroy() {
    if (this.wishlistDataSubscribe$) {
      this.wishlistDataSubscribe$.unsubscribe();
    }
    if (this.wishlistSubscribe$) {
      this.wishlistSubscribe$.unsubscribe();
    }
  }
}
