import { Component, OnInit } from '@angular/core';

import { Language, TranslationService } from 'angular-l10n';

import { UserService } from '../user.service';
import { API_STATUS_CODE } from '../../app.constants';
import { MatSnackBar } from '@angular/material';
import { SNACKBAR_ALERT } from '../../app-setting';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @Language() lang: string;
  dashboardDetails;
  constructor(private userService: UserService, private snackBar: MatSnackBar, private translation: TranslationService) { }

  ngOnInit() {
    this.getDashboardDetails();
  }

  getDashboardDetails() {
    this.userService.getDashboardDetails().subscribe((response: any) => {
      if (response) {
        this.dashboardDetails = response.data;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }
}
