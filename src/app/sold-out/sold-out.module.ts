import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule, MatButtonToggleModule} from '@angular/material';
import { TranslationModule } from 'angular-l10n';

import { SoldOutRoutingModule } from './sold-out-routing.module';
import { SharedModule } from '../shared/shared.module';

import { SoldOutComponent } from './sold-out.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatButtonToggleModule,
    TranslationModule,
    SoldOutRoutingModule
  ],
  declarations: [
    SoldOutComponent
  ]
})
export class SoldOutModule { }
