import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Subject } from 'rxjs';

import { HOME_URL } from './home.url-config';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  signInStatusChange = new Subject();
  cartCountUpdation = new Subject();
  compareCountUpdation = new Subject();
  searchProductData = new Subject();
  getCartDetailData = new Subject();
  constructor(private http: HttpClient) {
  }

  getTodayProductList(data) {
    return this.http.get(HOME_URL.TODAY_DEAL, { params: data });
  }

  getHotProductList(data) {
    return this.http.get(HOME_URL.HOT_PRODUCTS, { params: data });
  }

  getBestSellingProductList(data) {
    return this.http.get(HOME_URL.BEST_SELLING, { params: data });
  }

  getFeaturedBrandsList(featureObj) {
    return this.http.get(HOME_URL.BRANDS, {params: featureObj});
  }

  getFeaturedProductList(data) {
    return this.http.get(HOME_URL.FEATURED_PRODUCTS, { params: data });
  }

  getHomeBannersList() {
    return this.http.get(HOME_URL.BANNERS);
  }

  getAdvertisementList() {
    return this.http.get(HOME_URL.ADVERTISEMENT);
  }

  getAllBrandsList(data) {
    return this.http.get(HOME_URL.FEATURED_BRANDS, { params: data });
  }

  setSignInStatus(statusData) {
    this.signInStatusChange.next(statusData);
  }

  getSignInStatus() {
    return this.signInStatusChange.asObservable();
  }

  setCartCountChange(statusData) {
    this.cartCountUpdation.next(statusData);
  }

  getCartCountChange() {
    return this.cartCountUpdation.asObservable();
  }

  setCompareCountChange(statusData) {
    this.compareCountUpdation.next(statusData);
  }

  getCompareCountChange() {
    return this.compareCountUpdation.asObservable();
  }

  setSearchStatus(searchStatus) {
    this.searchProductData.next(searchStatus);
  }

  getSearchStatus() {
    return this.searchProductData.asObservable();
  }

  getCartDetails(cartDetail) {
    this.getCartDetailData.next(cartDetail);
  }

  getCartDetailStatus() {
    return this.getCartDetailData.asObservable();
  }

  advertisementAddLog(adData) {
    return this.http.patch(HOME_URL.ADVERTISEMENT_LOG + adData.adId, adData);
  }
}
