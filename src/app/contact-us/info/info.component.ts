import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material';

import { Language, TranslationService } from 'angular-l10n';

import { API_STATUS_CODE, COUNTRY_CODE } from './../../app.constants';
import { ContactUsService } from '../contact-us.service';
import { WINDOW } from '@ng-toolkit/universal';

import { SNACKBAR_ALERT } from '../../app-setting';
@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  @Language() lang: string;
  contactDetails: any;
  countryCode = COUNTRY_CODE;

  constructor(
    @Inject(WINDOW) private window: any,
    private contactService: ContactUsService,
    private translation: TranslationService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getStoreContactDetails();
  }

  getStoreContactDetails() {
    this.contactService.getStoreContactDetails().subscribe((response: any) => {
      if (response) {
        this.contactDetails = response.data[0];
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.message, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getLocation() {
    const googleUrl = 'https://www.google.com/maps/dir//' + this.contactDetails.latitude + ',' + this.contactDetails.longitude + '/@' +
                       this.contactDetails.latitude + ',' + this.contactDetails.longitude;
    return googleUrl;    
  }
}
