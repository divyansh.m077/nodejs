import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { TitleCasePipe } from '@angular/common';
import { MatDialog } from '@angular/material';

import { Language, TranslationService } from 'angular-l10n';
import * as moment from 'moment';
import { isEqual } from 'lodash';
import { PROFILE_INFO_RULE, SNACKBAR_ALERT, SNACKBAR_SUCCESS, PRO_INFO_RULE } from '../../../app-setting';
import { API_STATUS_CODE, COUNTRY_CODE } from '../../../app.constants';
import { UserService } from '../../user.service';
import { SharedService } from './../../../shared/shared.service';
import { MatSnackBar } from '@angular/material';
import { StorageControllerService } from '../../../storage-controller.service';
import { OtpComponent } from '../../../shared/otp/otp.component';
@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.scss']
})
export class ProfileDetailsComponent implements OnInit {
  @Input() mobPattern: any;
  @Language() lang: string;
  @ViewChild('dateFormat') dateFormat;
  profileInfo: FormGroup;
  profileInfoRules: any = PRO_INFO_RULE();
  profileDetails: any;
  buttonDisable: boolean;
  genderData: any;
  patternDetails: any;
  dob: Date;
  public maxDob: Date;
  countryCode = COUNTRY_CODE;

  constructor(
    private storage: StorageControllerService,
    private fb: FormBuilder,
    private userService: UserService,
    private sharedService: SharedService,
    private snackBar: MatSnackBar,
    private translation: TranslationService,
    private titleCase: TitleCasePipe,
    public dialog: MatDialog
  ) {
    this.genderData = [
      'Male',
      'Female',
      'Transgender'
    ];
    this.dob = new Date();
    this.maxDob = new Date(this.dob.getFullYear() - 18, this.dob.getMonth(), this.dob.getDate());
  }

  getSiteSettingDetailForPatterns() {
    this.patternDetails = this.sharedService.getSiteSettingDetails();
    if (this.patternDetails) {
      this.maxDob = new Date(this.dob.getFullYear() - this.patternDetails.minimumAge, this.dob.getMonth(), this.dob.getDate());
      if (this.patternDetails.phoneNumberFormat) {
        const phoneRegx = new RegExp(this.patternDetails.phoneNumberFormat);
        if (this.profileInfo.controls['phone'].value) {
          this.profileInfo.controls['phone'].markAsTouched();
        }
        this.profileInfo.controls['phone'].setValidators([Validators.required, Validators.pattern(phoneRegx)]);
        this.profileInfo.controls['phone'].updateValueAndValidity();
      }
    }
  }

  ngOnInit() {
    this.formInitialization();
    this.getUserProfileDetails();
  }

  formInitialization() {
    const profileRules = PRO_INFO_RULE();
    this.profileInfo = this.fb.group({
      'firstName': [this.profileDetails ? this.profileDetails.firstName : '',
      [Validators.required, Validators.minLength(profileRules.FIRST_NAME_MIN_LENGTH),
      Validators.maxLength(profileRules.NAME_MAX_LENGTH),
      Validators.pattern(profileRules.NAME_PATTERN)]],
      'lastName': [this.profileDetails ? this.profileDetails.lastName : '',
      [Validators.required, Validators.minLength(profileRules.LAST_NAME_MIN_LENGTH),
      Validators.maxLength(profileRules.NAME_MAX_LENGTH),
      Validators.pattern(profileRules.NAME_PATTERN)]],
      'email': [this.profileDetails ? this.profileDetails.userEmail : '',
      [Validators.required, Validators.pattern(profileRules.EMAIL_PATTERN),
      Validators.maxLength(profileRules.EMAIL_MAX_LENGTH)]],
      'phone': [this.profileDetails ? this.profileDetails.userMobile : '', [Validators.required]],
      'dateOfBirth': [this.profileDetails ? this.profileDetails.dateOfBirth : '', Validators.required],
      'gender': [this.profileDetails ? this.profileDetails.gender : '', Validators.required],
      'emailSubscription': [this.profileDetails ? (this.profileDetails.isEmailSubscription === 1 ? true : false) : false,
      Validators.required],
    });
    if (this.profileDetails) {
      this.onFormValueChanges();
    }
    this.getSiteSettingDetailForPatterns();
  }

  getUserProfileDetails() {
    if (!!this.storage.get('id')) {
      this.userService.getUserProfileDetails().subscribe((response: any) => {
        if (response) {
          this.profileDetails = response.data[0];
          this.profileInfo.controls['dateOfBirth'].setValue(this.profileDetails.dateOfBirth);
          this.buttonDisable = true;
          this.formInitialization();
        }
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
    }
  }


  onSubmit() {
    this.getProfileData();
    this.userService.saveUserProfileDetails(this.getProfileData()).subscribe((response: any) => {
      if (response) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        this.getUserProfileDetails();
        const userDetail = JSON.parse(this.storage.get('userDetails'));
        const updatedDetails = {
          firstName: this.titleCase.transform(this.profileInfo.get('firstName').value),
          lastName: this.titleCase.transform(this.profileInfo.get('lastName').value),
          userEmail: this.profileInfo.value.email,
          userMobile: this.profileInfo.value.phone,
          providers: userDetail.providers
        };
        this.storage.set('userDetails', JSON.stringify(updatedDetails));
        const name = this.profileInfo.value.firstName + ' ' + this.profileInfo.value.lastName;
        this.storage.set('userName', name);
        this.storage.set('userName', name);
        this.userService.setUserName(true);
        if (response.message === 'OTP_SEND_SUCCESSFULLY') {
          const dialogRef = this.dialog.open(OtpComponent, {
            panelClass: 'view-business-wrap',
            data: this.getProfileData(),
            disableClose: true
          })
          dialogRef.afterClosed().subscribe((response: any) => {
            this.getUserProfileDetails();
          });
        }
        this.buttonDisable = true;
      }
    }, (errorResponse: any) => {
      this.snackBar.open(this.translation.translate
        (errorResponse.error.error === undefined ?
       errorResponse.error.message : errorResponse.error.error),
       this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    });
  }

  getProfileData() {
    return ({
      firstName: this.profileInfo.value.firstName.charAt(0).toUpperCase() + this.profileInfo.value.firstName.slice(1).toLowerCase(),
      lastName: this.profileInfo.value.lastName.charAt(0).toUpperCase() + this.profileInfo.value.lastName.slice(1).toLowerCase(),
      userEmail: this.profileInfo.value.email,
      userMobile: this.profileInfo.value.phone,
      gender: this.profileInfo.value.gender,
      isEmailSubscription: this.profileInfo.value.emailSubscription === true ? 1 : 0,
      dateOfBirth: moment(this.profileInfo.value.dateOfBirth).format('YYYY-MM-DD')
    });
  }

  onFormValueChanges(): void {
    this.profileInfo.valueChanges.subscribe(val => {
      const firstName = isEqual(val.firstName, this.profileDetails.firstName);
      const lastName = isEqual(val.lastName, this.profileDetails.lastName);
      const email = isEqual(val.email, this.profileDetails.userEmail);
      const phone = isEqual(val.phone, this.profileDetails.userMobile);
      const dateOfBirth = isEqual(val.dateOfBirth, this.profileDetails.dateOfBirth);
      const gender = isEqual(val.gender, this.profileDetails.gender);
      const emailSubscription = isEqual(val.emailSubscription, (this.profileDetails ? (this.profileDetails.isEmailSubscription === 1 ? true : false) : false));

      if (firstName === false || lastName === false || email === false || phone === false || dateOfBirth === false ||
        gender === false || emailSubscription === false) {
        this.buttonDisable = false;
      } else {
        this.buttonDisable = true;
      }
    });
  }
}
