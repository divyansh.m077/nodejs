import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {
  USER_REGISTER_URL,
  USER_SIGNIN_URL,
  REMOVE_WISHLIST_URL,
  USER_REGISTER_SOCIAL_URL,
  FORGOT_PASSWORD_URL,
  RESET_PASSWORD_URL,
  CITY_LIST_URL,
  STATE_LIST_URL,
  COUNTRY_LIST_URL,
  ADDRESS_TYPE_URL,
  GET_MANAGE_ADDRESS_LIST_URL,
  ADD_MANAGE_ADDRESS_URL,
  UPDATE_MANAGE_ADDRESS_URL,
  DELETE_MANAGE_ADDRESS_URL,
  GET_MANAGE_ADDRESS_URL,
  CHANGE_PASSWORD_URL,
  UPDATE_USER_PROFILE_URL,
  GET_USER_USER_PROFILE_URL,
  CUSTOMER_REVIEW_URL,
  SAVE_NOTIFY_PREFERENCE_URL,
  GET_NOTIFY_PREFERENCE_URL,
  USER_URL,
  REGISTER_OTP_URL,
  REGION_URL,
  DISTRICT_URL 
} from './user.url-config';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userName = new Subject();
  userDetails: any;

  constructor(private http: HttpClient) {
  }

  initializeUserDetails(data) {
    this.userDetails = data;
  }

  getUserDetails() {
    return this.userDetails;
  }

  saveUserRegisterationDetails(userData) {
    return this.http.post(USER_REGISTER_URL, userData);
  }

  saveSocialUserRegisterationDetails(userData) {
    return this.http.post(USER_REGISTER_SOCIAL_URL, userData);
  }

  signInUserDetails(signInData) {
    return this.http.post(USER_SIGNIN_URL, signInData);
  }

  removeMyWishListProduct(wishListId) {
    return this.http.delete(REMOVE_WISHLIST_URL + wishListId);
  }

  forgotPassword(userData) {
    return this.http.post(FORGOT_PASSWORD_URL, userData);
  }

  resetPassword(userData) {
    return this.http.post(RESET_PASSWORD_URL, userData);
  }

  getUserProfileDetails() {
    return this.http.get(GET_USER_USER_PROFILE_URL);
  }

  saveUserProfileDetails(profileDetails: any) {
    return this.http.put(UPDATE_USER_PROFILE_URL, profileDetails);
  }

  changePassword(changePasswordetails: any) {
    return this.http.post(CHANGE_PASSWORD_URL, changePasswordetails);
  }

  getCountryList() {
    return this.http.get(COUNTRY_LIST_URL);
  }

  getCityList(stateId) {
    return this.http.get(CITY_LIST_URL + stateId);
  }

  getStateList(countryId) {
    return this.http.get(STATE_LIST_URL + countryId);
  }

  getAddressType() {
    return this.http.get(ADDRESS_TYPE_URL);
  }

  getManageAddressList() {
    return this.http.get(GET_MANAGE_ADDRESS_LIST_URL );
  }

  addManageAddress(dataToSend) {
    return this.http.post(ADD_MANAGE_ADDRESS_URL, dataToSend);
  }

  updateManageAddress(data) {
    return this.http.put(UPDATE_MANAGE_ADDRESS_URL, data);
  }

  deleteManageAddress(data) {
    return this.http.post(DELETE_MANAGE_ADDRESS_URL, data);
  }

  getAddressInfo(addressId) {
    return this.http.get(GET_MANAGE_ADDRESS_URL + addressId);
  }


  getCustomerReviewDetails(userData) {
    return this.http.post(CUSTOMER_REVIEW_URL, userData);
  }

  saveNotificationPreference(dataToSend) {
    return this.http.post(SAVE_NOTIFY_PREFERENCE_URL, dataToSend);
  }

  getNotificationPreference() {
    return this.http.get(GET_NOTIFY_PREFERENCE_URL);
  }

  getOrderComplaintTypeList() {
    return this.http.get(USER_URL.COMPLAINT_TYPE);
  }

  getOrderComplaintStatusList() {
    return this.http.get(USER_URL.COMPLAINT_STATUS);
  }

  getOrderList() {
    return this.http.get(USER_URL.ORDER);
  }

  getProductBasedOnOrder(orderId) {
    return this.http.get(USER_URL.PRODUCT_BY_ORDER + orderId);
  }

  getOrderComplaintList() {
    return this.http.get(USER_URL.COMPLAINT);
  }

  saveOrderComplaint(complaintData) {
    return this.http.post(USER_URL.ADD_COMPLAINT, complaintData);
  }

  getWalletTransaction(transactionDetails) {
    return this.http.post(USER_URL.WALLET_TRANSACTION_LIST, transactionDetails);
  }

  getWalletAmount() {
    return this.http.get(USER_URL.WALLET_AMOUNT);
  }

    /**
   * [GET] get orders made my a specific user with pagination
   */
  getMyOrders(startLimit, offSet) {
    return this.http.get<any>(USER_URL.MY_ORDERS, { params: { startLimit, offSet }});
  }

  /**
   * [GET] get reviews and rating given by a specific user with pagination
   */
  getReviewsAndRating(startLimit, offSet) {
    return this.http.get<any>(USER_URL.PRODUCT_REVIEWS_AND_RATING, { params: { startLimit, offSet }});
  }

  /**
   * [DELETE] delete a review
   */
  deleteReviewUsingProductIdAndUserId(productId) {
    return this.http.delete(`${USER_URL.DELETE_RATINGS_REVIEWS} + ${productId}`);
  }

  setUserName(userName) {
     this.userName.next(userName);
  }

  getUserName() {
     return this.userName.asObservable();
  }

  getCancelReasonList() {
    return this.http.get(USER_URL.CANCEL_REASON_LIST);
  }

  confirmOrderCancel(orderCancelData) {
    return this.http.post(USER_URL.CONFIRM_ORDER_CANCEL, orderCancelData);
  }

  getDashboardDetails() {
    return this.http.get(USER_URL.DASHBOARD);
  }

  getOrderSummary(orderId) {
    return this.http.get(USER_URL.ORDER_DETAILS + orderId);
  }

  verifyEmail(verifyToken) {
    return this.http.get(USER_URL.VERIFY_EMAIL + verifyToken);
  }

  resendEmail(userEmail) {
    return this.http.post(USER_URL.RESEND_EMAIL, userEmail);
  }

  updateProfileImage(imageData) {
    return this.http.post(USER_URL.PROFILE_IMAGE, imageData);
  }

  deleteProfileImage() {
    return this.http.delete(USER_URL.DELETE_PROFILE);
  }

  setPasswordDetails(passwordData) {
    return this.http.post(USER_URL.SET_PASSWORD, passwordData);
  }

  getPackageDetails(orderId) {
    return this.http.get(USER_URL.PACKAGE_DETAILS + '/' + orderId);
  }

  getTrackingDetails(trackingId) {
    return this.http.get(USER_URL.TRACKING_DETAILS + '/' + trackingId);
  }

  getRegisterOtp(reqObj) {
    return this.http.post(REGISTER_OTP_URL, reqObj);
  }

  resendRegisterOtp(reqObj) {
    return this.http.post(REGISTER_OTP_URL, reqObj);
  }

  updateUserProfile(userData) {
    return this.http.put(USER_URL.UPDATE_PROFILE, userData);
  }

  getRegionList(stateId) {
    return this.http.get(REGION_URL + stateId);
  }

  getDistrictList(regionId) {
    return this.http.get(DISTRICT_URL + regionId);
  }
}
