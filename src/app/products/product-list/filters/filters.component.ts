import { Component, OnInit, Output, EventEmitter, Input , Inject} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MatDialog, MatSnackBar } from '@angular/material';
import { Language, TranslationService } from 'angular-l10n';
import { cloneDeep, remove, forEach, uniqBy, isEmpty } from 'lodash';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';

import { ProductService } from '../../products.service';
import { FILTER_RULE } from './../../../app-setting';
import { API_STATUS_CODE } from '../../../app.constants';

import { SNACKBAR_ALERT } from '../../../app-setting';
import { StorageControllerService } from '../../../storage-controller.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
  providers: [NgbDropdownConfig]
})
export class FiltersComponent implements OnInit {
  @Output() filteredProductList = new EventEmitter<any>();
  filterList: any;
  originalfilterList: any;
  @Language() lang: string;
  searchProduct: any = [];
  searchOption: any;
  priceConfig: any = {
    behaviour: 'drag',
    connect: true,
    start: [1, 5],
    keyboard: true,  // same as [keyboard]="true"
    step: 1,
    pageSteps: 5,  // number of page steps, defaults to 10
    pips: {
      mode: 'count',
      density: 1,
      values: 5,
      stepped: true
    }
  };
  subCategoryId: number;
  filteredData: any = [];
  optionData: any = [];
  priceData: any = {};
  selectedOptions: any = [];
  filterRules: any = FILTER_RULE;
  @Input() set filterDataList(value) {
    this.filterList = value;
    this.filterList.Price[0].MinSalesPrice !== this.filterList.Price[0].MaxSalesPrice ?
    this.priceConfig.start = [this.filterList.Price[0].MinSalesPrice, this.filterList.Price[0].MaxSalesPrice] : null;
    this.originalfilterList = cloneDeep(this.filterList);
  }
  constructor(
    private storage: StorageControllerService,
    private productService: ProductService,
    public dialog: MatDialog,
    public config: NgbDropdownConfig,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private translation: TranslationService
  ) {
      this.config.placement = 'top-left';
      this.config.autoClose = false;
      this.route.params.subscribe(params => {
      this.subCategoryId = params.id;
    });
  }

  ngOnInit() {
  }

  findTheProduct(filterData: any, data: any, index: any) {
    if (data[index] === '') {
      const currentFilterList = remove(this.originalfilterList.data, {'Attribute': filterData.Attribute});
      this.originalfilterList.data.splice(index, 0, this.filterList.data[index]);
    } else {
      const currentFilterList = remove(this.originalfilterList.data, {'Attribute': filterData.Attribute});
      const filteredData = Object.assign([], this.filterList.data[index].Options).filter(
        item => item.Name.toLowerCase().indexOf(data[index].toLowerCase()) > -1
     );
      const filterObj = {
        'Attribute': filterData.Attribute,
        'AttributeName': filterData.AttributeName,
        'AttributeTypeName': filterData.AttributeTypeName,
        'Options': filteredData
      };
      this.originalfilterList.data.splice(index, 0, filterObj);
    }
   this.originalfilterList.data[index].expand = true;
  }

  getCheckedValues(filterOptions: any, optionValues: any, event: any) {

    if (event.checked === true) {
     if (this.selectedOptions.length > 0 ) {
        forEach(this.selectedOptions, (option) => {
          if (option.Attribute === filterOptions.Attribute) {
            option.Options.push(optionValues);
          } else {
            this.optionData.push(optionValues);
            this.selectedOptions.push({
              'Attribute': filterOptions.Attribute,
              'Options': this.optionData
            });
            this.optionData = [];
          }
        });
     } else {
        this.optionData.push(optionValues);
        this.selectedOptions.push({
          'Attribute': filterOptions.Attribute,
          'Options': this.optionData
        });
        this.optionData = [];
     }
      this.selectedOptions = uniqBy(this.selectedOptions, 'Attribute');
      this.getFilteredProducts();
    } else {
      if (this.selectedOptions.length > 0 ) {
        forEach(this.selectedOptions, (option) => {
          if (option.Attribute === filterOptions.Attribute) {
            remove(option.Options, {'Id': optionValues.Id});
          }
          if(option.Options.length === 0) {
            remove(this.selectedOptions, {'Attribute': filterOptions.Attribute});
          }
        });
        this.getFilteredProducts();
      }
    }
  }

  getPriceValue(priceValue) {
    this.priceData = {
      'MinSalesPrice': priceValue[0],
      'MaxSalesPrice': priceValue[1]
    };
    this.getFilteredProducts();
  }

  getFilteredProducts() {
    const filterArray = {
      'subCategoryId': this.subCategoryId,
      'Price': isEmpty(this.priceData) ? null : this.priceData,
      'data': this.selectedOptions.length === 0 ? null : this.selectedOptions,
      'userId': this.storage.get('id') ? this.storage.get('id') : 0
    };
    if (filterArray.data !== null || filterArray.Price !== null) {
      this.productService.getFilterProductLists(filterArray).subscribe((response: any) => {
        this.filteredProductList.emit(response);
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
    } else {
      this.filteredProductList.emit(true);
    }

  }
}
