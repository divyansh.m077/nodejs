import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { CookiesService } from '@ngx-utils/cookies';

@Injectable({
  providedIn: 'root'
})
export class StorageControllerService {

  constructor(
    @Inject(PLATFORM_ID) private platform: Object,
    private cookies: CookiesService,
  ) { }

  keysToStoreInLocalStorage: Array<string> = [
    'cartStorage',
    'compareStorage',
    'previousUrl',
    'currentUrl',
    'id',
    'language',
    'subscriptionPlan'
  ];
  keysToStoreInSessionStorage:  Array<string> = [
    'userDetails',
    'userImage',
    'couponCode',
    'isFromCategory',
    'searchData',
    'storeDetails',
    'offSet'
  ];
  keysToStoreInCookieStorage:  Array<string> = [
    'id',
    'token',
    'userName',
    'defaultLocale'
  ];

  public set(key, value) {
    this.whereToStore(key).map(storage => {
      this.setStorage(key, value, storage);
    });
  }

  public get(key) {
    return this.whereToStore(key).map(storage => {
       return this.getStorage(key, storage);
    })[0];
  }

  public remove(key) {
    this.whereToStore(key).map(storage => {
      this.removeStorage(key, storage);
    });
  }


  private setStorage(key, value, storage) {
    if (this.isBrowser()) {
      if (storage === 'localStorage') {
        localStorage.setItem(key, value);
      }
      if (storage === 'sessionStorage') {
        sessionStorage.setItem(key, value);
      }
    }
    if (storage === 'cookieStorage') {
      this.cookies.put(key, value);
    }
  }

  private getStorage(key, storage): string {
    if (this.isBrowser()) {
      if (storage === 'localStorage') {
        return localStorage.getItem(key);
      }
      if (storage === 'sessionStorage') {
        return sessionStorage.getItem(key);
      }
    }
    if (storage === 'cookieStorage') {
      return this.cookies.get(key);
    }
  }

  private removeStorage(key, storage) {
    if (this.isBrowser()) {
      if (storage === 'localStorage') {
         localStorage.removeItem(key);
      }
      if (storage === 'sessionStorage') {
         sessionStorage.removeItem(key);
      }
    }
    if (storage === 'cookieStorage') {
       this.cookies.remove(key);
    }
  }

  private isBrowser(): boolean {
    return  isPlatformBrowser(this.platform);
  }

  private whereToStore(key): Array<string> {
    const value: Array<string> = [];
    if (this.keysToStoreInLocalStorage.find(x => x === key)) {
      value.push('localStorage');
    }
    if (this.keysToStoreInSessionStorage.find(x => x === key)) {
      value.push('sessionStorage');
    }
    if (this.keysToStoreInCookieStorage.find(x => x === key)) {
      value.push('cookieStorage');
    }
    return value;
  }

}
