import { Component, Inject } from '@angular/core';
import { Language } from 'angular-l10n';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'description-alert-popup',
  templateUrl: './description-popup.component.html',
  styleUrls: ['./description-popup.component.scss']
})
export class DescriptionPopupComponent {
  @Language() lang: string;
  constructor(public dialog: MatDialogRef<DescriptionPopupComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  onClose() {
    this.dialog.close();
  }
}
