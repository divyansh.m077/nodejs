import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TitleCasePipe } from '@angular/common';

import { MatFormFieldModule, MatInputModule, MatButtonModule, MatIconModule } from '@angular/material';
import { TranslationModule } from 'angular-l10n';
import { NgxCaptchaModule } from 'ngx-captcha';

import { ContactUsComponent } from './contact-us.component';
import { ContactUsRoutingModule } from './contact-us-routing.module';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { InfoComponent } from './info/info.component';
import { ContactUsService } from './contact-us.service';

@NgModule({
  imports: [
    CommonModule,
    ContactUsRoutingModule,
    MatFormFieldModule, MatInputModule, MatButtonModule, MatIconModule,
    ReactiveFormsModule,
    TranslationModule,
    NgxCaptchaModule
  ],
  declarations: [ContactUsComponent, EnquiryComponent, InfoComponent],
  providers: [TitleCasePipe, ContactUsService]
})
export class ContactUsModule { }
