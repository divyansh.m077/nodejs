import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { Language, TranslationService } from 'angular-l10n';
import { MatDialog, MatSnackBar } from '@angular/material';

import { API_STATUS_CODE, ALERT_SYSTEM_TITLE, COUNTRY_CODE } from './../../app.constants';
import { ContactUsService } from '../contact-us.service';
import { SharedService } from '../../shared/shared.service';
import { ENQUIRY_RULE, settings, ENQ_RULE } from '../../app-setting';
import { AlertPopupComponent } from '../../shared/alert-popup/alert-popup.component';
import { SNACKBAR_ALERT } from '../../app-setting';
import { UserService } from '../../user/user.service';
import { HomeService } from '../../home/home.service';
import { StorageControllerService } from '../../storage-controller.service';

@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  styleUrls: ['./enquiry.component.scss']
})
export class EnquiryComponent implements OnInit, OnDestroy {
  @Language() lang: string;
  enquiryForm: FormGroup;
  patternDetails: any;
  public enquiryRules: any = ENQ_RULE();
  siteKey;
  captchaSize;
  captchaTheme;
  captchaType;
  captchaLanguage;
  isInBrowser = false;
  actionSubscribe$: any;
  isNameEditable: boolean;
  isEmailEditable: boolean;
  isPhoneEditable: boolean;
  signInSubscribe$: any;
  userDetails: any;
  countryCode = COUNTRY_CODE;
  constructor(private fb: FormBuilder,
              private contactService: ContactUsService,
              private userService: UserService,
              private sharedService: SharedService,
              private translation: TranslationService,
              private dialog: MatDialog,
              private router: Router,
              private snackBar: MatSnackBar,
              private homeService: HomeService,
              private storage: StorageControllerService
            ) {
    this.isNameEditable = false;
    this.isEmailEditable = false;
    this.isPhoneEditable = false;
    this.siteKey = settings.captchaKey;
    if (typeof window !== 'undefined') {
      this.isInBrowser = true;
    }
    this.captchaSize = 'Normal';
    this.captchaTheme = 'Light';
    this.captchaType = 'Image';
    this.captchaLanguage = 'en';
    this.formInitialization();
    this.signInSubscribe$ = this.homeService.getSignInStatus().subscribe((response: any) => {
      this.getUserInfo();
    });
  }

  formInitialization(data?) {
    const enquiryRules = ENQ_RULE();
    this.isNameEditable = (data && data.firstName) ? true : false;
    this.isEmailEditable = (data && data.userEmail) ? true : false;
    this.isPhoneEditable = (data && data.userMobile) ? true : false;
    this.enquiryForm = this.fb.group({
      'name': [{value: data ? data.firstName : '', disabled: this.isNameEditable},
                  [Validators.required, Validators.minLength(enquiryRules.NAME_MIN_LENGTH),
                    Validators.maxLength(enquiryRules.NAME_MAX_LENGTH),
                    Validators.pattern(enquiryRules.NAME_PATTERN)]],
      'email': [{value: data ? data.userEmail : '', disabled: this.isEmailEditable},
                [Validators.required, Validators.pattern(enquiryRules.EMAIL_PATTERN)]],
      'phone': [{value: data ? data.userMobile : '', disabled: this.isPhoneEditable}, [Validators.required]],
      'subject': ['', [Validators.required, Validators.minLength(enquiryRules.SUBJECT_MIN_LENGTH),
                      Validators.maxLength(enquiryRules.SUBJECT_MAX_LENGTH)]],
      'message': ['', [Validators.required, Validators.minLength(enquiryRules.MESSAGE_MIN_LENGTH)]],
      'isCaptchaVerified': ['', [Validators.required]],
    });
    this.getSiteSettingDetailForPatterns();
  }

  getSiteSettingDetailForPatterns() {
    this.patternDetails = this.sharedService.getSiteSettingDetails();
    if (this.patternDetails) {
      if (this.patternDetails.phoneNumberFormat) {
        const phoneRegx = new RegExp(this.patternDetails.phoneNumberFormat);
        this.enquiryForm.controls['phone'].setValidators([Validators.required, Validators.pattern(phoneRegx)]);
        this.enquiryForm.controls['phone'].updateValueAndValidity();
      }
    }
  }

  ngOnInit() {
    this.getUserInfo();
  }

  getUserInfo() {
    if (this.storage.get('id')) {
      this.userService.getUserProfileDetails().subscribe((response: any) => {
        if (response) {
          this.userDetails = response.data[0];
          this.formInitialization(this.userDetails);
        }
      }, (error: any) => {

      });
    } else {
      this.formInitialization();
    }
  }

  saveEnquiryFormDetails() {
    const enquiryDetails = {
      name: this.isNameEditable ? this.userDetails.firstName : this.enquiryForm.value.name,
      email: this.isEmailEditable ? this.userDetails.userEmail : this.enquiryForm.value.email,
      phone: this.isPhoneEditable ? this.userDetails.userMobile : this.enquiryForm.value.phone,
      subject: this.enquiryForm.value.subject,
      message: this.enquiryForm.value.message,
      isCaptchaVerified: this.enquiryForm.value.isCaptchaVerified,
    };
    this.contactService.addEnquiryDetails(enquiryDetails).subscribe((response: any) => {
      if (response) {
       const dialogRef =  this.dialog.open(AlertPopupComponent, {
          data: {
            title: this.translation.translate('labelSuccess'),
            message: [this.translation.translate('labelEnquirySuccess')],
            action: ALERT_SYSTEM_TITLE.success,
            buttonName: this.translation.translate('buttonContinueShopping')
          }
        });
        this.actionSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
          if (data) {
            this.router.navigate(['/']);
            this.dialog.closeAll();
          }
        });
        this.enquiryForm.reset();
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  ngOnDestroy() {
    if (this.signInSubscribe$) {
      this.signInSubscribe$.unsubscribe();
    }
  }
}
