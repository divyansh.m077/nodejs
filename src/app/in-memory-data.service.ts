import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 11, name: 'Mr. Nice' },
      { id: 12, name: 'Narco' },
      { id: 13, name: 'Bombasto' },
      { id: 14, name: 'Celeritas' },
      { id: 15, name: 'Magneta' },
      { id: 16, name: 'RubberMan' },
      { id: 17, name: 'Dynama' },
      { id: 18, name: 'Dr IQ' },
      { id: 19, name: 'Magma' },
      { id: 20, name: 'Tornado' }
    ];
    const categories = [
          {
            mainCategoryId : 25,
            categoryName : 'Electronics & office',
            src	: 'image1',
            ParentCategory : 0,
            subCategory : [
              {
                'categoryId' : 2,
                'categoryName' : 'Tv & videos',
                'src'	: 'image2',
                'ParentCategory' : 1,
                'subCategory' : [
                  {
                    'categoryId' : 4,
                    'categoryName' : 'TVs',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 5,
                    'categoryName' : 'DVD & Blue-ray players',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 6,
                    'categoryName' : 'Home Audio and Theaters',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 7,
                    'categoryName' : 'TV Accessories',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                ]
              },
              {
                'categoryId' : 3,
                'categoryName' : 'Cell Phones',
                'src'	: 'image2',
                'ParentCategory' : 1,
                'subCategory' : [
                  {
                    'categoryId' : 8,
                    'categoryName' : 'Stright Talk Phones',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 9,
                    'categoryName' : 'Unlocked Phones',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 9,
                    'categoryName' : 'Contract Phones',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 9,
                    'categoryName' : 'No-Contract Phones',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 9,
                    'categoryName' : 'Prepaid Minutes & Data',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 10,
                    'categoryName' : 'Cases & Accessories',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                ]
              },
              {
                'categoryId' : 11,
                'categoryName' : 'Shop Electronics',
                'src'	: 'image2',
                'ParentCategory' : 1,
                'subCategory' : [
                  {
                    'categoryId' : 8,
                    'categoryName' : 'Mobile Cables',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 9,
                    'categoryName' : 'Chargers',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 9,
                    'categoryName' : 'Selfie Sticks',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 9,
                    'categoryName' : 'Screen Guards',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 9,
                    'categoryName' : 'Power Banks',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 10,
                    'categoryName' : 'Mobile Cases',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                ]
              },
              {
                'categoryId' : 12,
                'categoryName' : 'Smart Homes',
                'src'	: 'image2',
                'ParentCategory' : 1,
                'subCategory' : [
                  {
                    'categoryId' : 8,
                    'categoryName' : 'Show Pieces',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 9,
                    'categoryName' : 'Wall Shelves',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 9,
                    'categoryName' : 'Mats & Carpets',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 9,
                    'categoryName' : 'Bean Bags',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 9,
                    'categoryName' : 'Curtains',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 10,
                    'categoryName' : 'Pots & Pans',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                ]
              },
              {
                'categoryId' : 15,
                'categoryName' : 'iPad & Tablets',
                'src'	: 'image2',
                'ParentCategory' : 1,
                'subCategory' : [
                  {
                    'categoryId' : 2,
                    'categoryName' : 'iPad',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 4,
                    'categoryName' : 'Windows Tablets',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 5,
                    'categoryName' : 'Android Tablets',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 6,
                    'categoryName' : 'Accessories',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  }
                ]
              },
              {
                'categoryId' : 16,
                'categoryName' : 'Computers',
                'src'	: 'image2',
                'ParentCategory' : 1,
                'subCategory' : [
                  {
                    'categoryId' : 2,
                    'categoryName' : 'Laptops',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 4,
                    'categoryName' : 'Desktops',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 5,
                    'categoryName' : 'PC Gaming',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 6,
                    'categoryName' : 'Printers & Suppliers',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 2,
                    'categoryName' : 'Networking',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 4,
                    'categoryName' : 'Software',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 5,
                    'categoryName' : 'Monitors',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 6,
                    'categoryName' : 'Accessories',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  }
                ]
              },
              {
                'categoryId' : 18,
                'categoryName' : 'Auto Electronics',
                'src'	: 'image2',
                'ParentCategory' : 1,
                'subCategory' : [
                  {
                    'categoryId' : 2,
                    'categoryName' : 'Gps & Navigation',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  }
                ]
              },
              {
                'categoryId' : 19,
                'categoryName' : 'Cameras & Camcorders',
                'src'	: 'image2',
                'ParentCategory' : 1,
                'subCategory' : [
                  {
                    'categoryId' : 2,
                    'categoryName' : 'Lens',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 2,
                    'categoryName' : 'Tripodes',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 2,
                    'categoryName' : 'DSLR',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  }
                ]
              },
              {
                'categoryId' : 20,
                'categoryName' : 'Portable Audio',
                'src'	: 'image2',
                'ParentCategory' : 1,
                'subCategory' : [
                  {
                    'categoryId' : 2,
                    'categoryName' : 'iPad & Mp3 Players',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 4,
                    'categoryName' : 'Head Phones',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 5,
                    'categoryName' : 'Speakers & Docks',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  }
                ]
              },
              {
                'categoryId' : 21,
                'categoryName' : 'Office Supplies',
                'src'	: 'image2',
                'ParentCategory' : 1,
                'subCategory' : [
                  {
                    'categoryId' : 2,
                    'categoryName' : 'Office Electronics',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 4,
                    'categoryName' : 'School Supplies',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  }
                ]
              },
              {
                'categoryId' : 23,
                'categoryName' : 'Video Games',
                'src'	: 'image2',
                'ParentCategory' : 1,
                'subCategory' : [
                  {
                    'categoryId' : 2,
                    'categoryName' : 'Joy sticks',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  },
                  {
                    'categoryId' : 2,
                    'categoryName' : 'Cards',
                    'src'	: 'image2',
                    'ParentCategory' : 2,
                  }
                ]
              },
            ]
          },
          {
            mainCategoryId : 13,
            categoryName : 'Movies,Music & books',
            src	: 'image1',
            ParentCategory : 0,
            subCategory : [
              {
                categoryId : 2,
                categoryName : 'Musical Instruments',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Guitars',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Keyboards',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Drums',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Studio Essentials',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Keyboard Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Guitar Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              },
              {
                categoryId : 2,
                categoryName : 'Indian Instruments',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Tabla',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Harmonium',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Digital Instruments',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Flute',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              },
              {
                categoryId : 2,
                categoryName : 'PA & Stage',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Dj & Equipments',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Effects',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Amplifiers',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  categoryId : 2,
                  categoryName : 'Other Instruments',
                  src	: 'image2',
                  ParentCategory : 1,
                  subCategory: [{
                    'categoryId' : 2,
                    'categoryName' : 'Karoke',
                    'src'	: 'image2',
                    'ParentCategory' : 1
                  },
                  {
                    'categoryId' : 2,
                    'categoryName' : 'Mouth Organs',
                    'src'	: 'image2',
                    'ParentCategory' : 1
                  }
                ]
              }
            ]
          },
        ]},
          {
            mainCategoryId : 14,
            categoryName : 'Home,Furniture & Appliances',
            src	: 'image1',
            ParentCategory : 0,
            subCategory : [
              {
                categoryId : 2,
                categoryName : 'Kitchen Storage',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Water Bottles',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Lunch Boxes',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Kitchen Containers',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Flasks & Casseroles',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              },
              {
                categoryId : 2,
                categoryName : 'Furnishing',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Bedsheets',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'curtains',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Cushion & Pillow Covers',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Kitchen & Dining Linen',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              },
              {
                categoryId : 2,
                categoryName : 'Dinning & Serving',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Coffee Bugs',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Dinnerware',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Crockery',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              },
              {
                categoryId : 2,
                categoryName : 'Furniture',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Beds',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Sofas',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Tables',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Shoe Racks',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Wardrobes',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Dinning Tables & sets',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              }
            ]
          },
          {
            mainCategoryId : 26,
            categoryName : 'Baby & Kids',
            src	: 'image1',
            ParentCategory : 0,
            subCategory : [
              {
                categoryId : 2,
                categoryName : 'Toys',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Remote Control Toys',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Soft Toys',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Educational Toys',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Dolls & Doll Houses',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              },
              {
                categoryId : 2,
                categoryName : 'Baby Care',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Baby Bedding',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Bath & Skin Care',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Baby Grooming',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Health & Safety',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              }
            ]
          },
          {
            mainCategoryId : 27,
            categoryName : 'Sports',
            src	: 'image1',
            ParentCategory : 0,
            subCategory : [
              {
                categoryId : 2,
                categoryName : 'Gaming & Accessories',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'PS3',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'PS4',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Xbox One',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Gaming Consoles',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              },
              {
                categoryId : 2,
                categoryName : 'Accessories',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Cricket Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Badmiton Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Skating Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Indoor Games Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              }
            ]
          },
          {
            mainCategoryId : 28,
            categoryName : 'Mens',
            src	: 'image1',
            ParentCategory : 0,
            subCategory : [
              {
                categoryId : 2,
                categoryName : 'Watches',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Fastrack',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Cassio',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Titan',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Sonata',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              },
              {
                categoryId : 2,
                categoryName : 'Accessories',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Backpacks',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Wallets',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Belts',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Sunglasses',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Frames',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              },
              {
                categoryId : 2,
                categoryName : 'Men`s Grooming',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Deodorants',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Perfumes',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Beard Care',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              }
            ]
          },
          {
            mainCategoryId : 29,
            categoryName : 'Womens',
            src	: 'image1',
            ParentCategory : 0,
            subCategory : [
              {
                categoryId : 2,
                categoryName : 'Jewellery',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Artificial Jewellery',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Silver Jewellery',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Precious Jewellery',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              },
              {
                categoryId : 2,
                categoryName : 'Accessories',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Smart Bands',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Clutches',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Sling Bag',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Wallets',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Totes',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              },
              {
                categoryId : 2,
                categoryName : 'Beauty & Grooming',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Skin Care',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Hair Care',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Deodorants & Perfumes',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              }
            ]
          },
          {
            mainCategoryId : 30,
            categoryName : 'Offer Zone',
            src	: 'image1',
            ParentCategory : 0,
            subCategory : [
              {
                categoryId : 2,
                categoryName : 'Accessories',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Office Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Home Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Game Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              }
            ]
          },
          {
            mainCategoryId : 31,
            categoryName : 'New Collections',
            src	: 'image1',
            ParentCategory : 0,
            subCategory : [
              {
                categoryId : 2,
                categoryName : 'Accessories',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Office Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Home Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Game Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              }
            ]
          },
          {
            mainCategoryId : 32,
            categoryName : 'Summer Collections',
            src	: 'image1',
            ParentCategory : 0,
            subCategory : [
              {
                categoryId : 2,
                categoryName : 'Accessories',
                src	: 'image2',
                ParentCategory : 1,
                subCategory: [{
                  'categoryId' : 2,
                  'categoryName' : 'Office Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Home Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                },
                {
                  'categoryId' : 2,
                  'categoryName' : 'Game Accessories',
                  'src'	: 'image2',
                  'ParentCategory' : 1
                }
                ]
              }
            ]
          }
        ];

    const todaysDeal = {
      'Status': true,
      'Result': {
          'title': 'Today Deals',
          'dateStart': '22-03-2018',
          'dateEnd': '22-03-2018',
          'products': [{
              'productId': 1,
              'productName': 'Watches',
              'image': 'assets/images/dummy/watch.jpg',
              'wishList': 50,
              'discountType': '%',
              'discount': 25,
              'priceType': '$',
              'priceStatus': 'from',
              'originalPrice': '1000',
              'discountPrice': '750'
            },
            {
              'productId': 2,
              'productName': 'FootWear',
              'image': 'assets/images/dummy/footwear.jpg',
              'wishList': 10,
              'discountType': '%',
              'discount': 20,
              'priceType': '$',
              'priceStatus': 'just',
              'originalPrice': '800',
              'discountPrice': '640'
            },
            {
              'productId': 3,
              'productName': 'Bags and Travels',
              'image': 'assets/images/dummy/bag.jpg',
              'wishList': 4,
              'discountType': '%',
              'discount': 15,
              'priceType': '$',
              'priceStatus': 'upto',
              'originalPrice': '2000',
              'discountPrice': '1700'
            },
            {
              'productId': 4,
              'productName': 'HeadPhones',
              'image': 'assets/images/dummy/headset.jpg',
              'wishList': 2,
              'discountType': '%',
              'discount': 30,
              'priceType': '$',
              'priceStatus': 'flat',
              'originalPrice': '1500',
              'discountPrice': '1050'
            }
          ]
      }
    };

    const hotProducts = {
      'Status': true,
      'Result': {
          'title': 'Hot Products',
          'products': [{
              'productId': 1,
              'productName': 'Intex power bank Index power bank Index power bank',
              'productTagLine': 'product tag line / offers any',
              'image': 'assets/images/dummy/charger.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 4.9,
              'ratingCount': 153,
              'normalPrice': 1899,
              'discountPrice': 999,
              'priceType': '$'
            },
            {
              'productId': 2,
              'productName': 'Samsung Datacard 32GB',
              'productTagLine': 'product tag line / offers any',
              'image': 'assets/images/dummy/memory.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 60,
              'rating': 4.3,
              'ratingCount': 133,
              'normalPrice': 1149,
              'discountPrice': 799,
              'priceType': '$'
            },
            {
              'productId': 3,
              'productName': 'Logitech Mouse',
              'productTagLine': 'product tag line / offers any',
              'image': 'assets/images/dummy/logitech.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 59,
              'rating': 4.1,
              'ratingCount': 113,
              'normalPrice': 745,
              'discountPrice': 599,
              'priceType': '$'
            },
            {
              'productId': 4,
              'productName': 'Printer',
              'productTagLine': 'product tag line / offers any',
              'image': 'assets/images/dummy/printer.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 60,
              'rating': 4.9,
              'ratingCount': 153,
              'normalPrice': 13950,
              'discountPrice': 10999,
              'priceType': '$'
            }
          ]
      }
    };
    const bestSelling = {
      'Status': true,
      'Result': {
          'title': 'Best Selling Products',
          'products': [{
              'productId': 1,
              'productName': 'Apple 6S',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/apple.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 60,
              'rating': 4.9,
              'ratingCount': 153,
              'salesPrice': 40000,
              'discountAmount': 34999,
              'currencySymbol': '$'
            },
            {
              'productId': 2,
              'productName': 'Apple Imake',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/appleimac.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 60,
              'rating': 4.3,
              'ratingCount': 133,
              'salesPrice': 130000,
              'discountAmount': 10999,
              'currencySymbol': '$'
            },
            {
              'productId': 3,
              'productName': 'Raybon sunclass',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/sunglass.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 59,
              'rating': 4.1,
              'ratingCount': 113,
              'salesPrice': 6490,
              'discountAmount': 5841,
              'currencySymbol': '$'
            },
            {
              'productId': 4,
              'productName': 'HP Laptop',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/hp.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 60,
              'rating': 4.9,
              'ratingCount': 153,
              'salesPrice': 35000,
              'discountAmount': 30000,
              'currencySymbol': '$'
            }
          ]
      }
    };
    const featuredBrands = {
      'Status': true,
      'Result': {
          'title': 'Feature Brands',
          'brands': [{
            'brandId': 1,
            'brandName': 'Adidas',
            'image': 'assets/images/products/adidas.jpg'
          },
          {
            'brandId': 2,
            'brandName': 'FabIndia',
            'image': 'assets/images/products/fab.jpg'
          },
          {
            'brandId': 3,
            'brandName': 'Lakme',
            'image': 'assets/images/products/lakme.jpg'
          },
          {
            'brandId': 4,
            'brandName': 'Nike',
            'image': 'assets/images/products/nike.jpg'
          }
        ]
      }
    };
    const homeBannerList = {
      'Status': true,
      'Result': {
          'title': 'Banners',
          'banners': [{
            'bannerId': 1,
            'bannerName': 'banner1',
            'image': 'assets/images/dummy/home-banner3.jpg'
          },
          {
            'bannerId': 2,
            'bannerName': 'banner2',
            'image': 'assets/images/dummy/home-banner2.jpg'
          },
          {
            'bannerId': 3,
            'bannerName': 'banner3',
            'image': 'assets/images/home-banner.jpg'
          }
        ]
      }
    };

    const homeAdvertisementList = {
      'Status': true,
      'Result': {
        'top': [{
            'adId': 1,
            'adName': 'Summer Offer',
            'image': 'assets/images/dummy/leftban2.png',
          },
          {
            'adId': 2,
            'adName': 'Summer Offer',
            'image': 'assets/images/dummy/leftban1.png',
          },
          {
            'adId': 3,
            'adName': 'Summer Offer',
            'image': 'assets/images/dummy/leftban3.png',
          },
          {
            'adId': 4,
            'adName': 'Summer Offer',
            'image': 'assets/images/dummy/leftban4.png',
          },
          {
            'adId': 5,
            'adName': 'Summer Offer',
            'image': 'assets/images/dummy/leftban5.png',
          }
        ],
        'bottom': {
            'adId': 6,
            'adName': 'Summer Offer',
            'image': 'assets/images/dummy/bootom-banner1.png',
        }
      }
    };

    const allDealsList = {
      'Status': true,
      'Result': {
          'products': [{
              'productId': 1,
              'productName': 'Watches',
              'image': 'assets/images/dummy/watch.jpg',
              'wishList': 50,
              'discountType': '%',
              'discount': 25,
              'priceType': '$',
              'priceStatus': 'from',
              'originalPrice': '1000',
              'discountPrice': '750',
              'rating': '4.5',
            },
            {
              'productId': 2,
              'productName': 'FootWear',
              'image': 'assets/images/dummy/footwear.jpg',
              'wishList': 10,
              'discountType': '%',
              'discount': 20,
              'priceType': '$',
              'priceStatus': 'just',
              'originalPrice': '800',
              'discountPrice': '640',
              'rating': '3'
            },
            {
              'productId': 3,
              'productName': 'Bags and Travels',
              'image': 'assets/images/dummy/bag.jpg',
              'wishList': 4,
              'discountType': '%',
              'discount': 15,
              'priceType': '$',
              'priceStatus': 'upto',
              'originalPrice': '2000',
              'discountPrice': '1700',
              'rating': '2.5',
            },
            {
              'productId': 4,
              'productName': 'HeadPhones',
              'image': 'assets/images/dummy/headset.jpg',
              'wishList': 2,
              'discountType': '%',
              'discount': 30,
              'priceType': '$',
              'priceStatus': 'flat',
              'originalPrice': '1500',
              'discountPrice': '1050',
              'rating': '3.5',
            },
            {
              'productId': 5,
              'productName': 'Watches',
              'image': 'assets/images/dummy/watch.jpg',
              'wishList': 50,
              'discountType': '%',
              'discount': 25,
              'priceType': '$',
              'priceStatus': 'from',
              'originalPrice': '1000',
              'discountPrice': '750',
              'rating': '1',
            },
            {
              'productId': 6,
              'productName': 'FootWear',
              'image': 'assets/images/dummy/footwear.jpg',
              'wishList': 10,
              'discountType': '%',
              'discount': 20,
              'priceType': '$',
              'priceStatus': 'just',
              'originalPrice': '800',
              'discountPrice': '640',
              'rating': '4',
            },
            {
              'productId': 7,
              'productName': 'Bags and Travels',
              'image': 'assets/images/dummy/bag.jpg',
              'wishList': 4,
              'discountType': '%',
              'discount': 15,
              'priceType': '$',
              'priceStatus': 'upto',
              'originalPrice': '2000',
              'discountPrice': '1700',
              'rating': '4',
            },
            {
              'productId': 8,
              'productName': 'HeadPhones',
              'image': 'assets/images/dummy/headset.jpg',
              'wishList': 2,
              'discountType': '%',
              'discount': 30,
              'priceType': '$',
              'priceStatus': 'flat',
              'originalPrice': '1500',
              'discountPrice': '1050',
              'rating': '4.5',
            },
            {
              'productId': 9,
              'productName': 'Watches',
              'image': 'assets/images/dummy/watch.jpg',
              'wishList': 50,
              'discountType': '%',
              'discount': 25,
              'priceType': '$',
              'priceStatus': 'from',
              'originalPrice': '1000',
              'discountPrice': '750',
              'rating': '2',
            },
            {
              'productId': 10,
              'productName': 'FootWear',
              'image': 'assets/images/dummy/footwear.jpg',
              'wishList': 10,
              'discountType': '%',
              'discount': 20,
              'priceType': '$',
              'priceStatus': 'just',
              'originalPrice': '800',
              'discountPrice': '640',
              'rating': '2.4',
            },
            {
              'productId': 11,
              'productName': 'Bags and Travels',
              'image': 'assets/images/dummy/bag.jpg',
              'wishList': 4,
              'discountType': '%',
              'discount': 15,
              'priceType': '$',
              'priceStatus': 'upto',
              'originalPrice': '2000',
              'discountPrice': '1700',
              'rating': '2',
            },
            {
              'productId': 12,
              'productName': 'HeadPhones',
              'image': 'assets/images/dummy/headset.jpg',
              'wishList': 2,
              'discountType': '%',
              'discount': 30,
              'priceType': '$',
              'priceStatus': 'flat',
              'originalPrice': '1500',
              'discountPrice': '1050',
              'rating': '2',
            },
            {
              'productId': 13,
              'productName': 'Watches',
              'image': 'assets/images/dummy/watch.jpg',
              'wishList': 50,
              'discountType': '%',
              'discount': 25,
              'priceType': '$',
              'priceStatus': 'from',
              'originalPrice': '1000',
              'discountPrice': '750',
              'rating': '2.6',
            },
            {
              'productId': 14,
              'productName': 'FootWear',
              'image': 'assets/images/dummy/footwear.jpg',
              'wishList': 10,
              'discountType': '%',
              'discount': 20,
              'priceType': '$',
              'priceStatus': 'just',
              'originalPrice': '800',
              'discountPrice': '640',
              'rating': '2',
            },
            {
              'productId': 15,
              'productName': 'Bags and Travels',
              'image': 'assets/images/dummy/bag.jpg',
              'wishList': 4,
              'discountType': '%',
              'discount': 15,
              'priceType': '$',
              'priceStatus': 'upto',
              'originalPrice': '2000',
              'discountPrice': '1700',
              'rating': '2',
            }
          ]
      }
    };

    const nextTenDeals = {
      'Status': true,
      'Result': {
        'products': [
          {
            'productId': 16,
            'productName': 'HeadPhones',
            'image': 'assets/images/dummy/headset.jpg',
            'wishList': 2,
            'discountType': '%',
            'discount': 30,
            'priceType': '%',
            'priceStatus': 'flat',
            'originalPrice': '1500',
            'discountPrice': '1050',
            'rating': '3.5',
          },
          {
            'productId': 17,
            'productName': 'Watches',
            'image': 'assets/images/dummy/watch.jpg',
            'wishList': 50,
            'discountType': '%',
            'discount': 25,
            'priceType': '$',
            'priceStatus': 'from',
            'originalPrice': '1000',
            'discountPrice': '750',
            'rating': '4',
          },
          {
            'productId': 18,
            'productName': 'FootWear',
            'image': 'assets/images/dummy/footwear.jpg',
            'wishList': 10,
            'discountType': '%',
            'discount': 20,
            'priceType': '$',
            'priceStatus': 'just',
            'originalPrice': '800',
            'discountPrice': '640',
            'rating': '3.5',
          },
          {
            'productId': 19,
            'productName': 'Bags and Travels',
            'image': 'assets/images/dummy/bag.jpg',
            'wishList': 4,
            'discountType': '%',
            'discount': 15,
            'priceType': '%',
            'priceStatus': 'upto',
            'originalPrice': '2000',
            'discountPrice': '1700',
            'rating': '2.6',
          },
          {
            'productId': 20,
            'productName': 'HeadPhones',
            'image': 'assets/images/dummy/headset.jpg',
            'wishList': 2,
            'discountType': '%',
            'discount': 30,
            'priceType': '%',
            'priceStatus': 'flat',
            'originalPrice': '1500',
            'discountPrice': '1050',
            'rating': '2.6',
          },
          {
            'productId': 21,
            'productName': 'Watches',
            'image': 'assets/images/dummy/watch.jpg',
            'wishList': 50,
            'discountType': '%',
            'discount': 25,
            'priceType': '$',
            'priceStatus': 'from',
            'originalPrice': '1000',
            'discountPrice': '750',
            'rating': '2',
          },
          {
            'productId': 22,
            'productName': 'FootWear',
            'image': 'assets/images/dummy/footwear.jpg',
            'wishList': 10,
            'discountType': '%',
            'discount': 20,
            'priceType': '$',
            'priceStatus': 'just',
            'originalPrice': '800',
            'discountPrice': '640',
            'rating': '2',
          },
          {
            'productId': 23,
            'productName': 'Bags and Travels',
            'image': 'assets/images/dummy/bag.jpg',
            'wishList': 4,
            'discountType': '%',
            'discount': 15,
            'priceType': '%',
            'priceStatus': 'upto',
            'originalPrice': '2000',
            'discountPrice': '1700',
            'rating': '2',
          },
          {
            'productId': 24,
            'productName': 'HeadPhones',
            'image': 'assets/images/dummy/headset.jpg',
            'wishList': 2,
            'discountType': '%',
            'discount': 30,
            'priceType': '%',
            'priceStatus': 'flat',
            'originalPrice': '1500',
            'discountPrice': '1050',
            'rating': '2',
          },
          {
            'productId': 25,
            'productName': 'Watches',
            'image': 'assets/images/dummy/watch.jpg',
            'wishList': 50,
            'discountType': '%',
            'discount': 25,
            'priceType': '$',
            'priceStatus': 'from',
            'originalPrice': '1000',
            'discountPrice': '750',
            'rating': '2',
          },
          {
            'productId': 26,
            'productName': 'FootWear',
            'image': 'assets/images/dummy/footwear.jpg',
            'wishList': 10,
            'discountType': '%',
            'discount': 20,
            'priceType': '$',
            'priceStatus': 'just',
            'originalPrice': '800',
            'discountPrice': '640',
            'rating': '2',
          },
          {
            'productId': 27,
            'productName': 'Bags and Travels',
            'image': 'assets/images/dummy/bag.jpg',
            'wishList': 4,
            'discountType': '%',
            'discount': 15,
            'priceType': '%',
            'priceStatus': 'upto',
            'originalPrice': '2000',
            'discountPrice': '1700',
            'rating': '2',
          },
          {
            'productId': 28,
            'productName': 'HeadPhones',
            'image': 'assets/images/dummy/headset.jpg',
            'wishList': 2,
            'discountType': '%',
            'discount': 30,
            'priceType': '%',
            'priceStatus': 'flat',
            'originalPrice': '1500',
            'discountPrice': '1050',
            'rating': '2',
          },
          {
            'productId': 29,
            'productName': 'Watches',
            'image': 'assets/images/dummy/watch.jpg',
            'wishList': 50,
            'discountType': '%',
            'discount': 25,
            'priceType': '$',
            'priceStatus': 'from',
            'originalPrice': '1000',
            'discountPrice': '750',
            'rating': '2',
          },
          {
            'productId': 30,
            'productName': 'FootWear',
            'image': 'assets/images/dummy/footwear.jpg',
            'wishList': 10,
            'discountType': '%',
            'discount': 20,
            'priceType': '$',
            'priceStatus': 'just',
            'originalPrice': '800',
            'discountPrice': '640',
            'rating': '2',
          }
        ]
      }
    };

    const productsList = {
      'Status': true,
      'Result': {
          'products': [{
              'productId': 1,
              'productName': 'Apple 6S',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/apple.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 4,
              'ratingCount': 153,
              'normalPrice': 40000,
              'discountPrice': 34999,
              'priceType': '$'
            },
            {
              'productId': 2,
              'productName': 'Apple Imake',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/appleimac.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 3,
              'ratingCount': 133,
              'normalPrice': 130000,
              'discountPrice': 10999,
              'priceType': '$'
            },
            {
              'productId': 3,
              'productName': 'Raybon sunclass',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/sunglass.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 59,
              'rating': 4,
              'ratingCount': 113,
              'normalPrice': 6490,
              'discountPrice': 5841,
              'priceType': '$'
            },
            {
              'productId': 4,
              'productName': 'HP Laptop',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/hp.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 3,
              'ratingCount': 153,
              'normalPrice': 35000,
              'discountPrice': 30000,
              'priceType': '$'
            },
            {
              'productId': 5,
              'productName': 'Titan Raga',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/watch.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 3.4,
              'ratingCount': 153,
              'normalPrice': '6795',
              'discountPrice': '5175',
              'priceType': '$'
            },
            {
              'productId': 6,
              'productName': 'Baby`s Footwear',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/footwear.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 3.4,
              'ratingCount': 133,
              'normalPrice': '800',
              'discountPrice': '640',
              'priceType': '$'
            },
            {
              'productId': 7,
              'productName': 'JBL Headset',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/headset.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 59,
              'rating': 5.5,
              'ratingCount': 113,
              'normalPrice': '1500',
              'discountPrice': '1050',
              'priceType': '$'
            },
            {
              'productId': 8,
              'productName': 'Roshan Bag',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/bag.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 3.5,
              'ratingCount': 153,
              'normalPrice': 2000,
              'discountPrice': 1700,
              'priceType': '$'
            },
            {
              'productId': 9,
              'productName': 'Samsung Memory Card',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/memory.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 2.9,
              'ratingCount': 153,
              'normalPrice': 1149,
              'discountPrice': 799,
              'priceType': '$'
            },
            {
              'productId': 10,
              'productName': 'Intex power bank Index power bank Index power bank',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/charger.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 3.3,
              'ratingCount': 133,
              'normalPrice': 1899,
              'discountPrice': 999,
              'priceType': '$'
            },
            {
              'productId': 11,
              'productName': 'Logitech Mouse',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/logitech.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 59,
              'rating': 3,
              'ratingCount': 113,
              'normalPrice': 6490,
              'discountPrice': 5841,
              'priceType': '$'
            },
            {
              'productId': 12,
              'productName': 'Hp Printer',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/printer.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 4,
              'ratingCount': 153,
              'normalPrice': 35000,
              'discountPrice': 30000,
              'priceType': '$'
            }
          ]
      }
    };

    const anotherProductsList = {
      'Status': true,
      'Result': {
          'products': [{
              'productId': 13,
              'productName': 'Apple 6S',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/apple.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 3.5,
              'ratingCount': 153,
              'normalPrice': 40000,
              'discountPrice': 34999,
              'priceType': '$'
            },
            {
              'productId': 14,
              'productName': 'Apple Imake',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/appleimac.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 4,
              'ratingCount': 133,
              'normalPrice': 130000,
              'discountPrice': 10999,
              'priceType': '$'
            },
            {
              'productId': 15,
              'productName': 'Raybon sunclass',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/sunglass.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 59,
              'rating': 4,
              'ratingCount': 113,
              'normalPrice': 6490,
              'discountPrice': 5841,
              'priceType': '$'
            },
            {
              'productId': 16,
              'productName': 'HP Laptop',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/hp.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 3.5,
              'ratingCount': 153,
              'normalPrice': 35000,
              'discountPrice': 30000,
              'priceType': '$'
            },
            {
              'productId': 17,
              'productName': 'Titan Raga',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/watch.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 5,
              'ratingCount': 153,
              'normalPrice': '6795',
              'discountPrice': '5175',
              'priceType': '$'
            },
            {
              'productId': 18,
              'productName': 'Baby`s Footwear',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/footwear.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 2.6,
              'ratingCount': 133,
              'normalPrice': '800',
              'discountPrice': '640',
              'priceType': '$'
            },
            {
              'productId': 19,
              'productName': 'JBL Headset',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/headset.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 59,
              'rating': 5,
              'ratingCount': 113,
              'normalPrice': '1500',
              'discountPrice': '1050',
              'priceType': '$'
            },
            {
              'productId': 20,
              'productName': 'Roshan Bag',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/bag.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 3,
              'ratingCount': 153,
              'normalPrice': 2000,
              'discountPrice': 1700,
              'priceType': '$'
            },
            {
              'productId': 21,
              'productName': 'Samsung Memory Card',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/memory.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 2.5,
              'ratingCount': 153,
              'normalPrice': 1149,
              'discountPrice': 799,
              'priceType': '$'
            },
            {
              'productId': 22,
              'productName': 'Intex power bank Index power bank Index power bank',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/charger.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 2,
              'ratingCount': 133,
              'normalPrice': 1899,
              'discountPrice': 999,
              'priceType': '$'
            },
            {
              'productId': 23,
              'productName': 'Logitech Mouse',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/logitech.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 59,
              'rating': 3,
              'ratingCount': 113,
              'normalPrice': 6490,
              'discountPrice': 5841,
              'priceType': '$'
            },
            {
              'productId': 24,
              'productName': 'Hp Printer',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/printer.jpg',
              'wishList': 0,
              'discountType': '%',
              'discount': 60,
              'rating': 4,
              'ratingCount': 153,
              'normalPrice': 35000,
              'discountPrice': 30000,
              'priceType': '$'
            }
          ]
      }
    };

    const favouriteList = {
      'Status': true,
      'Result': {
          'products': [{
              'productId': 1,
              'productName': 'Apple 6S',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/apple.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 60,
              'rating': 4,
              'ratingCount': 153,
              'normalPrice': 40000,
              'discountPrice': 34999,
              'priceType': '$'
            },
            {
              'productId': 2,
              'productName': 'Apple Imake',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/appleimac.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 60,
              'rating': 3,
              'ratingCount': 133,
              'normalPrice': 130000,
              'discountPrice': 10999,
              'priceType': '$'
            },
            {
              'productId': 3,
              'productName': 'Raybon sunclass',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/sunglass.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 59,
              'rating': 4,
              'ratingCount': 113,
              'normalPrice': 6490,
              'discountPrice': 5841,
              'priceType': '$'
            },
            {
              'productId': 4,
              'productName': 'HP Laptop',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/hp.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 60,
              'rating': 3,
              'ratingCount': 153,
              'normalPrice': 35000,
              'discountPrice': 30000,
              'priceType': '$'
            },
            {
              'productId': 5,
              'productName': 'Titan Raga',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/watch.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 60,
              'rating': 3.4,
              'ratingCount': 153,
              'normalPrice': '6795',
              'discountPrice': '5175',
              'priceType': '$'
            },
            {
              'productId': 6,
              'productName': 'Baby`s Footwear',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/footwear.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 60,
              'rating': 3.4,
              'ratingCount': 133,
              'normalPrice': '800',
              'discountPrice': '640',
              'priceType': '$'
            },
            {
              'productId': 7,
              'productName': 'JBL Headset',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/headset.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 59,
              'rating': 5.5,
              'ratingCount': 113,
              'normalPrice': '1500',
              'discountPrice': '1050',
              'priceType': '$'
            },
            {
              'productId': 8,
              'productName': 'Roshan Bag',
              'productTagLine': 'product tag line / offers any',
              'productImage': 'assets/images/dummy/bag.jpg',
              'wishList': 1,
              'discountType': '%',
              'discount': 60,
              'rating': 3.5,
              'ratingCount': 153,
              'normalPrice': 2000,
              'discountPrice': 1700,
              'priceType': '$'
            }
          ]
      }
    };

    const filterList = {
      'status': true,
      'Filters': [{
        'filterName': 'Type',
        'filterOptions': [{
          'filterId': 1,
          'filterOptionName': 'Bellies'
          },
          {
            'filterId': 2,
            'filterOptionName': 'Boot Shoes'
          },
          {
            'filterId': 3,
            'filterOptionName': 'Boots'
          },
          {
            'filterId': 4,
            'filterOptionName': 'Borgues'
          },
          {
            'filterId': 5,
            'filterOptionName': 'Canvas Shoes'
          },
          {
            'filterId': 6,
            'filterOptionName': 'Casual Shoes'
          }
        ]
      },
      {
        'filterName': 'Size',
        'filterOptions': [{
          'filterId': 7,
          'filterOptionName': '6'
          },
          {
            'filterId': 8,
            'filterOptionName': '7'
          },
          {
            'filterId': 9,
            'filterOptionName': '8'
          },
          {
            'filterId': 10,
            'filterOptionName': '9'
          },
          {
            'filterId': 11,
            'filterOptionName': '10'
          },
          {
            'filterId': 12,
            'filterOptionName': '12'
          },
          {
            'filterId': 12,
            'filterOptionName': '14'
          }
        ]
      },
      {
        'filterName': 'Color',
        'filterOptions': [{
            'filterId': 13,
            'filterOptionName': 'Black'
          },
          {
            'filterId': 14,
            'filterOptionName': 'Grey'
          },
          {
            'filterId': 15,
            'filterOptionName': 'White'
          },
          {
            'filterId': 16,
            'filterOptionName': 'Blue'
          },
          {
            'filterId': 17,
            'filterOptionName': 'Brown'
          },
          {
            'filterId': 18,
            'filterOptionName': 'Pink'
          },
          {
            'filterId': 18,
            'filterOptionName': 'Green'
          }
        ]
      },
      {
        'filterName': 'Occation',
        'filterOptions': [{
            'filterId': 19,
            'filterOptionName': 'Party'
          },
          {
            'filterId': 20,
            'filterOptionName': 'Casual'
          },
          {
            'filterId': 21,
            'filterOptionName': 'Everyday'
          },
          {
            'filterId': 22,
            'filterOptionName': 'Wedding'
          },
          {
            'filterId': 23,
            'filterOptionName': 'Fashion'
          },
          {
            'filterId': 24,
            'filterOptionName': 'Traditional'
          }
        ]
      }
    ]
    };

    const allBrands = {
      'Status': true,
      'Result': {
          'brands': [{
            'brandId': 1,
            'brandName': 'Adidas',
            'image': 'assets/images/products/adidas.jpg'
          },
          {
            'brandId': 2,
            'brandName': 'FabIndia',
            'image': 'assets/images/products/fab.jpg'
          },
          {
            'brandId': 3,
            'brandName': 'Lakme',
            'image': 'assets/images/products/lakme.jpg'
          },
          {
            'brandId': 4,
            'brandName': 'Nike',
            'image': 'assets/images/products/nike.jpg'
          },
          {
            'brandId': 1,
            'brandName': 'Adidas',
            'image': 'assets/images/products/adidas.jpg'
          },
          {
            'brandId': 2,
            'brandName': 'FabIndia',
            'image': 'assets/images/products/fab.jpg'
          },
          {
            'brandId': 3,
            'brandName': 'Lakme',
            'image': 'assets/images/products/lakme.jpg'
          },
          {
            'brandId': 4,
            'brandName': 'Nike',
            'image': 'assets/images/products/nike.jpg'
          },
          {
            'brandId': 1,
            'brandName': 'Adidas',
            'image': 'assets/images/products/adidas.jpg'
          },
          {
            'brandId': 2,
            'brandName': 'FabIndia',
            'image': 'assets/images/products/fab.jpg'
          }
        ]
      }
    };

    const anotherTenBrands = {
      'Status': true,
      'Result': {
          'brands': [{
            'brandId': 1,
            'brandName': 'Adidas',
            'image': 'assets/images/products/adidas.jpg'
          },
          {
            'brandId': 2,
            'brandName': 'FabIndia',
            'image': 'assets/images/products/fab.jpg'
          },
          {
            'brandId': 3,
            'brandName': 'Lakme',
            'image': 'assets/images/products/lakme.jpg'
          },
          {
            'brandId': 4,
            'brandName': 'Nike',
            'image': 'assets/images/products/nike.jpg'
          },
          {
            'brandId': 1,
            'brandName': 'Adidas',
            'image': 'assets/images/products/adidas.jpg'
          },
          {
            'brandId': 2,
            'brandName': 'FabIndia',
            'image': 'assets/images/products/fab.jpg'
          },
          {
            'brandId': 3,
            'brandName': 'Lakme',
            'image': 'assets/images/products/lakme.jpg'
          },
          {
            'brandId': 4,
            'brandName': 'Nike',
            'image': 'assets/images/products/nike.jpg'
          },
          {
            'brandId': 1,
            'brandName': 'Adidas',
            'image': 'assets/images/products/adidas.jpg'
          },
          {
            'brandId': 2,
            'brandName': 'FabIndia',
            'image': 'assets/images/products/fab.jpg'
          }
        ]
      }
    };
    return {heroes, categories, todaysDeal, hotProducts, bestSelling, featuredBrands, anotherProductsList,
            homeBannerList, homeAdvertisementList, allDealsList, nextTenDeals, productsList, favouriteList,
            filterList, allBrands, anotherTenBrands};
  }
}
