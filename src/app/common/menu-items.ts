export const menuItems = [
    {
        title: 'labelMenuHome',
        functionText: 'home'
    },
    {
        title: 'labelMenuProducts',
        functionText: 'products'
    },
    {
        title: 'labelMenuDeals',
        functionText: 'deals'
    },
    {
        title: 'labelMenuSoldOut',
        functionText: 'soldout'
    },
    // {
    //     title: 'labelMenuBlog',
    //     functionText: 'blog'
    // },
    {
        title: 'labelContactUs',
        functionText: 'contactus'
    },
];
