import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {
  MatButtonModule, MatButtonToggleModule, MatExpansionModule, MatCheckboxModule,
  MatIconModule, MatFormFieldModule, MatInputModule, MatTabsModule, MatMenuModule, MatSelectModule,
  MatRadioModule, MatTooltipModule, MatTableModule, DateAdapter
} from '@angular/material';
import {BarRatingModule} from 'ngx-bar-rating';
import {TranslationModule} from 'angular-l10n';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DragScrollModule} from 'ngx-drag-scroll';
import { NouisliderModule } from 'ng2-nouislider';
import { FocusModule } from 'angular2-focus';
import { TimeAgoPipe } from 'time-ago-pipe';

import {SharedModule} from '../shared/shared.module';
import {ProductsRoutingModule} from './products-routing.module';
import {ProductsComponent} from './products.component';
import {ProductDetailComponent} from './product-detail/product-detail.component';
import { ShareSocialOptionsComponent } from './product-detail/share-social-options/share-social-options.component';
import {ProductListComponent} from './product-list/product-list.component';
import {FiltersComponent} from './product-list/filters/filters.component';
import {NgxImgZoomModule} from 'ngx-img-zoom';
import {TrendingProductsComponent} from './trending-products/trending-products.component';
import {ProductReviewsComponent} from './product-reviews/product-reviews.component';
import { ProductComparisionComponent } from './product-comparision/product-comparision.component';
import { SearchProductComponent } from './search-product/search-product.component';

import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { CustomDateAdapter } from '../custom-date-adapter';
import { ReplyPopupComponent } from './product-detail/reply-popup/reply-popup.component';
import { ShareButtonsModule } from '@ngx-share/buttons';
@NgModule({
  imports: [
    ShareButtonsModule,
    CommonModule,
    ScrollToModule,
    SharedModule,
    FormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatExpansionModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    BarRatingModule,
    MatTabsModule,
    MatTableModule,
    TranslationModule,
    MatMenuModule,
    NgbModule,
    NgxImgZoomModule,
    ProductsRoutingModule,
    DragScrollModule,
    ReactiveFormsModule,
    MatTooltipModule,
    NouisliderModule,
    FocusModule,
    ProductsRoutingModule
  ],
  declarations: [
    ProductsComponent,
    ProductDetailComponent,
    ProductListComponent,
    FiltersComponent,
    TrendingProductsComponent,
    ProductReviewsComponent,
    ProductComparisionComponent,
    SearchProductComponent,
    ReplyPopupComponent,
    TimeAgoPipe,
    ShareSocialOptionsComponent
  ],
  providers: [
    { provide: DateAdapter, useClass: CustomDateAdapter }
  ],
  entryComponents: [ReplyPopupComponent]
})
export class ProductsModule {
}
