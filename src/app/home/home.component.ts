import { Component, OnInit, OnDestroy , Inject} from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatSnackBar } from '@angular/material';

import { concat, orderBy, filter, take } from 'lodash';
import { TranslationService } from 'angular-l10n';

import { HomeService } from './home.service';
import { ComonService } from './../common/comon.service';

import { API_STATUS_CODE } from '../app.constants';

import { SNACKBAR_ALERT, HOME_SETTINGS } from '../app-setting';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [DatePipe]
})
export class HomeComponent implements OnInit, OnDestroy {

  public todayProductList: any;
  public hotProductList: any;
  public bestProductList: any;
  public featuredBrandsList: any;
  public totalProductList: any;
  public bannerList: any;
  public bottomAd: any;
  public todayDate = new Date();
  public currentDate: any;
  public productSubscribe$: any;
  advertisementList: any;
  mainCategoryList: any;
  categorySubscribe$: any;
  homeSettings = HOME_SETTINGS;
  featuredProductList: any;
  constructor(
    private homeService: HomeService,
    public datepipe: DatePipe,
    private comonService: ComonService,
    private snackBar: MatSnackBar,
    private translation: TranslationService) {
    this.bottomAd = [];
    this.currentDate = this.datepipe.transform(this.todayDate, 'yyyy-MM-dd');
    this.productSubscribe$ = this.homeService.getSignInStatus().subscribe((response: any) => {
      if (response === false) {
        this.totalProductList = [];
        this.getTodayDealsList();
        this.getHotProductList();
        this.getBestSellingProductList();
      }
    });
    this.categorySubscribe$ = this.comonService.getCategoryData().subscribe((response: any) => {
      if (response === true) {
        this.getCategoryList();
      }
    });
  }

  ngOnInit() {
    this.getBannerList();
    this.getTodayDealsList();
    this.getHotProductList();
    this.getBestSellingProductList();
    this.getFeaturedBrandList();
    this.getAdvertisementList();
    this.getCategoryList();
    this.getFeaturedProductList();
  }

  getCategoryList() {
    this.mainCategoryList = this.comonService.getCategoryDetails();
  }

  getTodayDealsList() {
    const dealObj = {
      'startLimit': this.homeSettings.START_COUNT,
      'endLimit': this.homeSettings.END_COUNT,
      'currentDate': this.currentDate,
      'desc': true
    };
    this.homeService.getTodayProductList(dealObj).subscribe((response: any) => {
      if (response) {
        this.todayProductList = null;
        this.todayProductList = response;
        this.getTotalProductList(this.todayProductList);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getHotProductList() {
    const productObj = {
      'startLimit': this.homeSettings.START_COUNT,
      'endLimit': this.homeSettings.END_COUNT,
      'desc': true
    };
    this.homeService.getHotProductList(productObj)
    .subscribe((response: any) => {
      if (response) {
        this.hotProductList = null;
        this.hotProductList = response;
        this.getTotalProductList(this.hotProductList);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getBestSellingProductList() {
    const productObj = {
      'startLimit': this.homeSettings.START_COUNT,
      'endLimit': this.homeSettings.END_COUNT,
      'desc': true
    };
    // const productObj = 'startLimit=0&endLimit=6&desc=true';
    this.homeService.getBestSellingProductList(productObj)
    .subscribe((response: any) => {
      if (response) {
        this.bestProductList = null;
        // this.bestProductList = response;
        const bestSellingProduct = {
          title: response.title,
          product: response.data,
          productLength: 5
        };
        this.bestProductList = bestSellingProduct;
        this.getTotalProductList(this.bestProductList);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getFeaturedProductList() {
    const productObj = {
      'startLimit': this.homeSettings.START_COUNT,
      'endLimit': this.homeSettings.END_COUNT,
    };
    this.homeService.getFeaturedProductList(productObj)
    .subscribe((response: any) => {
      if (response) {
        this.featuredProductList = null;
        this.featuredProductList = response;
        this.getTotalProductList(this.featuredProductList);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getFeaturedBrandList() {
    const featureObj = {
      'startLimit': this.homeSettings.START_COUNT,
      'endLimit': this.homeSettings.BRAND_END_COUNT,
    };
    this.homeService.getFeaturedBrandsList(featureObj).subscribe((response: any) => {
      if (response) {
        this.featuredBrandsList = response;
      } else {
        this.featuredBrandsList = undefined;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getTotalProductList(dataList) {
    dataList = this.comonService.findCartCompareSelection(dataList, 'home');
    if (this.totalProductList !== undefined) {
      this.totalProductList = concat(this.totalProductList, dataList);
      this.totalProductList = orderBy(this.totalProductList, ['title'], ['desc']);
    } else {
      this.totalProductList = dataList;
    }
  }

  getBannerList() {
    this.homeService.getHomeBannersList().subscribe((response: any) => {
      if (response) {
        this.bannerList = response.data;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getAdvertisementList() {
    this.homeService.getAdvertisementList().subscribe((response: any) => {
      if (response) {
        this.advertisementList = filter(response.data, {'adPosition': 'Left'});
        this.advertisementList = this.advertisementList.length > 5 ? take(this.advertisementList, 5) : this.advertisementList;
        this.bottomAd = filter(response.data, {'adPosition': 'Bottom'});
        this.bottomAd = this.bottomAd.length > 1 ? take(this.bottomAd, 1) : this.bottomAd;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  ngOnDestroy() {
    if (this.productSubscribe$) {
      this.productSubscribe$.unsubscribe();
    }
    if (this.categorySubscribe$) {
      this.categorySubscribe$.unsubscribe();
    }
  }
}
