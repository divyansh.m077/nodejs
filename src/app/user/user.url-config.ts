import {BASE_URL} from '../common/common.url-config';

export const USER_REGISTER_URL = BASE_URL + 'user-register';
export const USER_REGISTER_SOCIAL_URL = BASE_URL + 'user-social-register';
export const USER_SIGNIN_URL = BASE_URL + 'user-login';
export const REMOVE_WISHLIST_URL = BASE_URL + 'favourite-product/';
export const FORGOT_PASSWORD_URL = BASE_URL + 'forgot-password';
export const RESET_PASSWORD_URL = BASE_URL + 'reset-password';
export const GET_USER_USER_PROFILE_URL = BASE_URL + 'get-profile-info/';
export const UPDATE_USER_PROFILE_URL = BASE_URL + 'update-profile-info';
export const CHANGE_PASSWORD_URL = BASE_URL + 'change-password';
export const COUNTRY_LIST_URL = BASE_URL + 'get-country-list';
export const STATE_LIST_URL = BASE_URL + 'get-particular-state/';
export const CITY_LIST_URL = BASE_URL + 'get-particular-city/';
export const ADDRESS_TYPE_URL = BASE_URL + 'get-address-type';
export const GET_MANAGE_ADDRESS_LIST_URL = BASE_URL + 'get-address-info/';
export const ADD_MANAGE_ADDRESS_URL = BASE_URL + 'add-new-address';
export const UPDATE_MANAGE_ADDRESS_URL = BASE_URL + 'update-address';
export const DELETE_MANAGE_ADDRESS_URL = BASE_URL + 'delete-address';
export const GET_MANAGE_ADDRESS_URL = BASE_URL + 'get-address-for-update/';
export const CUSTOMER_REVIEW_URL = BASE_URL + 'get-customer-review';
export const SAVE_NOTIFY_PREFERENCE_URL = BASE_URL + 'preference-setting';
export const GET_NOTIFY_PREFERENCE_URL = BASE_URL + 'get-preference-setting/';
export const REGISTER_OTP_URL = BASE_URL + 'sales-portal/customer-registration/generate-otp';
export const REGION_URL = BASE_URL + 'regions/';
export const DISTRICT_URL = BASE_URL + 'districts/';

export const USER_URL = {
    COMPLAINT_TYPE: BASE_URL + 'order-complaint/complaint/type',
    COMPLAINT_STATUS: BASE_URL + 'order-complaint/complaint/status',
    GET_COMPLAINT_LIST: BASE_URL + 'order-complaint/complaintlist',
    COMPLAINT: BASE_URL + 'order-complaint',
    ORDER: BASE_URL + 'order-complaint/order/',
    PRODUCT_BY_ORDER: BASE_URL + 'order-complaint/product/',
    ADD_COMPLAINT: BASE_URL + 'order-complaint/add',
    WALLET_TRANSACTION_LIST: BASE_URL + 'wallet/transaction/',
    WALLET_AMOUNT: BASE_URL + 'wallet/',
    MY_ORDERS: `${BASE_URL}order-checkout/pagination`,
    PRODUCT_REVIEWS_AND_RATING: `${BASE_URL}product-review/pagination`,
    DELETE_RATINGS_REVIEWS: `${BASE_URL}product-review/`,
    CANCEL_REASON_LIST: `${BASE_URL}order-cancel/status`,
    CONFIRM_ORDER_CANCEL: `${BASE_URL}order-cancel`,
    DASHBOARD: BASE_URL + 'dashboard/',
    ORDER_DETAILS: BASE_URL + 'order-checkout/',
    RESEND_EMAIL: BASE_URL + 'verify-email/resend',
    VERIFY_EMAIL: BASE_URL + 'verify-email/',
    ORDER_STATUS: BASE_URL + 'order-checkout/order-status',
    PROFILE_IMAGE: BASE_URL + 'avatar/',
    DELETE_PROFILE: BASE_URL + 'user-image/',
    SET_PASSWORD: BASE_URL + 'set-password/password',
    PACKAGE_DETAILS: BASE_URL + 'order-checkout/shipping-list',
    TRACKING_DETAILS: BASE_URL + 'order-checkout/shipping-tracking',
    UPDATE_PROFILE: BASE_URL + 'otp-update-profile-info'
};
