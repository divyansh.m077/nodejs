import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';


import { Language, TranslationService } from 'angular-l10n';
import { MatSnackBar } from '@angular/material';

import { UserService } from './../user.service';
import { API_STATUS_CODE } from '../../app.constants';
import { settings, SNACKBAR_SUCCESS, SNACKBAR_ALERT, RESET_PASSWORD_RULE } from './../../app-setting';

@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.scss']
})
export class SetPasswordComponent implements OnInit {

  @Language() lang: string;
  setPasswordForm: FormGroup;
  siteKey;
  captchaSize;
  captchaTheme;
  captchaType;
  captchaLanguage;
  isInBrowser;
  userEmail: any;
  passwordRule: boolean;
  constructor(private fb: FormBuilder, private userService: UserService, private router: Router,
              private snackBar: MatSnackBar, private translation: TranslationService, private route: ActivatedRoute) {
    this.passwordRule = false;
    this.userEmail = this.route.snapshot.queryParams.userEmail;
    this.formInitialization();
    this.siteKey = settings.captchaKey;
    this.captchaSize = 'Normal';
    this.captchaTheme = 'Light';
    this.captchaType = 'Image';
    this.captchaLanguage = 'en';
    if (typeof window !== 'undefined') {
      this.isInBrowser = true;
    }
  }

  formInitialization() {
    this.setPasswordForm = this.fb.group({
      'userEmail': [{value: this.userEmail, disabled: true}],
      'newPassword': ['', [Validators.required, Validators.pattern(RESET_PASSWORD_RULE.PASSWORD_PATTERN)]],
      'confirmPassword': ['', [Validators.required]],
      'isCaptchaVerified': ['', [Validators.required]]
    });
  }

  ngOnInit() {
  }

  showPasswordRules() {
    this.passwordRule = true;
  }

  hidePasswordRule() {
    this.passwordRule = false;
  }

  removeCurrentPassword() {
    if (this.setPasswordForm.controls['confirmPassword'].value !== this.setPasswordForm.controls['newPassword'].value) {
      this.setPasswordForm.controls['confirmPassword'].setValue('');
    }
  }

  onSubmit() {
    const passwordData = {
      'userEmail': this.userEmail,
      'isCaptchaVerified': this.setPasswordForm.value.isCaptchaVerified,
      'password': this.setPasswordForm.value.newPassword
    };
    this.userService.setPasswordDetails(passwordData).subscribe((response: any) => {
      if (response) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        this.router.navigate(['/signin']);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }


}
