import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CdkTableModule } from '@angular/cdk/table';
import { FormsModule } from '@angular/forms';
import { ClickOutsideModule } from 'ng4-click-outside';

import { MatSelectModule, MatButtonModule, MatButtonToggleModule, MatIconModule, MatDialogModule,
         MatMenuModule, MatSidenavModule} from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslationModule } from 'angular-l10n';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxStripeModule } from 'ngx-stripe';

import { CommonRoutingModule } from './common-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CategoryGroupComponent } from './header/category-groups/category-groups.component';
import { CategoriesComponent } from './header/category-groups/categories/categories.component';
import { ComonService } from './comon.service';
import { CmsComponent } from './footer/cms/cms.component';
import { SharedModule } from '../shared/shared.module';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

@NgModule({
  imports: [
    CommonModule,
    AngularFontAwesomeModule,
    MatSelectModule,
    CdkTableModule,
    MatButtonModule,
    MatIconModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatSidenavModule,
    MatMenuModule,
    NgbModule,
    TranslationModule,
    CommonRoutingModule,
    FormsModule,
    ClickOutsideModule,
    SharedModule,
    ScrollToModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    CategoryGroupComponent,
    CategoriesComponent,
    CmsComponent,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    CategoryGroupComponent,
    CategoriesComponent,
    CmsComponent
  ],
  providers: []
})
export class ComonModule { }
