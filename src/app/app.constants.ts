import { environment } from '../environments/environment';

export const API_STATUS_CODE = {
    'created': 201,
    'empty': 204,
    'badRequest': 400,
    'Unauthorized': 401,
    'internalServer': 500,
    'conflict': 409,
    'preconditionFailed': 412,
    'forbidden': 403,
    'unavailable': 451
};
  
export const TOASTER_CLICK_TO_CLOSE = {
    timeOut: 30000
};

export const ALERT_SYSTEM_TITLE = {
    'success': 'SUCCESS',
    'warning': 'WARNING',
    'error': 'ERROR',
    'info': 'INFO',
};


export const NO_IMAGE_URL = `${environment.assetsUrl}assets/images/no-image-found.jpg`;
export const LOADING_SPINNER_URL = `${environment.assetsUrl}assets/images/loading-spinner.gif`;

export enum COMPLAINT_TYPE {
    damagedProduct = 1,
    delayInDelivery = 2,
    gotWrongProduct= 3,
    paymentIssues = 4,
    delayInCashback = 5
};

export const COUNTRY_CODE = '+91';
export const ORDER_STATUS = {
  'new': {'fa_icon': 'fa fa-check', 'class_name': 'new-status'},
  'packed': {'fa_icon': '', 'class_name': 'other-status'},
  'shipped': {'fa_icon': '', 'class_name': 'other-status'},
  'delivered': {'fa_icon': '', 'class_name': 'other-status'},
  'cancel': {'fa_icon': 'fa fa-ban', 'class_name': 'cancel-status'},
  'hold': {'fa_icon': '', 'class_name': 'other-status'},
  'order-returned': {'fa_icon': '', 'class_name': 'other-status'},
  'pending': {'fa_icon': 'fa fa-exclamation', 'class_name': 'pending-status'},
  'failure': {'fa_icon': 'fa fa-times', 'class_name': 'cancel-status'}
};

export enum TRACK_ORDER_STATUS {
    New = 1,
    Packed = 2,
    Shipped = 3,
    Delivered = 4,
    Cancel = 5,
    Hold = 6,
    Order_Returned = 7,
    Pending = 8,
    Failure = 9
}

export enum CASHBACK_PLAN {
    silver = "SILVER",
    gold = "GOLD",
    diamond= "DIAMOND",
    platinum  = "PLATINUM"
};
