import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { MatButtonModule, MatButtonToggleModule} from '@angular/material';
import { TranslationModule } from 'angular-l10n';

import { ComonModule } from '../common/common.module';
import { SharedModule } from '../shared/shared.module';
import { DealsRoutingModule } from './deals-routing.module';

import { DealsComponent } from './deals.component';

@NgModule({
  imports: [
    CommonModule,
    ComonModule,
    MatButtonModule,
    MatButtonToggleModule,
    SharedModule,
    TranslationModule,
    DealsRoutingModule
  ],
  declarations: [
    DealsComponent
  ],
  providers: [DatePipe]
})
export class DealsModule { }
