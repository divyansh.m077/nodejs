import { Injectable } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class SeoService {

  constructor(
    private title: Title,
    private meta: Meta
  ) { }

  updateTitle(title: string) {
    this.title.setTitle(title);
    this.meta.updateTag({ name: 'og:title', content: title });
    this.meta.updateTag({ property: 'twitter:title', content: title });
  }

  updateDescription(description) {
    this.meta.updateTag({ name: 'description', content: description });
    this.meta.updateTag({ property: 'og:description', content: description });
    this.meta.updateTag({ property: 'twitter:description', content: description });
  }

  updateUrl(url) {
    this.meta.updateTag({ property: 'og:url', content: url});
    this.meta.updateTag({ property: 'twitter:url', content: url});
    this.meta.updateTag({ name: 'twitter:site', content: url });
  }

  updateOgImage(imageUrl) {
    this.meta.updateTag({ property: 'og:image', content: imageUrl, itemprop: 'image'});
    this.meta.updateTag({ property: 'og:type', content: 'website'});
    this.meta.updateTag({ property: 'twitter:card', content: 'summary_large_image'});
    this.meta.updateTag({ property: 'twitter:image', content: imageUrl});
    this.meta.updateTag({ property: 'twitter:image:src', content: imageUrl});
  }

  updateMetaKeywords(keywords) {
      this.meta.updateTag({ name: 'keywords', content: keywords });
  }
}
