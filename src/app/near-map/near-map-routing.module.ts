import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { NearMapComponent } from './near-map.component';
import { StoreDetailComponent } from './store-detail/store-detail.component';

const nearMapRoutes: Routes = [
  {
    path: '',
    component: NearMapComponent
  },
  {
    path: 'store-detail',
    component: StoreDetailComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      nearMapRoutes
    )
  ],
  declarations: [],
  exports: [RouterModule],
})
export class NearMapRoutingModule { }
