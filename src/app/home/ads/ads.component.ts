import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material';

import { TranslationService } from 'angular-l10n';

import { API_STATUS_CODE } from '../../app.constants';
import { HomeService } from '../../home/home.service';

import { SNACKBAR_ALERT } from '../../app-setting';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.scss']
})
export class AdsComponent implements OnInit {
  @Input() adList;
  constructor(@Inject(WINDOW) private window: any, private homeService: HomeService, private snackBar: MatSnackBar,
   private translation: TranslationService, private router: Router) {
  }

  ngOnInit() {
  }

  redirectToUrl(data) {
    this.homeService.advertisementAddLog({ adId: data.advertisementSettingId }).subscribe((response: any) => {
      if (response) {

      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
      if (error.status === API_STATUS_CODE.internalServer) {
        this.snackBar.open(error.error.error, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
    // const this.window = this.sharedService.nativeWindow;
    const toUrl = new URL(data.redirectUrl).hostname;
    const routerUrl = new URL(window.location.href).hostname;
    const add_url = toUrl.split('.');
    const liveUrl = routerUrl.split('.');
    if (add_url[add_url.length - 2] === liveUrl[liveUrl.length - 2]) {
      const pathname = new URL(data.redirectUrl).pathname;
      const search = new URL(data.redirectUrl).search;
      this.router.navigateByUrl(pathname + search);
    } else {
      this.window.open(data.redirectUrl, '_blank');
    }
  }
}
