import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {TitleCasePipe} from '@angular/common';
import { MatFormFieldModule, MatCheckboxModule, MatInputModule, MatButtonModule, MatSelectModule, MatMenuModule,
        MatIconModule, MatRadioModule, MatDatepickerModule, MatSlideToggleModule, MatSortModule, MatTableModule, MatTabsModule,
        MatPaginatorModule, MatStepperModule, MatDialogModule, MAT_MENU_SCROLL_STRATEGY, MAT_SELECT_SCROLL_STRATEGY, MAT_DATEPICKER_SCROLL_STRATEGY, DateAdapter } from '@angular/material';
import { TranslationModule } from 'angular-l10n';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {MatNativeDateModule} from '@angular/material';
import { NgxCaptchaModule } from 'ngx-captcha';
import { SharedModule } from './../shared/shared.module';
import { UserRoutingModule } from './user-routing.module';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { RegisterComponent } from './register/register.component';
import { SigninComponent } from './signin/signin.component';
import { EqualValidator } from './password.match.directive';
import { UserService } from './user.service';
import { settings } from './../app-setting';
import { ProfileComponent } from './profile/profile.component';
import { WishListComponent } from './wish-list/wish-list.component';
import { ProfileInfoComponent } from './profile-info/profile-info.component';
import { ManageAddressComponent } from './manage-address/manage-address.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { ReviewsRatingComponent } from './reviews-rating/reviews-rating.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotificationComponent } from './notification/notification.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ProfileDetailsComponent } from './profile-info/profile-details/profile-details.component';
import { ChangePasswordComponent } from './profile-info/change-password/change-password.component';
import { OrderComplaintsComponent } from './order-complaints/order-complaints.component';
import { WalletComponent } from './wallet/wallet.component';
import { OrderSummaryComponent } from './my-orders/order-summary/order-summary.component';
import { OrderCancelComponent } from './my-orders/order-cancel/order-cancel.component';
import { UserActivationComponent } from './user-activation/user-activation.component';
import { TrackingDetailsComponent } from './my-orders/order-summary/tracking-details/tracking-details.component';
import { SocialmediaEmailComponent } from './socialmedia-email/socialmedia-email.component';
import { ComplaintImageComponent } from './order-complaints/complaint-image/complaint-image.component';
import { TermsConditionComponent } from './register/terms-condition/terms-condition.component';
import { ComonModule } from "../common/common.module";
import { SetPasswordComponent } from './set-password/set-password.component';
import { Overlay, OverlayModule } from '@angular/cdk/overlay';
import { CustomDateAdapter } from '../custom-date-adapter';
import { TrackingComponent } from './my-orders/order-summary/tracking/tracking.component';

export function getAuthServiceConfigs() {
  const config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider(settings.facebookLoginProvider)
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider(settings.googleLoginProvider)
      },
    ]
  );
  return config;
}

@NgModule({
  imports: [
    CommonModule,
    AngularFontAwesomeModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatDatepickerModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslationModule,
    SocialLoginModule,
    MatMenuModule,
    MatIconModule,
    MatRadioModule,
    SharedModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatStepperModule,
    MatPaginatorModule,
    MatDialogModule,
    NgxCaptchaModule,
    ComonModule,
    OverlayModule,
    ScrollToModule
  ],
  declarations: [
    RegisterComponent,
    SigninComponent,
    EqualValidator,
    ProfileComponent,
    WishListComponent,
    ProfileInfoComponent,
    ManageAddressComponent,
    MyOrdersComponent,
    ReviewsRatingComponent,
    DashboardComponent,
    NotificationComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    ProfileDetailsComponent,
    ChangePasswordComponent,
    OrderComplaintsComponent,
    WalletComponent,
    OrderSummaryComponent,
    OrderCancelComponent,
    UserActivationComponent,
    TrackingDetailsComponent,
    SocialmediaEmailComponent,
    ComplaintImageComponent,
    TermsConditionComponent,
    SetPasswordComponent,
    TrackingComponent
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    TitleCasePipe,
    {
      provide: MAT_MENU_SCROLL_STRATEGY,
      deps: [ Overlay ],
      useFactory: function(overlay) { return () => overlay.scrollStrategies.block(); }
    },
    {
      provide: MAT_SELECT_SCROLL_STRATEGY,
      deps: [ Overlay ],
      useFactory: function(overlay) { return () => overlay.scrollStrategies.block(); }
    },
    {
      provide: MAT_DATEPICKER_SCROLL_STRATEGY,
      deps: [ Overlay ],
      useFactory: function(overlay) { return () => overlay.scrollStrategies.block(); }
    },
    { provide: DateAdapter, useClass: CustomDateAdapter }
  ],
  entryComponents: [OrderCancelComponent, TrackingDetailsComponent, ComplaintImageComponent,
  TermsConditionComponent, TrackingComponent],
})
export class UserModule {
}
