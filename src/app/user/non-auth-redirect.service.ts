import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { StorageControllerService } from '../storage-controller.service';

@Injectable({
  providedIn: 'root'
})
export class NonAuth implements CanActivate {

  constructor(
    private router: Router,
    private storage: StorageControllerService,
  ) { }

  canActivate() {
    if (!!this.storage.get('userName')) {
        this.router.navigate(['']);
        return false;
      } else {
        return true;
      }
  }
}
