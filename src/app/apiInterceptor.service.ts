import {Injectable, Inject} from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { isPlainObject } from 'lodash';

import {environment} from '../environments/environment';
import { API_STATUS_CODE } from './app.constants';
import { SharedService } from './shared/shared.service';
import { StorageControllerService } from './storage-controller.service';

@Injectable()
export class APIInterceptor implements HttpInterceptor {
  public clone;
  constructor(
    private router: Router,
    private storage: StorageControllerService,
    private sharedService: SharedService
  ) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes(environment.serverUrl) && isPlainObject(req.body) === false && (req.method === 'POST' || req.method === 'PUT')) {
      this.clone = req.clone({ setHeaders: { 'Authorization': `bearer ${this.storage.get('token')}` } });
    } else {
      this.clone = req.clone({ setHeaders: {
        'Authorization': `bearer ${this.storage.get('token')}`,
        'Content-Type': 'application/json'
      }});
    }
    return next.handle(this.clone).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === API_STATUS_CODE.forbidden) {
          this.sharedService.clearUserInfoFromStorage();
          this.router.navigate(['/signin']);
        }
        if (error.status === API_STATUS_CODE.Unauthorized || error.status === API_STATUS_CODE.unavailable) {
          this.router.navigate(['/unauthorized']);
        }
        return throwError(error);
      }));
  }
}
