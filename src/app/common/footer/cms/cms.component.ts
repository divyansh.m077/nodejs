import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { Input } from '@angular/core';

import { MatSnackBar } from '@angular/material';
import { TranslationService } from 'angular-l10n';

import { ComonService } from '../../../common/comon.service';
import { API_STATUS_CODE } from '../../../app.constants';
import { SNACKBAR_ALERT } from '../../../app-setting';

@Component({
  selector: 'app-cms',
  templateUrl: './cms.component.html',
  styleUrls: ['./cms.component.scss']
})
export class CmsComponent implements OnInit {
  pageTitle: any;
  from: any;
  pageName: any;
  cmsDetail: any;
  @Input('pageName') set _pageName(val) {
    this.pageName = val;
  }
  @Input('from') set _from(val) {
    this.from = val ? val : '';
  }
  constructor(private activatedRoute: ActivatedRoute, private comonService: ComonService,
              private snackBar: MatSnackBar, private translation: TranslationService,
              private meta: Meta, private titleService: Title, private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    this.activatedRoute.params.subscribe(params => {
      this.pageName = params.pageName;
      this.pageTitle = params.pageTitle;
    });
  }

  ngOnInit() {
    if (this.pageName) {
       this.getCmsDetails();
    }
  }

  getCmsDetails() {
    this.comonService.getCmsDetail(this.pageName).subscribe((response: any) => {
      if (response) {
        this.cmsDetail = response.data[0];
        if (this.cmsDetail && !this.from && this.pageTitle) {
          this.titleService.setTitle(this.pageTitle);
          this.meta.updateTag({ name: 'description', content: this.cmsDetail.metaDescription });
          this.meta.updateTag({ name: 'keywords', content: this.cmsDetail.metaKeyWords });
        }
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }
}
