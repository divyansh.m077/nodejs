import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Language } from 'angular-l10n';
import * as moment from 'moment';
import { forEach } from 'lodash';
@Component({
  selector: 'app-tracking',
  templateUrl: './tracking.component.html',
  styleUrls: ['./tracking.component.scss']
})
export class TrackingComponent implements OnInit {
  trackingList: any[] = [];
  @Language() lang: string;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private matDialogRef: MatDialogRef<TrackingComponent>,
  ) {
    this.trackingList = [];
    forEach(data.trackingDetails, (obj) => {
      let formattedObj = {
        dayName: moment(obj.createdAt).format('dddd'),
        date: moment(obj.createdAt).format('DD'),
        month: moment(obj.createdAt).format('MMMM'),
        year: moment(obj.createdAt).format('YYYY'),
        datetime: moment(obj.createdAt).format('hh:mm A'),
        message: obj.message,
        status: ('OrderStatus' + obj.orderStatus)
      };
      this.trackingList.push(formattedObj);
    })

   }
   
  ngOnInit() {
  }
 

}
