import { Component, OnInit, ViewChild , Inject} from '@angular/core';

import { MatSort, MatPaginator, MatTableDataSource, MatSnackBar } from '@angular/material';
import { Language, TranslationService } from 'angular-l10n';
import { forEach } from 'lodash';

import { UserService } from '../user.service';
import { API_STATUS_CODE } from '../../app.constants';
import { settings, SNACKBAR_ALERT } from '../../app-setting';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {
  @Language() lang: string;
  limit: any;
  offSet: any;
  currentPage: number;
  orderBy: string;
  totalRecord: any;
  totalPages: number;
  constructor(
    private userService: UserService,
    private snackBar: MatSnackBar,
    private translation: TranslationService
  ) { }

  walletAmount: any;
  transactionList: any;
  displayedColumns: string[] = ['date', 'referenceNo', 'description', 'amount', 'type'];
  showTransactionSummary: boolean;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;


  ngOnInit() {
    this.limit = settings.LIMIT;
    this.offSet = settings.OFFSET;
    this.currentPage = 1;
    this.orderBy = '';
    this.showTransactionSummary = false;
    this.getWalletAmount();
    this.getWalletTransaction();
  }

  getWalletTransaction() {
    const transactionDetails = {
      offSet: this.offSet,
      startLimit: this.limit,
      orderBy: this.orderBy
    };
    this.getWalletTransactionList(transactionDetails);
  }

  getWalletTransactionList(transactionDetails) {
    this.userService.getWalletTransaction(transactionDetails).subscribe((response: any) => {
      if (response) {
        this.transactionList = response.data;
        forEach(this.transactionList, (transaction) => {
          transaction.type = Number(transaction.type);
        });
        this.totalRecord = response.totalRecordCount;
        this.totalPages = Math.ceil(this.totalRecord / settings.LIMIT);
        this.transactionList = new MatTableDataSource(this.transactionList);
      } else {
        this.transactionList = undefined;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.conflict) {
        this.snackBar.open(error.error.message, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  sortEvent(event) {
    const sortBy = event.active === 'referenceNo' ? 'refNo' : event.active;
    this.orderBy = event.direction === 'asc' ? sortBy : (event.direction === '' ? '' : sortBy + 'Desc');
    const orderDetails = {
      offSet: this.offSet,
      startLimit: this.limit,
      orderBy: this.orderBy
    };
    this.getWalletTransactionList(orderDetails);
  }

  onPageChange(offset) {
    this.offSet = offset;
    this.getWalletTransaction();
  }

  getWalletAmount() {
    this.userService.getWalletAmount().subscribe((response: any) => {
      if (response) {
        this.walletAmount = response.data[0];
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.conflict) {
        this.snackBar.open(error.error.message, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  showTransactionList() {
    this.showTransactionSummary = !this.showTransactionSummary;
  }
}
