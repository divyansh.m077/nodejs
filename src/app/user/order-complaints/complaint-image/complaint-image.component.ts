import { Component, OnInit, Inject } from '@angular/core';

import { MAT_DIALOG_DATA } from '@angular/material';
import { Language } from 'angular-l10n';

@Component({
  selector: 'app-complaint-image',
  templateUrl: './complaint-image.component.html',
  styleUrls: ['./complaint-image.component.scss']
})
export class ComplaintImageComponent implements OnInit {
 @Language() lang: string;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

}
