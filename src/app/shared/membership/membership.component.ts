import { Component, OnInit, Inject } from '@angular/core';
import { Language, TranslationService } from 'angular-l10n';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatDialog } from '@angular/material';
import { forEach } from 'lodash';

import { SNACKBAR_ALERT, SNACKBAR_SUCCESS, SNACKBAR_WARNING } from '../../app-setting';
import { API_STATUS_CODE, ALERT_SYSTEM_TITLE, CASHBACK_PLAN } from '../../app.constants';
import { SharedService } from '../shared.service';
import { AlertPopupComponent } from '../alert-popup/alert-popup.component';
import { Router } from '@angular/router';
import { MembershipPolicyComponent } from '../membership-policy/membership-policy.component';
import { StorageControllerService } from '../../storage-controller.service';
import { MEMBERSHIP_RULE } from '../../app-setting';

@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.scss']
})
export class MembershipComponent implements OnInit {
  @Language() lang;
  subcscriptionPlanList: any = [];
  activatedPlan;
  termsAgreed: boolean;
  actionSubscribe$: any;
  cashbackPlan: any = CASHBACK_PLAN;
  planObj: any;
  memberShipRule = MEMBERSHIP_RULE;
  get settings() { return this.sharedService.getSiteSettingDetails(); }

  constructor(public dialogRef: MatDialogRef<MembershipComponent>,
    public dialog: MatDialog,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private sharedService: SharedService,
    private translation: TranslationService,
    private snackBar: MatSnackBar,
    private storage: StorageControllerService) { }

  ngOnInit() {
    this.termsAgreed = false;
    this.getSubscriptionPlanDetails();
  }

  activatePlan(plan) {
    this.activatedPlan = plan.systemName;
    this.planObj = plan;
    forEach(this.subcscriptionPlanList, (obj) => {
      if(this.activatedPlan != obj.systemName){
        obj.budget = null;
      }      
    })
  }

  setActivatedPlan() {
    if (this.data && this.data.orderTotal) {
      forEach(this.subcscriptionPlanList, (plan: any) => {
        if (this.data.orderTotal >= plan.minOrderAmount && this.data.orderTotal <= plan.maxOrderAmount) {
          this.activatedPlan = plan.systemName;
          this.planObj = plan;
        }
      });
    }
  }

  getSubscriptionPlanDetails() {
    this.sharedService.getSubcriptionPlanDetails().subscribe((response: any) => {
      if (response) {
        this.subcscriptionPlanList = response.subscriptionList;
        this.setActivatedPlan();
      }
    }, (error: any) => {
      if (error.status === (API_STATUS_CODE.badRequest || API_STATUS_CODE.badRequest)) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  // Validate Platinum amount
  validatePlatinum(obj) {
    let returnValue;
    if (obj.budget < obj.minOrderAmount || obj.budget > obj.maxOrderAmount) {
      returnValue = (this.translation.translate("BUDGET_SHOULD_BE") + obj.minOrderAmount + ' ' + this.translation.translate("TO") + ' ' + obj.maxOrderAmount);
    } else if ((obj.budget % this.memberShipRule.MULTIPLE_OF_PLATINUM) != 0) {
      returnValue = (this.translation.translate("PLATINUM_ERROR") + this.memberShipRule.MULTIPLE_OF_PLATINUM);
    }
    return returnValue;
  }

  subscribeMemberShipPlan() {
    let message;
    if (!this.planObj) {
      message = this.translation.translate("ATLEAST_ACTIVATE_ONE_PLAN");
    }
    else if (this.planObj && !this.planObj.budget) {
      message = (this.translation.translate(this.planObj.systemName == 'platinum' ? "ENTER_BUDGET" : "SELECT_BUDGET"));
    }
    else if (this.planObj && this.planObj.budget && this.planObj.systemName == 'platinum' && this.validatePlatinum(this.planObj)) {
      message = this.validatePlatinum(this.planObj);
    }
    else if (!this.termsAgreed) {
      message = this.translation.translate("AGREE_TERMS_AND_CONDITIONS");
    }

    if (message) {
      this.snackBar.open(message, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
    } else {
      const reqObj = {
        plan: this.activatedPlan,
        amount: +this.planObj.budget
      };
      this.sharedService.subscribePlan(reqObj).subscribe(
        (response: any) => {
          this.storage.set('subscriptionPlan', this.activatedPlan);
          let responseMessage;
          if (response.message == "SUBSCRIPTION_SUCCESSFULL") {
            const plan = this.cashbackPlan[this.activatedPlan];
            responseMessage = (`${this.translation.translate('SUBSCRIPTION_SUCCESSFUL')}${this.translation.translate(plan)}${this.translation.translate('MEMBERSHIP_PLAN_LABEL')}!`)
          } else {
            responseMessage = this.translation.translate(response.message);
          }

          this.openAlertMessage(responseMessage, ALERT_SYSTEM_TITLE.success, 'labelSuccess', true);
        },
        (error: any) => {
          if (error.status === API_STATUS_CODE.badRequest || error.status === API_STATUS_CODE.conflict) {
            if (error.error.message !== 'SUBSCRIPTION_ALREADY_EXISTS') {
              this.openAlertMessage(error.error.message, ALERT_SYSTEM_TITLE.warning, 'labelWarning');
            } else {
              this.openAlertMessage(error.error.message, ALERT_SYSTEM_TITLE.info, 'labelInfo');
            }
          }
        });
    }
  }

  openAlertMessage(message, action, title, langConverted?: boolean) {
    const dialogRef = this.dialog.open(AlertPopupComponent, {
      data: {
        title: this.translation.translate(title),
        message: [langConverted ? message : this.translation.translate('message')],
        action: action,
        buttonName: this.translation.translate('buttonOk')
      }
    });
    this.actionSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
      if (data) {
        this.dialogRef.close(data);
      }
    });
  }

  openTermsAndConditions() {
    const dialogRef = this.dialog.open(MembershipPolicyComponent, {
      data: {
        subscriptionPlanDetails: this.subcscriptionPlanList
      }
    });
    dialogRef.afterClosed().subscribe((response: any) => {
      if (response) {
        this.dialogRef.close();
      }
    });
  }

  // Skip trigger
  skip(){
    this.dialogRef.close(true);
  }

  // Parse Budget
  parsePlanBudget(plan) {
    return (plan ? JSON.parse(plan.optionCommitmentAmount) : []);
  }

}
