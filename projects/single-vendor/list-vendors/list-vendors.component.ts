
import { Component, Input } from '@angular/core';
@Component({
  selector: 'app-list-vendors',
  templateUrl: './list-vendors.component.html'
})
export class ListVendorsComponent {
  @Input() productDetail;
  @Input() showDiscount;
  constructor() {
  }

}
