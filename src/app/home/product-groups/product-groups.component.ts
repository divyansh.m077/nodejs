import { Component, OnInit, Input, PLATFORM_ID, Inject, NgZone, OnDestroy, ApplicationRef } from '@angular/core';
import { WINDOW } from '@ng-toolkit/universal';
import { Router, NavigationEnd } from '@angular/router';
import { formatDate, isPlatformBrowser, DatePipe } from '@angular/common';

import { MatSnackBar } from '@angular/material';
import { TranslationService, Language } from 'angular-l10n';
import { forEach } from 'lodash';

import { HomeService } from './../../home/home.service';
import { API_STATUS_CODE } from '../../app.constants';
import { SNACKBAR_ALERT, HOME_SETTINGS } from '../../app-setting';
import { SharedService } from '../../shared/shared.service';
import { first, filter, take } from 'rxjs/operators';
import { WebWorkerService } from '../../web-worker.service';

@Component({
  selector: 'app-product-groups',
  templateUrl: './product-groups.component.html',
  styleUrls: ['./product-groups.component.scss']
})
export class ProductGroupsComponent implements OnInit, OnDestroy {
  @Input() productList;
  @Input() brandList;
  @Input() bottomAd;

  @Language() lang: string;
  public from: string;
  public productFrom: string;
  public todayDate;
  compareCount: number;
  timer: string;
  currentDate: string;
  siteSettingDetails: any;
  homeSettings = HOME_SETTINGS;
  timerInterval;
  timerSetInterval;
  appIsStable = false;
  now;

  constructor(@Inject(WINDOW) private window: any,
    private router: Router,
    private homeService: HomeService,
    @Inject(PLATFORM_ID) private platform: Object,
    private snackBar: MatSnackBar,
    private translation: TranslationService,
    private sharedService: SharedService,
    private zone: NgZone,
    private datepipe: DatePipe,
    private appRef: ApplicationRef,
    private webWorkerService: WebWorkerService
  ) {
    this.appRef.isStable.pipe(first(x => x === true), take(1)).subscribe(_ => {
      this.appIsStable = true;
      this.zone.run(() => {
        this.getCurrentDate();
      })
    });
    this.router.events.pipe(
      filter((event: any) => event instanceof NavigationEnd), take(1)).subscribe(_ => {
        this.appIsStable = true;
      });


    this.productFrom = 'home';
    this.compareCount = 0;
  }

  initializeGlobalVariables() {
    this.currentDate = formatDate(this.todayDate, 'yyyy-MM-dd', 'en-US');
    this.from = 'home';
    this.productFrom = 'home';
  }

  getCurrentDate() {
    if (isPlatformBrowser(this.platform) && this.appIsStable) {
      const curDate = new Date(this.todayDate);
      curDate.setHours(23, 59, 59, 9999);
      this.calculateCountdownTime(curDate.getTime());
    }
  }

  calculateCountdownTime(countDownDate) {
    if (isPlatformBrowser(this.platform)) {
      this.timerSetInterval = setInterval(() => {
        const distance = countDownDate - this.now;
        this.webWorkerService.getHourMinuteSecFromTime(distance);
        this.timer = this.webWorkerService.calculatedTime;
        if (distance < 0) {
          this.timer = undefined;
          clearInterval(this.timerSetInterval);
          this.todayDate.setDate(this.todayDate.getDate() + 1);
          this.currentDate = formatDate(this.todayDate, 'yyyy-MM-dd', 'en-US');
          this.getCurrentDate();
        }
      });
    }
  }

  ngOnInit() {
    this.getCompareCount();
    const dealObj = {
      'startLimit': this.homeSettings.START_COUNT,
      'endLimit': this.homeSettings.END_COUNT,
      'currentDate': this.datepipe.transform(new Date(), 'yyyy-MM-dd'),
      'desc': true
    };

    this.homeService.getTodayProductList(dealObj).subscribe((x: any) => {
      if (x) {
        this.todayDate = ('startTime' in x && x !== null) ? x.startTime : new Date();
        this.now = new Date(this.todayDate).getTime();
        this.zone.runOutsideAngular(() =>
          this.timerInterval = setInterval(_ => {
            this.now = this.now + 1000;
          }, 1000)
        );
        this.initializeGlobalVariables();
        this.getCurrentDate();
      }
    });
  }

  getCompareCount() {
    this.siteSettingDetails = this.sharedService.getSiteSettingDetails();
    if (this.siteSettingDetails) {
      this.compareCount = this.siteSettingDetails.compareLimit;
    } else {
      this.compareCount = 0;
    }
  }

  showRespectivePage(data) {
    if (data === 'TODAY_DEALS') {
      this.router.navigate(['/deals']);
    } else {
      this.router.navigate(['/trending-products', data]);
    }
  }

  showAllBrands() {
    this.router.navigate(['/brands']);
  }

  getProductActionDetails($event: any) {
    if (this.productList.length > 0) {
      forEach(this.productList, (product) => {
        forEach(product.product, (prod) => {
          if ($event.action === 'wishList' && prod.productId === $event.productId) {
            (prod.favUser = $event.status === 1 ? 1 : 0);
          } else {
            if (prod.vendorId) {
              if (prod.productId === $event.productId && prod.vendorId === $event.productData.vendorId) {
                $event.action === 'cart' ? prod.cartUser = 1 : prod.compareProduct = 1;
              }
            } else {
              if (prod.productId === $event.productId) {
                $event.action === 'cart' ? prod.cartUser = 1 : prod.compareProduct = 1;
              }
            }
          }
        });
      });
    }
  }

  redirectToUrl(data) {
    this.homeService.advertisementAddLog({ adId: data.advertisementSettingId }).subscribe((response: any) => {
      if (response) {

      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
      if (error.status === API_STATUS_CODE.internalServer) {
        this.snackBar.open(error.error.error, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
    const toUrl = new URL(data.redirectUrl).hostname;
    const routerUrl = new URL(window.location.href).hostname;
    const add_url = toUrl.split('.');
    const liveUrl = routerUrl.split('.');
    if (add_url[add_url.length - 2] === liveUrl[liveUrl.length - 2]) {
      const pathname = new URL(data.redirectUrl).pathname;
      const search = new URL(data.redirectUrl).search;
      this.router.navigateByUrl(pathname + search);
    } else {
      this.window.open(data.redirectUrl, '_blank');
    }
  }

  ngOnDestroy() {
    clearInterval(this.timerInterval);
    clearInterval(this.timerSetInterval);
    this.webWorkerService.terminateGetHourMinuteSecFromTime();
  }
}

