import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TitleCasePipe } from '@angular/common';

import { Language, TranslationService } from 'angular-l10n';
import { forEach, filter, isEqual, find } from 'lodash';
import { MatDialog, MatSnackBar } from '@angular/material';

import { UserService } from '../user.service';
import { MANAGE_ADDRESS_RULE, SNACKBAR_WARNING, SNACKBAR_ALERT, SNACKBAR_SUCCESS, MANAGE_ADD_RULE } from '../../app-setting';
import { API_STATUS_CODE, ALERT_SYSTEM_TITLE, COUNTRY_CODE } from '../../app.constants';
import { SharedService } from '../../shared/shared.service';
import { ConfirmationPopupComponent } from '../../shared/confirmation-popup/confirmation-popup.component';
import { StorageControllerService } from '../../storage-controller.service';
@Component({
  selector: 'app-manage-address',
  templateUrl: './manage-address.component.html',
  styleUrls: ['./manage-address.component.scss']
})
export class ManageAddressComponent implements OnInit, OnDestroy {
  buttonDisable: boolean;
  @Language() lang: string;
  showManageAddressDetails: any = false;
  countryList: any[] = [];
  stateList: any[] = [];
  cityList: any[] = [];
  addressTypeList: any[] = [];
  manageAddressList: any[] = [];
  manageAddressSubscribe$: any;
  isEditMode: boolean;
  addressId: any;
  ManageAddressForm: FormGroup;
  manageAddressDetails;
  manageRule: any = MANAGE_ADD_RULE();
  patternDetails: any;
  isLoading: boolean;
  countryCode = COUNTRY_CODE;
  regionList: any[] = [];
  districtList: any[] = [];

  constructor(private userService: UserService,
    private storage: StorageControllerService,
    private formBuilder: FormBuilder,
    private titleCase: TitleCasePipe,
    private matDialog: MatDialog,
    private translation: TranslationService,
    private sharedService: SharedService,
    private snackBar: MatSnackBar
  ) {
    this.getCountryList();
    this.getAddressTypeList();
  }

  ngOnInit() {
    this.isLoading = true;
    this.isEditMode = false;
    this.formInitialization();
  }

  formInitialization(addressDetails?) {

    if (!addressDetails) {
      this.ManageAddressForm = this.formBuilder.group({
        firstName: [null, [Validators.required, Validators.pattern(this.manageRule.FIRST_NAME),
        Validators.minLength(this.manageRule.FIRST_NAME_MIN_LENGTH),
        Validators.maxLength(this.manageRule.FIRST_NAME_MAX_LENGTH)]],
        lastName: [null, [Validators.required, Validators.pattern(this.manageRule.LAST_NAME),
        Validators.minLength(this.manageRule.LAST_NAME_MIN_LENGTH),
        Validators.maxLength(this.manageRule.LAST_NAME_MAX_LENGTH)]],
        Mobile: [null, [Validators.required]],
        alternateMobile: [null],
        Address1: [null, [Validators.required, Validators.minLength(this.manageRule.ADDRESS_MIN_LENGTH),
        Validators.maxLength(this.manageRule.ADDRESS_MAX_LENGTH)]],
        Address2: [null, [Validators.minLength(this.manageRule.ADDRESS_MIN_LENGTH),
        Validators.maxLength(this.manageRule.ADDRESS_MAX_LENGTH)]],
        Country: [null, [Validators.required]],
        State: [null, [Validators.required]],
        divisionId: [null, [Validators.required]],
        districtId: [null, [Validators.required]],
        City: [null, [Validators.required]],
        Pincode: [null, [Validators.required, Validators.pattern(this.manageRule.PINCODE_PATTERN),
        Validators.minLength(this.manageRule.PINCODE_MIN_LENGTH), Validators.maxLength(this.manageRule.PINCODE_MAX_LENGTH)]],
        AddressType: [null, [Validators.required]]
      });
    } else {
      this.ManageAddressForm = this.formBuilder.group({
        firstName: [addressDetails.FirstName ? addressDetails.FirstName : '',
        [Validators.required, Validators.pattern(this.manageRule.FIRST_NAME),
        Validators.minLength(this.manageRule.FIRST_NAME_MIN_LENGTH),
        Validators.maxLength(this.manageRule.FIRST_NAME_MAX_LENGTH)]],
        lastName: [addressDetails.LastName ? addressDetails.LastName : '',
        [Validators.required, Validators.pattern(this.manageRule.LAST_NAME),
        Validators.minLength(this.manageRule.LAST_NAME_MIN_LENGTH), Validators.maxLength(this.manageRule.LAST_NAME_MAX_LENGTH)]],
        Mobile: [addressDetails.Phone ? addressDetails.Phone : '', [Validators.required]],
        alternateMobile: [addressDetails.alternateMobile ? addressDetails.alternateMobile : ''],
        Address1: [addressDetails.AddressOne ? addressDetails.AddressOne : '',
        [Validators.required, Validators.minLength(this.manageRule.ADDRESS_MIN_LENGTH),
        Validators.maxLength(this.manageRule.ADDRESS_MAX_LENGTH)]],
        Address2: [addressDetails.AddressTwo ? addressDetails.AddressTwo : '',
        [Validators.minLength(this.manageRule.ADDRESS_MIN_LENGTH),
        Validators.maxLength(this.manageRule.ADDRESS_MAX_LENGTH)]],
        Country: [addressDetails.CountryID ? addressDetails.CountryID : '', [Validators.required]],
        State: [addressDetails.StateId ? addressDetails.StateId : '', [Validators.required]],
        divisionId: [addressDetails.divisionId ? addressDetails.divisionId : '', [Validators.required]],
        districtId: [addressDetails.districtId ? addressDetails.districtId : '', [Validators.required]],
        City: [addressDetails.townVillageId ? addressDetails.townVillageId : '', [Validators.required]],
        Pincode: [addressDetails.ZipCode ? addressDetails.ZipCode : '',
        [Validators.required, Validators.pattern(this.manageRule.PINCODE_PATTERN),
        Validators.minLength(this.manageRule.PINCODE_MIN_LENGTH), Validators.maxLength(this.manageRule.PINCODE_MAX_LENGTH)]],
        AddressType: [addressDetails.AddressTypeID ? addressDetails.AddressTypeID : '', [Validators.required]]
      });
      this.onChanges();

      if (this.ManageAddressForm.value.Country) {
        this.onChangeCountry(false);
      }
      if (this.ManageAddressForm.value.State) {
        this.onChangeState(false);
      }
      if (this.ManageAddressForm.value.divisionId) {
        this.onChangeRegion(false);
      }
      if (this.ManageAddressForm.value.districtId) {
        this.onChangeDistrict(false);
      }
    }
    this.getSiteSettingDetailForPatterns();
  }

  onChanges(): void {
    this.ManageAddressForm.valueChanges.subscribe(val => {
      const firstName = isEqual(val.firstName, this.manageAddressDetails.FirstName);
      const lastName = isEqual(val.lastName, this.manageAddressDetails.LastName);
      const Mobile = isEqual(val.Mobile, this.manageAddressDetails.Phone);
      const alternateMobile = this.manageAddressDetails.alternateMobile !== null ? isEqual(val.alternateMobile,
        this.manageAddressDetails.alternateMobile) : true;
      const Address1 = isEqual(val.Address1, this.manageAddressDetails.AddressOne);
      const Address2 = this.manageAddressDetails.AddressTwo !== null ? isEqual(val.Address2, this.manageAddressDetails.AddressTwo) : true;
      const Country = isEqual(val.Country, this.manageAddressDetails.CountryID);
      const State = isEqual(val.State, this.manageAddressDetails.StateId);
      const Region = isEqual(val.divisionId, this.manageAddressDetails.divisionId);
      const District = isEqual(val.districtId, this.manageAddressDetails.districtId);
      const City = isEqual(val.City, this.manageAddressDetails.townVillageId);
      const Pincode = isEqual(val.Pincode, this.manageAddressDetails.ZipCode);
      const AddressType = isEqual(val.AddressType, this.manageAddressDetails.AddressTypeID);
      if (firstName === false || lastName === false || Mobile === false || alternateMobile === false || Address1 === false ||
        Address2 === false || Country === false || State === false || Region === false || District === false || City === false || Pincode === false || AddressType === false) {
        this.buttonDisable = false;
      } else {
        this.buttonDisable = true;
      }
    });
  }

  getSiteSettingDetailForPatterns() {
    this.patternDetails = this.sharedService.getSiteSettingDetails();
    if (this.patternDetails) {
      if (this.patternDetails.phoneNumberFormat) {
        const phoneRegx = new RegExp(this.patternDetails.phoneNumberFormat);
        if (this.ManageAddressForm.controls['Mobile'].value) {
          this.ManageAddressForm.controls['Mobile'].markAsTouched();
        }
        if (this.ManageAddressForm.controls['alternateMobile'].value) {
          this.ManageAddressForm.controls['alternateMobile'].markAsTouched();
        }
        this.ManageAddressForm.controls['Mobile'].setValidators([Validators.pattern(phoneRegx)]);
        this.ManageAddressForm.controls['Mobile'].updateValueAndValidity();
        this.ManageAddressForm.controls['alternateMobile'].setValidators([Validators.pattern(phoneRegx)]);
        this.ManageAddressForm.controls['alternateMobile'].updateValueAndValidity();
      }
    }
  }

  onClickAddAddress() {
    if (this.showManageAddressDetails) {
      this.snackBar.open(this.translation.translate('alertFieldFill'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
    }
    this.showManageAddressDetails = true;
    if (!this.isEditMode) {
      this.getProfileInfo();
    }
  }

  getProfileInfo() {
    const userInfo = JSON.parse(this.storage.get('userDetails'));
    if (!userInfo)
      return;
    const profileInfo = {
      FirstName: userInfo.firstName,
      LastName: userInfo.lastName,
      Phone: userInfo.userMobile
    };
    this.manageAddressDetails = profileInfo;
    this.formInitialization(this.manageAddressDetails);
  }

  returnMergedAddress(obj, address1, address2, city, pinCode) {
    const Address1: string = obj[address1] ? obj[address1] : '';
    const Address2: string = obj[address2] ? obj[address2] : '';
    const City: string = obj[city] ? obj[city] : '';
    const PinCode: string = obj[pinCode] ? obj[pinCode] : '';
    return (Address1 + ' ' + Address2 + ', ' + City + '-' + PinCode);
  }

  getAddressTypeList() {
    this.userService.getAddressType().subscribe((response: any) => {
      if (response) {
        this.addressTypeList = response.data;
        this.getManageAddressList();
      }
    }, (errorResponse: any) => {
      this.addressTypeList = [];
      if (errorResponse.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(errorResponse.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  getManageAddressList() {
    this.isLoading = true;
    this.userService.getManageAddressList().subscribe((response: any) => {
      if (response) {
        this.manageAddressList = response.data;
        forEach(this.manageAddressList, (obj, index) => {
          obj.addressTypeName = filter(this.addressTypeList, { 'addressTypeId': obj.addressTypeID })[0].addressType;
          obj.addressList = this.returnMergedAddress(obj, 'addressOne', 'addressTwo', 'townVillageName', 'zipCode');
        });
        this.isLoading = false;
      } else {
        this.isLoading = false;
        this.manageAddressList = undefined;
      }
    }, (errorResponse: any) => {
      this.isLoading = false;
      this.manageAddressList = [];
    });
  }

  pinCodePatternCheck(event) {
    const pincodePattern = MANAGE_ADDRESS_RULE.PINCODE_KEYPRESS_PATTERN;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pincodePattern.test(inputChar)) {
      // event.preventDefault();
    }
  }

  phonePatternCheck(event) {
    if (this.patternDetails) {
      const pincodePattern = new RegExp(this.patternDetails.phoneNumberFormat);
      const inputChar = String.fromCharCode(event.charCode);
      if (!pincodePattern.test(inputChar)) {
        // event.preventDefault();
      }
    }
  }

  saveManageAddressDetails() {
    if (!this.isEditMode) {
      this.userService.addManageAddress(this.formToJson()).subscribe((response: any) => {
        if (response) {
          this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
          this.resetManageAddressForm();
          this.getManageAddressList();
        }
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
    } else {
      this.userService.updateManageAddress(this.formToJson()).subscribe((response: any) => {
        if (response) {
          this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
          this.resetManageAddressForm();
          this.getManageAddressList();
        }
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
    }

  }

  onEdit(addressId: string) {
    this.isEditMode = true;
    this.addressId = addressId;
    const dataObject = {
      'addressId': addressId
    };
    this.userService.getAddressInfo(addressId).subscribe((response: any) => {
      if (response) {
        this.manageAddressDetails = response[0];
        this.showManageAddressDetails = true;
        this.buttonDisable = true;
        this.formInitialization(this.manageAddressDetails);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  formToJson() {
    const manageAddressFormValue = {
      'firstName': this.titleCase.transform(this.ManageAddressForm.get('firstName').value),
      'lastName': this.ManageAddressForm.get('lastName').value,
      'phone': this.ManageAddressForm.get('Mobile').value,
      'alternateMobile': this.ManageAddressForm.get('alternateMobile').value,
      // 'companyName': this.ManageAddressForm.get('Company').value,
      'zipCode': this.ManageAddressForm.get('Pincode').value,
      'address1': this.ManageAddressForm.get('Address1').value,
      'address2': this.ManageAddressForm.get('Address2').value,
      'addressType': this.ManageAddressForm.get('AddressType').value,
      'country': this.ManageAddressForm.get('Country').value,
      'state': this.ManageAddressForm.get('State').value,
      'regionId': this.ManageAddressForm.get('divisionId').value,
      'districtId': this.ManageAddressForm.get('districtId').value,
      'townVillageId': this.ManageAddressForm.get('City').value,
      'addressId': this.addressId
    };
    return manageAddressFormValue;
  }

  onDelete(addressId: string) {
    const dialogRef = this.matDialog.open(ConfirmationPopupComponent, {
      data: {
        title: this.translation.translate('labelDialogTitle'),
        message: this.translation.translate('alertMsgAddressRemove'),
        action: ALERT_SYSTEM_TITLE.warning,
        buttonOneName: this.translation.translate('buttonDelete'),
        buttonTwoName: this.translation.translate('buttonCancel'),
      }
    });
    this.manageAddressSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
      if (data) {
        const dataJson = {
          'addressId': addressId
        };
        this.userService.deleteManageAddress(dataJson).subscribe((response: any) => {
          if (response) {
            this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
            this.getManageAddressList();
            this.matDialog.closeAll();
          }
        }, (error: any) => {
          this.matDialog.closeAll();
          if (error.status === API_STATUS_CODE.badRequest) {
            this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
          }
        });
      }
    });
  }

  resetManageAddressForm() {
    this.ManageAddressForm.reset();
    this.formInitialization();
    this.showManageAddressDetails = false;
    this.isEditMode = false;
  }

  ngOnDestroy() {
    if (this.manageAddressSubscribe$) {
      this.manageAddressSubscribe$.unsubscribe();
    }
  }

  getCountryList() {
    this.userService.getCountryList().subscribe((response: any) => {
      if (response) {
        this.countryList = response.data;
        this.setZipCodePattern();
        let defalutCountry = find(this.countryList, { countryName: 'India' });
        this.ManageAddressForm.patchValue({
          Country: defalutCountry ? defalutCountry.countryId : undefined
        });
        this.onChangeCountry(false);
      }
    }, (errorResponse: any) => {
      this.countryList = [];
    });
  }

  // On Change Country to get States
  onChangeCountry(isReset?: boolean) {
    if (this.ManageAddressForm.value.Country) {
      this.setZipCodePattern();
      this.getStateList(this.ManageAddressForm.value.Country);
      if (isReset)
        this.resetLocationList('Country');
    }
  }

  // Get State list
  getStateList(countryId) {
    this.userService.getStateList(countryId).subscribe((response: any) => {
      if (response) {
        this.stateList = response.data;
      }
    }, (errorResponse: any) => {
      this.stateList = [];
      this.resetLocationList('State');
    });
  }

  // On Change State 
  onChangeState(isReset?: boolean) {
    if (this.ManageAddressForm.value.State) {
      this.getRegionList(this.ManageAddressForm.value.State);
      if (isReset)
        this.resetLocationList('State');
    }
  }

  // Set Zipcode pattern based on Country
  setZipCodePattern() {
    let countryObj: any = find(this.countryList, { countryId: this.ManageAddressForm.value.Country });
    if (countryObj && countryObj.zipCodeRegex) {
      this.ManageAddressForm.controls['Pincode'].setValidators([Validators.required, Validators.pattern(countryObj.zipCodeRegex),
      Validators.minLength(this.manageRule.PINCODE_MIN_LENGTH), Validators.maxLength(this.manageRule.PINCODE_MAX_LENGTH)]);
      this.ManageAddressForm.controls['Pincode'].updateValueAndValidity();
    }
  }

  // Get Region list
  getRegionList(stateId) {
    this.userService.getRegionList(stateId).subscribe((response: any) => {
      if (response) {
        this.regionList = response.data;
      }
    }, (errorResponse: any) => {
      this.regionList = [];
      this.resetLocationList('Region');
    });
  }

  // Get District list
  getDistrictList(divisionId) {
    this.userService.getDistrictList(divisionId).subscribe((response: any) => {
      if (response) {
        this.districtList = response.data;
      }
    }, (errorResponse: any) => {
      this.districtList = [];
      this.resetLocationList('District');
    });
  }

  // Get City list
  getCityList(districtId) {
    const reqObj = {
      "districtId": districtId,
      "selectedId": this.manageAddressDetails ? this.manageAddressDetails.townVillageId : null,
      "searchString": ""
    }
    this.sharedService.getCityTown(reqObj).subscribe((response: any) => {
      if (response) {
        this.cityList = response.data;
      }
    }, (errorResponse: any) => {
      this.cityList = [];
      this.ManageAddressForm.patchValue({
        townVillageId: undefined
      })
    });
  }

  // On Change Region 
  onChangeRegion(isReset?: boolean) {
    if (this.ManageAddressForm.value.divisionId) {
      this.getDistrictList(this.ManageAddressForm.value.divisionId);
      if (isReset)
        this.resetLocationList('Region');
    }
  }
  // On Change District 
  onChangeDistrict(isReset?: boolean) {
    if (this.ManageAddressForm.value.districtId) {
      this.getCityList(this.ManageAddressForm.value.districtId);
      if (isReset)
        this.resetLocationList('District');
    }
  }

  // Reset list
  resetLocationList(targetLocation, isReset?: boolean) {
    if (targetLocation == 'Country') {
      this.stateList = [];
      this.regionList = [];
      this.districtList = [];
      this.cityList = [];
      this.ManageAddressForm.patchValue({
        State: undefined,
        divisionId: undefined,
        districtId: undefined,
        City: undefined
      })
    } else if (targetLocation == 'State') {
      this.regionList = [];
      this.districtList = [];
      this.cityList = [];
      this.ManageAddressForm.patchValue({
        divisionId: undefined,
        districtId: undefined,
        City: undefined
      })
    } else if (targetLocation == 'Region') {
      this.districtList = [];
      this.cityList = [];
      this.ManageAddressForm.patchValue({
        districtId: undefined,
        City: undefined
      })
    } else if (targetLocation == 'District') {
      this.cityList = [];
      this.ManageAddressForm.patchValue({
        City: undefined
      })
    }
  }

}
