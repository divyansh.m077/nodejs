import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { Language, TranslationService } from 'angular-l10n';

import { UserService } from './../user.service';
import { API_STATUS_CODE } from '../../app.constants';

import { MatSnackBar } from '@angular/material';

import { settings, REGISTRATION_RULE, SNACKBAR_SUCCESS, SNACKBAR_ALERT } from './../../app-setting';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  @Language() lang: string;
  forgotPasswordForm: FormGroup;
  siteKey;
  captchaSize;
  captchaTheme;
  captchaType;
  captchaLanguage;
  isInBrowser;
  constructor(private fb: FormBuilder, private userService: UserService, private router: Router, private snackBar: MatSnackBar, private translation: TranslationService) {
    this.formInitialization();
    this.siteKey = settings.captchaKey;
    this.captchaSize = 'Normal';
    this.captchaTheme = 'Light';
    this.captchaType = 'Image';
    this.captchaLanguage = 'en';
    if (typeof window !== 'undefined') {
      this.isInBrowser = true;
    }
  }

  formInitialization() {
    this.forgotPasswordForm = this.fb.group({
      'userEmail': ['', [Validators.required, Validators.pattern(REGISTRATION_RULE.EMAIL_PATTERN)]],
      'isCaptchaVerified': ['', [Validators.required]]
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    this.userService.forgotPassword({'userEmail': this.forgotPasswordForm.value.userEmail,
                                     'isCaptchaVerified': this.forgotPasswordForm.value.isCaptchaVerified}).subscribe((response: any) => {
      if (response) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        this.router.navigate(['/signin']);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

}
