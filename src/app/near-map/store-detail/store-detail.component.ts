import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Language, TranslationService } from 'angular-l10n';
import { WINDOW } from '@ng-toolkit/universal';
import { MatSnackBar } from '@angular/material';
import { uniqBy, concat, find } from 'lodash';

import { NearMapService } from '../near-map.service';
import { PRODUCT_LIST_SETTINGS, SNACKBAR_ALERT } from '../../app-setting';
import { API_STATUS_CODE } from '../../app.constants';
import { HomeService } from '../../home/home.service';
import { ComonService } from '../../common/comon.service';
import { SharedService } from '../../shared/shared.service';
import { StorageControllerService } from '../../storage-controller.service';

@Component({
  selector: 'app-store-detail',
  templateUrl: './store-detail.component.html',
  styleUrls: ['./store-detail.component.scss']
})
export class StoreDetailComponent implements OnInit, OnDestroy {
  storeInfo: any;
  @Language() lang: string;
  isImageLogo: boolean;
  public productFrom = 'product';
  public productList: any = [];
  public productsCount: any = PRODUCT_LIST_SETTINGS;
  public productTotalLength: number;
  public productSubscribe$: any;
  public compareCount: any = 0;
  siteSettingDetails: any;
  constructor(private nearMapService: NearMapService,
              private router: Router,
              @Inject(WINDOW) private window: any,
              private homeService: HomeService,
              private comonService: ComonService,
              private snackBar: MatSnackBar,
              private translation: TranslationService,
              private sharedService: SharedService,
              private storage: StorageControllerService
            ) {
    this.productSubscribe$ = this.homeService.getSignInStatus().subscribe((response: any) => {
      if (response === false) {
        this.productsCount.START_COUNT = 0;
        this.productList = [];
        this.getAllProductList();
      }
    });
  }

  ngOnInit() {
    this.getStoreInfoDetails();
    this.getCompareCount();
    this.productsCount.START_COUNT = 0;
    this.productList = [];
    this.productTotalLength = 0;
  }

  getStoreInfoDetails() {
    const storeData = this.storage.get('storeDetails');
    this.storeInfo = JSON.parse(storeData);
    this.isImageLogo = this.storeInfo.storeLogo ? true : false;
    this.getAllProductList();
  }

  getLocationOnMap() {
    const googleUrl = 'https://www.google.com/maps/dir//' + this.storeInfo.vendorLatitude + ',' + this.storeInfo.vendorLongitude + '/@' +
    this.storeInfo.vendorLatitude + ',' + this.storeInfo.vendorLongitude;
    const windowObject = this.window;
    windowObject.open(googleUrl, '_blank');
  }

  getCompareCount() {
    this.siteSettingDetails = this.sharedService.getSiteSettingDetails();
    if (this.siteSettingDetails) {
      this.compareCount = this.siteSettingDetails.compareLimit;
    } else {
      this.compareCount = 0;
    }
  }

  getAllProductList() {
    const productObj = {
      'startLimit': this.productsCount.START_COUNT,
      'endLimit': this.productsCount.END_COUNT,
      'targetCurrency': 'USD',
      'vendorId': this.storeInfo.vendorId
    };
    this.nearMapService.getAllProductsList(productObj)
    .subscribe((response: any) => {
      if (response) {
        this.productList = concat(this.productList, response.products.product);
        this.productList = uniqBy(this.productList, 'productId');
        this.productList = this.comonService.findCartCompareSelection(this.productList);
        this.productTotalLength = response.products.productlength;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  showMoreProducts() {
    this.productsCount.START_COUNT += this.productsCount.SHOW_MORE_COUNT;
    this.getAllProductList();
  }

  getProductActionDetails($event: any) {
    if ($event.action === 'wishList') {
      const productData = find(this.productList, {'productId': $event.productId});
      productData.favUser = $event.status === 1 ? 1 : 0;
    } else if ($event.action === 'cart') {
      const productData = find(this.productList, {'productId': $event.productId});
      productData.cartUser = 1;
    } else {
      const productData = find(this.productList, {'productId': $event.productId});
      productData.compareProduct = 1;
    }
  }

  ngOnDestroy() {
    if (this.productSubscribe$) {
      this.productSubscribe$.unsubscribe();
    }
  }
}
