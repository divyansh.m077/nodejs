import { AppComponent } from './app.component';
import { AppModule } from './app.module';
import { NgModule } from '@angular/core';
import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { BrowserCookiesModule } from '@ngx-utils/cookies/browser';
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';


@NgModule({
   bootstrap: [AppComponent],
   imports: [
      BrowserModule.withServerTransition({appId: 'app-root'}),
      BrowserTransferStateModule,
      BrowserCookiesModule.forRoot(environment.cookieConfig),
      AppModule,
      ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
   ]
})
export class AppBrowserModule {
   constructor() {}

}


