import { Component, OnInit } from '@angular/core';

import { Language, TranslationService } from 'angular-l10n';
import { concat } from 'lodash';

import { MatSnackBar } from '@angular/material';

import { ProductService } from '../products/products.service';
import { SOLD_PRODUCT_SETTINGS, SNACKBAR_ALERT } from './../app-setting';
import { API_STATUS_CODE } from '../app.constants';

@Component({
  selector: 'app-sold-out',
  templateUrl: './sold-out.component.html',
  styleUrls: ['./sold-out.component.scss']
})

export class SoldOutComponent implements OnInit {
  @Language() lang: string;
  public productList: any = [];
  public productFrom: string;
  public soldOutCount: any = SOLD_PRODUCT_SETTINGS;
  public soldOutLength: number;
  constructor(private productService: ProductService, private snackBar: MatSnackBar, private translation: TranslationService) {
    this.soldOutLength = 0;
    this.productFrom = 'sold';
  }

  ngOnInit() {
    this.soldOutCount.START_COUNT = 0;
    this.getAllProductsList();
  }

  getAllProductsList() {
    const soldObj = {
      'startLimit': this.soldOutCount.START_COUNT,
      'endLimit': this.soldOutCount.END_COUNT,
      'targetCurrency': 'USD'
    };
    this.productService.getSoldProductList(soldObj)
    .subscribe((response: any) => {
      if (response) {
        this.productList = concat(this.productList, response.products.product);
        this.soldOutLength = response.products.productlength;
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  showMoreProducts() {
    this.soldOutCount.START_COUNT += this.soldOutCount.SHOW_MORE_COUNT;
    this.getAllProductsList();
  }

}
