import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SHARED_URL } from './shared.url.config';
import { TOASTER_CLICK_TO_CLOSE } from '../app.constants';
import { WINDOW } from '@ng-toolkit/universal';
import { CookiesService } from '@ngx-utils/cookies';
import { EMPTY } from 'rxjs';
import { StorageControllerService } from '../storage-controller.service';
// import { CURRENCY_FORMAT } from 'app/app-setting';

// function _window(): any {
//   return window;
// }
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  siteSetting: any;
  currencySetting: any;
  numberFormat: any;
  latitude: number;
  longitude: number;
  membershipPlanDetails: any;

  constructor(
    private storage: StorageControllerService,
    private http: HttpClient,
    private cookies: CookiesService,
    @Inject(WINDOW) private window: any,
  ) { }

  addProductToWishList(productData) {
    return this.http.put(SHARED_URL.ADD_TO_WISH_LIST, productData);
  }

  addCartDetails(cartDetails) {
    return this.http.post(SHARED_URL.ADD_TO_CART, cartDetails);
  }

  trackCompareProductDetails(productData) {
    return EMPTY;
  }

  initializeSiteSettingDetails(data) {
    this.siteSetting = data;
  }

  getSiteSettingDetails() {
    return this.siteSetting;
  }

  getTimingForNotification() {
    return TOASTER_CLICK_TO_CLOSE;
  }

  // get nativeWindow(): any {
  //   return _window();
  // }

  clearUserInfoFromStorage() {
    this.storage.remove('token');
    this.storage.remove('userImage');
    this.storage.remove('id');
    this.storage.remove('subscriptionPlan');
    this.cookies.removeAll();
  }

  getCityTown(reqObj) {
    return this.http.post(SHARED_URL.GET_CITY_TOWN_URL, reqObj);
  }

  getSubcriptionPlanDetails() {
    return this.http.get(SHARED_URL.SUBSCRIPTION_PLAN_LIST);
  }

  subscribePlan(reqObj) {
    return this.http.post(SHARED_URL.SUBSCRIPTION, reqObj);
  }

  getSubscriptionDetails() {
    return this.http.get(SHARED_URL.SUBSCRIPTION);
  }

  getLocation() {
    if (this.window.navigator.geolocation) {
      this.window.navigator.geolocation.getCurrentPosition(position => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude
      });
    }
  }

  // Get Location data
  getTrackLocationData() {
    let returnObj: any = {
      latitude: undefined,
      longitude: undefined
    };
    const siteSettingDetails = this.getSiteSettingDetails();
    if (siteSettingDetails.trackLocation == 1) {
      returnObj = {
        latitude: this.latitude,
        longitude: this.longitude
      }
    }
    return returnObj;
  }
}
