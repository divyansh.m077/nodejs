import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ProductsComponent } from './products.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductListComponent } from './product-list/product-list.component';
import { TrendingProductsComponent } from './trending-products/trending-products.component';
import { ProductReviewsComponent } from './product-reviews/product-reviews.component';
import { ProductComparisionComponent } from './product-comparision/product-comparision.component';
import { SearchProductComponent } from './search-product/search-product.component';

const productRoutes: Routes = [
    {
      path: 'products',
      component: ProductsComponent
    },
    {
      path: 'product-detail/:productId',
      component: ProductDetailComponent
    },
    {
      path: 'product-list/:id',
      component: ProductListComponent
    },
    {
      path: 'trending-products/:from',
      component: TrendingProductsComponent
    },
    {
      path: 'product-reviews/:id',
      component: ProductReviewsComponent
    },
    {
      path: 'product-comparision',
      component: ProductComparisionComponent
    },
    {
      path: 'search-product',
      component: SearchProductComponent
    }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      productRoutes
    )
  ],
  declarations: [],
  exports: [RouterModule],
})
export class ProductsRoutingModule { }
