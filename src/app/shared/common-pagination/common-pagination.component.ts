import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

import { Language } from 'angular-l10n';
import { StorageControllerService } from '../../storage-controller.service';

@Component({
  selector: 'app-common-pagination',
  templateUrl: './common-pagination.component.html',
  styleUrls: ['./common-pagination.component.scss']
})
export class CommonPaginationComponent implements OnInit, OnChanges {
  @Language() lang: string;
  @Input() offset = 0;
  @Input() limit = 1;
  @Input() size = 1;
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();

  currentPage: number;
  totalPages: number;

  constructor(private storage: StorageControllerService) { }

  ngOnInit() {
    this.getPages(this.offset, this.limit, this.size);
  }

  ngOnChanges() {
    this.getPages(this.offset, this.limit, this.size);
  }

  getPages(offset: number, limit: number, size: number) {
    this.currentPage = this.getCurrentPage(offset, limit);
    const totPages = this.getTotalPages(limit, size);
    this.totalPages = totPages ? totPages : 1;
  }

  isValidPageNumber(page: number, totalPages: number): boolean {
    return page > 0 && page <= totalPages;
  }

  getCurrentPage(offset: number, limit: number): number {
    return Math.floor(offset / limit) + 1;
  }

  getTotalPages(limit: number, size: number): number {
    return Math.ceil(Math.max(size, 1) / Math.max(limit, 1));
  }

  selectPage(page: number, event) {
    this.cancelEvent(event);
    if (this.isValidPageNumber(page, this.totalPages)) {
      this.storage.set('offSet', ((page - 1) * this.limit).toString());
      this.pageChange.emit((page - 1) * this.limit);
    }
  }

  cancelEvent(event) {
    event.preventDefault();
  }

}
