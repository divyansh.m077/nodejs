import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { BlogComponent } from './blog.component';

const blogRoutes: Routes = [
  {
    path: '',
    component: BlogComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      blogRoutes
    )
  ],
  declarations: [],
  exports: [RouterModule],
})
export class BlogRoutingModule { }
