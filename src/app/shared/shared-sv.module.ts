import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorModule } from '../../../projects/single-vendor/single-vendor.module';
import { TranslationModule } from 'angular-l10n';
import { MatInputModule, MatButtonModule, MatCheckboxModule,
  MatExpansionModule, MatTableModule, MatFormFieldModule,
   MatRippleModule, MatSelectModule, MatListModule, MatDialogModule, MatRadioModule, DateAdapter } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    VendorModule,
    TranslationModule,
    CommonModule,
    TranslationModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatTableModule,
    MatFormFieldModule,
    MatRippleModule,
    MatSelectModule,
    MatListModule,
    MatDialogModule,
    MatRadioModule
  ],
  declarations: [],
  exports: [
    VendorModule
  ],
  providers: []
})
export class SharedVendorModule { }
