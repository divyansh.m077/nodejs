import { BASE_URL } from '../common/common.url-config';

export const SHARED_URL = {
    ADD_TO_WISH_LIST: BASE_URL + 'favourite-product',
    ADD_TO_CART: BASE_URL + 'cart/add-cart-multiple',
    ADD_TO_COMPARE_TRACK: BASE_URL + 'product-compare/track',
    GET_CITY_TOWN_URL: BASE_URL + 'get-town-village/',
    SUBSCRIPTION_PLAN_LIST: BASE_URL + 'subscription-plan',
    SUBSCRIPTION: BASE_URL + 'subscription'
};
