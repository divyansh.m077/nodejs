import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { BrandsComponent } from './brands.component';

const brandsRoutes: Routes = [
  {
    path: '',
    component: BrandsComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      brandsRoutes
    )
  ],
  declarations: [],
  exports: [RouterModule],
})
export class BrandsRoutingModule { }
