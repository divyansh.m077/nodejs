export const settings = {
  'productInitialStartCount': 0,
  'productInitialEndCount': 10,
  'productShowMoreCount': 10,
  'productTapOptionLatest': 1,
  'productTapOptionLowest': 2,
  'productTapOptionHighest': 3,
  'productTapOptionPopularity': 4,
  'adminUrl': '/admin/',
  // 'adminUrl': 'http://206.189.229.18/demo/admin/',
  //'languageUrl': 'http://192.168.0.35:7500/assets/locale-',
  'languageUrl': '/assets/locale-',
  'facebookLoginProvider': '443587969583610',
  'googleLoginProvider': '1005348728505-r2l14iuo9lg7jdt3vren605jbe022vcr.apps.googleusercontent.com',
  'complaintImageSize': 5000000,
  'acceptImageTypes': 'image/jpeg,image/jpg,image/png',
  'LIMIT': 10,
  'OFFSET': 0,
  'captchaKey': '6LcMhooUAAAAALWL6E8447klGtrtw0Hkc1Q1iFXA',
};

export const USER_IMAGE_RULE = {
  'MAX_SIZE': 1000000,
  'MIN_RESOLUTION': 100,
  'MAX_RESOLUTION': 1000
};

export const REGISTRATION_RULE = {
  'PASSWORD_PATTERN': '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[`~#^()_+={}<>";:|[/,.$@$!%*?&-])[A-Za-z0-9`~#^()_+={}<>":;|[/,.$@$!%*?&-]{6,20}',
  'NAME_PATTERN': '[A-Za-z]+(\\s[A-Za-z]+){0,}?',
  'EMAIL_PATTERN': '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$',
  'MOBILE_PATTERN': '[+0-9]*',
  'FIRST_NAME_MIN_LENGTH': 3,
  'LAST_NAME_MIN_LENGTH': 1,
  'NAME_MAX_LENGTH': 20,
  'MOBILE_MIN_LENGTH': 4,
  'MOBILE_MAX_LENGTH': 13,
};

export const REG_RULE = () => {
  return {
    'PASSWORD_PATTERN': '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[`~#^()_+={}<>";:|[/,.$@$!%*?&-])[A-Za-z0-9`~#^()_+={}<>":;|[/,.$@$!%*?&-]{6,20}',
    'NAME_PATTERN': `[${getUniCode()}]+(\\s[${getUniCode()}]+){0,}?`,
    'EMAIL_PATTERN': '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$',
    'MOBILE_PATTERN': '[0-9]*',
    'FIRST_NAME_MIN_LENGTH': 3,
    'LAST_NAME_MIN_LENGTH': 1,
    'NAME_MAX_LENGTH': 20,
    'MOBILE_MIN_LENGTH': 4,
    'MOBILE_MAX_LENGTH': 13,
  };
};

export const ADD_COMPLAINT_RULE = {
  'MAX_LENGTH': 300
};

export const ENQUIRY_RULE = {
  'NAME_PATTERN': '[A-Za-z]+(\\s[A-Za-z]+){0,}?',
  'NAME_MAX_LENGTH': 20,
  'NAME_MIN_LENGTH': 3,
  'EMAIL_PATTERN': '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$',
  'MOBILE_PATTERN': '[+0-9]*',
  'SUBJECT_MAX_LENGTH': 50,
  'SUBJECT_MIN_LENGTH': 5,
  'MESSAGE_MIN_LENGTH': 50,
  'MESSAGE_MAX_LENGTH': 500
};

export const ENQ_RULE = () => {
  return {
    'NAME_PATTERN': `[${getUniCode()}]+(\\s[${getUniCode()}]+){0,}?`,
    'NAME_MAX_LENGTH': 20,
    'NAME_MIN_LENGTH': 3,
    'EMAIL_PATTERN': '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$',
    'MOBILE_PATTERN': '[+0-9]*',
    'SUBJECT_MAX_LENGTH': 50,
    'SUBJECT_MIN_LENGTH': 5,
    'MESSAGE_MIN_LENGTH': 50,
    'MESSAGE_MAX_LENGTH': 500
  };
};

export const SEARCH_RULE = {
  'MAX_LENGTH': 100
};

export const ORDER_CANCEL_RULE = {
  'MAX_LENGTH': 2000
};

export const FILTER_RULE = {
  'MAX_LENGTH': 30
};

export const RESET_PASSWORD_RULE = {
  'PASSWORD_PATTERN': '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[`~#^()_+={}<>";:|[/,.$@$!%*?&-])[A-Za-z0-9`~#^()_+={}<>":;|[/,.$@$!%*?&-]{6,20}'
};

export const REVIEW_RULE = {
  'REVIEW_TITLE_PATTERN': '[A-Za-z0-9&,@()\'"!%\-]+(\\s[A-Za-z0-9&,@()\'"!%\-]+){0,}?',
  'KEYPRESS_REVIEW_TITLE_PATTERN': /^[a-zA-Z0-9&,@()'"!%\- ]+$/,
  'MIN_LENGTH': 3,
  'MAX_LENGTH': 200,
  'REVIEW_MIN_LENGTH': 3,
  'REVIEW_MAX_LENGTH': 50
};

export const PRODUCT_AVAILABLE_RULE = {
  'ZIPCODE_MAXLENGTH': 10,
  'ZIPCODE_MINLENGTH': 4,
  'ZIPCODE_PATTERN': '[0-9]*'
};

export const REV_RULE = () => {
  return {
    'REVIEW_TITLE_PATTERN': '[' + getUniCode() + '0-9&,@()\'"!%\-]+(\\s[' + getUniCode() + '0-9&,@()\'"!%\-]+){0,}?',
    'KEYPRESS_REVIEW_TITLE_PATTERN': /^[a-zA-Z0-9&,@()'"!%\- ]+$/,
    'MIN_LENGTH': 3,
    'MAX_LENGTH': 200,
    'REVIEW_MIN_LENGTH': 3,
    'REVIEW_MAX_LENGTH': 50
  };
};

export const BRAND_SETTINGS = {
  'START_COUNT': 0,
  'END_COUNT': 12,
  'SHOW_MORE_COUNT': 12
};

export const PRODUCT_LIST_SETTINGS = {
  'START_COUNT': 0,
  'END_COUNT': 10,
  'SHOW_MORE_COUNT': 10
};

export const SEARCH_PRODUCT_LIST_SETTINGS = {
  'START_COUNT': 0,
  'END_COUNT': 10,
  'SHOW_MORE_COUNT': 10
};

export const SOLD_PRODUCT_SETTINGS = {
  'START_COUNT': 0,
  'END_COUNT': 10,
  'SHOW_MORE_COUNT': 10
};

export const TRENDING_PRODUCT_SETTINGS = {
  'START_COUNT': 0,
  'END_COUNT': 10,
  'SHOW_MORE_COUNT': 10
};

export const DEALS_SETTINGS = {
  'START_COUNT': 0,
  'END_COUNT': 10,
  'SHOW_MORE_COUNT': 10
};

export const HOME_SETTINGS = {
  'START_COUNT': 0,
  'END_COUNT': 4,
  'SHOW_MORE_COUNT': 10,
  'BRAND_END_COUNT': 4
};

export const REVIEW_SETTINGS = {
  'START_COUNT': 0,
  'END_COUNT': 5,
  'SHOW_MORE_COUNT': 5
};

export const WISHLIST_SETTINGS = {
  'START_COUNT': 0,
  'END_COUNT': 10,
  'SHOW_MORE_COUNT': 10
};

export const CART_RULE = {
  'QUANTITY_PATTERN': /[0-9]/,
  'QUANTITY_MAX_VALUE': 999,
  'QUANTITY_MAX_LENGTH': 3,
  'QUANTITY_MIN_VALUE': 1,
};

export const PROFILE_INFO_RULE = {
  'NAME_PATTERN': '[A-Za-z]+(\\s[A-Za-z]+){0,}?',
  'MOBILE_PATTERN': '[+0-9]*',
  'EMAIL_PATTERN': '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$',
  'FIRST_NAME_MIN_LENGTH': 3,
  'LAST_NAME_MIN_LENGTH': 1,
  'NAME_MAX_LENGTH': 20,
  'MOBILE_MIN_LENGTH': 4,
  'MOBILE_MAX_LENGTH': 13,
  'EMAIL_MAX_LENGTH': 50,
  'RESOLUTION': {
    'RESOLUTION_WIDTH' : 100,
    'RESOLUTION_HEIGHT' : 100
  }
};

export const PRO_INFO_RULE = () => {
  return {
    'NAME_PATTERN': `[${getUniCode()}]+(\\s[${getUniCode()}]+){0,}?`,
    'MOBILE_PATTERN': '[+0-9]*',
    'EMAIL_PATTERN': '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$',
    'FIRST_NAME_MIN_LENGTH': 3,
    'LAST_NAME_MIN_LENGTH': 1,
    'NAME_MAX_LENGTH': 20,
    'MOBILE_MIN_LENGTH': 4,
    'MOBILE_MAX_LENGTH': 13,
    'EMAIL_MAX_LENGTH': 50,
    'RESOLUTION': {
      'RESOLUTION_WIDTH' : 100,
      'RESOLUTION_HEIGHT' : 100
    }
  };
};

export const MANAGE_ADDRESS_RULE = {
  'FIRST_NAME': '[A-Za-z]+(\\s[A-Za-z]+){0,}?',
  'FIRST_NAME_MIN_LENGTH': 3,
  'FIRST_NAME_MAX_LENGTH': 20,
  'LAST_NAME': '[A-Za-z]+(\\s[A-Za-z]+){0,}?',
  'LAST_NAME_MIN_LENGTH': 1,
  'LAST_NAME_MAX_LENGTH': 20,
  'MOBILE': '[0-9]*',
  'MOBILE_MIN_LENGTH': 4,
  'MOBILE_MAX_LENGTH': 13,
  'ADDRESS_MIN_LENGTH': 3,
  'ADDRESS_MAX_LENGTH': 100,
  'PINCODE_PATTERN': '[0-9]*',
  'PINCODE_KEYPRESS_PATTERN': /[0-9]/,
  'PINCODE_MIN_LENGTH': 4,
  'PINCODE_MAX_LENGTH': 10,
};

export const MANAGE_ADD_RULE = () => {
  return {
    'FIRST_NAME': `[${getUniCode()}]+(\\s[${getUniCode()}]+){0,}?`,
    'FIRST_NAME_MIN_LENGTH': 3,
    'FIRST_NAME_MAX_LENGTH': 20,
    'LAST_NAME': `[${getUniCode()}]+(\\s[${getUniCode()}]+){0,}?`,
    'LAST_NAME_MIN_LENGTH': 1,
    'LAST_NAME_MAX_LENGTH': 20,
    'MOBILE': '[0-9]*',
    'MOBILE_MIN_LENGTH': 4,
    'MOBILE_MAX_LENGTH': 13,
    'ADDRESS_MIN_LENGTH': 3,
    'ADDRESS_MAX_LENGTH': 100,
    'PINCODE_PATTERN': '[0-9]*',
    'PINCODE_KEYPRESS_PATTERN': /[0-9]/,
    'PINCODE_MIN_LENGTH': 4,
    'PINCODE_MAX_LENGTH': 10,
  };
};

export const CURRENCY_FORMAT = {
  'COMMA_SEPERATOR': '1.0-0',
  'ROUND_OFF': '1.0-0',
  'TWO_DECIMAL': '2.2-2',
  'THREE_DECIMAL': '1.3-3',
  'FOUR_DECIMAL': '1.4-4'
};

export const ORDER_SUMMARY_IMAGES = {
  'COD_IMAGE': 'assets/images/cod.png',
  'STRIPE_IMAGE': 'assets/images/stripe.png',
  'WALLET_IMAGE': 'assets/images/wallet-icon.png',
  'RAZOR_PAY': 'assets/images/razorpay.svg',
  'HDFC_IMAGE': 'assets/images/hdfc.svg',
  'CASH_FREE_IMAGE': 'assets/images/cashfree.png'
};

export const SNACKBAR_WARNING = {
  duration: 4000,
  panelClass: 'snack-warning'
};

export const SNACKBAR_ALERT = {
  duration: 10000,
  panelClass: 'snack-alert'
};

export const SNACKBAR_INFO = {
  duration: 2500,
  panelClass: 'snack-info'
};

export const SNACKBAR_SUCCESS = {
  duration: 2500,
  panelClass: 'snack-success'
};

export const NEAR_MAP_SETTINGS = {
  'START_COUNT': 0,
  'END_COUNT': 10,
  'SHOW_MORE_COUNT': 10,
  'RANGE_MIN_VALUE': 1,
  'RANGE_MAX_VALUE': 999
};

export function getUniCode() {
  if (typeof window !== 'undefined') {
    const language = localStorage.getItem('language');     // language should be maintained in cookies through out, for future scope.
    switch (language) {
      case 'en':
        return '\u0041-\u005A\u0061-\u007A';
      case 'ta':
        return '\u0B80-\u0BFF';
      case 'es':
        return '\u0B80-\u0BFF';
      case 'it':
        return 'A-Za-z\u00E0\u00E8\u00E9\u00EC\u00F2\u00F3\u00F9';
    }
  } else {
    return '\u0041-\u005A\u0061-\u007A';
  }
}

export const OTP_RULE = {
  PATTERN: '[0-9]*',
  LENGTH: 6
}

export const MEMBERSHIP_RULE = {
  'MULTIPLE_OF_PLATINUM': 1000,
  'PLATINUM_LENGTH': 6
}
