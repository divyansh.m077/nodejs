import { BASE_URL } from '../common/common.url-config';

export const HOME_URL = {
    TODAY_DEAL: BASE_URL + 'get-home-todaydeal-all',
    HOT_PRODUCTS: BASE_URL + 'get-hot-product-all',
    BEST_SELLING: BASE_URL + 'get-best-selling-product-all',
    BRANDS: BASE_URL + 'get-featured-brands-all',
    BANNERS: BASE_URL + 'get-slider',
    ADVERTISEMENT: BASE_URL + 'get-advertisement',
    FEATURED_BRANDS: BASE_URL + 'get-featured-brands-all',
    ADVERTISEMENT_LOG: BASE_URL + 'ad-view-log/',
    FEATURED_PRODUCTS: BASE_URL + 'get-featured-product-all',
};
