import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { take, filter, find } from 'lodash';
import { Language } from 'angular-l10n';

import { settings } from '../../../app-setting';
import { ComonService } from '../../comon.service';
import { StorageControllerService } from '../../../storage-controller.service';
@Component({
  selector: 'app-category-group',
  templateUrl: './category-groups.component.html',
  styleUrls: ['./category-groups.component.scss']
})
export class CategoryGroupComponent implements OnInit {
  @Language() lang: string;
  public showSubMenu = false;
  public mainCategoryList;
  public filteredCategoryList = [];
  public showCategoryIds = [];
  public subCategoryList;
  public selectedItem;
  public productStartCount;
  public productsEndCount;
  public productsShowMoreCount;
  menuOpenSubscribe$: any;
  @Input()
  set category(data) {
    if (data) {
      this.getMainCategoryList(data);
    }
  }
  constructor(
    private comonService: ComonService,
    private router: Router,
    private storage: StorageControllerService
  ) {
    this.productStartCount = settings.productInitialStartCount;
    this.productsEndCount = settings.productInitialEndCount;
    this.productsShowMoreCount = settings.productShowMoreCount;
  }

  ngOnInit() {
  }

  getMainCategoryList(data) {
    this.filteredCategoryList = data;
    if (this.filteredCategoryList.length > 8) {
      this.mainCategoryList = take(this.filteredCategoryList, 8);
    } else {
      this.mainCategoryList = this.filteredCategoryList;
    }
  }

  showSubCategory(category) {
    if (this.showCategoryIds.length === 0) {
      this.showCategoryIds.push({ 'categoryId': category.categoryGroupId });
      this.subCategoryList = category.category;
      this.showSubMenu = true;
      if (this.showSubMenu === true) {
        this.selectedItem = category.categoryGroupName;
      }
    } else {
      const filteredCategoryId = filter(this.showCategoryIds, { 'categoryId': category.categoryGroupId });
      if (filteredCategoryId.length === 0) {
        this.showCategoryIds = [];
        this.showCategoryIds.push({ 'categoryId': category.categoryGroupId });
        this.subCategoryList = category.category;
        this.showSubMenu = true;
        if (this.showSubMenu === true) {
          this.selectedItem = category.categoryGroupName;
        }
      } else {
        if (filteredCategoryId[0].categoryId === category.categoryGroupId) {
          this.showSubMenu = false;
          if (this.showSubMenu === false) {
            this.selectedItem = '';
          }
          this.showCategoryIds = [];
        } else {
          this.showCategoryIds = [];
          this.showCategoryIds.push({ 'categoryId': category.categoryGroupId });
          this.subCategoryList = category.category;
          this.showSubMenu = true;
          if (this.showSubMenu === true) {
            this.selectedItem = category.categoryGroupName;
          }
        }
      }
    }
  }

  showProductList(category: any, isCategory: boolean) {
    this.closeSubMenu(true);
    if (isCategory) {
      this.storage.set('isFromCategory', 'true');
      return this.router.navigate(['/product-list', category.categoryId]);
    } else {
      this.storage.remove('isFromCategory');
      return this.router.navigate(['/product-list', category.subCategoryId]);
    }
  }

  showMoreCategories() {
    this.router.navigate(['/categories']);
  }

  closeSubMenu(fromMoreCategory?) {
    this.showSubMenu = false;
    this.selectedItem = '';
    this.showCategoryIds = [];
    if (fromMoreCategory) {
      this.comonService.setCategoryMenuCloseStatus(true);
    }
  }
}
