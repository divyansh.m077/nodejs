import { Component, OnInit, Inject } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpEventType } from '@angular/common/http';

import { NotificationsService } from 'angular2-notifications';
import { TranslationService, Language } from 'angular-l10n';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';

import { settings, SNACKBAR_WARNING, USER_IMAGE_RULE } from '../../../app-setting';
import { API_STATUS_CODE } from '../../../app.constants';
import { UserService } from '../../../user/user.service';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss']
})
export class UploadImageComponent implements OnInit {
  @Language() lang: string;
  imageTypes: string[];
  imageData: any;
  imageResolution: number;
  croppedImageData: any;
  url: any;
  fileSize: number;
  acceptImageTypes = settings.acceptImageTypes;
  uploadedPercentage: number;
  progress: number;
  imageValidation = USER_IMAGE_RULE;
  constructor(private notify: NotificationsService, private translation: TranslationService,
              @Inject(MAT_DIALOG_DATA) public data: any, private sanitizer: DomSanitizer,
              private matDialogRef: MatDialogRef<UploadImageComponent>,
              private userService: UserService, private snackBar: MatSnackBar) {
    this.uploadedPercentage = 0;
    this.progress = 0;
  }

  ngOnInit() {
    this.imageTypes = settings.acceptImageTypes.split(',');
    this.fileSize = this.imageValidation.MAX_SIZE / 1000000;
  }

  onSelectFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      if (this.imageTypes.indexOf(event.target.files[0].type) >= 0) {
        if (event.target.files[0].size < this.imageValidation.MAX_SIZE) {
          const reader = new FileReader();
          reader.readAsDataURL(event.target.files[0]);
          reader.onload = ((data: any) => {
            const image = new Image();
            image.src = data.target.result;
            image.onload = () => {
              if ((image.height < this.imageValidation.MIN_RESOLUTION || image.width < this.imageValidation.MIN_RESOLUTION) ||
                 (image.height > this.imageValidation.MAX_RESOLUTION || image.width > this.imageValidation.MAX_RESOLUTION)) {
                  this.snackBar.open(this.translation.translate('proPicValidation'),
                  this.translation.translate('buttonClose'), SNACKBAR_WARNING);
              } else {
                this.imageData = event;
              }
            };
          });
        } else {
            const msg = `${this.translation.translate('IMAGE_SIZE_EXCEED')} ${this.fileSize} ${this.translation.translate('LABEL_MB')}`;
            this.snackBar.open(msg, this.translation.translate('buttonClose'), SNACKBAR_WARNING);
        }
      } else {
        this.snackBar.open(this.translation.translate('PLS_SELECT_ONLY_IMAGE'),
        this.translation.translate('buttonClose'), SNACKBAR_WARNING);
      }
    }
  }

  getImageData(imageData: any) {
    if (imageData) {
      this.croppedImageData = imageData.fileData;
      this.url = imageData.fileData;
    }
  }

  saveUserImages() {
    if (!this.data.showProgress) {
      this.matDialogRef.close(this.url);
    } else {
      this.updateUserImages(this.croppedImageData);
    }
  }

  updateUserImages(imageData) {
    this.userService.updateProfileImage({'userId': this.data.userId,
        'baseImage': imageData, 'fieldId': this.data.fieldCount}).subscribe((response: any) => {
      if (response) {
        this.processResponse(response);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.notify.error(this.translation.translate(error.error.message));
      }
    });
  }

  processResponse(event) {
    switch (event.type) {
      case HttpEventType.Sent:
        break;
      case HttpEventType.Response:
        this.matDialogRef.close(event.body);
        break;
      case 1: {
        if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)) {
          this.uploadedPercentage = event['loaded'] / event['total'] * 100;
          this.progress = Math.round(this.uploadedPercentage);
        }
        break;
      }
    }
  }
}
