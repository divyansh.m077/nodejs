import { Component, OnInit, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-before-install-prompt',
  templateUrl: './before-install-prompt.component.html',
  styleUrls: ['./before-install-prompt.component.scss']
})
export class BeforeInstallPromptComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<BeforeInstallPromptComponent>) { }

  ngOnInit() {

  }


  onCancel() {
    this.dialogRef.close();
  }

}
