import { Injectable, EventEmitter, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { forEach } from 'lodash';
import { MatDialog } from '@angular/material';
import { TranslationService } from 'angular-l10n';

import { COMMON_URL } from './common.url-config';
import { Subject } from 'rxjs';
import { AlertPopupComponent } from '../shared/alert-popup/alert-popup.component';
import { ALERT_SYSTEM_TITLE } from '../app.constants';
import { SharedService } from '../shared/shared.service';
import { StorageControllerService } from '../storage-controller.service';


@Injectable({
  providedIn: 'root'
})
export class ComonService {
  categoryList: any;
  public mobnav: any;
  actionSubscribe$: any;
  categoryMenuClose = new Subject();
  categoryChange = new Subject();
  pagePosition = new Subject();
  whislist = new Subject();
  scrollEmit = new Subject();
  showHideUserImage = new Subject();
  siteSettings: any;
  localCartData: any;
  constructor(
    private storage: StorageControllerService,
    private http: HttpClient,
    private dialog: MatDialog,
    private translation: TranslationService,
    private sharedService: SharedService
  ) {

  }

  getSiteSettingDetails() {
    this.siteSettings = this.sharedService.getSiteSettingDetails();
  }

  initializeCategoryList(data) {
    this.categoryList = data;
  }

  getCategoryDetails() {
    return this.categoryList;
  }

  setPagePosition(status) {
    this.pagePosition.next(status);
  }

  getPagePosition() {
    return this.pagePosition.asObservable();
  }

  setUserImage(status) {
    this.showHideUserImage.next(status);
  }

  getUserImage() {
    return this.showHideUserImage.asObservable();
  }

  setCategoryMenuCloseStatus(statusData) {
    this.categoryMenuClose.next(statusData);
  }

  setScrollEmitValues(scrollValue) {
    this.scrollEmit.next(scrollValue);
  }

  getscrollEmaiValues() {
    return this.scrollEmit.asObservable();
  }

  getCategoryMenuCloseStatus() {
    return this.categoryMenuClose.asObservable();
  }

  setCategoryData(data) {
    this.categoryChange.next(data);
  }

  getCategoryData() {
    return this.categoryChange.asObservable();
  }

  getAllCategories() {
    return this.http.get(COMMON_URL.CATEGORY_ALL);
  }

  getWishListCount() {
    return this.http.get(COMMON_URL.WISHLIST_COUNT );
  }

  getCartCount(userData) {
    return this.http.get(COMMON_URL.CART_COUNT, { params: userData });
  }

  getLangauges() {
    return this.http.get(COMMON_URL.LANGUAGE);
  }

  getSocailMediaList() {
    return this.http.get(COMMON_URL.SOCIAL_MEDIA);
  }

  findCartCompareSelection(dataList, from?) {
    const originalDataList = dataList;
    if (from) {
      const title = dataList.title;
      dataList = dataList.product;
      forEach(dataList, (data) => {
        data.title = title;
      });
    }
    forEach(dataList, (data) => {
      data.compareProduct = 0;
    });
    if (!this.storage.get('id')) {
      forEach(dataList, (data) => {
        data.cartUser = 0;
      });
      if (!!this.storage.get('cartStorage')) {
        const cartData = JSON.parse(this.storage.get('cartStorage'));
        forEach(dataList, (data) => {
          forEach(cartData, (cart) => {
            if (data.vendorId) {
              if (cart.productId === data.productId && cart.vendorId === data.vendorId) {
                data.cartUser = 1;
              }
            } else {
              if (cart.productId === data.productId) {
                data.cartUser = 1;
              }
            }
          });
        });
      }
    }
    if (!!this.storage.get('compareStorage')) {
      const compareData = JSON.parse(this.storage.get('compareStorage'));
      forEach(dataList, (data) => {
        forEach(compareData, (compare) => {
          if (data.vendorId) {
            if (compare.productId === data.productId && compare.vendorId === data.vendorId) {
              data.compareProduct = 1;
            }
          } else {
            if (compare.productId === data.productId) {
              data.compareProduct = 1;
            }
          }
        });
      });
    }
    if (from) {
      originalDataList.product = dataList;
      return originalDataList;
    } else {
      return dataList;
    }
  }

  getCmsList() {
    return this.http.get(COMMON_URL.CMS_LIST);
  }

  getCmsDetail(cmsId) {
    return this.http.get(COMMON_URL.CMS_DETAIL + cmsId);
  }

  getAdvertisementList() {
    return this.http.get(COMMON_URL.ADVERTISEMENT_URL);
  }

  addEmailSubscription(subscriptionData) {
    return this.http.put(COMMON_URL.SUBSCRIPTION_URL, subscriptionData);
  }

  logoutUserSession() {
    return this.http.get(COMMON_URL.LOGOUT);
  }

  setWhislistCount(whislistCount) {
    this.whislist.next(whislistCount);
  }

  getWhislistCount() {
      return this.whislist.asObservable();
  }

  showAlertForProductQuantity(response, productDetail) {
    let errorMessage;
    if (response.message === 'MINIMUM_QUANTITY_CHECK_FAILED') {
      errorMessage = productDetail.productName + ' - ' +
        this.translation.translate('alertQuantityMinimum') + productDetail.minimumQuantity;
    } else {
      errorMessage = productDetail.productName + ' - ' +
      this.translation.translate('alertQuantityMaximum') + productDetail.maximumQuantity +
      this.translation.translate('alertQuantityMaximumError');
    }
    this.openDialog([errorMessage]);
  }

  openDialog(message) {
   const dialogRef =  this.dialog.open(AlertPopupComponent, {
      data: {
        title: this.translation.translate('labelWarning'),
        message: message,
        action: ALERT_SYSTEM_TITLE.warning,
        buttonName: this.translation.translate('buttonOk')
      }
    });
    this.actionSubscribe$ = dialogRef.componentInstance.actionEvent.subscribe((data: any) => {
      if (data) {
        this.dialog.closeAll();
      }
    });
  }

  calculatePriceAmountWithTax(salesPrice, discountAmount, taxAmount, taxType, taxValue) {
    this.getSiteSettingDetails();
    let salesTax = 0;
    let discountTax = 0;
    let taxSalesPrice = 0;
    let taxDiscountPrice = 0;
    // display product price with tax value 1 means need to display product price with tax amount
    if (this.siteSettings && this.siteSettings.productSettings.displayProductPriceWithTax === 1) {
      discountTax = taxAmount;
      if (discountAmount) {
        salesTax = taxType === 1 ? taxValue : ((salesPrice * taxValue) / 100);
      } else {
        salesTax = taxAmount;
      }
    } else {
      discountTax = 0;
      salesTax = 0;
    }
    taxSalesPrice = salesPrice + salesTax;
    if (discountAmount === 0) {
      taxDiscountPrice = 0;
    } else {
      taxDiscountPrice = discountAmount + discountTax;
    }
    const productData = {
      taxDiscountPrice: taxDiscountPrice,
      taxSalesPrice: taxSalesPrice
    };
    return productData;
  }
}
