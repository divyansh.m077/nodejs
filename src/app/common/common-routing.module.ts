import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { CmsComponent } from './footer/cms/cms.component';
import { CategoriesComponent } from './header/category-groups/categories/categories.component';

const commonRoutes: Routes = [
  {
    path: 'page/:pageName',
    component: CmsComponent
  },
  {
    path: 'categories',
    component: CategoriesComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      commonRoutes
    )
  ],
  declarations: [],
  exports: [RouterModule],
})
export class CommonRoutingModule { }
