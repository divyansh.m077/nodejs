import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-image-crop',
  templateUrl: './image-crop.component.html',
  styleUrls: ['./image-crop.component.scss']
})
export class ImageCropComponent {
  // @Input() imageChangedEvent;
  @Input() resolution;
  @Output() croppedImageEvent = new EventEmitter<any>();
  croppedImage: any = '';
  showCropper: boolean;
  imageChanged;
  constructor() {
    this.showCropper = true;
  }
  @Input()
  set imageChangedEvent(data) {
    this.imageChanged = data;
  }

  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
      this.croppedImageEvent.emit({ 'fileData': this.croppedImage });
  }

  imageLoaded() {
    this.showCropper = true;
  }
  
}
