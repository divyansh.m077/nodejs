import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { Language, TranslationService } from 'angular-l10n';
import { API_STATUS_CODE } from '../../../../app.constants';
import * as moment from 'moment';
import { UserService } from "../../../../user/user.service";
import { ORDER_SUMMARY_IMAGES, SNACKBAR_ALERT, SNACKBAR_SUCCESS } from './../../../../app-setting';


@Component({
  selector: 'app-tracking-details',
  templateUrl: './tracking-details.component.html',
  styleUrls: ['./tracking-details.component.scss']
})
export class TrackingDetailsComponent implements OnInit {
  trackingDetails: any;
  @Language() lang: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private matDialogRef: MatDialogRef<TrackingDetailsComponent>,
    private snackBar: MatSnackBar,
    private translation: TranslationService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.getTrackingDetails();
  }

  // Format datetime
  formatDateTime(targetDate, formatString) {   
    return moment(targetDate).format(formatString);
  }

  // Get Status language Id
  // getStatusLangId(statusId){
  //   return ('OrderStatus' + statusId)
  // }

getTrackingDetails() {
    this.userService.getTrackingDetails(this.data.trackingId).subscribe((response) => {
      if (response) {
         this.trackingDetails = response;
      }
    }, (error) => {
         if (error.status === API_STATUS_CODE.badRequest || API_STATUS_CODE.conflict) {
        this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }



}
