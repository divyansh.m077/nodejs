import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { WINDOW } from '@ng-toolkit/universal';

import { Language } from 'angular-l10n';

import { UserService } from '../user.service';
import { ComonService } from '../../common/comon.service';
import { StorageControllerService } from '../../storage-controller.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  fileData: any;
  @Language() lang: string;
  public userName: any;
  userNameSubscribe$: any;
  pagePositionSubscribe$: any;
  userDetails;
  constructor(
    private storage: StorageControllerService,
    private userService: UserService,
    private comonService: ComonService,
    @Inject(WINDOW) private window: any,
  ) {
    this.getUserName();
    this.userNameSubscribe$ = this.userService.getUserName().subscribe((response: any) => {
      if (response === true) {
        this.getUserName();
      }
    });
    this.userName = this.storage.get('userName');
    this.pagePositionSubscribe$ = this.comonService.getPagePosition().subscribe((response: any) => {
      if (response === true) {
        this.window.scrollTo(0, 0);
      }
    });
  }

  ngOnInit() {
    this.userDetails = JSON.parse(this.storage.get('userDetails'));
    if (!this.userName && this.userDetails) {      
      const name = (this.userDetails.firstName + ' ' + this.userDetails.lastName);
      this.storage.set('userName', name);
      this.userName = name;
    }
  }

  getUserName() {
    this.userName = this.storage.get('userName');
  }

  ngOnDestroy() {
    if (this.pagePositionSubscribe$) {
      this.pagePositionSubscribe$.unsubscribe();
    }
  }
}
