import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-vendor-order-name',
  templateUrl: './vendor-name.component.html',
  styleUrls: ['./vendor-name.component.scss']
})
export class VendorNameComponent implements OnInit {
  vendor: any;
  @Input() set vendorName(val) {
    this.vendor = val;
  }
  constructor() { }

  ngOnInit() {

  }

}
