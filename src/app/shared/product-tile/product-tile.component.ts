import { Component, OnInit, Input, EventEmitter, Output , Inject} from '@angular/core';
import { Router } from '@angular/router';

import { forEach } from 'lodash';
import { Language, TranslationService } from 'angular-l10n';
import { MatSnackBar } from '@angular/material';

import { SharedService } from '../shared.service';
import { API_STATUS_CODE } from '../../app.constants';
import { HomeService } from '../../home/home.service';
import { ComonService } from '../../common/comon.service';

import { SNACKBAR_ALERT, SNACKBAR_SUCCESS, SNACKBAR_WARNING } from '../../app-setting';

import { NO_IMAGE_URL } from '../../../app/app.constants';
import { StorageControllerService } from '../../storage-controller.service';


@Component({
  selector: 'app-product-tile',
  templateUrl: './product-tile.component.html',
  styleUrls: ['./product-tile.component.scss']
})
export class ProductTileComponent implements OnInit {
  // @Input() productDetail;
  @Input() from;
  @Input() compareCount;
  @Output() productAction = new EventEmitter<any>();
  @Language() lang: string;
  toggleCardAnimation = false;
  toggleCardCompareIconAnimation = false;
  toggleCardWishListIconAnimation = false;
  toggleCardCartIconAnimation = false;
  compareStorage: any = [];
  emitStatus: boolean;
  siteSettingDetails: () => any;
  discountDetails: any;
  prodDetails: any;
  showDiscount: any;
  numberFormat: any;
  currencyCode: any;
  defaultNumber: any;
  currencyFormat: any;
  isBrowser = false;
  noImgUrl = NO_IMAGE_URL;
  productData: any;
  @Input()
  set productDetail(data) {
    if (data) {
     const productPrice = this.comonService.calculatePriceAmountWithTax(data.salesPrice, data.discountAmount, data.taxAmount,
      data.taxType, data.taxValue);
     data.taxDiscountPrice = productPrice.taxDiscountPrice;
     data.taxSalesPrice = productPrice.taxSalesPrice;
     this.productData = data;
    }
  }
  constructor(
    private storage: StorageControllerService,
    private router: Router,
    private sharedService: SharedService,
    private homeService: HomeService,
    private translation: TranslationService,
    private comonService: ComonService,
    private snackBar: MatSnackBar,
  ) {
      this.getSiteSettingProdDetails();
  }

  ngOnInit() {
    if (!this.storage.get('id')) {
      this.productData.favUser = 0;
    }
    this.prodDetails = this.sharedService.getSiteSettingDetails();
    this.currencyCode = this.prodDetails.currencyCode;
    this.emitStatus = true;
  }

  getSiteSettingProdDetails() {
    this.prodDetails = this.sharedService.getSiteSettingDetails();
    if (this.prodDetails) {
      this.showDiscount = this.prodDetails.productSettings.displayDiscountedPrice;
     }

    }

  toggleCardAnimationEffect(value: boolean) {
    this.toggleCardAnimation = value;
  }

  toggleCardIconAnimationEffect(icon, value: boolean) {
    switch (icon) {
      case 'compare':
        this.toggleCardCompareIconAnimation = value;
        break;
      case 'wishList':
        this.toggleCardWishListIconAnimation = value;
        break;
      default:
        this.toggleCardCartIconAnimation = value;
    }
  }

  moveToProductDetail(productId, vendorId) {
    if (vendorId) {
      this.router.navigate(['/product-detail', productId], { queryParams : {'id': vendorId}});
    } else {
      this.router.navigate(['/product-detail', productId]);
    }
  }

  getProductAction(productId: number, action: any, productData?: any) {
    const productObj = {
      'productId': productId, 'status': action === 'wishList' ? (productData.favUser === 1 ? 0 : 1) : 0,
      'action': action, 'productData': productData ? productData : ''
    };
    if (action === 'cart') {
      const isProductOutOfStock = productData.quantity - productData.soldOut;
      if (isProductOutOfStock === 0) {
        this.snackBar.open(this.translation.translate('alertProductOutOfStock'),
        this.translation.translate('buttonClose'), SNACKBAR_WARNING);
      } else {
        if (productData.isProductInAttribute === 1 || (Number(productData.minimumQuantity) > 1)) {
          if (productData.vendorId) {
           this.router.navigate(['/product-detail', productData.productId], { queryParams: { 'id' : productData.vendorId}} );
          } else {
            this.router.navigate(['/product-detail', productData.productId ]);
          }
        } else {
          if (productData.cartUser === 0) {
            this.addProductToCart(productObj);
          } else {
            this.router.navigate(['/cart-checkout']);
          }
        }
      }
    } else if (action === 'wishList') {
      if (!!this.storage.get('token')) {
        this.addProductToWishlist(productObj);
      } else {
        this.snackBar.open(this.translation.translate('alertProductWishlist'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
        this.router.navigate(['/signin']);
      }
    } else {
      if (productData.compareProduct === 1) {
        const storedItems = this.storage.get('compareStorage') ? JSON.parse(this.storage.get('compareStorage')) : [];
        storedItems.length > 1 ? this.router.navigate(['/product-comparision']) :
          this.snackBar.open(this.translation.translate('alertAddProductMore'),
          this.translation.translate('buttonClose'), SNACKBAR_WARNING);
      } else {
        const compareObj = {
          'productId': productId,
          'categoryId': productData.categoryId,
          'vendorId': productData.vendorId ? productData.vendorId : null
        };
        if (!!this.storage.get('compareStorage')) {
          const storedItems = this.storage.get('compareStorage') ? JSON.parse(this.storage.get('compareStorage')) : [];
          forEach(storedItems, (items) => {
            this.compareStorage.push(items);
          });
          if (this.compareStorage.length < this.compareCount) {
            if (this.compareStorage.length !== 0) {
              if (this.compareStorage[0].categoryId === compareObj.categoryId) {
                this.compareStorage.push(compareObj);
                this.emitStatus = true;
                this.snackBar.open(this.translation.translate('alertProductToCompare'),
                this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
              } else {
                this.emitStatus = false;
                this.snackBar.open(this.translation.translate('alertProductSameCategory'),
                this.translation.translate('buttonClose'), SNACKBAR_WARNING);
              }
            }
          } else {
            this.emitStatus = false;
            this.snackBar.open(this.translation.translate('alertProductComparisonLimit') + this.compareCount,
            this.translation.translate('buttonClose'), SNACKBAR_WARNING);
          }
        } else {
          this.emitStatus = true;
          this.compareStorage.push(compareObj);
          this.snackBar.open(this.translation.translate('alertProductToCompare'),
          this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        }
        if (this.emitStatus) {
          this.storage.set('compareStorage', JSON.stringify(this.compareStorage));
          productData.compareProduct = 1;
          this.homeService.setCompareCountChange(true);
          this.productAction.emit(productObj);
          if (!!this.storage.get('id')) {
            this.trackProductComparisonDetails(productId);
          }
        }
      }
    }
  }

  addProductToWishlist(productObj) {
    this.sharedService.addProductToWishList({
      'productId': productObj.productId, 
      'status': productObj.status, 
      'vendorId': productObj.productData.vendorId ? productObj.productData.vendorId : null,
      'vendorProductId' : productObj.productData.vendorProductId ? productObj.productData.vendorProductId : null
    })
      .subscribe((response: any) => {
        if (response) {
          this.homeService.setSignInStatus(true);
          this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
          this.productAction.emit(productObj);
        }
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        } else {
        this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
  }

  addProductToCart(productDetail) {
    const productPrice = productDetail.productData.discountAmount === 0 ? productDetail.productData.salesPrice :
                         productDetail.productData.discountAmount;
    const taxAmount = productDetail.productData.taxAmount;
    const quantity = (productDetail.minimumQuantity && Number(productDetail.minimumQuantity !== 0)) ?
                      productDetail.minimumQuantity : 1;
    if (!!this.storage.get('id')) {
      const cartObj = {
        'productCart': [{
          'productId': productDetail.productId,
          'price': productPrice,
          'cartQuantity': quantity,
          'subTotal': productPrice * quantity,
          'taxTotal': taxAmount * quantity,
          'productTax': taxAmount,
          'productOption': [],
          'mrpSalesPrice': productPrice,
          'vendorId': productDetail.productData.vendorId,
          'companyName': productDetail.productData.companyName,
          'vendorProductId': productDetail.productData.vendorProductId
        }]
      };
      this.sharedService.addCartDetails(cartObj).subscribe((response: any) => {
        if (response) {
          this.homeService.setCartCountChange(true);
          this.snackBar.open(this.translation.translate(response.data), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
          this.productAction.emit(productDetail);
        }
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        } else if (error.status === API_STATUS_CODE.preconditionFailed) {
          const productDetails = error.error.data[0];
          this.comonService.showAlertForProductQuantity(productDetails, productDetail.productData);
        }
      });
    } else {
      const cartStorage = [];
      const productObj = {
        'productId': productDetail.productId,
        'taxValue': productDetail.productData.taxValue,
        'taxAmount': productDetail.productData.taxAmount,
        'taxType': productDetail.productData.taxType,
        'discountAmount': productDetail.productData.discountAmount,
        'salesPrice': productDetail.productData.salesPrice,
        'productName': productDetail.productData.productName,
        'productImage': productDetail.productData.productImage,
        'productCode': productDetail.productData.productCode,
        'shippingValue': productDetail.productData.shippingValue,
        'freeShipping': productDetail.productData.freeShipping,
        'quantity': productDetail.productData.quantity,
        'minimumQuantity': productDetail.productData.minimumQuantity,
        'maximumQuantity': productDetail.productData.maximumQuantity,
        'lowStockName': productDetail.productData.lowStockName,
        'currencySymbol': productDetail.productData.currencySymbol,
        'cartQuantity': quantity,
        'subTotal': productPrice * quantity,
        'taxTotal': taxAmount * quantity,
        'productTax': taxAmount,
        'productOption': [],
        'soldOut': productDetail.productData.soldOut,
        'mrpSalesPrice': productPrice,
        'cartUser': 1,
        'vendorId': productDetail.productData.vendorId,
        'companyName': productDetail.productData.companyName,
        'vendorProductId': productDetail.productData.vendorProductId
      };
      cartStorage.push(productObj);
      if (!!this.storage.get('cartStorage')) {
        const storedItems = this.storage.get('cartStorage') ? JSON.parse(this.storage.get('cartStorage')) : [];
        forEach(storedItems, (items) => {
          cartStorage.push(items);
        });
      }
      this.storage.set('cartStorage', JSON.stringify(cartStorage));
      this.homeService.setCartCountChange(true);
      this.snackBar.open(this.translation.translate('alertProductCart'), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      this.productAction.emit(productDetail);
    }
  }

  trackProductComparisonDetails(productId) {
    this.sharedService.trackCompareProductDetails({
      'productId': [productId]
    }).subscribe((response: any) => {
      if (response) {

      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }
}
