import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule, MatButtonToggleModule} from '@angular/material';
import { TranslationModule } from 'angular-l10n';

import { ComonModule } from '../common/common.module';
import { SharedModule } from '../shared/shared.module';
import { BrandsComponent } from './brands.component';
import { BrandsRoutingModule } from './brands-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ComonModule,
    MatButtonModule,
    MatButtonToggleModule,
    SharedModule,
    TranslationModule,
    BrandsRoutingModule
  ],
  declarations: [
    BrandsComponent
  ],
  providers: []
})
export class BrandsModule { }
