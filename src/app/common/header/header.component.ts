import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, OnDestroy, Inject, PLATFORM_ID, NgZone } from '@angular/core';
import { Router, Event } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { DOCUMENT, Title } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
import { MatSnackBar, MatDialog } from '@angular/material';
import { LocaleService, Language, TranslationService } from 'angular-l10n';
import { uniqBy, forEach, find, differenceBy, cloneDeep, uniqWith, filter } from 'lodash';

import { settings, SEARCH_RULE } from './../../app-setting';
import { ComonService } from '../comon.service';
import { HomeService } from './../../home/home.service';
import { CartService } from './../../cart/cart.service';
import { API_STATUS_CODE } from '../../app.constants';
import { SharedService } from '../../shared/shared.service';
import { UserService } from '../../user/user.service';
import { SNACKBAR_ALERT, SNACKBAR_SUCCESS, SNACKBAR_WARNING } from '../../app-setting';
import { StorageControllerService } from '../../storage-controller.service';
import { environment } from '../../../environments/environment';
import { MembershipComponent } from '../../shared/membership/membership.component';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('SignInDropDown', [
      state('false', style({
        height: '0',
        opacity: '0'
      })),
      state('true', style({
        transform: '*',
        opacity: '1'
      })),
      transition('1 => 0', animate('400ms ease-in')),
      transition('0 => 1', animate('400ms ease-out'))
    ]),
    trigger('langDropDown', [
      state('false', style({
        height: '0'
      })),
      state('true', style({
        transform: '*',
        opacity: '1'
      })),
      transition('1 => 0', animate('350ms ease-in')),
      transition('0 => 1', animate('350ms ease-out'))
    ]),
    trigger('mobileHeader', [
      state('false', style({
        height: '126px',
      })),
      state('true', style({
        opacity: 1,
        height: '60px'
      })),
      transition('1 => 0', animate('0.2s ease-in')),
      transition('0 => 1', animate('0.5s ease-out'))
    ]),

  ]
})
export class HeaderComponent implements OnInit, OnDestroy {
  events: string[] = [];
  opened: boolean;
  signInOpen: boolean;
  langOpen: boolean;
  profileOpen: boolean;
  langOpenMobile: boolean;
  profileMobile: boolean;
  public show: boolean;
  public showChild: boolean;
  public showSubChild: boolean;
  public showSubChilds: boolean;
  public showDropdown: boolean;
  public countryImage: any;
  public countryName: any;
  public shortValue: any;
  @Language() lang: string;
  public userLogin: boolean;
  public scrollValuetrue: boolean;
  public scrollHeader: boolean;
  public loginUser;
  public language;
  public languages = [
    { value: 'en', image: 'assets/images/flag-united-kingdom.png', viewValue: 'English' }
  ];
  collapse = true;
  public showSubMenu = false;
  public selectedItem;
  public showCategoryIds = [];
  public isHome = true;
  public mainCategoryList;
  public cartPopup = false;
  public categoryList = [];
  public subCategoryList;
  public productList;
  public wishListCount = 0;
  public cartCount = 0;
  public compareCount = 0;
  public adminUrl = settings.adminUrl;
  public hideMenu = false;
  public signInSubscribe$: any;
  public cartCountSubscribe$: any;
  public compareCountSubscribe$: any;
  searchName: any = '';
  categoryId: string;
  searchRules: any = SEARCH_RULE;
  siteSettingDetails: any;
  companyLogo: string;
  isImageLogo: boolean;
  userNameSubscribe$: any;
  whislistCountSubscribe$: any;
  menuSubscribe$: any;
  scrollValue$: any;
  showAllCategories: boolean;
  profileImage: string;
  pagePosition: boolean;
  userDetails;
  menuName;
  becomeVendor;
  salesPortal;
  constructor(
    private storage: StorageControllerService,
    @Inject(PLATFORM_ID) private platform: Object,
    @Inject(WINDOW) private window: any,
    private router: Router,
    private comonService: ComonService,
    public locale: LocaleService,
    private homeService: HomeService,
    private cartService: CartService,
    private translation: TranslationService,
    @Inject(DOCUMENT) private _document: HTMLDocument,
    private sharedService: SharedService,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private zone: NgZone,
    private titleService: Title,
    @Inject('MENU_ITEMS') public menuItems,
    public dialog: MatDialog,
  ) {
    this.becomeVendor = environment.becomeaVendorURL;
    this.salesPortal = environment.salesPortalURL;
    this.pagePosition = false;
    this.getSiteSettingDetails();
    this.companyLogo = '';
    this.initializeGlobalVariables();
    this.routerChange();
    router.events.subscribe((event: Event) => {
      this.routerChange();
    });
    this.signInSubscribe$ = this.homeService.getSignInStatus().subscribe((response: any) => {
      if (response === true) {
        this.getUserName();
        this.getWishListCount();
      } else {
        this.wishListCount = 0;
      }
    });
    this.userNameSubscribe$ = this.userService.getUserName().subscribe((response: any) => {
      if (response === true) {
        this.getUserName();
      }
    });
    this.whislistCountSubscribe$ = this.comonService.getWhislistCount().subscribe((response: any) => {
      if (response === true) {

        this.getWishListCount();
      }
    });
    this.cartCountSubscribe$ = this.homeService.getCartCountChange().subscribe((response: any) => {
      if (response === true) {
        this.getCartCount();
        if (!this.storage.get('cartStorage')) {
          this.cartCount = 0;
        }
      } else {
        if (!!this.storage.get('cartStorage')) {
          const cartValue = this.getLocalCartCountValues();
          this.cartCount = cartValue.length;
        } else {
          this.cartCount = 0;
        }
      }
    });
    this.compareCountSubscribe$ = this.homeService.getCompareCountChange().subscribe((response: any) => {
      this.getCompareCount();
    });
    this.menuSubscribe$ = this.comonService.getCategoryMenuCloseStatus().subscribe((response: any) => {
      if (response) {
        this.showCategoryMenu();
      }
    });

    this.scrollValue$ = this.comonService.getscrollEmaiValues().subscribe((response: any) => {
      if (response) {
        this.scrollValuetrue = true;
        this.pagePosition = true;
      } else {
        this.scrollValuetrue = false;
      }
    });
  }

  getSiteSettingDetails() {
    this.siteSettingDetails = this.sharedService.getSiteSettingDetails();
    if (this.siteSettingDetails) {
      if (this.siteSettingDetails.storeLogo) {
        this.isImageLogo = true;
      } else {
        this.isImageLogo = false;
      }
      this._document.getElementById('appFavicon').setAttribute('href', this.siteSettingDetails.faviconLogo);
    }
  }

  initializeGlobalVariables() {
    this.signInOpen = false;
    this.langOpen = false;
    this.langOpenMobile = false;
    this.profileMobile = false;
    this.showAllCategories = false;
    this.show = true;
    this.showChild = false;
    this.showSubChild = false;
    this.showSubChilds = false;
    this.userLogin = false;
  }

  routerChange() {
    this.defaultActivateMenu(this.router.url);
    if (this.router.url !== '') {
      this.isHome = true;
      if (this.router.url.split('/')[1] === '') {
        this.isHome = true;
      } else {
        this.isHome = true;
      }
    }
    if (this.router.url.split('/')[1] !== 'page') {
      this.setTitle(this.siteSettingDetails.systemTitle);
    }
    this.getUserName();
    if (isPlatformBrowser(this.platform)) {
      this.zone.runOutsideAngular(() => {
        setTimeout(() => {
          if (this.router.url.split('/')[1] !== 'search-product') {
            // this.storage.remove('searchData');
            this.searchName = '';
            this.categoryId = null;
          }
        }, 1000);
      });
    }
  }

  setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }

  getUserName() {
    this.loginUser = this.storage.get('userName');
    const userImage = this.storage.get('userImage');
    if (userImage) {
      this.profileImage = userImage;
    } else {
      this.profileImage = 'assets/images/usersignout.png';
    }
    if (this.loginUser) {
      this.userLogin = true;
      if (this.storage.get('userDetails')) {
      this.userDetails = JSON.parse(this.storage.get('userDetails'));
    }
    } else {
      this.userLogin = false;
    }
  }

  showCategoryMenu() {
    if (this.router.url !== '' && this.router.url.split('/')[1] === '') {
      this.isHome = true;
    } else if (this.router.url !== '' && this.router.url.split('/')[1] !== '') {
      this.isHome = !this.isHome;
    }
  }

  ngOnInit() {
    this.getMainCategoryList();
    this.storage.set('language', (this.locale.getCurrentLanguage()));
    this.language = this.locale.getCurrentLanguage();
    const countryImg = find(this.languages, {'value': this.language});
    this.countryImage = countryImg.image;
    this.shortValue = countryImg.value;
    // if (this.language !== 'undefined' || this.countryImage !== 'undefined') {
    //  const storageLanguage = find(this.languages, { 'value': this.language, 'image' : this.countryImage });
    //  this.countryName = storageLanguage.viewValue;
    //  this.shortValue = storageLanguage.value;
    // } else {
    //   this.onChangeLanguage(this.languages[0]);
    // }
    if (typeof window !== 'undefined') {
      this.getCartCount();
      this.getCompareCount();
      if (!!this.storage.get('id')) {
        this.getWishListCount();
      }
    }
    this.getCompareCount();
    this.getCategoryList();
  }

  getCategoryList() {
    this.mainCategoryList = this.comonService.getCategoryDetails();
  }

  showSubCategory(category) {
    if (this.showCategoryIds.length === 0) {
      this.showCategoryIds.push({ 'categoryId': category.categoryGroupId });
      this.subCategoryList = category.category;
      this.showSubMenu = true;
      this.selectedItem = category.categoryGroupName;
    } else {
      const filteredCategoryId = filter(this.showCategoryIds, { 'categoryId': category.categoryGroupId });
      if (filteredCategoryId.length === 0) {
        this.showCategoryIds = [];
        this.showCategoryIds.push({ 'categoryId': category.categoryGroupId });
        this.subCategoryList = category.category;
        this.showSubMenu = true;
        this.selectedItem = category.categoryGroupName;
      } else {
        if (filteredCategoryId[0].categoryId === category.categoryGroupId) {
          this.showSubMenu = false;
          this.selectedItem = '';
          this.showCategoryIds = [];
        } else {
          this.showCategoryIds = [];
          this.showCategoryIds.push({ 'categoryId': category.categoryGroupId });
          this.subCategoryList = category.category;
          this.showSubMenu = true;
          this.selectedItem = category.categoryGroupName;
        }
      }
    }
  }

  getMainCategoryList() {
    this.comonService.getAllCategories().subscribe((response: any) => {
      if (response) {
        this.categoryList = uniqBy(response.data, 'categoryGroupId');
        this.mainCategoryList = cloneDeep(this.categoryList);
        this.comonService.initializeCategoryList(this.mainCategoryList);
        this.comonService.setCategoryData(true);
        this.categoryList.unshift({ categoryGroupId: null, categoryGroupName: 'All' });
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.message, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  showMoreCategories() {
    this.router.navigate(['/categories']);
  }

  getWishListCount() {
    if (typeof window !== 'undefined') {
      this.comonService.getWishListCount().subscribe((response: any) => {
        if (response) {
          this.wishListCount = response[0].count;
        } else {
          this.wishListCount = 0;
        }
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(error.error.message, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
    }
  }

  getCartCount() {
    if (!!this.storage.get('id')) {
      const cartObj = {
        'targetCurrency': 'USD'
      };
      this.comonService.getCartCount(cartObj).subscribe((response: any) => {
        if (response) {
          this.cartCount = response.count;
          if (response.count > 0) {
            this.homeService.getCartDetails(response);
          }
          if (!!this.storage.get('cartStorage')) {
            const cartValue = this.getLocalCartCountValues();
            const differCartValue = differenceBy(cartValue, response.product, 'productId');
            // const differCartValue = differenceBy(differenceBy(cartValue, response.product, 'productId'), 'vendorId');
            if (differCartValue) {
              this.addLocalCartValuesToDB(differCartValue);
            }
          }
        } else {
          this.cartCount = 0;
          if (!!this.storage.get('cartStorage')) {
            this.addTheLocalCartToDb();
          }
        }
      }, (error: any) => {
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(error.error.message, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
    } else {
      if (!!this.storage.get('cartStorage')) {
        const cartValue = this.getLocalCartCountValues();
        this.cartCount = cartValue.length;
      }
    }
  }

  addTheLocalCartToDb() {
    const cartValue = this.getLocalCartCountValues();
    this.addLocalCartValuesToDB(cartValue);
  }

  getLocalCartCountValues() {
    let productIds = JSON.parse(this.storage.get('cartStorage'));
    // productIds = uniqBy(productIds, 'productId');
    productIds = uniqWith(productIds, 'vendorId', 'productId');
    return productIds;
  }

  addLocalCartValuesToDB(localData: any) {
    forEach(localData, (data) => {
      data.price = data.discountAmount === 0 ? data.salesPrice : data.discountAmount;
    });
    const cartObj = {
      'productCart': localData
    };
    this.cartService.addCartDetails(cartObj).subscribe((data: any) => {
      const currentUrl = this.storage.get('currentUrl').split('/');
         if ((this.storage.get('previousUrl') === '/signin') && (currentUrl[1] === 'product-detail')) {
           this.comonService.localCartData = JSON.parse(this.storage.get('cartStorage'));
         }
      this.storage.remove('cartStorage');
      this.getCartCount();
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      } else if (error.status === API_STATUS_CODE.preconditionFailed) {
        const productDetails = error.error.data;
        const errorMessage = [];
        forEach(productDetails, (product) => {
          forEach(localData, (data) => {
            if (product.productId === data.productId) {
              if (product.message === 'MINIMUM_QUANTITY_CHECK_FAILED') {
                const existsCheck = find(errorMessage, data.productName + ' - ' +
                  this.translation.translate('alertQuantityMinimum') + data.minimumQuantity);
                if (!existsCheck) {
                  errorMessage.push(data.productName + ' - ' +
                    this.translation.translate('alertQuantityMinimum') + data.minimumQuantity);
                }
              } else {
                const existsCheck = find(errorMessage, errorMessage.push(data.productName + ' - ' +
                  this.translation.translate('alertQuantityMaximum') + data.maximumQuantity +
                  this.translation.translate('alertQuantityMaximumError')));
                if (!existsCheck) {
                  errorMessage.push(data.productName + ' - ' +
                    this.translation.translate('alertQuantityMaximum') + data.maximumQuantity +
                    this.translation.translate('alertQuantityMaximumError'));
                }
              }
            }
          });
        });
        this.comonService.openDialog(errorMessage);
        this.storage.remove('cartStorage');
      }
    });
  }

  getCompareCount() {
    if (!!this.storage.get('compareStorage')) {
      const storedItems = this.storage.get('compareStorage') ? JSON.parse(this.storage.get('compareStorage')) : [];
      this.compareCount = storedItems.length;
    } else {
      this.compareCount = 0;
    }
  }

  onChangeLanguage(language) {
    this.locale.setCurrentLanguage(language.value);
    this.countryImage = language.image;
    this.countryName = language.viewValue;
    this.shortValue = language.value;
    this.storage.set('language', language.value);
    this.language = this.storage.get('language');
  }

  moveToRespectivePages(page: any, data?: any) {
    this.scrollValuetrue = false;
    this.opened = false;
    switch (page) {
      case 'products':
        return this.router.navigate(['/products']);
      case 'product-list':
        return this.router.navigate(['/product-list', data]);
      case 'deals':
        return this.router.navigate(['/deals']);
      case 'home':
        return this.router.navigate(['/']);
      case 'soldout':
        return this.router.navigate(['/sold-out']);
      case 'blog':
        return this.router.navigate(['/blog']);
      case 'nearmap':
        return this.router.navigate(['/near-map']);
      case 'contactus':
        return this.router.navigate(['/contact-us']);
      case 'register':
        return this.router.navigate(['/register']);
      case 'signin':
        return this.router.navigate(['/signin']);
      case 'wishlist':
        if (!this.storage.get('token')) {
          this.snackBar.open(this.translation.translate('alertWishlistLogin'), this.translation.translate('buttonClose'), SNACKBAR_WARNING);
        }
        this.movingPagePosition();
        return this.router.navigate(['/profile/wishlist']);
      case 'profile':
        return this.router.navigate(['/profile']);
      case 'dashboard':
        return this.router.navigate(['/profile/dashboard']);
      case 'wallet':
        return this.router.navigate(['/profile/my-wallet']);
      case 'info':
        return this.router.navigate(['/profile/info']);
      case 'changePassword':
        return this.router.navigate(['/profile/change-password']);
      case 'address':
        return this.router.navigate(['/profile/address']);
      case 'notification':
        return this.router.navigate(['/profile/notification']);
      case 'orders':
        return this.router.navigate(['/profile/orders']);
      case 'rating':
        return this.router.navigate(['/profile/rating']);
      case 'complaint':
        return this.router.navigate(['/profile/order-complaint']);
      case 'cart':
        this.movingPagePosition();
        return this.router.navigate(['/cart-checkout']);
      case 'compare':
        if (!!this.storage.get('compareStorage')) {
          this.movingPagePosition();
          return this.router.navigate(['/product-comparision']);
        } else {
          this.snackBar.open(this.translation.translate('alertProductCompare'),
            this.translation.translate('buttonClose'), SNACKBAR_WARNING);
        }
    }
  }

  movingPagePosition() {
    if (this.pagePosition) {
      this.comonService.setPagePosition(true);
    }
  }

  logoutSession() {
    this.signInOpen = false;
    this.comonService.logoutUserSession().subscribe((response: any) => {
      if (response) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
    this.compareCount = 0;
    this.clearStorageDetails();
  }

  clearStorageDetails() {
    this.sharedService.clearUserInfoFromStorage();
    const splitUrl = this.storage.get('currentUrl').split('/');
    var currentUrl = splitUrl[1] === 'profile' || splitUrl[1] === 'order-summary' ||
      splitUrl[1] === 'cart-checkout' ? '/' : decodeURIComponent(this.storage.get('currentUrl'));
    this.routerChange();
    this.homeService.setSignInStatus(false);
    this.homeService.setCartCountChange(false);
    currentUrl = (currentUrl !== 'null' &&  currentUrl !== null) ? currentUrl : '/';
    this.router.navigateByUrl(currentUrl);
  }


  toggle() {
    this.show = !this.show;
  }

  showChildData(category?) {
    this.hideMenu = !this.hideMenu;
    this.showChild = !this.showChild;
    if (category) {
      this.subCategoryList = category.category;
    }
  }

  showSubChildData(category?) {
    this.showSubChild = !this.showSubChild;
    if (category) {
      this.productList = category.subCategory;
    }
  }

  showSubChildData1() {
    this.showSubChilds = !this.showSubChilds;
  }

  getSearchProducts(category, searchName) {
    searchName = searchName.trim();
    const searchData = {
      'categoryGroupId': category !== undefined ? category : null,
      'searchText': searchName === null ? '' : searchName
    };
    this.storage.remove('searchData');
    this.storage.set('searchData', JSON.stringify(searchData));
    this.homeService.setSearchStatus(true);
    this.router.navigate(['/search-product']);
  }

  goToAdmin() {
    const windowObject = this.window;
    windowObject.open(this.adminUrl, '_blank');
  }

  ngOnDestroy() {
    if (this.signInSubscribe$) {
      this.signInSubscribe$.unsubscribe();
    }
    if (this.cartCountSubscribe$) {
      this.cartCountSubscribe$.unsubscribe();
    }
    if (this.whislistCountSubscribe$) {
      this.whislistCountSubscribe$.unsubscribe();
    }
    if (this.menuSubscribe$) {
      this.menuSubscribe$.unsubscribe();
    }
    if (this.userNameSubscribe$) {
      this.userNameSubscribe$.unsubscribe();
    }
    if (this.cartCountSubscribe$) {
      this.cartCountSubscribe$.unsubscribe();
    }
    if (this.scrollValue$) {
      this.scrollValue$.unsubscribe();
    }
  }

  // signin Popover
  toggleSignInPopover() {
    this.signInOpen = !this.signInOpen;
  }
  toggleLang() {
    this.langOpen = !this.langOpen;
  }
  toggleprofile() {
    this.profileOpen = !this.profileOpen;
  }

  toggleLangMobile() {
    this.langOpenMobile = !this.langOpenMobile;
  }
  toggleProfileMobile() {
    this.profileMobile = !this.profileMobile;
  }
  toggleShowAllCategories() {
    this.showAllCategories = !this.showAllCategories;
  }

  showProductList(category: any, isCategory: boolean) {
    if (isCategory) {
      this.storage.set('isFromCategory', 'true');
      return this.router.navigate(['/product-list', category.categoryId]);
    } else {
      this.storage.remove('isFromCategory');
      return this.router.navigate(['/product-list', category.subCategoryId]);
    }
  }
  clearSearchString() {
    this.searchName = '';
  }

  activeMenu(menu) {
    this.menuName = menu.functionText;
  }

  defaultActivateMenu(menu) {
    switch (menu) {
      case '/': {
        this.menuName = 'home';
        break;
      }
      case '/products': {
        this.menuName = 'products';
        break;
      }
      case '/deals': {
        this.menuName = 'deals';
        break;
      }
      case '/sold-out': {
        this.menuName = 'soldout';
        break;
      }
      case '/near-map': {
        this.menuName = 'nearmap';
        break;
      }
      case '/blog': {
        this.menuName = 'blog';
        break;
      }
      case '/contact-us': {
        this.menuName = 'contactus';
        break;
      }
      default: {
        this.menuName = menu.search('/product-detail') >= 0 ? 'products' : '';
        break;
     }
    }
    return this.menuName;
  }
  showMembershipPlan() {
    this.dialog.open(MembershipComponent);
  }
}
