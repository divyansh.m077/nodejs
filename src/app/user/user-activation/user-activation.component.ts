import { Component, OnInit, Inject } from '@angular/core';
import { Language, TranslationService } from 'angular-l10n';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { API_STATUS_CODE } from '../../app.constants';
import { SNACKBAR_ALERT, SNACKBAR_SUCCESS } from '../../app-setting';
import { MatSnackBar } from '@angular/material';
import { StorageControllerService } from '../../storage-controller.service';
@Component({
  selector: 'app-user-activation',
  templateUrl: './user-activation.component.html',
  styleUrls: ['./user-activation.component.scss']
})
export class UserActivationComponent implements OnInit {

  @Language() lang: string;
  userEmail;
  urlParam: string;
  userToken: string;
  verified: boolean;
  alreadyVerified: boolean;
  notVerified: boolean;
  isFromSignIn: boolean;
  constructor(private router: Router,
              private route: ActivatedRoute,
              private userService: UserService,
              private snackBar: MatSnackBar,
              private translation: TranslationService,
              private storage: StorageControllerService,
            ) {
    const email = this.route.snapshot.queryParams['userEmail'];
    const token = this.route.snapshot.queryParams['token'];
    this.isFromSignIn = false;
    if (email && token) {
      this.isFromSignIn = false;
      this.userToken = token;
      this.userEmail = email;
    } else {
      this.isFromSignIn = true;
      this.userEmail = email;
    }

  }

  ngOnInit() {
    this.storage.set('previousUrl', '');
    if (!this.isFromSignIn) {
      this.getVerified(this.userToken);
    }
  }

  getVerified(verifyToken) {
    this.userService.verifyEmail(verifyToken).subscribe((response: any) => {
      if (response) {
        this.verified = true;
      }
    }, (error: any) => {
      console.log(error);
      if (error.status === API_STATUS_CODE.badRequest) {
        this.storage.set('previousUrl', '');
        if (error.error && error.error.message === 'VERIFICATION_FAILED') {
           this.notVerified = true;
        }
        if (error.error && error.error.message === 'ALREADY_VERIFIED') {
          this.alreadyVerified = true;
        }
      } else {
        if (error.error) {
          this.snackBar.open(error.error.message, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      }
    });
  }

  onSignIn() {
    this.router.navigate(['signin/']);
  }

  onResend() {
    this.userService.resendEmail({userEmail: this.userEmail}).subscribe((response: any) => {
      if (response && response.message) {
        this.snackBar.open(this.translation.translate(response.message), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest && error.error) {
        this.snackBar.open(this.translation.translate(error.error.message), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }
}
