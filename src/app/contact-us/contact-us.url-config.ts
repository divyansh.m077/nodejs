import { BASE_URL } from '../common/common.url-config';

export const CONTACT_US_URL =  {
  ENQUIRY: BASE_URL + 'enquiry',
  CONTACT_DETAILS: BASE_URL + 'store-contact'
};
