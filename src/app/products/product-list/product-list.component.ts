import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { MatSnackBar } from '@angular/material';
import { concat, forEach, find, uniqBy } from 'lodash';
import { Language, TranslationService } from 'angular-l10n';

import { ProductService } from '../products.service';
import { HomeService } from '../../home/home.service';
import { ComonService } from './../../common/comon.service';
import { settings } from '../../app-setting';
import { API_STATUS_CODE } from '../../app.constants';
import { SNACKBAR_ALERT } from '../../app-setting';
import { SharedService } from '../../shared/shared.service';
import { StorageControllerService } from '../../storage-controller.service';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit, OnDestroy {
  @Language() lang: string;
  public productList = [];
  public productFrom = 'product';
  public tabOption = 'latest';
  public productsStartCount;
  public productsEndCount;
  public productsShowMoreCount;
  public isLoading: boolean;
  productTapOptionLatest;
  productTapOptionLowest;
  productTapOptionHighest;
  productTapOptionPopularity;
  id;
  details;
  displayName = false;
  public isFromBrand;
  public tabOptionValue;
  public subCategoryName: any;
  public totalProducts: any;
  public productSubscribe$: any;
  filterList: any;
  filterShow: boolean;
  public compareCount: any = 0;
  siteSettingDetails: any;
  constructor(
    private storage: StorageControllerService,
    private productService: ProductService,
    private router: Router,
    private route: ActivatedRoute,
    private homeService: HomeService,
    private comonService: ComonService,
    private snackBar: MatSnackBar,
    private translation: TranslationService,
    private sharedService: SharedService
  ) {
    this.filterShow = false;
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    this.isFromBrand = this.route.snapshot.queryParams['isFromBrand'];
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
    this.productSubscribe$ = this.homeService.getSignInStatus().subscribe((response: any) => {
      if (response === false) {
        this.productsStartCount = 0;
        this.productList = [];
        this.productTapOptionLatest = settings.productTapOptionLatest;
        this.tabOption = 'latest';
        this.getAllProductsList(this.tabOption, this.productTapOptionLatest);
      }
    });
    this.productsStartCount = settings.productInitialStartCount;
    this.productsEndCount = settings.productInitialEndCount;
    this.productsShowMoreCount = settings.productShowMoreCount;
    this.productTapOptionLatest = settings.productTapOptionLatest;
    this.productTapOptionLowest = settings.productTapOptionLowest;
    this.productTapOptionHighest = settings.productTapOptionHighest;
    this.productTapOptionPopularity = settings.productTapOptionPopularity;
    this.tabOptionValue = this.productTapOptionLatest;
  }

  ngOnInit() {
    this.getCompareCount();
    this.getAllProductsList(this.tabOption, this.productTapOptionLatest);
    if (!this.isFromBrand) {
      // this.getFilterList();
    }
  }

  getCompareCount() {
    this.siteSettingDetails = this.sharedService.getSiteSettingDetails();
    if (this.siteSettingDetails) {
      this.compareCount = this.siteSettingDetails.compareLimit;
    } else {
      this.compareCount = 0;
    }
  }

  getAllProductsList(option, tabValue) {
    if (this.tabOptionValue !== tabValue) {
      this.productsStartCount = settings.productInitialStartCount;
      this.productsEndCount = settings.productInitialEndCount;
    }
    const productObj = {
      'subCategoryId': this.id,
      'categoryId': this.id,
      'startLimit': this.productsStartCount,
      'endLimit': this.productsEndCount,
      'sortStatus': tabValue,
      'targetCurrency': 'USD'
    };
    if (option !== this.tabOption) {
      this.productList = [];
    }
    this.tabOption = option;
    this.tabOptionValue = tabValue;
    this.isLoading = true;
    if (this.storage.get('isFromCategory')) {
      this.displayName = false;
      this.productService.getProductListByCategory(productObj).subscribe((response: any) => {
        this.processResponse(response);
      }, (error: any) => {
        this.isLoading = false;
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
    } else if (this.isFromBrand) {
      const productObject = {
        'subCategoryId': this.id,
        'brandId': this.id,
        'startLimit': this.productsStartCount,
        'endLimit': this.productsEndCount,
        'sortStatus': tabValue,
        'targetCurrency': 'USD'
      };
      this.productService.getProductListBasedBrand(productObject).subscribe((response: any) => {
        this.processResponse(response);
      }, (error: any) => {
        this.isLoading = false;
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
    } else {
      this.productService.getProductList(productObj).subscribe((response: any) => {
        this.displayName = true;
        this.processResponse(response);
      }, (error: any) => {
        this.isLoading = false;
        if (error.status === API_STATUS_CODE.badRequest) {
          this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
        }
      });
    }
  }

  processResponse(response: any) {
    this.isLoading = false;
    if (response) {
      this.details = response;
      this.subCategoryName = response.subCategory;
      this.totalProducts = response.length;
      this.productList = concat(this.productList, response.data);
      this.productList = uniqBy(this.productList, 'productId');
    } else {
      this.productList = [];
    }
    this.cartCompareDetailsForProduct();
  }

  getFilterList() {
    this.isLoading = true;
    this.productService.getFilterList(this.id).subscribe((response: any) => {
      this.isLoading = false;
      if (response) {
        this.filterList = response;
        this.filterList.Price[0].MinSalesPrice = parseInt(this.filterList.Price[0].MinSalesPrice, 0);
        this.filterList.Price[0].MaxSalesPrice = parseInt(this.filterList.Price[0].MaxSalesPrice, 0);
        forEach(this.filterList, (list) => {
          forEach(list, (data) => {
            data.expand = false;
          });
        });
        this.filterShow = this.filterList.data.length > 1 ? true : false;
      }
    });
  }

  cartCompareDetailsForProduct() {
    this.productList = this.comonService.findCartCompareSelection(this.productList);
  }

  showMoreProducts() {
    this.productsStartCount += this.productsShowMoreCount;
    this.getAllProductsList(this.tabOption, this.tabOptionValue);
  }

  getProductActionDetails($event: any) {
    if ($event.action === 'wishList') {
      const productData = find(this.productList, { 'productId': $event.productId });
      productData.favUser = $event.status === 1 ? 1 : 0;
    } else if ($event.action === 'cart') {
      const productData = find(this.productList, { 'productId': $event.productId });
      productData.cartUser = 1;
    } else {
      const productData = find(this.productList, { 'productId': $event.productId });
      productData.compareProduct = 1;
    }
  }

  getFilteredProductList(event: any) {
    if (event === true) {
      this.productsStartCount = 0;
      this.productList = [];
      this.getAllProductsList(this.tabOption, this.productTapOptionLatest);
    } else {
      this.productList = [];
      this.subCategoryName = event.subcategory;
      this.totalProducts = event.totalProduct;
      this.productList = concat(this.productList, event.products);
      this.productList = uniqBy(this.productList, 'productId');
      this.cartCompareDetailsForProduct();
    }
  }
  moveToRespectivePages(page: string, id: number) {
    switch (page) {
      case 'product-list':
        this.storage.set('isFromCategory', 'true');
        return this.router.navigate(['/product-list', this.details.categoryId]);
      case 'home':
        return this.router.navigate(['/']);
    }
  }
  ngOnDestroy() {
    if (this.productSubscribe$) {
      this.productSubscribe$.unsubscribe();
    }
  }
}
