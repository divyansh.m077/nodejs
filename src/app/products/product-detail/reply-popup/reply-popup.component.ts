import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Language, TranslationService } from 'angular-l10n';
import { NotificationsService } from 'angular2-notifications';

import { ProductService } from '../../products.service';
@Component({
  selector: 'app-reply-popup',
  templateUrl: './reply-popup.component.html',
  styleUrls: ['./reply-popup.component.scss']
})
export class ReplyPopupComponent implements OnInit {
  vendorReviewList;
  pluralMapping = {
    'replyCount': {
      '=1': 'LABEL_REPLY',
      'other': 'LABEL_REPLIES'
    }
  };
  constructor(private productService: ProductService,
    private translation: TranslationService,
    private notify: NotificationsService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private matDialogRef: MatDialogRef<ReplyPopupComponent>) { }

  ngOnInit() {
    if (this.data) {
      this.getReplyMessage(this.data);
    }
  }

  getReplyMessage(reviewId) {
    if (reviewId) {
      this.productService.getReplyMessage(reviewId).subscribe((response: any) => {
        this.vendorReviewList = response.data;
      }, (error: any) => {
        this.showError(error);
      });
    }
  }

  closePopup() {
    this.matDialogRef.close();
  }

  showError(error) {
    this.notify.error(this.translation.translate(error.error.message));
  }

}
