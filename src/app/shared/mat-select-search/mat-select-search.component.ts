import { Component, OnInit, OnDestroy, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { MatSelect } from '@angular/material';
import { take, takeUntil } from 'rxjs/operators';
import { find, remove, uniqBy, orderBy } from 'lodash';
import { Language, TranslationService } from 'angular-l10n';
import { MatSnackBar } from '@angular/material';
import { API_STATUS_CODE } from '../../app.constants';
import { SNACKBAR_ALERT } from '../../app-setting';
import { SharedService } from '../../shared/shared.service';

@Component({
  selector: 'app-mat-select-search',
  templateUrl: './mat-select-search.component.html',
  styleUrls: ['./mat-select-search.component.scss']
})
export class MatSelectSearchComponent implements OnInit, OnDestroy {

  public filteredList: any;
  @Language() lang: string;
  dropdownList: any;
  @Input() label;
  @Input() displayField;
  @Input() valueField;
  @Input() requiredMsg;
  @Input() myFormControl;
  @Input() searchPlaceholder;
  @Input() noResultsLabel;
  @Input() multiple;
  @Input() bindObject;
  @Input() provideAllOption;
  @Input() emptyOption;
  @Input() isRequired;
  @Input() isInactive;
  @Input() serverSide;
  @Input() dynamicSearchId;
  @Input() cityId;
  @Output() selectionChange = new EventEmitter<any>();
  @Input()
  set list(data: any) {
    if (data) {
      this.filteredList = [];
      this.dropdownList = [];
      this.filteredList = data;
      this.dropdownList = data;
      this.sortingList();
      if (this.provideAllOption) {
        // this.setAllOptions();
      }
    }
  }
  public searchBoxCtrl: FormControl = new FormControl();
  public dropdownCtrl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  allOption;
  @ViewChild('matSelect') matSelect: MatSelect;
  disableItems: boolean;
  searchSelect: boolean;
  constructor(
    private snackBar: MatSnackBar,
    private sharedRef: SharedService,
    private translation: TranslationService,
  ) {
    this.dropdownList = [];
    this.filteredList = [];
    this.disableItems = false;
    this.searchSelect = false;
  }

  ngOnInit() {
    this.dropdownCtrl.reset();
    this.filteredList = this.dropdownList ? this.dropdownList.slice() : this.dropdownList;
    if (this.filteredList) {
      this.sortingList();
    }
    if (this.provideAllOption) {
      this.disableItems = this.myFormControl ? (this.myFormControl.value[0] === 0 ? true : false) : false;
    }
    this.searchBoxCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterList();
      });
  }

  sortingList() {
    if (this.displayField) {
      this.filteredList = orderBy(this.dropdownList, [this.displayField ? filter => filter[this.displayField].toLowerCase() :
              filter => filter.toLowerCase()], ['asc']);
    } else {
      this.filteredList = orderBy(this.dropdownList, [this.displayField ? filter => filter[this.displayField].toLowerCase() :
              filter => filter.toLowerCase()], ['asc']);
    }
  }

  setAllOptions() {
    this.allOption = {};
    this.allOption[this.valueField] = 0;
    this.allOption[this.displayField] = 'All';
    if (this.filteredList) {
      this.filteredList.unshift(this.allOption);
      remove(this.filteredList, {'undefined': 'All'});
      const findId = find(this.filteredList, {'id' : 0});
      if (findId) {
        this.filteredList = uniqBy(this.filteredList, 'id');
      }
    }
  }

  private filterList() {
    if (!this.dropdownList || !this.searchBoxCtrl) {
      return;
    }
    // get the search keyword
    let search = this.searchBoxCtrl.value;
    if (!search) {
      this.searchSelect = false;
      this.filteredList = this.dropdownList.slice();
      this.sortingList();
      return;
    } else {
      this.searchSelect = true;
      search = search.toLowerCase();
    }
    // filter the list
    this.filteredList =
      this.dropdownList.filter(this.displayField ? item => item[this.displayField].toLowerCase().indexOf(search) > -1 :
                item => item.toLowerCase().indexOf(search) > -1);
     this.sortSearchedResult(this.filteredList);
  }

  sortSearchedResult(searchList) {
    this.filteredList = [];
    if (this.displayField) {
      this.filteredList = orderBy(searchList, [this.displayField ? filter => filter[this.displayField].toLowerCase() :
              filter => filter.toLowerCase()], ['asc']);
    } else {
      this.filteredList = orderBy(searchList, [this.displayField ? filter => filter[this.displayField].toLowerCase() :
              filter => filter.toLowerCase()], ['asc']);
    }
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  onValueChange(event) {
    if (this.provideAllOption &&  this.multiple) {
      let isAllExists;
      if (this.bindObject === true) {
        isAllExists = find(event.value, {id: 0});
      } else {
        isAllExists = event.value.indexOf(0) >= 0;
      }
      if (isAllExists) {
        this.disableItems = true;
        this.myFormControl.setValue([0]);
      } else {
        this.disableItems = false;
      }
    } else {
       this.disableItems = false;
    }
    if (this.selectionChange && typeof this.selectionChange.emit === 'function') {
      this.selectionChange.emit(event);
    }
  }

  // Server side Search for Cities
  serverSearch(){
    const reqObj = {
      "districtId": this.dynamicSearchId, 
      "selectedId": this.cityId, 
      "searchString": this.searchBoxCtrl.value
    };
    this.sharedRef.getCityTown(reqObj).subscribe((response: any) => {
      const responseList = response && response.data ? response.data : [];
      this.filteredList = responseList;   
      this.list = responseList;   
    }, (errorResponse: any) => {
      this.filteredList = [];
      this.list = [];
      if (errorResponse.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(errorResponse.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }


}
