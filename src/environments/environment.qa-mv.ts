export const environment = {
    production: true,
    serverUrl: 'https://qa.purchasecommerce.com:6443/',
    assetsUrl: `https://qa.purchasecommerce.com/`,
    becomeaVendorURL: '',
    SSRPort: 8081,
    cookieConfig: {
      path: '/',
      domain: 'qa.purchasecommerce.com',
      expires: '01.01.2021',
      secure: true,
      httpOnly: false
    }
};
