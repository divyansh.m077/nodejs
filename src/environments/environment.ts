// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  //  serverUrl: 'http://staging.purchasecommerce.com:7500/' // demo url
  // serverUrl: 'http://staging.purchasecommerce.com:9000/', // Dev url
  serverUrl: 'https://ichhapurti.purchasecommerce.com:2443/', // Dev url
  assetsUrl: `http://localhost:4200/`,
  becomeaVendorURL: 'https://ichhapurti.purchasecommerce.com/become-vendor/signup',
  salesPortalURL: 'https://sales-ichhapurti.purchasecommerce.com/',
  SSRPort: 8080,
  cookieConfig: {
    path: '/',
    domain: 'localhost',
    expires: '01.01.2021',
    secure: false,
    httpOnly: false
  }
};

