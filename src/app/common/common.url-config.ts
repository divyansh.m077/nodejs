import { environment } from '../../environments/environment';

export const BASE_URL = environment.serverUrl;

export const COMMON_URL = {
    CATEGORY: BASE_URL + 'get-category-group-menu-all',
    CATEGORY_ALL: BASE_URL + 'get-category-group-menu-group',
    WISHLIST_COUNT: BASE_URL + 'favourite-product/',
    CART_COUNT: BASE_URL + 'cart/cart-count',
    LANGUAGE: BASE_URL + 'get-language',
    CMS_LIST: BASE_URL + 'cms',
    CMS_DETAIL: BASE_URL + 'cms/',
    SOCIAL_MEDIA: BASE_URL + 'get-social-media',
    SITE_SETTING_URL: BASE_URL + 'get-site-settings',
    ADVERTISEMENT_URL: BASE_URL + 'get-advertisement',
    SUBSCRIPTION_URL: BASE_URL + 'email-subscription',
    LOGOUT: BASE_URL + 'user-logout'
};
