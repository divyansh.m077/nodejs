import {Component, OnInit, Inject} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

import { Language, TranslationService } from 'angular-l10n';

import {UserService} from '../user.service';
import { API_STATUS_CODE } from '../../app.constants';

import { SNACKBAR_ALERT, SNACKBAR_SUCCESS } from '../../app-setting';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  notificationForm: FormGroup;
  preferenceList: any;
  @Language() lang: string;
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private translation: TranslationService
  ) {
    this.formInitialize();
  }

  ngOnInit() {
    this.getNotificationDetails();
  }

  getNotificationDetails() {
    this.userService.getNotificationPreference().subscribe((response: any) => {
      if (response) {
        this.preferenceList = response.data[0];
        this.formInitialize(this.preferenceList);
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  formInitialize(details?) {
    if (!details) {
      this.notificationForm = this.fb.group({
        'newsletterSubscription': [, [Validators.required]],
        'marketingUpdate': ['', [Validators.required]]
      });
    } else {
      this.notificationForm = this.fb.group({
        'newsletterSubscription': [details.newsLetterSubscription, [Validators.required]],
        'marketingUpdate': [details.marketingUpdate, [Validators.required]]
      });
    }
  }

  onSubmit() {
    const jsonObject = {
      'newsLetterSubscription': this.notificationForm.get('newsletterSubscription').value,
      'marketingUpdate': this.notificationForm.get('marketingUpdate').value,
    };
    this.userService.saveNotificationPreference(jsonObject).subscribe((response: any) => {
      if (response) {
        this.snackBar.open(this.translation.translate(response.data), this.translation.translate('buttonClose'), SNACKBAR_SUCCESS);
        this.getNotificationDetails();
      }
    }, (error: any) => {
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(this.translation.translate(error.error.data), this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

}
