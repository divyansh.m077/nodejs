import { Component, OnInit, OnDestroy , Inject} from '@angular/core';

import { MatSnackBar } from '@angular/material';
import { concat, forEach, uniqBy, find } from 'lodash';
import { Language, TranslationService } from 'angular-l10n';

import { ProductService } from './../products.service';
import { SEARCH_PRODUCT_LIST_SETTINGS } from './../../app-setting';
import { API_STATUS_CODE } from '../../app.constants';
import { HomeService } from './../../home/home.service';
import { ComonService } from './../../common/comon.service';
import { SNACKBAR_ALERT } from '../../app-setting';
import { SharedService } from '../../shared/shared.service';
import { StorageControllerService } from '../../storage-controller.service';

@Component({
  selector: 'app-search-product',
  templateUrl: './search-product.component.html',
  styleUrls: ['./search-product.component.scss']
})
export class SearchProductComponent implements OnInit, OnDestroy {
  @Language() lang: string;
  public productFrom = 'product';
  public productList: any = [];
  public productsCount: any = SEARCH_PRODUCT_LIST_SETTINGS;
  public productTotalLength: number;
  public productSubscribe$: any;
  public searchDataSubscribe$: any;
  data: any;
  public compareCount: any = 0;
  public isSearchLoading: boolean;
  siteSettingDetails: any;
  constructor(
    private storage: StorageControllerService,
    private productsService: ProductService,
    private homeService: HomeService,
    private comonService: ComonService,
    private snackBar: MatSnackBar,
    private translation: TranslationService,
    private sharedService: SharedService
  ) {
    this.productSubscribe$ = this.homeService.getSignInStatus().subscribe((response: any) => {
      if (response === false) {
        this.productsCount.START_COUNT = 0;
        this.productList = [];
        this.getCompareCount();
        this.getSearchedProductList();
      }
    });
    this.searchDataSubscribe$ = this.homeService.getSearchStatus().subscribe((response: any) => {
      if (response === true) {
        if (this.storage.get('searchData')) {
          this.getCompareCount();
          this.productList = [];
          this.productsCount.START_COUNT = 0;
          this.getSearchedProductList();
        }
      }
    });
  }

  ngOnInit() {
    if (this.storage.get('searchData')) {
      this.productsCount.START_COUNT = 0;
      this.productList = [];
      this.getCompareCount();
      this.getSearchedProductList();
    }
  }

  getCompareCount() {
    this.siteSettingDetails = this.sharedService.getSiteSettingDetails();
    if (this.siteSettingDetails) {
      this.compareCount = this.siteSettingDetails.compareLimit;
    } else {
      this.compareCount = 0;
    }
  }

  getSearchedProductList() {
    this.data = JSON.parse(this.storage.get('searchData'));
    this.data.targetCurrency = 'USD';
    this.data.startLimit = this.productsCount.START_COUNT;
    this.data.endLimit = this.productsCount.END_COUNT;
    this.isSearchLoading = true;
    this.productsService.getSearchProducts(this.data).subscribe((response: any) => {
      this.isSearchLoading = false;
      if (response) {
        this.productList = concat(this.productList, response.data.product);
        this.productList = uniqBy(this.productList, 'productId');
        // if (!this.storage.get('id')) {
        //   forEach(this.productList, (product) => {
        //     product.cartUser = 0;
        //   });
        //   if (sessionStorage.getItem('cartStorage')) {
        //     const cartData = JSON.parse(sessionStorage.getItem('cartStorage'));
        //     forEach(this.productList, (product) => {
        //       forEach(cartData, (cart) => {
        //         if (cart.productId === product.productId) {
        //           product.cartUser = 1;
        //         }
        //       });
        //     });
        //   }
        // }
        this.productList = this.comonService.findCartCompareSelection(this.productList);
        this.productTotalLength = response.data.productlength;
      } else {
        this.productTotalLength = 0;
      }
    }, (error: any) => {
      this.productTotalLength = 0;
      this.isSearchLoading = false;
      if (error.status === API_STATUS_CODE.badRequest) {
        this.snackBar.open(error.error.data, this.translation.translate('buttonClose'), SNACKBAR_ALERT);
      }
    });
  }

  showMoreProducts() {
    this.productsCount.START_COUNT += this.productsCount.SHOW_MORE_COUNT;
    this.getSearchedProductList();
  }

  getProductActionDetails($event: any) {
    if ($event.action === 'wishList') {
      const productData = find(this.productList, {'productId': $event.productId});
      productData.favUser = $event.status === 1 ? 1 : 0;
    } else if ($event.action === 'cart') {
      const productData = find(this.productList, {'productId': $event.productId});
      productData.cartUser = 1;
    } else {
      const productData = find(this.productList, {'productId': $event.productId});
      productData.compareProduct = 1;
    }
  }

  ngOnDestroy() {
    if (this.productSubscribe$) {
      this.productSubscribe$.unsubscribe();
    }
    if (this.searchDataSubscribe$) {
      this.searchDataSubscribe$.unsubscribe();
    }
  }
}
